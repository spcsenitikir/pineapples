﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pineapples.Data.DB;
using Softwords.DataTools;
using System.Data;
using System.Data.SqlClient;

namespace Pineapples.Data
{
  public interface IDSPerfAssess
  {
    IDataResult Read(int Id);
    Models.PerfAssessment Template(string framework, int? teacherId);
    IDataResult Save(Models.PerfAssessment pf);

    IDataResult Filter(PerfAssessFilter fltr);
    IDataResult Table(PerfAssessTableFilter fltr);
  }
}
