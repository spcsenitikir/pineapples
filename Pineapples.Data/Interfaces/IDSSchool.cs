﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Data;
using System.Data.SqlClient;

namespace Pineapples.Data
{
    public interface IDSSchool
    {
        IDataResult Filter(SchoolFilter fltr);
        IDataResult Table(string rowsplit, string colsplit, SchoolFilter fltr);
        IDataResult Geo(string geoType, SchoolFilter fltr);

        // crud methods
        SchoolBinder Update(SchoolBinder school, System.Security.Claims.ClaimsIdentity identity);
        IDataResult Create(SchoolBinder school, System.Security.Claims.ClaimsIdentity identity);
        IDataResult Read(string SchoolNo);
        SchoolBinder Delete(string SchoolNo, System.Security.Claims.ClaimsIdentity identity);
    }
}
