﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;        // really need to change this interface

namespace Pineapples.Data
{
  public interface IDSLookups
  {
    DataSet CoreLookups();
    DataSet PaLookups();
    // specific lookups
    DataSet getLookupSets(string[] lookups);

    void SaveEntry(string lookupName, Models.LookupEntry entry);
  }
}
