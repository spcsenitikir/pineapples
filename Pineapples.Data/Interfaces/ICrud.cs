﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Security.Claims;

namespace Pineapples.Data
{
    /// <summary>
    ///  Generic interface for IDSxxx interfaces that will support Crud methods
    ///  Assumes the primary key is int
    ///  identity is passed for purposes of logging
    /// </summary>
    /// <typeparam name="T">
    ///  The Binder object, derived from ModelBinder
    /// </typeparam>
    public interface IDSCrud<T> : IDSCrud<T, int>
    {
    }

    public interface IDSCrud<T, TKey>
    {
        // crud methods
        T Update(T binder, ClaimsIdentity identity);
        IDataResult Create(T binder, ClaimsIdentity identity);
        IDataResult Read(TKey ID);
        T Delete(TKey ID, ClaimsIdentity identity);
    }

}

