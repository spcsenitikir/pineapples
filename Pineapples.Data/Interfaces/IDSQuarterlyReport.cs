﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;

namespace Pineapples.Data
{
    public interface IDSQuarterlyReport
    {
        IDataResult Filter(QuarterlyReportFilter fltr);

        // crud methods
        IDataResult Create(QuarterlyReportBinder qr, System.Security.Claims.ClaimsIdentity identity);
        IDataResult Read(int QrID);
        QuarterlyReportBinder Update(QuarterlyReportBinder qr, System.Security.Claims.ClaimsIdentity identity);
        QuarterlyReportBinder Delete(int QrID, System.Security.Claims.ClaimsIdentity identity);
    }
}
