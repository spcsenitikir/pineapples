﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;

namespace Pineapples.Data
{
    public interface IDSSchoolAccreditation
    {
        IDataResult Filter(SchoolAccreditationFilter fltr);

        // crud methods
        IDataResult Create(SchoolAccreditationBinder sa, System.Security.Claims.ClaimsIdentity identity);
        IDataResult Read(int SaID);
        SchoolAccreditationBinder Update(SchoolAccreditationBinder qr, System.Security.Claims.ClaimsIdentity identity);
        SchoolAccreditationBinder Delete(int SaID, System.Security.Claims.ClaimsIdentity identity);
    }
}
