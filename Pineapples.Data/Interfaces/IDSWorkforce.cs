﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Data;
using System.Data.SqlClient;

namespace Pineapples.Data
{
  public interface IDSWorkforce
  {
    IDataResult Filter(WorkforceFilter fltr);
  }
}
