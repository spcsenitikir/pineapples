﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;        // really need to change this interface
using Softwords.DataTools;

namespace Pineapples.Data
{
    public interface IDSIndicators
    {
        Task<System.Xml.Linq.XDocument> getVermData(int? year);
        Task makeWarehouse(int? year);
    }
}
