﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Data;
using System.Data.SqlClient;

namespace Pineapples.Data
{
    public interface IDSTeacher
    {
        IDataResult Filter(TeacherFilter fltr);
        IDataResult Table(string rowsplit, string colsplit, TeacherFilter fltr);
        IDataResult Geo(string geoType, TeacherFilter fltr);

        // crud methods
        TeacherBinder Update(TeacherBinder teacherIdentity, System.Security.Claims.ClaimsIdentity identity);
        IDataResult Create(TeacherBinder teacherIdentity, System.Security.Claims.ClaimsIdentity identity);
        TeacherBinder Delete(int TeacherID, System.Security.Claims.ClaimsIdentity identity);
        IDataResult Read(int TeacherID, bool readX);
    }
}
