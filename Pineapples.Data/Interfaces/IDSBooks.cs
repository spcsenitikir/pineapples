﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;

namespace Pineapples.Data
{
    public interface IDSBooks
    {
        IDataResult Filter(BookFilter fltr);

        // crud methods
        BookBinder Update(BookBinder book, System.Security.Claims.ClaimsIdentity identity);
        IDataResult Create(BookBinder book, System.Security.Claims.ClaimsIdentity identity);
        IDataResult Read(string BookCode);
        BookBinder Delete(string BookCode, System.Security.Claims.ClaimsIdentity identity);
    }
}
