﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;        // really need to change this interface
using Softwords.DataTools;

namespace Pineapples.Data
{
  public interface IDSSelectors
  {
    IDataResult Schools(string s);
    IDataResult Teachers(string s);
  }
}
