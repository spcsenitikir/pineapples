﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;


using Pineapples.Data.DB;
using Softwords.DataTools;

namespace Pineapples.Data.DataLayer
{
    internal class DSLookups : BaseRepository, IDSLookups
    {
        Dictionary<string, string> lkpCommands;

        public DSLookups(DB.PineapplesEfContext cxt) : base(cxt)
        {
            lkpCommands = new Dictionary<string, string>();
            lkpCommands.Add("schoolTypes", "SELECT stCode C, stDescription N from TRSchooltypes;");
            lkpCommands.Add("districts", "SELECT dID C, [dName] N from Districts ORDER BY dName;");
            lkpCommands.Add("authorities", "SELECT authCode C, authName N, authType T from TRAuthorities;");
            lkpCommands.Add("authorityTypes",
                "SELECT  [codeDescription] N, [codeCode] C, codeGroup G from TRAuthorityType;");
            lkpCommands.Add("authorityGovt",
                "SELECT  [codeDescription] N, [codeCode] C from TRAuthorityGovt;");
            lkpCommands.Add("islands", "SELECT  [iName] N, [iCode] C, iGroup D FROM lkpIslands ORDER BY [iName];");
            lkpCommands.Add("electoraten",
                "SELECT  [codeDescription] N, [codeCode] C FROM lkpElectorateN ORDER BY [codeDescription];");
            lkpCommands.Add("electoratel",
                "SELECT  [codeDescription] N, [codeCode] C FROM lkpElectorateL ORDER BY [codeDescription];");
            lkpCommands.Add("examTypes",
                 "SELECT exCode C, exName N FROM lkpExamTypes ORDER BY exName");
            lkpCommands.Add("languages",
                "SELECT  [langName] N, [langCode] C FROM TRLanguage ORDER BY [langName];");
            lkpCommands.Add("listNames",
                "SELECT  [lstDescription] N, [lstName] C FROM Lists ORDER BY [lstName];");
            lkpCommands.Add("schoolClasses",
                "SELECT [codeCode] C, [codedescription] N FROM TRSchoolClass ORDER BY [codedescription];");
            lkpCommands.Add("schoolNames",
                "SELECT  [schName] N, [schNo] C FROM Schools ORDER BY [schName];");
            // pa lookups
            lkpCommands.Add("paFrameworks",
                "SELECT  [pafrmDescription] N, [pafrmCode] C FROM paFrameworks_ ORDER BY [pafrmDescription];");
            lkpCommands.Add("paCompetencies",
                "SELECT  [pacomDescription] N, [comFullID] C, paComID ID FROM paCompetencies ORDER BY [comFullID];");
            lkpCommands.Add("paElements",
                "SELECT  [paelmDescription] N, [elmFullID] C, paelmID ID FROM paElements ORDER BY [elmFullID];");
            lkpCommands.Add("paIndicators",
                "SELECT  [paindText] N, [indFullID] C, paindID ID " +
                " FROM paIndicators ORDER BY[indFullID]");
            lkpCommands.Add("paFields",
                "SELECT   C FROM paFields ORDER BY seq");
            lkpCommands.Add("schoolFieldOptions",
                "Select f C, g from common.schoolFieldOptions ORDER BY seq;");
            lkpCommands.Add("schoolDataOptions",
                "Select C, N from common.schoolDataOptions ORDER BY seq;");
            lkpCommands.Add("teacherFieldOptions",
                            "Select f C, g from common.teacherFieldOptions ORDER BY seq;");
            lkpCommands.Add("teacherDataOptions",
                            "Select C, N from common.teacherDataOptions ORDER BY seq;");
            lkpCommands.Add("appDataOptions",
                            "Select C, N from common.appDataOptions ORDER BY seq;");

            lkpCommands.Add("vocab",
                "SELECT  isnull(vocabTerm, vocabName) N, vocabName C FROM sysVocab");
            lkpCommands.Add("sysParams",
                "Select paramName C, paramText N, paramInt I from sysParams");
            lkpCommands.Add("schoolRegistration",
                "SELECT rxCode C, rxDescription N FROM TRSchoolRegistration");
            lkpCommands.Add("surveyYears",
                "SELECT svyYear C, svyYear N FROM Survey ORDER BY svyYear DESC");
            // WHERE clause used to only retrieve table lookups that already work; in future either add to where clause of remove when they're all supported
            lkpCommands.Add("tableDefs",
                "SELECT tdefCode C, tdefName N FROM metaPupilTableDefs WHERE tdefCode = 'ENROL_PRI' OR tdefCode = 'ENROL_SEC' OR tdefCode = 'REP' OR tdefCode = 'DIS' OR tdefCode = 'TRIN' OR tdefCode = 'TROUT' UNION SELECT 'ENROL', 'Enrolments';");
            lkpCommands.Add("subjects",
                "SELECT subjCode C, subjName N FROM TRSubjects ORDER BY subjName");
            lkpCommands.Add("publishers",
                "SELECT pubCode C, pubName N FROM Publishers ORDER BY pubName");
            lkpCommands.Add("bookTG",
                "SELECT codeCode C, codeDescription N FROM TRBookTG ORDER BY codeDescription");
            lkpCommands.Add("levels",
                "SELECT codeCode C, codeDescription N, lvlYear YoEd from TrLevels ORDER BY lvlYear");
            lkpCommands.Add("gender",
                    "SELECT codeCode C, codeDescription N FROM TRGender ORDER BY codeDescription");
            lkpCommands.Add("teacherRegStatus",
                "Select 'PROVISIONAL' C, 'Provisional' N UNION Select 'REGISTERED' C, 'Registered' N");

            // documents and links
            lkpCommands.Add("teacherLinkTypes",
                 "SELECT codeCode C, codeDescription N FROM TRTeacherLinkTypes ORDER BY codeSort");
            lkpCommands.Add("schoolLinkTypes",
                 "SELECT codeCode C, codeDescription N FROM TRSchoolLinkTypes ORDER BY codeSort");


        }
        public DataSet CoreLookups()
        {
            string[] lookups = new string[]
                        {
                            "schoolTypes",
                            "districts",
                            "authorities",
                            "authorityTypes",
                            "authorityGovt",
                            "islands",
                            "electoraten",
                            "electoratel",
                            "languages",
                            "listNames",
                            "schoolClasses",
                            "schoolNames",
                            "vocab",
                            "sysParams",
                            "paFields",
                            "paFrameworks",
                            "schoolFieldOptions",
                            "schoolDataOptions",
                            "schoolRegistration",
                            "surveyYears",
                            "tableDefs",
                            "subjects",
                            "publishers",
                            "bookTG",
                            "levels",
                            "gender",
                            "teacherRegStatus",
                            "teacherLinkTypes",
                            "schoolLinkTypes"
                        };
            return getLookupSets(lookups);
        }

        public DataSet PaLookups()
        {
            string[] lookups = new string[]
                         {
                            "paCompetencies",
                            "paElements",
                            "paIndicators"

                         };
            return getLookupSets(lookups);
        }

        public DataSet getLookupSets(string[] lookups)
        {
            string lkpSql = string.Empty;
            string cmdSql = string.Empty;
            StringBuilder sb = new StringBuilder(String.Empty);

            foreach (string lkpName in lookups)
            {
                if (lkpCommands.TryGetValue(lkpName, out lkpSql))
                {
                    sb.AppendLine(lkpSql);
                }
                else
                {
                    throw new Exception(String.Format(@"Lookup table {0} is not defined", lkpName));
                }
            }

            // all were identified
            SqlCommand cmd = new SqlCommand();

            cmd.CommandType = CommandType.Text;
            // !!!!!!
            // IMPORTANT - this method should only be used when sql is a LITERAL
            cmd.CommandText = sb.ToString();
            DataSet ds = cmdToDataset(cmd);
            for (int i = 0; i < lookups.Length; i++)
            {
                ds.Tables[i].TableName = lookups[i];
            }
            return ds;
        }


        private IDataResult getLookups(string sql)
        {
            SqlCommand cmd = new SqlCommand();

            cmd.CommandType = CommandType.Text;
            // !!!!!!
            // IMPORTANT - this method should only be used when sql is a LITERAL
            cmd.CommandText = sql;
            return sqlExec(cmd, false);

        }

        public void SaveEntry(string lookupName, Models.LookupEntry entry)
        {

        }
    }
}
