﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pineapples.Data.Models
{
    [Table("TeacherIdentity")]
    public class TeacherIdentity:ChangeTracked
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int tID { get; set; }
        [MaxLength(50)]
        public string tRegister { get; set; }
        [MaxLength(50)]
        public string tPayroll { get; set; }
        [MaxLength(50)]
        public string tProvident { get; set; }
        public DateTime? tDOB { get; set; }
        public bool? tDOBEst { get; set; }
        [MaxLength(1)]
        public string tSex { get; set; }
        [MaxLength(50)]
        public string tNamePrefix { get; set; }
        [MaxLength(50)]
        public string tGiven { get; set; }
        [MaxLength(50)]
        public string tMiddleNames { get; set; }
        [MaxLength(50)]
        public string tSurname { get; set; }
        [MaxLength(20)]
        public string tNameSuffix { get; set; }
        [MaxLength(5)]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string tGivenSoundex { get; set; }
        [MaxLength(5)]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string tSurnameSoundex { get; set; }
        public int? tSrcID { get; set; }
        [MaxLength(50)]
        public string tSrc { get; set; }
        public string tComment { get; set; }
        public int? tYearStarted { get; set; }
        public int? tYearFinished { get; set; }
        public DateTime? tDatePSAppointed { get; set; }
        public DateTime? tDatePSClosed { get; set; }
        [MaxLength(50)]
        public string tCloseReason { get; set; }
        public bool? tPayErr { get; set; }
        [MaxLength(50)]
        public string tPayErrUser { get; set; }
        public DateTime? tPayErrDate { get; set; }
        public bool? tEstErr { get; set; }
        [MaxLength(50)]
        public string tEstErrUser { get; set; }
        public DateTime? tEstErrDate { get; set; }
        [MaxLength(10)]
        public string tSalaryPoint { get; set; }
        public DateTime? tSalaryPointDate { get; set; }
        public DateTime? tSalaryPointNextIncr { get; set; }
        [MaxLength(5,ErrorMessage = "Language is maximum 5 characters")]
        public string tLangMajor { get; set; }
        [MaxLength(5)]
        public string tLangMinor { get; set; }
        [MaxLength(10)]
        public string tpayptCode { get; set; }
        public int? tImage { get; set; }
        public DateTime? tDateRegister { get; set; }
        public DateTime? tDateRegisterEnd { get; set; }
        [MaxLength(50)]
        public string tRegisterEndReason { get; set; }
        [MaxLength(400)]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string tShortName { get; set; }
        [MaxLength(400)]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string tFullName { get; set; }
        [MaxLength(400)]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string tLongName { get; set; }
        [MaxLength(20)]
        public string tRegisterStatus { get; set; }

        [MaxLength(50)]
        public string pEditContext { get; set; }

    }

}
