﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Claims;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Xml.Linq;
using Newtonsoft.Json;


namespace Pineapples.Data
{
    [JsonConverter(typeof(FilterJsonConverter))]

    public class SchoolFilter : Filter
    {
        public SchoolFilter():base()
        {
        }

        public SchoolFilter(string server, string database) : base(server, database)
        {
        }
        
        public IFilterParam SchoolNo { get { return getParam("SchoolNo", typeof(string)); } }
        public IFilterParam SchoolName { get { return getParam("SchoolName", typeof(string)); } }
        public IFilterParam SearchAlias { get { return getParam("SearchAlias", typeof(int)); } }
        public IFilterParam AliasSource { get { return getParam("AliasSource", typeof(string)); } }
        public IFilterParam AliasNot { get { return getParam("AliasNot", typeof(int)); } }
        public IFilterParam SchoolType { get { return getParam("SchoolType", typeof(string)); } }
        public IFilterParam SchoolClass { get { return getParam("SchoolClass", typeof(string)); } }
        public IFilterParam District { get { return getParam("District", typeof(string)); } }
        public IFilterParam Island { get { return getParam("Island", typeof(string)); } }
        public IFilterParam ElectorateN { get { return getParam("ElectorateN", typeof(string)); } }
        public IFilterParam ElectorateL { get { return getParam("ElectorateL", typeof(string)); } }
        public IFilterParam Authority { get { return getParam("Authority", typeof(string)); } }
        public IFilterParam Language { get { return getParam("Language", typeof(string)); } }
        public IFilterParam InfrastructureClass { get { return getParam("InfrastructureClass", typeof(string)); } }
        public IFilterParam InfrastructureSize { get { return getParam("InfrastructureSize", typeof(string)); } }
        public IFilterParam NearestShipping { get { return getParam("NearestShipping", typeof(string)); } }
        public IFilterParam NearestBank { get { return getParam("NearestBank", typeof(string)); } }
        public IFilterParam NearestPo { get { return getParam("NearestPo", typeof(string)); } }
        public IFilterParam NearestAirstrip { get { return getParam("NearestAirstrip", typeof(string)); } }
        public IFilterParam NearestClinic { get { return getParam("NearestClinic", typeof(string)); } }
        public IFilterParam Nearesthospital { get { return getParam("Nearesthospital", typeof(string)); } }
        public IFilterParam SearchIsExtension { get { return getParam("SearchIsExtension", typeof(int)); } }
        public IFilterParam PArentSchool { get { return getParam("PArentSchool", typeof(string)); } }
        public IFilterParam SEarchHasExtension { get { return getParam("SEarchHasExtension", typeof(int)); } }
        public IFilterParam ExtensionReferenceYear { get { return getParam("ExtensionReferenceYear", typeof(int)); } }
        public IFilterParam YearEstablished { get { return getParam("YearEstablished", typeof(int)); } }
        public IFilterParam RegistrationStatus { get { return getParam("RegistrationStatus", typeof(string)); } }
        public IFilterParam SearchRegNo { get { return getParam("SearchRegNo", typeof(int)); } }
        public IFilterParam CreatedAfter { get { return getParam("CreatedAfter", typeof(DateTime)); } }
        public IFilterParam EditedAfter { get { return getParam("EditedAfter", typeof(DateTime)); } }
        public IFilterParam IncludeClosed { get { return getParam("IncludeClosed", typeof(int)); } }
        public IFilterParam ListName { get { return getParam("ListName", typeof(string)); } }
        public IFilterParam ListValue { get { return getParam("ListValue", typeof(string)); } }
        public IFilterParam XmlFilter { get { return getParam("XmlFilter", typeof(XDocument)); } }


        internal void OnParamToChange(object sender, ParamToChangeEventArgs args)
        {
            switch (args.FilterParam.Name)
            {
                case "JobFunction":
                    args.cancel = true;
                    break;
                default:
                    break;

            }
        }


        //public IDataResult Table(string rowsplit, string columnsplit)
        //{


        //    DataLayer.DSSchool ds = new DataLayer.DSSchool();
        //    SearchResult = (ds.Table(rowsplit, columnsplit, this));
        //    return SearchResult;


        //}


        public void ApplyUserFilter(ClaimsIdentity identity)
        {
            Claim c = identity.FindFirst(x => x.Type == "filterDistrict");
            if (c != null)
            {
                District.Value = c.Value;
            }
            c = identity.FindFirst(x => x.Type == "filterAuthority");
            if (c != null)
            {
                Authority.Value = c.Value;
            }
  
        }

    }
}