﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Claims;
using Softwords.DataTools;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace Pineapples.Data
{
    public class BookFilter : Filter
    {
        public string BookCode { get; set; }
        public string Title { get; set; }
        public string PublisherCode { get; set; }
        public string ISBN { get; set; }
        public string Subject {get;set;}
        public string TG { get; set; }
        // relating to curriculum
        public string Level { get; set; }
        public string EdLevelCode { get; set; }
        public string SchoolType { get; set; }
        public string CurriculumYear { get; set; }
        // relating to stock holding
        public string HoldingAt { get; set; }




    }
}
