﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pineapples.Data.Models
{
    [Table("paAssessment_")]
    public partial class PerfAssessment : ChangeTracked
    {
        public PerfAssessment()
        {
            Indicators = new List<PerfAssessmentIndicator>();
            IndicatorLevels = new List<PerfAssessmentIndicatorLevel>();
        }
        // for largely historical reasons, this definiction aliases all the column names
        [Column("paID")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int? Id { get; set; }        // nullable to support template of new record


        [Column("tID")]
        public int? TeacherId { get; set; }

        [Column("paSchNo")]
        [MaxLength(50)]
        public string SchoolNo { get; set; }

        [Column("pafrmCode")]
        [MaxLength(10)]
        public string Framework { get; set; }

        [Column("paDate")]
        public DateTime Date { get; set; }

        [Column("paConductedBy")]
        [MaxLength(100)]
        public string ConductedBy { get; set; }

        [NotMapped]
        public string TeacherName { get; set; }

        [NotMapped]
        public string SchoolName { get; set; }

        public virtual ICollection<PerfAssessmentIndicator> Indicators { get; set; }
        public List<PerfAssessmentIndicatorLevel> IndicatorLevels;
    }


}
