﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Xml.Linq;
using Newtonsoft.Json;
using System.Security.Claims;

namespace Pineapples.Data
{

    [JsonConverter(typeof(FilterJsonConverter))]
    public class TeacherFilter : Filter
    {

        public IFilterParam TeacherID { get { return getParam("TeacherID", typeof(int)); } }
        public IFilterParam Surname { get { return getParam("Surname", typeof(string)); } }
        public IFilterParam GivenName { get { return getParam("GivenName", typeof(string)); } }
        public IFilterParam PayrollNo { get { return getParam("PayrollNo", typeof(string)); } }
        public IFilterParam RegistrationNo { get { return getParam("RegistrationNo", typeof(string)); } }
        public IFilterParam ProvidentFundNo { get { return getParam("ProvidentFundNo", typeof(string)); } }
        public IFilterParam Gender { get { return getParam("Gender", typeof(string)); } }
        public IFilterParam DoB { get { return getParam("DoB", typeof(DateTime)); } }
        public IFilterParam DobEnd { get { return getParam("DobEnd", typeof(DateTime)); } }
        public IFilterParam Language { get { return getParam("Language", typeof(string)); } }
        public IFilterParam PaidBy { get { return getParam("PaidBy", typeof(string)); } }
        public IFilterParam Qualification { get { return getParam("Qualification", typeof(string)); } }
        public IFilterParam EdQualification { get { return getParam("EdQualification", typeof(string)); } }
        public IFilterParam Subject { get { return getParam("Subject", typeof(string)); } }
        public IFilterParam SearchSubjectTaught { get { return getParam("SearchSubjectTaught", typeof(int)); } }
        public IFilterParam SearchTrained { get { return getParam("SearchTrained", typeof(int)); } }
        public IFilterParam AtSchool { get { return getParam("AtSchool", typeof(string)); } }
        public IFilterParam AtSchoolType { get { return getParam("AtSchoolType", typeof(string)); } }
        public IFilterParam InYear { get { return getParam("InYear", typeof(int)); } }
        public IFilterParam Role { get { return getParam("Role", typeof(string)); } }
        public IFilterParam LevelTaught { get { return getParam("LevelTaught", typeof(string)); } }
        public IFilterParam YearOfEdMin { get { return getParam("YearOfEdMin", typeof(int)); } }
        public IFilterParam YearOfEdMax { get { return getParam("YearOfEdMax", typeof(int)); } }
        public IFilterParam ISCEDSubClass { get { return getParam("ISCEDSubClass", typeof(string)); } }
        public IFilterParam UseOr { get { return getParam("UseOr", typeof(int)); } }
        public IFilterParam CrossSearch { get { return getParam("CrossSearch", typeof(int)); } }
        public IFilterParam SoundSearch { get { return getParam("SoundSearch", typeof(int)); } }
        public IFilterParam XmlFilter { get { return getParam("XmlFilter", typeof(XDocument)); } }

        internal void OnParamToChange(object sender, ParamToChangeEventArgs args)
        {
            switch (args.FilterParam.Name)
            {
                case "JobFunction":
                    args.cancel = true;
                    break;
                default:
                    break;

            }
        }

        //public void ApplyUserFilter(ClaimsIdentity identity)
        //{
        //    Claim c = identity.FindFirst(x => x.Type == "filterDistrict");
        //    if (c != null)
        //    {
        //        District.Value = c.Value;
        //    }
        //    c = identity.FindFirst(x => x.Type == "filterAuthority");
        //    if (c != null)
        //    {
        //        Authority.Value = c.Value;
        //    }
  
        //}

    }

    public class TeacherTableFilter
    {
        public string row { get; set; }
        public string col { get; set; }
        public TeacherFilter filter { get; set; }
    }

}