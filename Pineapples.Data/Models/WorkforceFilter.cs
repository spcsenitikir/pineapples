﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using Newtonsoft.Json;

namespace Pineapples.Data
{
    [JsonConverter(typeof(FilterJsonConverter))]
    public class WorkforceFilter:Filter
    {
        public IFilterParam asAtDate { get { return getParam("asAtDate", typeof(DateTime)); } }
        public IFilterParam SchoolNo { get { return getParam("SchoolNo", typeof(string)); } }
        public IFilterParam Authority { get { return getParam("Authority", typeof(string)); } }
        public IFilterParam SchoolType { get { return getParam("SchoolType", typeof(string)); } }
        public IFilterParam RoleGrade { get { return getParam("RoleGrade", typeof(string)); } }
        public IFilterParam Role { get { return getParam("Role", typeof(string)); } }
        public IFilterParam Sector { get { return getParam("Sector", typeof(string)); } }
        public IFilterParam Unfilled { get { return getParam("Unfilled", typeof(int)); } }
        public IFilterParam PayslipExceptions { get { return getParam("PayslipExceptions", typeof(int)); } }
        public IFilterParam SurveyExceptions { get { return getParam("SurveyExceptions", typeof(int)); } }
        public IFilterParam SalaryPointExceptions { get { return getParam("SalaryPointExceptions", typeof(int)); } }
        public IFilterParam PositionFlag { get { return getParam("PositionFlag", typeof(string)); } }
        public IFilterParam PositionFlagOp { get { return getParam("PositionFlagOp", typeof(int)); } }

     }
}
