using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpRegion")]
    [Description(@"Category of island. Generally used as an indicator of remoteness. Stored in iOuter.")]
    public partial class Region : SimpleCodeTable { }
}
