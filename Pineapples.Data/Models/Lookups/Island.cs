﻿using System;
using System.ComponentModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("Islands")]
    [Description(@"Islands in the country. Each school is on an island - ie iCode is a foreign key on schools.
    Each Island is part of a District.
    Therefore, the District of the school is determined by Island.")]
    public partial class Island : ChangeTracked
    {
        [Key]
        [Column("iCode", TypeName = "nvarchar")]
        [MaxLength(2)]
        [StringLength(2)]
        [Required(ErrorMessage = "i Code is required")]
        [Display(Name = "Code")]
        public string Code { get; set; }
        [Column("iName", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Column("iSeq", TypeName = "int")]
        [Display(Name = "i Seq")]
        public int? Seq { get; set; }
        [Column("iGroup", TypeName = "nvarchar")]
        [MaxLength(5)]
        [StringLength(5)]
        [Display(Name = "District")]
        public string District { get; set; }
        [Column("iOuter", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "i Outer")]
        public string Outer { get; set; }

    }

}
