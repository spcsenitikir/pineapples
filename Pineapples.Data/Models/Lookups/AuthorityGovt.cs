﻿using System;
using System.ComponentModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{

    [Table("lkpAuthorityGovt")]
    [Description(@"Top level consolidation for Authorities is Gov or Non-Govt. Effectively, system defined. 
    Appears as foreign key on AuthorityTypes.")]
    public partial class AuthorityGovt: SimpleCodeTable
    {
        [Key]
        [Column("codeCode", TypeName = "nvarchar")]
        [MaxLength(1)]
        [StringLength(1)]
        [Required(ErrorMessage = "Code is required")]
        [Display(Name = "Code")]
        public override string Code { get; set; }
 
    }


}
