﻿using System;
using System.ComponentModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{


    [Table("lkpDisabilities")]
    [Description("Codes for disability types")]
    public partial class Disability:SequencedCodeTable
    {
    }

}
