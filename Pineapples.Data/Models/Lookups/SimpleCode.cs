﻿using Softwords.Web.Models;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Softwords.Web.Models;

namespace Pineapples.Data.Models
{

    public partial class SimpleCodeTable: ChangeTracked
    {
        [Key]
        [MaxLength(10)]
        [StringLength(10)]
        [Required(ErrorMessage = "Code is required")]
        [Display(Name = "Code")]
        [Column("codeCode")]
        [ForceUpperCase]
        public virtual string Code { get; set; }
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Description")]
        [Column("codeDescription")]
        [Required(ErrorMessage = "Description is required")]
        public virtual string Description { get; set; }
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Description L1")]
        [Column("codeDescriptionL1")]
        public virtual string DescriptionL1 { get; set; }
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Description L2")]
        [Column("codeDescriptionL2")]
        public virtual string DescriptionL2 { get; set; }
    }

    public partial class SequencedCodeTable : SimpleCodeTable
    {
        [Column("codeSeq", TypeName = "int")]
        [Display(Name = "Sequence")]
        public virtual int? Seq { get; set; }

    }
}
