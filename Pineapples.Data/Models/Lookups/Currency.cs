using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpCurrencies")]
    public partial class Currency : SimpleCodeTable
    {
        [Key]

        [Column("currCode", TypeName = "nvarchar")]
        [MaxLength(3)]
        [StringLength(3)]
        [Required(ErrorMessage = "curr Code is required")]
        [Display(Name = "curr Code")]
        public override string Code { get; set; }

        [Column("currName", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "curr Name")]
        public override string Description { get; set; }

        [Column("currNameL1", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "curr Name L1")]
        public override string DescriptionL1 { get; set; }

        [Column("currNameL2", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "curr Name L2")]
        public override string DescriptionL2 { get; set; }
    }
}
