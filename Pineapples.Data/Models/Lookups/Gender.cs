﻿using System;
using System.ComponentModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{


    [Table("lkpGender")]
    [Description("Codes for Gender. This table is used to cross join many quiries to normalise on Gender, for pivot tables.")]
    public partial class Gender : SimpleCodeTable
    {
        // override on length
        [Key]
        [MaxLength(1)]
        [StringLength(1)]
        [Required(ErrorMessage = "Code is required")]
        [Display(Name = "codeCode")]
        [ForceUpperCase]
        public override string Code { get; set; }


    }

}
