using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpNationality")]
    [Description(@"nationality of teacher")]
    public partial class Nationality : SequencedCodeTable { }
}
