using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpPrimaryTransitions")]
    [Description(@"Different transitions possible after primary school. Appears in PupilTables")]
    public partial class PrimaryTransition : SequencedCodeTable { }
}
