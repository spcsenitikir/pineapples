--  
-- Script to Update dbo.Schools in zali.SIEMIS 
-- Generated Monday, April 18, 2016, at 01:40 PM 
--  
-- Please backup zali.SIEMIS before executing this script
--  
-- ** Script Begin **
BEGIN TRANSACTION
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

PRINT 'Updating dbo.Schools Table'
GO

SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, QUOTED_IDENTIFIER, CONCAT_NULL_YIELDS_NULL ON
GO

SET NUMERIC_ROUNDABORT OFF
GO


IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_Buildings_Schools')
      ALTER TABLE [dbo].[Buildings] DROP CONSTRAINT [FK_Buildings_Schools]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'EnrolmentProjection_FK01')
      ALTER TABLE [dbo].[EnrolmentProjection] DROP CONSTRAINT [EnrolmentProjection_FK01]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_Establishment_Schools')
      ALTER TABLE [dbo].[Establishment] DROP CONSTRAINT [FK_Establishment_Schools]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'ExamScores_FK01')
      ALTER TABLE [dbo].[ExamScores] DROP CONSTRAINT [ExamScores_FK01]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'ExamStandardTest_FK01')
      ALTER TABLE [dbo].[ExamStandardTest] DROP CONSTRAINT [ExamStandardTest_FK01]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'ExamSubjectGrades_FK01')
      ALTER TABLE [dbo].[ExamSubjectGrades] DROP CONSTRAINT [ExamSubjectGrades_FK01]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'ListSchools_FK01')
      ALTER TABLE [dbo].[ListSchools] DROP CONSTRAINT [ListSchools_FK01]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_paAssessment_School')
      ALTER TABLE [dbo].[paAssessment_] DROP CONSTRAINT [FK_paAssessment_School]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'SchoolAlias_FK00')
      ALTER TABLE [dbo].[SchoolAlias] DROP CONSTRAINT [SchoolAlias_FK00]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_Finance_School')
      ALTER TABLE [dbo].[SchoolFinanceCodes] DROP CONSTRAINT [FK_Finance_School]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'SchoolLedger_FK01')
      ALTER TABLE [dbo].[SchoolLedger] DROP CONSTRAINT [SchoolLedger_FK01]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'SchoolLinks_FK00')
      ALTER TABLE [dbo].[SchoolLinks] DROP CONSTRAINT [SchoolLinks_FK00]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'SchoolNotes_FK00')
      ALTER TABLE [dbo].[SchoolNotes] DROP CONSTRAINT [SchoolNotes_FK00]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_SchoolParent')
      ALTER TABLE [dbo].[Schools] DROP CONSTRAINT [FK_SchoolParent]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_SchoolSurvey_Schools')
      ALTER TABLE [dbo].[SchoolSurvey] DROP CONSTRAINT [FK_SchoolSurvey_Schools]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_SchoolYearHistory_Schools')
      ALTER TABLE [dbo].[SchoolYearHistory] DROP CONSTRAINT [FK_SchoolYearHistory_Schools]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_SchoolYearHistory_SchoolsParent')
      ALTER TABLE [dbo].[SchoolYearHistory] DROP CONSTRAINT [FK_SchoolYearHistory_SchoolsParent]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'SurveyYearRank_FK00')
      ALTER TABLE [dbo].[SurveyYearRank] DROP CONSTRAINT [SurveyYearRank_FK00]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'TeacherAppointment_FK03')
      ALTER TABLE [dbo].[TeacherAppointment] DROP CONSTRAINT [TeacherAppointment_FK03]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'TeacherAttendance_FK00')
      ALTER TABLE [dbo].[TeacherAttendance] DROP CONSTRAINT [TeacherAttendance_FK00]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_Schools_WorkItems')
      ALTER TABLE [dbo].[WorkItems] DROP CONSTRAINT [FK_Schools_WorkItems]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_SchoolParent')
      ALTER TABLE [dbo].[Schools] DROP CONSTRAINT [FK_SchoolParent]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'Schools_FK00')
      ALTER TABLE [dbo].[Schools] DROP CONSTRAINT [Schools_FK00]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'Schools_FK01')
      ALTER TABLE [dbo].[Schools] DROP CONSTRAINT [Schools_FK01]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'Schools_FK02')
      ALTER TABLE [dbo].[Schools] DROP CONSTRAINT [Schools_FK02]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'Schools_FK03')
      ALTER TABLE [dbo].[Schools] DROP CONSTRAINT [Schools_FK03]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'Schools_FK04')
      ALTER TABLE [dbo].[Schools] DROP CONSTRAINT [Schools_FK04]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'Schools_FK05')
      ALTER TABLE [dbo].[Schools] DROP CONSTRAINT [Schools_FK05]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'Schools_FK06')
      ALTER TABLE [dbo].[Schools] DROP CONSTRAINT [Schools_FK06]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'Schools_FK07')
      ALTER TABLE [dbo].[Schools] DROP CONSTRAINT [Schools_FK07]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'DF__Schools__schClos__1AD3FDA4')
      ALTER TABLE [dbo].[Schools] DROP CONSTRAINT [DF__Schools__schClos__1AD3FDA4]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'DF__Schools__schXtrn__1BC821DD')
      ALTER TABLE [dbo].[Schools] DROP CONSTRAINT [DF__Schools__schXtrn__1BC821DD]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'DF_Schools_schDormant')
      ALTER TABLE [dbo].[Schools] DROP CONSTRAINT [DF_Schools_schDormant]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   CREATE TABLE [dbo].[tmp_Schools] (
   [schNo] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
   [schName] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
   [schVillage] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [iCode] [nvarchar] (2) COLLATE Latin1_General_CI_AS NULL,
   [schType] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
   [schAddr1] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [schAddr2] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [schAddr3] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [schAddr4] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [schPh1] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [schPh2] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [schFax] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [schEmail] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [schWWW] [nvarchar] (150) COLLATE Latin1_General_CI_AS NULL,
   [schElectN] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
   [schElectL] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
   [schAuth] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
   [schLang] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
   [schLandOwner] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
   [schLandUse] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
   [schLandUseExpiry] [datetime] NULL,
   [schEst] [smallint] NULL,
   [schEstBy] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [schClosed] [smallint] NULL CONSTRAINT [DF__Schools__schClos__1AD3FDA4] DEFAULT ((0)),
   [schCloseReason] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [schRegStatus] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
   [schReg] [nvarchar] (20) COLLATE Latin1_General_CI_AS NULL,
   [schRegYear] [int] NULL,
   [schRegDate] [datetime] NULL,
   [schRegStatusDate] [datetime] NULL,
   [schXtrnID] [int] NULL CONSTRAINT [DF__Schools__schXtrn__1BC821DD] DEFAULT ((0)),
   [schClass] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
   [schXNo] [nvarchar] (8) COLLATE Latin1_General_CI_AS NULL,
   [schpayptCode] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
   [schClosedLimit] as (case [schclosed] when (0) then (9999) else [schclosed] end) PERSISTED,
   [schOrgUnitNumber] [int] NULL,
   [schGLSalaries] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [schParent] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [schEstablishmentPoint] as (isnull([schParent],[schNo])) PERSISTED NOT NULL,
   [schIsExtension] as (case when [schParent] IS NULL then (0) else (1) end),
   [pRowversion] [timestamp] NOT NULL,
   [pCreateDateTime] [datetime] NULL,
   [pCreateUser] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [pEditDateTime] [datetime] NULL,
   [pEditUser] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [pEditContext] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [schDormant] [bit] NOT NULL CONSTRAINT [DF_Schools_schDormant] DEFAULT ((0)),
   [schElev] [decimal] (8, 2) NULL,
   [schLat] [decimal] (12, 8) NULL,
   [schLong] [decimal] (12, 8) NULL
)
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   INSERT INTO [dbo].[tmp_Schools] ([schNo], [schName], [schVillage], [iCode], [schType], [schAddr1], [schAddr2], [schAddr3], [schAddr4], [schPh1], [schPh2], [schFax], [schEmail], [schWWW], [schElectN], [schElectL], [schAuth], [schLang], [schLandOwner], [schLandUse], [schLandUseExpiry], [schEst], [schEstBy], [schClosed], [schCloseReason], [schRegStatus], [schReg], [schRegYear], [schRegDate], [schRegStatusDate], [schXtrnID], [schClass], [schXNo], [schpayptCode], [schOrgUnitNumber], [schGLSalaries], [schParent], [pCreateDateTime], [pCreateUser], [pEditDateTime], [pEditUser], [pEditContext], [schDormant], [schElev], [schLat], [schLong])
   SELECT [schNo], [schName], [schVillage], [iCode], [schType], [schAddr1], [schAddr2], [schAddr3], [schAddr4], [schPh1], [schPh2], [schFax], [schEmail], [schWWW], [schElectN], [schElectL], [schAuth], [schLang], [schLandOwner], [schLandUse], [schLandUseExpiry], [schEst], [schEstBy], [schClosed], [schCloseReason], [schRegStatus], [schReg], [schRegYear], [schRegDate], [schRegStatusDate], [schXtrnID], [schClass], [schXNo], [schpayptCode], [schOrgUnitNumber], [schGLSalaries], [schParent], [pCreateDateTime], [pCreateUser], [pEditDateTime], [pEditUser], [pEditContext], [schDormant], [schElev], [schLat], [schLong]
   FROM [dbo].[Schools]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   DROP TABLE [dbo].[Schools]
GO

sp_rename N'[dbo].[tmp_Schools]', N'Schools'

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   ALTER TABLE [dbo].[Schools] ADD CONSTRAINT [aaaaaSchools1_PK] PRIMARY KEY NONCLUSTERED ([schNo])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   CREATE INDEX [IX_SchoolsName] ON [dbo].[Schools] ([schName]) INCLUDE ([schNo])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   CREATE INDEX [IX_SchoolsParent] ON [dbo].[Schools] ([schParent])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   ALTER TABLE [dbo].[Schools] ADD CONSTRAINT [CK_Schools_Lat] CHECK ([schLat] IS NULL OR [schLat]>=(-90) AND [schLat]<=(90))
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   ALTER TABLE [dbo].[Schools] ADD CONSTRAINT [CK_Schools_Long] CHECK ([schLong] IS NULL OR [schLong]>=(-180) AND [schLong]<=(180))
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   ALTER TABLE [dbo].[Schools] ADD CONSTRAINT [CK_Schools] CHECK ([schParent] IS NULL OR [schParent]<>[SchNo])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   ALTER TABLE [dbo].[Schools] ADD CONSTRAINT [CK_Schools_Elev] CHECK ([schElev] IS NULL OR [schElev]>=(-10) AND [schElev]<=(8000))
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT ALTER ON OBJECT::[dbo].[Schools] TO [pineapples]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([pCreateDateTime]) ON OBJECT::[dbo].[Schools] TO [pineapples]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([pCreateUser]) ON OBJECT::[dbo].[Schools] TO [pineapples]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([pEditContext]) ON OBJECT::[dbo].[Schools] TO [pineapples]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([pEditDateTime]) ON OBJECT::[dbo].[Schools] TO [pineapples]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([pEditUser]) ON OBJECT::[dbo].[Schools] TO [pineapples]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT SELECT ON OBJECT::[dbo].[Schools] TO [pSchoolRead]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([iCode]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schAddr1]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schAddr2]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schAddr3]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schAddr4]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schAuth]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schClass]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schElectL]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schElectN]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schElev]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schEmail]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schEst]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schEstablishmentPoint]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schEstBy]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schFax]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schGLSalaries]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schIsExtension]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schLandOwner]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schLandUse]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schLandUseExpiry]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schLang]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schLat]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schLong]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schName]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schOrgUnitNumber]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schParent]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schpayptCode]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schPh1]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schPh2]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schType]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schVillage]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schWWW]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schXNo]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schXtrnID]) ON OBJECT::[dbo].[Schools] TO [pSchoolWrite]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT DELETE ON OBJECT::[dbo].[Schools] TO [pSchoolWriteX]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT INSERT ON OBJECT::[dbo].[Schools] TO [pSchoolWriteX]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schClosed]) ON OBJECT::[dbo].[Schools] TO [pSchoolWriteX]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schClosedLimit]) ON OBJECT::[dbo].[Schools] TO [pSchoolWriteX]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schCloseReason]) ON OBJECT::[dbo].[Schools] TO [pSchoolWriteX]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schDormant]) ON OBJECT::[dbo].[Schools] TO [pSchoolWriteX]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schReg]) ON OBJECT::[dbo].[Schools] TO [pSchoolWriteX]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schRegDate]) ON OBJECT::[dbo].[Schools] TO [pSchoolWriteX]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schRegStatus]) ON OBJECT::[dbo].[Schools] TO [pSchoolWriteX]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schRegStatusDate]) ON OBJECT::[dbo].[Schools] TO [pSchoolWriteX]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT UPDATE ([schRegYear]) ON OBJECT::[dbo].[Schools] TO [pSchoolWriteX]
GO
IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   GRANT VIEW DEFINITION ON OBJECT::[dbo].[Schools] TO [public]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1

exec('-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 05 2010
-- Description:	
-- =============================================
CREATE TRIGGER [dbo].[Schools_AuditDelete] 
   ON  dbo.Schools 
   
   WITH EXECUTE as ''pineapples''
   AFTER DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @NumChanges int
	declare @AuditID int
	
	Select @NumChanges = count(*) from DELETED
	
	
	exec @AuditID =  audit.LogAudit ''Schools'', null, ''D'', @Numchanges
	
	INSERT INTO audit.AuditRow
	(auditID, arowKeyS, arowName)
	SELECT DISTINCT @AuditID
	, schNo, schName
	FROM DELETED	



END
')
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1

exec('-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 05 2010
-- Description:	
-- =============================================
CREATE TRIGGER [dbo].[Schools_AuditInsert] 
   ON  dbo.Schools 
    
   WITH EXECUTE as ''pineapples''

   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @NumChanges int
	declare @AuditID int
	
	Select @NumChanges = count(*) from INSERTED
	
	
	exec @AuditID =  audit.LogAudit ''Schools'', null, ''I'', @Numchanges
	
	INSERT INTO audit.AuditRow
	(auditID, arowKeyS)
	SELECT DISTINCT @AuditID
	, schNo
	FROM INSERTED	

-- update the created and edited fields
	UPDATE Schools
		SET pCreateDateTime = aLog.auditDateTime
			, pCreateUser = alog.auditUser

			, pEditDateTime = aLog.auditDateTime
			, pEditUser = alog.auditUser
	FROM INSERTED I
		CROSS JOIN Audit.auditLog alog
	WHERE Schools.schNo = I.schNo
	AND aLog.auditID = @auditID


END
')
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1

exec('-- =============================================
-- Author:		Brian Lewis
-- Create date: 2009-0912
-- Description:	Logging trigger
-- =============================================
CREATE TRIGGER [dbo].[Schools_AuditUpdate] 
   ON  dbo.Schools 

	-- pineapples has access to write to TeacherIdentityAudit
	WITH EXECUTE AS ''pineapples''
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @AuditID int



		declare @NumChanges int
		
		Select @NumChanges = count(*) from INSERTED
		
		
		exec @AuditID =  audit.LogAudit ''Schools'', null, ''U'', @Numchanges
		
		INSERT INTO audit.AuditRow
		(auditID, arowKeyS)
		SELECT DISTINCT @AuditID
		, schNo
		FROM INSERTED	



		
		INSERT INTO audit.AuditColumn
		(arowID, acolName, acolBefore, acolAfter)	
		
		SELECT arowID
		, ColumnName
		, DValue
		, IValue
		FROM audit.AuditRow Row
		INNER JOIN 
		(
				SELECT 
			I.schNo
			, case num
				when 1 then ''schNo''
				when 2 then ''schName''
				when 3 then ''schVillage''
				when 4 then ''iCode''
				when 5 then ''schType''
				when 6 then ''schAddr1''
				when 7 then ''schAddr2''
				when 8 then ''schAddr3''
				when 9 then ''schAddr4''
				when 10 then ''schPh1''
				when 11 then ''schPh2''
				when 12 then ''schFax''
				when 13 then ''schEmail''
				when 14 then ''schWWW''
				when 15 then ''schElectN''
				when 16 then ''schElectL''
				when 17 then ''schAuth''
				when 18 then ''schLang''
				when 19 then ''schLandOwner''
				when 20 then ''schLandUse''
				when 21 then ''schLandUseExpiry''
				when 22 then ''schEst''
				when 23 then ''schEstBy''
				when 24 then ''schClosed''
				when 25 then ''schCloseReason''
				when 26 then ''schRegStatus''
				when 27 then ''schReg''
				when 28 then ''schRegYear''
				when 29 then ''schRegDate''
				when 30 then ''schRegStatusDate''
				when 31 then ''schXtrnID''
				when 32 then ''schClass''
				when 33 then ''schXNo''
				when 34 then ''schpayptCode''
				when 35 then ''schClosedLimit''
				when 36 then ''schOrgUnitNumber''
				when 37 then ''schGLSalaries''
				when 38 then ''schParent''
				when 39 then ''schDormant''
			end ColumnName
			
			, case num 
				when 1 then cast(D.schNo as nvarchar(50))
				when 2 then cast(D.schName as nvarchar(50))
				when 3 then cast(D.schVillage as nvarchar(50))
				when 4 then cast(D.iCode as nvarchar(50))
				when 5 then cast(D.schType as nvarchar(50))
				when 6 then cast(D.schAddr1 as nvarchar(50))
				when 7 then cast(D.schAddr2 as nvarchar(50))
				when 8 then cast(D.schAddr3 as nvarchar(50))
				when 9 then cast(D.schAddr4 as nvarchar(50))
				when 10 then cast(D.schPh1 as nvarchar(50))
				when 11 then cast(D.schPh2 as nvarchar(50))
				when 12 then cast(D.schFax as nvarchar(50))
				when 13 then cast(D.schEmail as nvarchar(50))
				when 14 then cast(D.schWWW as nvarchar(50))
				when 15 then cast(D.schElectN as nvarchar(50))
				when 16 then cast(D.schElectL as nvarchar(50))
				when 17 then cast(D.schAuth as nvarchar(50))
				when 18 then cast(D.schLang as nvarchar(50))
				when 19 then cast(D.schLandOwner as nvarchar(50))
				when 20 then cast(D.schLandUse as nvarchar(50))
				when 21 then cast(D.schLandUseExpiry as nvarchar(50))
				when 22 then cast(D.schEst as nvarchar(50))
				when 23 then cast(D.schEstBy as nvarchar(50))
				when 24 then cast(D.schClosed as nvarchar(50))
				when 25 then cast(D.schCloseReason as nvarchar(50))
				when 26 then cast(D.schRegStatus as nvarchar(50))
				when 27 then cast(D.schReg as nvarchar(50))
				when 28 then cast(D.schRegYear as nvarchar(50))
				when 29 then cast(D.schRegDate as nvarchar(50))
				when 30 then cast(D.schRegStatusDate as nvarchar(50))
				when 31 then cast(D.schXtrnID as nvarchar(50))
				when 32 then cast(D.schClass as nvarchar(50))
				when 33 then cast(D.schXNo as nvarchar(50))
				when 34 then cast(D.schpayptCode as nvarchar(50))
				when 35 then cast(D.schClosedLimit as nvarchar(50))
				when 36 then cast(D.schOrgUnitNumber as nvarchar(50))
				when 37 then cast(D.schGLSalaries as nvarchar(50))
				when 38 then cast(D.schParent as nvarchar(50))
				when 39 then cast(D.schDormant as nvarchar(50))


			 end DValue
			 
			, case num 
				when 1 then cast(I.schNo as nvarchar(50))
				when 2 then cast(I.schName as nvarchar(50))
				when 3 then cast(I.schVillage as nvarchar(50))
				when 4 then cast(I.iCode as nvarchar(50))
				when 5 then cast(I.schType as nvarchar(50))
				when 6 then cast(I.schAddr1 as nvarchar(50))
				when 7 then cast(I.schAddr2 as nvarchar(50))
				when 8 then cast(I.schAddr3 as nvarchar(50))
				when 9 then cast(I.schAddr4 as nvarchar(50))
				when 10 then cast(I.schPh1 as nvarchar(50))
				when 11 then cast(I.schPh2 as nvarchar(50))
				when 12 then cast(I.schFax as nvarchar(50))
				when 13 then cast(I.schEmail as nvarchar(50))
				when 14 then cast(I.schWWW as nvarchar(50))
				when 15 then cast(I.schElectN as nvarchar(50))
				when 16 then cast(I.schElectL as nvarchar(50))
				when 17 then cast(I.schAuth as nvarchar(50))
				when 18 then cast(I.schLang as nvarchar(50))
				when 19 then cast(I.schLandOwner as nvarchar(50))
				when 20 then cast(I.schLandUse as nvarchar(50))
				when 21 then cast(I.schLandUseExpiry as nvarchar(50))
				when 22 then cast(I.schEst as nvarchar(50))
				when 23 then cast(I.schEstBy as nvarchar(50))
				when 24 then cast(I.schClosed as nvarchar(50))
				when 25 then cast(I.schCloseReason as nvarchar(50))
				when 26 then cast(I.schRegStatus as nvarchar(50))
				when 27 then cast(I.schReg as nvarchar(50))
				when 28 then cast(I.schRegYear as nvarchar(50))
				when 29 then cast(I.schRegDate as nvarchar(50))
				when 30 then cast(I.schRegStatusDate as nvarchar(50))
				when 31 then cast(I.schXtrnID as nvarchar(50))
				when 32 then cast(I.schClass as nvarchar(50))
				when 33 then cast(I.schXNo as nvarchar(50))
				when 34 then cast(I.schpayptCode as nvarchar(50))
				when 35 then cast(I.schClosedLimit as nvarchar(50))
				when 36 then cast(I.schOrgUnitNumber as nvarchar(50))
				when 37 then cast(I.schGLSalaries as nvarchar(50))
				when 38 then cast(I.schParent as nvarchar(50))
				when 39 then cast(I.schDormant as nvarchar(50))

			 end [IValue]
		FROM INSERTED I 
		INNER JOIN DELETED D
			ON I.schNo = D.schNo
		CROSS JOIN metaNumbers
		WHERE num between 1 and 39
		) Edits
		ON Edits.schNo = Row.arowKeyS
		WHERE Row.auditID = @AuditID
		AND isnull(DVAlue,'''') <> isnull(IValue,'''')

		
		-- update the last edited field	
		UPDATE Schools
			SET pEditDateTime = aLog.auditDateTime
				, pEditUser = alog.auditUser
		FROM INSERTED I
			CROSS JOIN Audit.auditLog alog
		WHERE Schools.schNo = I.schNo
		AND aLog.auditID = @auditID
	
END
')
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1

exec('-- =============================================
-- Author:		Brian Lewis
-- Create date: 22 5 2010
-- Description:	Maintain SchoolYearHistory when Schools changes
-- =============================================
CREATE TRIGGER [dbo].[Schools_RelatedData] 
   ON  [dbo].[Schools] 
   AFTER INSERT, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @ActiveYear int
	declare @MaxYear int
	declare @StartHistory int
	declare @LastSurvey int	
	
	select @ActiveYear = common.ActiveSurveyYear()
	-- the reason for this is:
	-- when the new survey year is created, any provisional settings in SchoolYearHistory for the new year
	-- (e.g. created by Establishment0 are applied to the Schools table making them current.
	-- These could then be restrosecptively applied to the current year records
	select @StartHistory = min(svyYear)
	, @LastSurvey = max(svyYear)
	from Survey
	
	select @MaxYear = max(syYear)		-- there may be years in history thatare not in survey
	from SchoolYearHistory
	
	-- if its an insert
	-- the range of years to add should be from 
	-- min(Established Year, current Year)
	-- to the maximum year represented in SurveyYear History
	-- This will add them to the establishment plan and survey control
	
	-- note this query will add any needed records if an established date is changed
	INSERT INTO SchoolYearHistory
	( syYear
		, schNo
		, systCode
		, syAuth
		, syParent
		, syDormant

	)
	SELECT num
	, INSERTED.schNo
	, INSERTED.schType
	, INSERTED.schAuth
	, INSERTED.schParent
	, INSERTED.schDormant
	from INSERTED
		
	INNER JOIN metaNumbers
	ON num between
		case when INSERTED.schEst < @StartHistory then @StartHistory else INSERTED.schEst end
		and 
		case when INSERTED.schClosed = 0 then @MaxYear  
			 when INSERTED.schClosed > @MaxYear then @MaxYear
				else INSERTED.schClosed - 1
		end 
	LEFT JOIN SchoolYearHistory
		ON SchoolYearHistory.schNo = INSERTED.schNo
		AND metaNumbers.num = SchoolYearHistory.syYear
	WHERE schoolYearHistory.syID is null
	
	-- similarly for updates, we should clear history records outside the est/closed range
	DELETE FROM SchoolYearHistory
	FROM SchoolYearHistory 
	INNER JOIN INSERTED
		ON SchoolYearHistory.schNo = INSERTED.schNo
	INNER JOIN DELETED
		ON INSERTED.schNo = DELETED.schNo
	WHERE SchoolYearHistory.syYear < INSERTED.schEst
		OR
		( INSERTED. schClosed <> 0 AND SchoolYearHistory.syYear > INSERTED.schClosed)
		 
	

-- update the current year school year history
-- it''s always the same as the school record

	UPDATE SchoolYearHistory
		SET systCode = INSERTED.schtype 
			
			, syAuth =INSERTED.schAuth
			, syParent = INSERTED.schParent 
			 
			, syDormant = INSERTED.schDormant 
	from SchoolYearHistory
	INNER JOIN INSERTED
		ON SchoolYearHistory.schNo = INSERTED.schNo
	INNER JOIN DELETED
		ON INSERTED.schNo = DELETED.schNo
	WHERE SchoolYearHistory.syYear = @ActiveYear

-- update all future school history from the current year
-- only change values that were the same as on the schools
-- ie if there was an override in the future, preserve it

	UPDATE SchoolYearHistory
		SET systCode =
			case when systCode = deleted.schType then INSERTED.schtype else systCode end
			
			, syAuth = 
			 case when syAuth = deleted.schAuth then INSERTED.schAuth else syAuth end
			, syParent =
			 case when isnull(syParent,'''') = isnull(DELETED.schParent,'''') then INSERTED.schParent else syParent end
			 
			, syDormant = 
				case when syDormant = DELETED.schDormant then INSERTED.schDormant else syDormant end
	from SchoolYearHistory
	INNER JOIN INSERTED
		ON SchoolYearHistory.schNo = INSERTED.schNo
	INNER JOIN DELETED
		ON INSERTED.schNo = DELETED.schNo
	WHERE SchoolYearHistory.syYear between @ActiveYear + 1 and @MaxYear

END
')
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1

exec('-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 05 2010
-- Description:	
-- =============================================
CREATE TRIGGER dbo.Schools_Validations 
   ON  dbo.Schools 
   AFTER INSERT, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	begin try
	declare @count int
	
	-- cannot have a exension registration status without a parent school
	
	Select @count = count(*)
	FROM INSERTED I
	WHERE schRegStatus in (''PX'',''X'')
	AND schParent is null
	
	if @count > 0 begin
		RAISERROR(''<ValidationError field="schParent">
					A school with registration status (PX,X) must specify a parent school.
					</ValidationError>'', 16,1)
	end 
	
	
	--	-- cannot have a  registration status with a parent school
	
	Select @count = count(*)
	FROM INSERTED I
	WHERE schRegStatus in (''PR'',''R'')
	AND schParent is not null
	
	if @count > 0 begin
		RAISERROR(''<ValidationError field="schRegStatus">
					An extension school can only have registration status PX or X.
					</ValidationError>'', 16,1)
	end 
	
	-- and while we''re at it, the parent school cannot be an extension
	-- 

	Select @count = count(*)
	FROM INSERTED I
	WHERE schParent is not null
	and schParent in (select schNo from Schools WHERE schParent is not null)
	
  	if @count > 0 begin
		RAISERROR(''<ValidationError field="schParent">
					The parent school cannot be an Extension.
					</ValidationError>'', 16,1)
	end 

	---.... the current school cannot be an extension if it is a parent
	
	Select @count = count(*)
	FROM INSERTED I 
	WHERE schParent is not null
	and schNo in 
	(select schParent from Schools)

	
  	if @count > 0 begin
		RAISERROR(''<ValidationError field="schParent">
					A school with Extensions cannot have a Parent.
					</ValidationError>'', 16,1)
	end 	
	

	-- if there is a parent, it must be the same authority as this school
		Select @count = count(*)
	FROM INSERTED I 
	INNER JOIN Schools P
		on I.schParent = P.schNo
	WHERE I.schAuth <> p.schAuth

  	if @count > 0 begin
		RAISERROR(''<ValidationError field="schParent">
					A school and its parent must belong to the same Authority.
					</ValidationError>'', 16,1)
	end 	
	
	-- cannot have a registation status without a registration date
	Select @count = count(*)
	FROM INSERTED I
	WHERE schRegStatus is not null and schRegStatusDate is null
	
	if @count > 0 begin
		RAISERROR(''<ValidationError field="schRegStatusDate">
					A school with registration status must specify the registration status date.
					</ValidationError>'', 16,1)
	end 	

	-- cannot have a registation date without a registration status
	Select @count = count(*)
	FROM INSERTED I
	WHERE schRegStatus is null and schRegStatusDate is not null
	
	if @count > 0 begin
		RAISERROR(''<ValidationError field="schRegStatusDate">
					A school with no registration status cannot have a registration status date.
					</ValidationError>'', 16,1)
	end 
	-- registered status R needs registration number
	Select @count = count(*)
	FROM INSERTED I
	WHERE schRegStatus = ''R'' and schReg is null
	
	if @count > 0 begin
		RAISERROR(''<ValidationError field="schReg">
					A school that is Registered must specify the Registration No.
					</ValidationError>'', 16,1)
	end 	
	
	
	
	end try	
	/****************************************************************
	Generic catch block
	****************************************************************/
	begin catch
		
		DECLARE @err int,
			@ErrorMessage NVARCHAR(4000), 
			@ErrorSeverity INT, 
			@ErrorState INT; 
		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()


		rollback transaction


		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState); 

	end catch

END
')
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
BEGIN
   PRINT 'dbo.Schools Table Updated Successfully'
   COMMIT TRANSACTION
END ELSE
BEGIN
   PRINT 'Failed To Update dbo.Schools Table'
END
GO


--  
-- Script to Create common.schoolDataOptions in zali.SIEMIS 
-- Generated Monday, April 18, 2016, at 01:40 PM 
--  
-- Please backup zali.SIEMIS before executing this script
--  
-- ** Script Begin **
BEGIN TRANSACTION
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

PRINT 'Creating common.schoolDataOptions View'
GO

SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, QUOTED_IDENTIFIER, CONCAT_NULL_YIELDS_NULL ON
GO

SET NUMERIC_ROUNDABORT OFF
GO


exec('CREATE VIEW [common].[schoolDataOptions]
AS
Select C, N, seq
from
(
Select ''total'' c
, ''Total'' N
, 0 seq
UNION
Select ''enrol'' C
, ''Enrol'' N
, 1 seq
UNION
Select ''percF'' C
, ''Female %'' N
, 2 seq

) S
')
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
BEGIN
   PRINT 'common.schoolDataOptions View Added Successfully'
   COMMIT TRANSACTION
END ELSE
BEGIN
   PRINT 'Failed To Add common.schoolDataOptions View'
END
GO


--  
-- Script to Create common.schoolFieldOptions in zali.SIEMIS 
-- Generated Monday, April 18, 2016, at 01:40 PM 
--  
-- Please backup zali.SIEMIS before executing this script
--  
-- ** Script Begin **
BEGIN TRANSACTION
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

PRINT 'Creating common.schoolFieldOptions View'
GO

SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, QUOTED_IDENTIFIER, CONCAT_NULL_YIELDS_NULL ON
GO

SET NUMERIC_ROUNDABORT OFF
GO


exec('CREATE VIEW [common].[schoolFieldOptions]
AS
Select f , g, seq
from
(
Select ''Authority'' f
, null g
, 0 seq
UNION
Select ''School Type'' f
, null g
, 0 seq
UNION
Select vocabTerm f
, null g
, 0 seq
from sysVocab 
WHERE vocabName = ''District''
UNION
Select vocabTerm f
, null g
, 0 seq
from sysVocab 
WHERE vocabName = ''National Electorate''
UNION
Select vocabTerm f
, null g
, 0 seq
from sysVocab 
WHERE vocabName = ''Local Electorate''



) S
')
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
BEGIN
   PRINT 'common.schoolFieldOptions View Added Successfully'
   COMMIT TRANSACTION
END ELSE
BEGIN
   PRINT 'Failed To Add common.schoolFieldOptions View'
END
GO


--  
-- Script to Update pSchoolRead.SchoolFilterPaged in zali.SIEMIS 
-- Generated Monday, April 18, 2016, at 01:40 PM 
--  
-- Please backup zali.SIEMIS before executing this script
--  
-- ** Script Begin **
BEGIN TRANSACTION
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

PRINT 'Updating pSchoolRead.SchoolFilterPaged Procedure'
GO

SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, QUOTED_IDENTIFIER, CONCAT_NULL_YIELDS_NULL ON
GO

SET NUMERIC_ROUNDABORT OFF
GO


exec('-- =============================================
-- Author:		Brian Lewis
-- Create date: 
-- Description:	
-- 6 11 2012 added collate to temp tables
-- added primary key to @keys table
-- =============================================
ALTER PROCEDURE [pSchoolRead].[SchoolFilterPaged]
	-- Add the parameters for the stored procedure here

	@ColumnSet int = 0,
	@PageSize int = 0,
	@PageNo int = 1,
	@SortColumn sysname = null,
	@SortDir int = 0,

--filter parameters

	@SchoolNo   nvarchar(50) = null, 
	@SchoolName nvarchar(50) = null,
    @SearchAlias int = 0, -- 1 = search 2 = search for exclusion
    @AliasSource nvarchar(50) = null,
    @AliasNot int = 0,
    @SchoolType nvarchar(10) = null,
	@SchoolClass nvarchar(10) = null,
	@District nvarchar(10) = null,
    @Island nvarchar(10) = null,
	@ElectorateN nvarchar(10) = null,
	@ElectorateL nvarchar(10) = null,
	@Authority nvarchar(10) = null,
	@Language nvarchar(10) = null,
	@InfrastructureClass nvarchar(10) = null,
	@InfrastructureSize nvarchar(10) = null,
	@NearestShipping nvarchar(50) = null,
	@NearestBank nvarchar(50) = null,
	@NearestPo nvarchar(50) = null,
	@NearestAirstrip nvarchar(50) = null,
	@NearestClinic nvarchar(50) = null,
	@Nearesthospital nvarchar(50) = null,
	@SearchIsExtension int = null,
	@PArentSchool nvarchar(50) = null,
	@SEarchHasExtension int = null,
	@ExtensionReferenceYear int = null,	-- look on school year history - if null, use the Active Year
	@YearEstablished int = null,
	@RegistrationStatus nvarchar(10) = null,	-- see case for allowed values
	@SearchRegNo int = 0,				--search for the school name text in the reg no too
	@CreatedAfter datetime = null,
	@EditedAfter datetime = null,
	@IncludeClosed int = 0, 
	@ListName nvarchar(20) = null,
	@ListValue nvarchar(10)= null,
	@XmlFilter xml = null

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	SET NOCOUNT ON;
	SET FMTONLY OFF;	-- becuase Entity Framework 4.0 is a typical Microsoft 2 foot deep pool!


 	DECLARE @keys TABLE
	(
		schNo nvarchar(50) PRIMARY KEY
		, recNo int
	)
	
	DECLARE @NumMatches int
	
	INSERT INTO @keys
	EXEC pSchoolRead.SchoolFilterIDS 
		@NumMatches OUT,
		@PageSize,
		@PageNo,
		@SortColumn,
		@SortDir
		
	--filter parameters
	, @SchoolNo
	, @SchoolName
	, @SearchAlias
	, @AliasSource
	, @AliasNot
	, @SchoolType
	, @SchoolClass
	, @District
	, @Island
	, @ElectorateN
	, @ElectorateL
	, @Authority
	, @Language
	, @InfrastructureClass
	, @InfrastructureSize
	, @NearestShipping
	, @NearestBank
	, @NearestPO
	, @NearestAirstrip
	, @NearestClinic
	, @NearestHospital
	, @SearchIsExtension
	, @ParentSchool
	, @SearchHasExtension
	, @ExtensionReferenceYear
	, @YearEstablished
	, @RegistrationStatus
	, @SearchRegNo
	, @CreatedAfter
	, @EditedAfter
	, @IncludeClosed
	, @ListName 
	, @ListValue 
	, @xmlFilter



-------------------------------------
-- return results

if (@Columnset = 0) begin

Select S.*
, I.iName
, I.iGroup
, D.dName 
, N.codeDescription ElectNName
, L.codeDescription ElectLName
from Schools S 
	INNER JOIN @Keys K
			on S.schNo = K.schNo
	LEFT OUTER JOIN Islands I
		on S.iCode = I.iCode
	LEFT OUTER JOIN Districts D
		on D.dID = I.iGroup
	LEFT OUTER JOIN lkpElectorateN N 
		ON S.schElectN = N.codeCode
	LEFT OUTER JOIN lkpElectorateL L 
		ON S.schElectL = L.codeCode
	--LEFT OUTER JOIN (Select syParent, count(schNo) NumExtensions from SchoolYearHistory
	--					WHERE syYear = isnull(@ExtensionReferenceYear, common.ActiveSurveyYear())
	--					GROUP BY syParent
	--				) Extensions
	--	ON S.schNo = Extensions.syParent
	--LEFT OUTER JOIN (Select schNo PSchNo, syParent
	--					FROM SchoolYearHistory
	--					WHERE syYear = isnull(@ExtensionReferenceYear, common.ActiveSurveyYear())
	--				) Parent

	ORDER BY K.recNo
end

if (@Columnset = 1) begin
	Select S.schNo, S.schName, S.schType, S.schAuth, S.schClass, SS.svyYear
	, R.ssID, SS.ssEnrol, SS.ssEnrolM, SS.ssEnrolF,  TT.TNum
	
	FROM SChools S
	INNER JOIN @Keys K
			on S.schNo = K.schNo
	LEFT JOIN 
	(Select ssID , schNo
	, row_number() OVER (PARTITION BY schNo ORDER BY svyYEar DESC) rn
	From SchoolSurvey
	) R
	ON S.schNo = R.schNo
	AND R.rn = 1
	LEFT JOIN SchoolSurvey SS
	ON R.ssID = SS.ssID
	LEFT JOIN (
		select ssID, count(*) TNum from TeacherSurvey
		GROUP BY ssID
	) TT
	ON R.ssID = TT.ssID

end
if (@Columnset = 2) begin

Select S.schNo, S.schName, S.schType, S.schAuth, S.schClass
	, schParent, schReg, schRegDate, schRegStatus, schRegStatusDate
	, schEst, schEstBy, schClosed, schCloseReason
	FROM SChools S
	INNER JOIN @Keys K
			on S.schNo = K.schNo
	

end		
-- finally the summary			
		SELECT @NumMatches NumMatches
		, min(RecNo) PageFirst
		, max(recNo) PageLast
		, @PageSize PageSize
		, @PageNo PageNo
		, @Columnset columnSet
		FROM
		@Keys K
	
	
END
')
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
BEGIN
   PRINT 'pSchoolRead.SchoolFilterPaged Procedure Updated Successfully'
   COMMIT TRANSACTION
END ELSE
BEGIN
   PRINT 'Failed To Update pSchoolRead.SchoolFilterPaged Procedure'
END
GO


--  
-- Script to Create pEnrolmentRead.SchoolTable in zali.SIEMIS 
-- Generated Monday, April 18, 2016, at 01:40 PM 
--  
-- Please backup zali.SIEMIS before executing this script
--  
-- ** Script Begin **
BEGIN TRANSACTION
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

PRINT 'Creating pEnrolmentRead.SchoolTable Procedure'
GO

SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, QUOTED_IDENTIFIER, CONCAT_NULL_YIELDS_NULL ON
GO

SET NUMERIC_ROUNDABORT OFF
GO


exec('-- =============================================
-- Author:		Brian Lewis
-- Create date: 14 04 2016
-- Description:	quick Pivot for schools
-- =============================================
CREATE PROCEDURE [pEnrolmentRead].[SchoolTable]
	-- Add the parameters for the stored procedure here
	@RName nvarchar(50) = ''School Type''		-- identifier of row
	, @CName nvarchar(50) = ''Authority''	-- identifier of column
	, @xmlFilter xml = null				-- xml filter of selected data
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @ids TABLE
(
	schNo nvarchar(50)
	, recNo int
)

declare @NumM int =0


declare @currentSurvey int
Select @currentSurvey = max(svyYear)
from SchoolSurvey

if @xmlFilter is not null begin

	INSERT INTO @ids
	EXEC pSchoolRead.SchoolFilterIDs @NumMatches = @NumM OUT, @xmlFilter = @xmlFilter

end
    -- Insert statements for procedure here
declare @district nvarchar(100)

declare @electN nvarchar(100)

declare @electL nvarchar(100)

Select @electN = vocabTerm 
from sysVocab 
WHERE vocabName = ''National Electorate''

Select @electL = vocabTerm 
from sysVocab 
WHERE vocabName = ''Local Electorate''

Select @district = vocabTerm 
from sysVocab 
WHERE vocabName = ''District''
declare @output TABLE
(
	R nvarchar(200) NULL, 
	C nvarchar(200) NULL,
	RC nvarchar(200) NULL,
	Num int NULL,
	E int NULL,
	F int NULL,
	S int NULL,
	RName nvarchar(400) NULL,
	CName nvarchar(400) NULL
)
INSERT INTO @output

Select

	isnull(R,''<>'') R
	, isnull(C,''<>'') C
	, isnull(RC,''<>'') RC
	, count(*) Num
	, sum (Enrol) E
	, sum(EnrolF) F
	, sum (Estimate) S

	, @RName Rname
	, @CName Cname
FROM 
(

Select 
	convert(nvarchar(100),
	case @RName 
				when ''School Type'' then ST.stDescription
				when ''SchoolType'' then ST.stDescription
				when ''Authority'' then A.authName
				when ''District'' then D.dName
				when @district then D.dName
				when @electN then S.schElectL
			end) R
,	
	convert(nvarchar(100),
	case @RName 
				when ''School Type'' then schType
				when ''SchoolType'' then schType
				when ''Authority'' then schAuth
				when ''District'' then D.dID
			end) RC
		
	, 
	case @CName 
				when ''School Type'' then ST.stDescription
				when ''SchoolType'' then ST.stDescription
				when ''Authority'' then A.authName
				when ''District'' then D.dName

			end  C
	, IDE.Enrol Enrol
	, IDE.EnrolF EnrolF
	, case when E.Estimate = 1 then E.bestEnrol else null end Estimate
From Schools S
	LEFT JOIN SchoolTypes ST
		ON S.schType = ST.stCode
	LEFT JOIN Authorities A
		ON S.schAuth = A.authCode
	LEFT JOIN Islands I
		ON S.iCode = I.iCode
	LEFT JOIN Districts D
		ON I.iGroup = D.dID
	LEFT JOIN lkpElectorateN N
		ON S.schElectN = N.codeCode
	LEFT JOIN lkpElectorateL L
		ON S.schElectL = L.codeCode
	LEFT JOIN dbo.tfnEstimate_BestSurveyEnrolments() E
		ON E.LifeYear = @currentSurvey
		AND E.schNo = S.schNo
	LEFT JOIN pEnrolmentRead.ssIDEnrolment IDE
		ON E.bestssId = IDE.ssID



				

WHERE (@xmlFilter is null or S.schNo in (Select schNo from @Ids))
) SUB
GROUP BY R,C,RC

declare @summaryRows int


Select R,C, RC, RName, CName

, Num, E, F, S
from @Output
ORDER BY R,C

Select @summaryRows = @@rowcount -- = count(*) from @output

-- finally the summary                        
                SELECT @NumM NumMatches
                , 1 PageFirst
                , @NumM PageLast
                , 0 PageSize
                , 1 PageNo
                , 0 columnSet
                , @summaryRows SummaryRows

END
')
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
BEGIN
   PRINT 'pEnrolmentRead.SchoolTable Procedure Added Successfully'
   COMMIT TRANSACTION
END ELSE
BEGIN
   PRINT 'Failed To Add pEnrolmentRead.SchoolTable Procedure'
END
GO


--  
-- Script to Update dbo.Schools in zali.SIEMIS 
-- Generated Monday, April 18, 2016, at 01:40 PM 
--  
-- Please backup zali.SIEMIS before executing this script
--  
-- ** Script Begin **
BEGIN TRANSACTION
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

PRINT 'Updating dbo.Schools Table'
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_SchoolParent')
      ALTER TABLE [dbo].[Schools] DROP CONSTRAINT [FK_SchoolParent]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_SchoolParent')
      ALTER TABLE [dbo].[Schools] ADD CONSTRAINT [FK_SchoolParent] FOREIGN KEY ([schParent]) REFERENCES [dbo].[Schools] ([schNo])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'Schools_FK00')
      ALTER TABLE [dbo].[Schools] DROP CONSTRAINT [Schools_FK00]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'Schools_FK00')
      ALTER TABLE [dbo].[Schools] ADD CONSTRAINT [Schools_FK00] FOREIGN KEY ([schElectN]) REFERENCES [dbo].[lkpElectorateN] ([codeCode])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'Schools_FK01')
      ALTER TABLE [dbo].[Schools] DROP CONSTRAINT [Schools_FK01]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'Schools_FK01')
      ALTER TABLE [dbo].[Schools] ADD CONSTRAINT [Schools_FK01] FOREIGN KEY ([schClass]) REFERENCES [dbo].[lkpSchoolClass] ([codeCode])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'Schools_FK02')
      ALTER TABLE [dbo].[Schools] DROP CONSTRAINT [Schools_FK02]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'Schools_FK02')
      ALTER TABLE [dbo].[Schools] ADD CONSTRAINT [Schools_FK02] FOREIGN KEY ([schElectL]) REFERENCES [dbo].[lkpElectorateL] ([codeCode])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'Schools_FK03')
      ALTER TABLE [dbo].[Schools] DROP CONSTRAINT [Schools_FK03]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'Schools_FK03')
      ALTER TABLE [dbo].[Schools] ADD CONSTRAINT [Schools_FK03] FOREIGN KEY ([iCode]) REFERENCES [dbo].[Islands] ([iCode])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'Schools_FK04')
      ALTER TABLE [dbo].[Schools] DROP CONSTRAINT [Schools_FK04]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'Schools_FK04')
      ALTER TABLE [dbo].[Schools] ADD CONSTRAINT [Schools_FK04] FOREIGN KEY ([schAuth]) REFERENCES [dbo].[Authorities] ([authCode])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'Schools_FK05')
      ALTER TABLE [dbo].[Schools] DROP CONSTRAINT [Schools_FK05]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'Schools_FK05')
      ALTER TABLE [dbo].[Schools] ADD CONSTRAINT [Schools_FK05] FOREIGN KEY ([schLandOwner]) REFERENCES [dbo].[lkpLandOwnership] ([codeCode]) ON UPDATE CASCADE
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'Schools_FK06')
      ALTER TABLE [dbo].[Schools] DROP CONSTRAINT [Schools_FK06]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'Schools_FK06')
      ALTER TABLE [dbo].[Schools] ADD CONSTRAINT [Schools_FK06] FOREIGN KEY ([schLandUse]) REFERENCES [dbo].[lkpLandUse] ([codeCode]) ON UPDATE CASCADE
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'Schools_FK07')
      ALTER TABLE [dbo].[Schools] DROP CONSTRAINT [Schools_FK07]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'Schools_FK07')
      ALTER TABLE [dbo].[Schools] ADD CONSTRAINT [Schools_FK07] FOREIGN KEY ([schType]) REFERENCES [dbo].[SchoolTypes] ([stCode])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_Buildings_Schools')
      ALTER TABLE [dbo].[Buildings] ADD CONSTRAINT [FK_Buildings_Schools] FOREIGN KEY ([schNo]) REFERENCES [dbo].[Schools] ([schNo]) ON UPDATE CASCADE
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'EnrolmentProjection_FK01')
      ALTER TABLE [dbo].[EnrolmentProjection] ADD CONSTRAINT [EnrolmentProjection_FK01] FOREIGN KEY ([schNo]) REFERENCES [dbo].[Schools] ([schNo]) ON UPDATE CASCADE ON DELETE CASCADE
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_Establishment_Schools')
      ALTER TABLE [dbo].[Establishment] ADD CONSTRAINT [FK_Establishment_Schools] FOREIGN KEY ([schNo]) REFERENCES [dbo].[Schools] ([schNo])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'ExamScores_FK01')
      ALTER TABLE [dbo].[ExamScores] ADD CONSTRAINT [ExamScores_FK01] FOREIGN KEY ([schNo]) REFERENCES [dbo].[Schools] ([schNo]) ON UPDATE CASCADE
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'ExamStandardTest_FK01')
      ALTER TABLE [dbo].[ExamStandardTest] ADD CONSTRAINT [ExamStandardTest_FK01] FOREIGN KEY ([schNo]) REFERENCES [dbo].[Schools] ([schNo]) ON UPDATE CASCADE
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'ExamSubjectGrades_FK01')
      ALTER TABLE [dbo].[ExamSubjectGrades] ADD CONSTRAINT [ExamSubjectGrades_FK01] FOREIGN KEY ([schNo]) REFERENCES [dbo].[Schools] ([schNo]) ON UPDATE CASCADE
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'ListSchools_FK01')
      ALTER TABLE [dbo].[ListSchools] ADD CONSTRAINT [ListSchools_FK01] FOREIGN KEY ([schNo]) REFERENCES [dbo].[Schools] ([schNo]) ON UPDATE CASCADE ON DELETE CASCADE
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_paAssessment_School')
      ALTER TABLE [dbo].[paAssessment_] ADD CONSTRAINT [FK_paAssessment_School] FOREIGN KEY ([paSchNo]) REFERENCES [dbo].[Schools] ([schNo])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'SchoolAlias_FK00')
      ALTER TABLE [dbo].[SchoolAlias] ADD CONSTRAINT [SchoolAlias_FK00] FOREIGN KEY ([schNo]) REFERENCES [dbo].[Schools] ([schNo]) ON UPDATE CASCADE ON DELETE CASCADE
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_Finance_School')
      ALTER TABLE [dbo].[SchoolFinanceCodes] ADD CONSTRAINT [FK_Finance_School] FOREIGN KEY ([schNo]) REFERENCES [dbo].[Schools] ([schNo]) ON UPDATE CASCADE ON DELETE CASCADE
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'SchoolLedger_FK01')
      ALTER TABLE [dbo].[SchoolLedger] ADD CONSTRAINT [SchoolLedger_FK01] FOREIGN KEY ([schNo]) REFERENCES [dbo].[Schools] ([schNo]) ON UPDATE CASCADE ON DELETE CASCADE
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'SchoolLinks_FK00')
      ALTER TABLE [dbo].[SchoolLinks] ADD CONSTRAINT [SchoolLinks_FK00] FOREIGN KEY ([schNo]) REFERENCES [dbo].[Schools] ([schNo]) ON UPDATE CASCADE
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'SchoolNotes_FK00')
      ALTER TABLE [dbo].[SchoolNotes] ADD CONSTRAINT [SchoolNotes_FK00] FOREIGN KEY ([schNo]) REFERENCES [dbo].[Schools] ([schNo]) ON UPDATE CASCADE
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_SchoolParent')
      ALTER TABLE [dbo].[Schools] ADD CONSTRAINT [FK_SchoolParent] FOREIGN KEY ([schParent]) REFERENCES [dbo].[Schools] ([schNo])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_SchoolSurvey_Schools')
      ALTER TABLE [dbo].[SchoolSurvey] ADD CONSTRAINT [FK_SchoolSurvey_Schools] FOREIGN KEY ([schNo]) REFERENCES [dbo].[Schools] ([schNo])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_SchoolYearHistory_Schools')
      ALTER TABLE [dbo].[SchoolYearHistory] ADD CONSTRAINT [FK_SchoolYearHistory_Schools] FOREIGN KEY ([schNo]) REFERENCES [dbo].[Schools] ([schNo]) ON UPDATE CASCADE
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_SchoolYearHistory_SchoolsParent')
      ALTER TABLE [dbo].[SchoolYearHistory] ADD CONSTRAINT [FK_SchoolYearHistory_SchoolsParent] FOREIGN KEY ([syParent]) REFERENCES [dbo].[Schools] ([schNo])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'SurveyYearRank_FK00')
      ALTER TABLE [dbo].[SurveyYearRank] ADD CONSTRAINT [SurveyYearRank_FK00] FOREIGN KEY ([schNo]) REFERENCES [dbo].[Schools] ([schNo]) ON UPDATE CASCADE ON DELETE CASCADE
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'TeacherAppointment_FK03')
      ALTER TABLE [dbo].[TeacherAppointment] ADD CONSTRAINT [TeacherAppointment_FK03] FOREIGN KEY ([SchNo]) REFERENCES [dbo].[Schools] ([schNo]) ON UPDATE CASCADE
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'TeacherAttendance_FK00')
      ALTER TABLE [dbo].[TeacherAttendance] ADD CONSTRAINT [TeacherAttendance_FK00] FOREIGN KEY ([schNo]) REFERENCES [dbo].[Schools] ([schNo]) ON UPDATE CASCADE
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_Schools_WorkItems')
      ALTER TABLE [dbo].[WorkItems] ADD CONSTRAINT [FK_Schools_WorkItems] FOREIGN KEY ([schNo]) REFERENCES [dbo].[Schools] ([schNo]) ON UPDATE CASCADE
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
BEGIN
   PRINT 'dbo.Schools Table Updated Successfully'
   COMMIT TRANSACTION
END ELSE
BEGIN
   PRINT 'Failed To Update dbo.Schools Table'
END
GO
