﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace Pineapples.Hubs
{
    public class NdoeProcessState
    {
        public string schools { get; set; }
        public string students { get; set; }
        public string staff { get; set; }
        public string wash { get; set; }
    }

    /// <summary>
    /// this interface may be called by the client
    /// normally you would use this 
    /// </summary>
    public interface INdoeProcessClient : IHub
    {
        [HubMethodName("connectFile")]
        void ConnectFile(string FileId);

        [HubMethodName("disconnectFile")]
        void DisconnectFile(string FileId);
    }
    /// <summary>
    /// this interface defines the callbcks that the server can make to the client
    /// the hub is a generic based on this interface
    /// Clients.All exposes this interface
    /// </summary>
    public interface INdoeProcessCallbacks 
    {
        /// <summary>
        /// send a progress message to the 
        /// the listener can then read the code set
        /// </summary>
        /// <param name="name">name of the code set that got changed</param>
        void progress(string fileId, NdoeProcessState state);

    }


    [HubName("ndoeprocess")]
    public class NdoeProcessHub : Hub<INdoeProcessCallbacks>, INdoeProcessClient
    {
        public override Task OnConnected()
        {
            return base.OnConnected();
        }

        public void ConnectFile(string FileId)
        {
            Groups.Add(Context.ConnectionId, FileId);
        }

        public void DisconnectFile(string FileId)
        {
            Groups.Remove(Context.ConnectionId, FileId);
        }
    }
}