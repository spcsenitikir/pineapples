﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pineapples.Models
{
    public enum PermissionTopicEnum
    {
        Enrolment = 0,
        Establishment,
        Exam,
        Finance,
        Infrastructure,
        Inspection,
        IST,
        PA,
        School,
        Survey,
        Teacher
    }

    public class PineapplesPermissionAttribute : Softwords.Web.PermissionAttribute
    {
        public PineapplesPermissionAttribute(PermissionTopicEnum topic, Softwords.Web.PermissionAccess access) : base()
        {
            this.Topic = (int)topic;
            this.Access = access;
        }

        public static string Name(PermissionTopicEnum topic, Softwords.Web.PermissionAccess access)
        {
            return Name<PermissionTopicEnum>(topic, access);
        }
    }
}

