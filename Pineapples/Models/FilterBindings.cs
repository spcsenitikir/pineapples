﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pineapples.Models
{
    // various parameter bindings

    #region School
    public class SchoolTableBindingModel
    {
        // filter is a complex node in the incoming json - it is converted to the filter
        // by the JSONConverter attribute on the definition of the Filter itself
        public Data.SchoolFilter filter { get; set; }
        public string row { get; set; }
        public string col { get; set; }
    }
    #endregion

    #region Teacher
    public class TeacherTableBindingModel
    {
        // filter is a complex node in the incoming json - it is converted to the filter
        // by the JSONConverter attribute on the definition of the Filter itself
        public Data.TeacherFilter filter { get; set; }
        public string row { get; set; }
        public string col { get; set; }
    }
    #endregion

}