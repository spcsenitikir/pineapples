﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pineapples.mvcControllers
{
    public class GenericController : Softwords.Web.mvcControllers.mvcControllerBase
    {

        #region Generic-centric components
        public ActionResult FlashFindSearchComponent()
        {
            return View();
        }

        public ActionResult PagedListParamsComponent()
        {
            return View();
        }

        public ActionResult ChartManipulationComponent()
        {
            return View();
        }

        public ActionResult MapParamsComponent()
        {
            return View();
        }

        public ActionResult Panel()
        {
            return View();
        }

        public ActionResult Navigator()
        {
            return View();
        }
        #endregion

    }
}