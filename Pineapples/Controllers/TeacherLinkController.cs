﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web.mvcControllers;
using Softwords.Web;

namespace Pineapples.mvcControllers
{
    public class TeacherLinkController : Softwords.Web.mvcControllers.mvcControllerBase
    {
        // GET: Teacher
        [Authorize]
        public ActionResult Searcher(string version)
        {
            // pass through the data needed for the model
            TeacherSearcherModel model = new TeacherSearcherModel();
            string viewName = "TeacherSearcher";

            switch (CurrentUser.MenuKey)
            {
                default:
                    viewName = viewName + version;
                    break;
            }
            return View(viewName, model);
        }

        [LayoutInjector("EditPageLayout")]
        public ActionResult Item()
        {
            ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Teacher, PermissionAccess.Write);
            return View("TeacherItem");
        }

        #region Teacherlink-centric components
        public ActionResult SearcherComponent()
        {
            return View();
        }

        public ActionResult Upload()
        {
            return View();
        }

        [LayoutInjector("MaterialDialogLayout")]
        public ActionResult UploadDialog()
        {
            return View();
        }

        [LayoutInjector("EditDialogLayout")]
        public ActionResult EditDialog()
        {
            return View();
        }

        public ActionResult Thumbnail()
        {
            return View();
        }
        #endregion
    }
}