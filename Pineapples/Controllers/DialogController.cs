﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pineapples.mvcControllers
{
    public class DialogController : Softwords.Web.mvcControllers.mvcControllerBase
    {
        // GET: Data

        public ActionResult Conflict()
        {
            return View("conflictResponse");
        }
        public ActionResult Validation()
        {
            return View("validationResponse");
        }
        public ActionResult DataError()
        {
            return View("dataErrorResponse");
        }
        public ActionResult DataAddNew()
        {
            return View("dataAddNewResponse");
        }
        public ActionResult DataNotFound()
        {
            return View("dataNotFoundResponse");
        }
        public ActionResult DataDuplicate()
        {
            return View("dataDuplicateResponse");
        }
        public ActionResult PermissionError()
        {
            return View("dataNoPermissionResponse");
        }
        public ActionResult CodePicker()
        {
          return View("codePicker");
        }
  }
}