﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web;
using Softwords.Web.mvcControllers;

namespace Pineapples.mvcControllers
{
    public class SchoolController : Softwords.Web.mvcControllers.mvcControllerBase
    {
        public ActionResult Dashboard()
        {
            return View();
        }

        [Authorize]
        public ActionResult Searcher(string version)
        {
            SchoolSearcherModel model = new SchoolSearcherModel();
            string viewName = "SchoolSearcher";
            return View(viewName, model);
        }

        public ActionResult PagedList()
        {
            return View();
        }

        public ActionResult PagedListEditable()
        {
            return View();
        }

        [LayoutInjector("EditPageLayout")]
        public ActionResult Item()
        {
            ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.School, PermissionAccess.Write);
            return View("SchoolItem");
        }

        public ActionResult LatLng()
        {
            return View("SchoolLatLng");
        }

        #region School-centric components
        public ActionResult surveyList()
        {
            return View();
        }

        public ActionResult QuarterlyReports()
        {
            return View();
        }

        public ActionResult SchoolAccreditations()
        {
            return View();
        }

        public ActionResult SearcherComponent()
        {
            return View();
        }

        public ActionResult LinkList()
        {
            return View();
        }

        #endregion

        public ActionResult ContextTabs()
        {
            return View();
        }
    }
}