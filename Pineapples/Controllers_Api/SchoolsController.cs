﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Pineapples.Data;
using DataLayer = Pineapples.Data.DataLayer;
using System.Security.Claims;
using Pineapples.Models;

namespace Pineapples.Controllers
{
    [RoutePrefix("api/schools")]
    public class SchoolsController : PineapplesApiController
    {
        public SchoolsController(DataLayer.IDSFactory factory) : base(factory) { }

        // NOTE: region ResourceList and pupiltable routes were previousl also in SurveyEntryController.cs
        // but only ones here used.

        #region ResourceList
        [HttpGet]
        [Route(@"resourcelist/{schoolNo}/{year:int}/{category}")]
        public object ResourceList(string schoolNo, int year, string category)
        {
            return Factory.SurveyEntry().ResourceList(schoolNo, year, category); //.ResultSet;
        }

        [HttpPost]
        [Route(@"resourcelist")]
        public object SaveResourceList(ResourceList rl)
        {
            return Factory.SurveyEntry().SaveResourceList(rl, this.User.Identity.Name); //.ResultSet;
        }
        #endregion

        #region pupiltable
        [HttpGet]
        [Route(@"pupiltable/{schoolNo}/{year:int}/{tableName}")]
        public object PupilTable(string schoolNo, int year, string tableName)
        {
            return Factory.SurveyEntry().PupilTable(schoolNo, year, tableName); //.ResultSet;
        }

        [HttpPost]
        [Route(@"pupiltable")]
        public object SavePupilTable(PupilTable pt)
        {
            return Factory.SurveyEntry().SavePupilTable(pt, this.User.Identity.Name); //.ResultSet;
        }
        #endregion

        #region School CRUD methods
        [HttpPost]
        [Route(@"")]
        [Route(@"{schoolNo}")]
        [PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.WriteX)]
        public HttpResponseMessage Create(SchoolBinder school)
        {
            try
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created,
                    Ds.Create(school, (ClaimsIdentity)User.Identity));
                return response;
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }


        }

        [HttpPut]
        [Route(@"{schoolNo}")]
        [PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.Write)]
        public object Update(SchoolBinder school)
        {
            try
            {
                return Ds.Update(school, (ClaimsIdentity)User.Identity).definedProps;
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }


        }

        [HttpGet]
        [Route(@"{schoolNo}")]
        [PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.Read)]
        public object GetSchool(string schoolNo)
        {
            return Factory.School().Read(schoolNo);
        }

        [HttpDelete]
        [Route(@"{schoolNo}")]
        [PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.Write)]
        public object Delete([FromBody] SchoolBinder school)
        {
            // TO DO
            return school.definedProps;
        }
        #endregion

        #region School Collection methods

        [HttpPost]
        [ActionName("filter")]
        public object Filter(SchoolFilter fltr)
        {
            fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
            return Ds.Filter(fltr);
            //  return fltr.Search();
        }

        [HttpPost]
        [ActionName("table")]
        public object Table(Models.SchoolTableBindingModel model)
        {
            SchoolFilter fltr = model.filter;
            fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
            return Ds.Table(model.row, model.col, fltr);
        }

        [HttpPost]
        [ActionName("geo")]
        public object GeoData(SchoolFilter fltr)
        {
            return Factory.School().Geo("POINT", fltr); //.ResultSet;
        }

        #endregion
        private IDSSchool Ds
        {
            get { return Factory.School(); }
        }
    }
}
