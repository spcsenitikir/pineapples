﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using Pineapples.Data;
using DataLayer = Pineapples.Data.DataLayer;
using Pineapples.Data.DB;
using Softwords.DataTools;
using System.Security.Claims;

using Pineapples.Models;
using Softwords.Web;

namespace Pineapples.Controllers
{
    [RoutePrefix("api/teachers")]
    public class TeachersController : PineapplesApiController
    {
        public TeachersController(DataLayer.IDSFactory factory) : base(factory) { }

        [HttpPost]
        [ActionName("filter")]
        public object Filter(TeacherFilter fltr)
        {
            return Factory.Teacher().Filter(fltr);
        }

        //[HttpPost]
        //[ActionName("table")]
        //public object Table(TeacherFilter fltr)
        //{
        //    return Factory.Teacher().Table(fltr);
        //}

        [HttpPost]
        [ActionName("table")]
        public object Table(Models.TeacherTableBindingModel model)
        {
            TeacherFilter fltr = model.filter;
            // fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
            return Ds.Table(model.row, model.col, fltr);
        }

        [HttpPost]
        [ActionName("geo")]
        public object GeoData(TeacherFilter fltr)
        {
            return Factory.Teacher().Geo("POINT", fltr); //.ResultSet;
        }

        #region Teacher Create Read Update

        [HttpGet]
        [Route(@"{teacherID:int}")]
        [PineapplesPermission(PermissionTopicEnum.Teacher, PermissionAccess.Read)]
        public object GetTeacher(int teacherID)
        {
            return Ds.Read(teacherID, ((ClaimsIdentity)User.Identity).hasPermission((int)PermissionTopicEnum.Teacher, PermissionAccess.ReadX));
        }

        [HttpPost]
        [Route(@"")]
        // [Route(@"{teacherID}")] this route can never make sense becuase teacherID is an identity and so
        // cannot be specified by the client
        [PineapplesPermission(PermissionTopicEnum.Teacher, Softwords.Web.PermissionAccess.WriteX)]
        public object Create(TeacherBinder teacherIdentity)
        {
            try
            {
                return Ds.Create(teacherIdentity, (ClaimsIdentity)User.Identity);
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }
        }

        [HttpPut]
        [Route(@"{teacherID:int}")]
        [PineapplesPermission(PermissionTopicEnum.Teacher, Softwords.Web.PermissionAccess.Write)]
        public object Update(TeacherBinder teacherIdentity)
        {
            try
            {
                return Ds.Update(teacherIdentity, (ClaimsIdentity)User.Identity).definedProps;
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }
        }

        #endregion

        private IDSTeacher Ds
        {
            get { return Factory.Teacher(); }
        }
    }
}
