module Pineapples {
  let modes = [
    {
      key: "Location",
      columnSet: 0,
      gridOptions: {
        columnDefs: [
          {
            field: 'iName',
            name: 'island',
            displayName: 'Island',
            cellClass: 'gdAlignLeft'
          },
        // not sure why this field alone was editable?
          //{
          //  field: 'dID',
          //  name: 'district',
          //  displayName: 'District',
          //  headerCellFilter:'vocab',
          //  cellClass: 'gdAlignLeft',
          //  enableCellEdit: true,
          //  editType: 'dropdown',
          //  lookup: 'districts',
          //  editableCellTemplate: 'ui-grid/dropdownEditor',
          //  editDropdownIdLabel: 'C',
          //  editDropdownValueLabel: 'N'
          //},
          {
            field: 'dName',
            name: 'dName',
            displayName: 'District',
            headerCellFilter: 'vocab',
            cellClass: 'gdAlignLeft'
          },
          {
            field: 'schVillage',
            name: 'village',
            displayName: 'Village',
            cellClass: 'gdAlignLeft'
          },
          {
            field: 'schLat',
            name: 'LatLng',
            displayName: 'Lat/Lng',
            cellTemplate: '<div class="ui-grid-cell-contents"><a  ng-click="grid.appScope.listvm.action(\'latlng\',\'schNo\', row.entity);">[{{row.entity.schLat | number:4}}, {{row.entity.schLong |number:4}}]</a></div>'
          },
        ]
      }
    },
    {
      key: "Latest Survey",
      columnSet: 1,
      gridOptions: {
        columnDefs: [
          {
            field: 'svyYear',
            name: 'svyYear',
            displayName: 'Survey Year',
          },
          {
            field: 'ssEnrol',
            name: 'ssEnrol',
            displayName: 'Enrol',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'ssEnrolM',
            name: 'ssEnrolM',
            displayName: 'Enrol Male',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'ssEnrolF',
            name: 'ssEnrolF',
            displayName: 'Enrol Female',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'TNum',
            name: 'TNum',
            displayName: 'Teachers',
            cellClass: 'gdAlignRight'
          },
        ]         // columndefs
      }           // gridoptions
    },            // mode
    {
      key: 'Registration',
      columnSet: 2,
      editable: true,
      gridOptions: {
        columnDefs: [
          {
            field: 'schRegStatus',
            name: 'schRegStatus',
            displayName: 'Registration Status',
            editable: true,
            lookup: "schoolRegistration"

          },
          {
            field: 'schRegStatusDate',
            name: 'schRegStatusDate',
            displayName: 'Status Date',
            cellFilter: 'date:"dd-MMM-yyyy"',
            editable: true,
            type: 'date',
          },
          {
            field: 'schReg',
            name: 'schReg',
            displayName: 'Registration No',
            editable: true,
          },
          {
            fields: "SchEst",
            name: "schEst",
            displayName: "Est.",
            editable: true,
            type: 'number',
          },
          {
            fields: "SchClosed",
            name: "schClosed",
            displayName: "Closed",
            editable: true,
            type: 'number',
          },

        ]      // columndefs
      }        // gridoptions
    }          // mode
  ];           // modes

  var pushModes = function (filter) {
    filter.PushViewModes(modes);
  };

  angular
    .module('pineapples')
    .run(['SchoolFilter', pushModes]);
}
