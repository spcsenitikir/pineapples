﻿namespace Pineapples.Dashboard {

  interface IBindings {
    school: any;
    ngDisabled: boolean;
  }

  class Controller extends Sw.Charts.SizeableChartController implements IBindings {

    // injections
    static $inject = ["$compile", "$timeout", "$scope", "$element"];
    constructor($compile: ng.ICompileService, $timeout: ng.ITimeoutService, scope: ng.IScope, element) {
      super($compile, $timeout, scope, element);
    }

    // bindings
    public school: any;
    public ngDisabled: boolean;

    private _tip;

    public $onChanges(changes: any) {
      console.log(changes);
    }

    /**
     * @method newSize
     * @override Sw.Charts.SizeableComponentController's newSize
     * @description apply some rounding so this is not respnding to tiny fluctuations
     */
    public setSize(newSize: Sw.Charts.Size) {
      let scrollwidth = ((window.innerWidth - $(window).width())) || 0;
      this.size.width = Math.floor(newSize.width / 10) * 10 - (20 - scrollwidth);
      this.size.height = Math.floor(this.size.width / 3);       //3:1 aspect ratio is the default
    }

    /**
     * @method newSize
     * @override Sw.Charts.SizeableComponentController's newSize
     */
    public onResize(element) {
      if (this.size.height === 0) {
        return;
      }
      if (this.chart) {
        this.chart.redraw();
      } else {
        this.makeChart(element);
      }
    }

    private stackedBarV;
    private chart: Plottable.Component;

    public clickHandler(d) {
      alert(d.svyYear);
    }
    public makeChart = (element) => {
      let categoryScaleV = new Plottable.Scales.Category();
      let valueScaleV = new Plottable.Scales.Linear();
      let colorScaleV = new Plottable.Scales.Color();
      let categoryAxisV = new Plottable.Axes.Category(categoryScaleV, "bottom");
      categoryAxisV.tickLabelAngle(-90);
      let valueAxisV = new Plottable.Axes.Numeric(valueScaleV, "left");
      // colorLegend legend
      let legendV = new Plottable.Components.Legend(colorScaleV)
        .maxEntriesPerRow(2)
        .xAlignment('left');

      var dsM = new Plottable.Dataset(this.school.Surveys).metadata("M");
      var dsF = new Plottable.Dataset(_.clone(this.school.Surveys, true)).metadata("F");

      let tip = d3.tip()
        .attr('class', 'd3-tip')
        .offset([-10, 0])
        .html(function (d, i) {
          if (d.dataset === "F") {
            return "F: " + d.ssEnrolF;
          }
          return "M: " + d.ssEnrolM;
        });
      this._tip = tip;      // set aside so we can destroy it later...

      let compile = this.$compile;
      let scope = this.scope;
      let self = this;
      let callbackAnimate = function (jr: Plottable.Drawers.JoinResult, attr, drawer) {
        (<d3.selection.Update<any>>jr.merge)
          .on("mouseover", tip.show)
          .on("mouseout", tip.hide)
          .on("click", self.clickHandler)
          .call(function () {
            compile(this[0].parentNode)(scope);
          });

        this.innerAnimate(jr, attr, drawer);
      };
      let innerAnimator = new Plottable.Animators.Easing()
        // .exitEasingMode(Plottable.Animators.EasingFunctions.squEase("linear", 0, .5))
        // .easingMode(<any>Plottable.Animators.EasingFunctions.squEase("linear", .5, 1))
        .stepDuration(50)
        .stepDelay(0);
      let animator = new Plottable.Animators.Callback()
        .callback(callbackAnimate)
        .innerAnimator(innerAnimator);

      this.stackedBarV = new Plottable.Plots.StackedBar(Plottable.Plots.Bar.ORIENTATION_VERTICAL)
        .addDataset(dsM)
        .addDataset(dsF)
        .x((d) => d.svyYear, categoryScaleV)
        .y((d, i, dataset) => {
          if (dataset.metadata() === "F") {
            d.dataset = "F";        // modify the data here for the benefit of the d3tip - which does not have access to the dataset
            return d.ssEnrolF
          };
          return d.ssEnrolM;
        }, valueScaleV)
        .attr("fill", (d, i, dataset) => dataset.metadata(), colorScaleV)
        .attr("opacity", .8)
        .animated(true)
        .animator(Plottable.Plots.Animator.MAIN, animator);

      this.chart = new Plottable.Components.Table([
        [valueAxisV, this.stackedBarV],
        [null, categoryAxisV],
        [null, legendV]

      ]);

      let div = $(element).find("div")
      let svg = $(element).find("div>svg");
      let d3svg = d3.selectAll(svg.toArray());
      d3svg.call(<any>tip);
      this.chart.renderTo(d3svg);
    }
  }

  class EnrolLevelChart implements ng.IComponentOptions {
    public bindings: any = {
      school: "<",
      ngDisabled: "="
    };
    public controller: any = Controller;
    public controllerAs: string = "vm";

    // angular doesn't watch ngOptions - so we have to make sure the right string is in the template when
    // it is first rendered
    public template = `<div><svg width="{{vm.size.width}}" height="{{vm.size.height}}"></svg></div>`;
  }

  angular
    .module("pineapples")
    .component("enrolLevelChart", new EnrolLevelChart());
}
