﻿namespace Pineapples.Schools {

  interface IBindings {
    surveys: any;
    editSurveyOpened: boolean;
    activeSurvey: any;
  }

  class Controller implements IBindings {
    public surveys: any;
    public editSurveyOpened: boolean = false;
    public activeSurvey: any;

    static $inject = ["$mdDialog", "$location", "$state"];
    constructor(public mdDialog: ng.material.IDialogService, private $location: ng.ILocationService, public $state: ng.ui.IStateService) { }

    public $onChanges(changes) {
      if (changes.surveys) {
        console.log(this.surveys);
      }
      if (changes.activeSurvey) {
        console.log(this.activeSurvey);
      }
    }

    public showAuditLog(schNo: string, year: number) {
      let options: any = {
        clickOutsideToClose: true,
        templateUrl: "survey/auditlogdialog",
        controller: DialogController,
        controllerAs: "dvm",
        bindToController: true,
        resolve: {
          log: ["surveyAPI", (api) => {
            return api.audit(schNo, year);
          }]
        }

      }
      this.mdDialog.show(options);
    }

    public showEditSurvey(school: string, year: number) {
      this.activeSurvey = {
        schoolNo: school,
        year: year
      }      
      this.editSurveyOpened = true;
    }

    public closeEditSurvey(school: string, year: number) {
      this.activeSurvey.year = null;
      this.editSurveyOpened = false;
    }


    private _surveyReport: Pineapples.Reporting.Report;
    /**
     * Create a report definition to feed to the reportdef component
     **/
    public get surveyReport() {
      if (!this._surveyReport) {
        let report = new Pineapples.Reporting.Report();
        report.provider = "jasper";
        report.path = "School_Reports/School_Enrollments_by_EdLevel_Gender";
        report.name = "Enrollments";
        report.format = "PDF";
        // putting this here just for demonstration, in order to get the dialog always
        report.promptForParams = "always";
        this._surveyReport = report;
      }
      return this._surveyReport;
    }

    /**
     * callback from reportdef component on report execution
     * @param report - the Report object
     * @param context - context as passed to the report component
     * @param instanceInfo - instanceInfo object, its properties can be
     * set in this call back, specifically:
     * parameters: these are the report parameters that will go to the report server
     * filename: if supplied can be a more meaningful filename for the downloaded file
     */
    public setupReport(report, context, instanceInfo) {
      instanceInfo.parameters = {
        school: context.schNo,
        year: context.svyYear
      }
      instanceInfo.filename = "Enrolment_" + context.schNo + "_" + context.svyYear;
    }
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        surveys: '<',
        editSurveyOpened: '<',
        activeSurvey: '<'
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "school/surveylist";
    }
  }

  // popup dialog for audit log
  class DialogController extends Sw.Component.MdDialogController {
    static $inject = ["$mdDialog", "log"];
    constructor(mdDialog: ng.material.IDialogService, public log) {
      super(mdDialog);
    }
  }

  angular
    .module("pineapples")
    .component("componentSchoolSurveyList", new Component());
}
