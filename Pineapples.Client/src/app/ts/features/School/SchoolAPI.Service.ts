﻿module Pineapples.Api {

  let factory = ($q: ng.IQService, restAngular: restangular.IService, errorService) => {

    let svc: any = Sw.Api.ApiFactory.getApi(restAngular, errorService, "schools");
    svc.read = (id) => svc.get(id); //.catch(errorService.catch);

    svc.pupilTable = (schoolNo: string, year: number, tableName: string) => {
      let path: string = "pupiltable" + "/" + schoolNo + "/" + year + "/" + tableName;
      return svc.customGET(path);
    };
    svc.savePupilTable = (pt: PupilTable) => {
      let path: string = "pupiltable";
      pt.reduce();      // convert to sparse array
      return svc.customPOST(pt, path);
    };

    svc.resourceList = (schoolNo: string, year: number, category: string) => {
      let path: string = "resourcelist" + "/" + schoolNo + "/" + year + "/" + category;
      return svc.customGET(path);
    };
    svc.saveResourceList = (rl: Pineapples.Survey.ResourceList) => {
      let path: string = "resourcelist";
      //rl.reduce();      // TODO: prepare for sending to server
      return svc.customPOST(rl, path);
    };

    svc.new = () => {
      let e = new Pineapples.Schools.School({
        schNo: null,
        schClosed: 0        // TO DO this default should be at the database level
      });
      restAngular.restangularizeElement(null, e, "schools");
      var d = $q.defer();
      d.resolve(e);
      return d.promise;
    }
    return svc;
  };

  angular.module("pineapplesAPI")
    .factory("schoolsAPI", ['$q', 'Restangular', 'ErrorService', factory]);
}
