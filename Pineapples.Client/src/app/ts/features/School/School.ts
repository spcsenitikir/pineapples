﻿namespace Pineapples.Schools {

  // this needs to be maintained if more resultsets are added to the stored proc
  export class School extends Pineapples.Api.Editable implements Sw.Api.IEditable{

    // constructor just expects an object that is the school data, not the related data
    // this can be called from new school
    constructor(schoolData) {
      super();
      angular.extend(this, schoolData);
    }

    // create static method returns the object constructed from the resultset object from the server
    public static create(resultSet: any) {
      let school = new School(resultSet);

      // single row is returned as a 1 element array when we retrn DataTab
      switch (school.Surveys.length) {
        case 0:
          school.LatestSurvey = null;
          break;
        case 1:
          school.LatestSurvey = school.Surveys[0];
          break;
        default:
          // lodash '_.property' iteratee syntax
          let idx = _.findLastIndex(school.Surveys, "ssEnrol");
          if (idx === -1) {
            idx = school.Surveys.length - 1;
          }
          school.LatestSurvey = school.Surveys[idx];
          break;
      }
      return school;
    }

    // IEditable
    public _name() {
      return (<any>this).schName;
    }
    public _type() {
      return "school";
    }
    public _id() {
      return (<any>this).schNo
    }

    public Surveys: any[];
    public Exams: any[];
    public Inspections: any[];
    public QuarterlyReports: any[];
    public Documents: any[];
    public LatestSurvey: any;

    public schPhoto: string;
  }
}
