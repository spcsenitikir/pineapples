﻿namespace Pineapples.QuarterlyReports {

  interface IBindings {
    model: QuarterlyReport;
  }

  class Controller extends Sw.Component.ComponentEditController implements IBindings {
    public model: QuarterlyReport;

    static $inject = ["ApiUi", "quarterlyReportsAPI"];
    constructor(apiui: Sw.Api.IApiUi, api: any) {
      super(apiui, api);
    }

    public $onChanges(changes) {
      super.$onChanges(changes);
    }

  }

  angular
    .module("pineapples")
    .component("componentQuarterlyReport", new Sw.Component.ItemComponentOptions("quarterlyreport", Controller));
}