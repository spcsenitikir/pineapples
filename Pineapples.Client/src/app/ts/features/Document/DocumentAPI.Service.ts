﻿namespace Pineapples.Document {

  var api = function ($q: ng.IQService, restAngular: restangular.IService, errorService) {
 

    var svc = restAngular.all('documents');

    return {

      read: function (id) {
        return svc.get(id).catch(errorService.catch);
      },
      //rowSave from grid
      update: function (rowData) {
        return svc.customPUT(rowData, rowData.docID).catch(errorService.catch);
      },
      save: function (rowData) {
        return svc.post(rowData).catch(errorService.catch);
      }
    }
  };

  angular.module('pineapplesAPI')
    .factory('documentsAPI', ['$q', 'Restangular', 'ErrorService', api]);

}
