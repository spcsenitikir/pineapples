﻿namespace Pineapples.Exams {


  /**
   * the first recordset return is the validations array
   * if this is empty, then the upload will take place and the
   * pupil table is delivered in the last 4 recordsets
   * the second recordset is Warnings related to conflicts of National ID number.
   * These will not prevent the upload if present.
   * see the stored proc NdoeLoadStudents
   */
  class examProcessResponse {

    constructor(private responseData: any[]) {

    }
    public get hasValidations(): boolean {
      return (this.responseData[0].length > 0);
    }
    public get validations() {
      return (this.responseData[0]);
    }

    public get hasWarnings(): boolean {
      return (this.responseData[1].length > 0);
    }
    public get warnings() {
      return (this.responseData[1]);
    }
 

  }

  class Controller extends Pineapples.Documents.DocumentUploader {

    public doc: any;      // the document record representing the photo

    public model: any;      // teacher link, bound from the ui

    public imageHeight: number;

    public document: any;     // this is the document object
    public allowUpload: boolean

    public docPath: string;

    public processResponseData: examProcessResponse;
    public uploadResponseData: any;

    public summary: any;


    static $inject = ["identity", "documentsAPI", "FileUploader", "$mdDialog", "Hub", "$rootScope", "$http"];
    constructor(public identity: Sw.Auth.IIdentity, docApi: any, FileUploader
      , mdDialog: ng.material.IDialogService
      , public Hub: ngSignalr.HubFactory
      , public rootScope: ng.IRootScopeService
      , public http: ng.IHttpService) {
      super(identity, docApi, FileUploader, mdDialog);
      this.uploader.url = "api/exams/upload";

      this.uploader.filters.push({
        name: "openxml", fn: (file) => this.isXml(file.name),
        msg: "The Exam summary file must be an Xml document with extension .xml"
      });
    };


    protected onAfterAddingFile(fileItem: angularFileUpload.FileItem) {
      super.onAfterAddingFile(fileItem);
      // if we have started to process a new file, clear all the state
      this.uploadResponseData = null;
      this.summary = null;
    };

    protected onCompleteItem(fileItem: angularFileUpload.FileItem, response, status , headers) {
      this.uploadResponseData = response;    // note slightly different format - not an array item
      let fileId = response.id;
      // show the dislog for confirmation to continue;
      this._confirmationDialog(response).then(() => {
        // connect to signalR
        
        this.http.get("api/exams/process/" + fileId).then((processResponse) => {
          console.log(processResponse);
          this.processResponseData = new examProcessResponse(<any[]>(<any>processResponse.data).ResultSet);
          this.summary = this.processResponseData;
  
          this.uploader.queue.forEach((item) => {
            item.remove 
          });
        });
      }, () => {
        // cancel the upload
        this.http.get("api/exams/cancelprocess/" + fileId);
        this.uploadResponseData = null;
        this.summary = null;
        this.uploader.queue.forEach((item) => {
          item.remove();
        });
      });
   
    };

    private nestTheData(flatData) {
      this.summary = d3.nest()
        .key((d: any) => d.schNo)
        .key((d: any) => d.GradeLevel)
        .key((d: any) => d.Gender)
        .entries(flatData);
    }

    protected onBeforeUploadItem(item) {
      super.onBeforeUploadItem(item);
    }

    private _confirmationDialog(info) {
      let options: ng.material.IDialogOptions = {
        locals: { info: info },
        controller: confirmController,
        controllerAs: 'dvm',
        bindToController: true,
        templateUrl: "exam/uploadconfirm"

      }
      return this.mdDialog.show(options);
    }
    // life cycyle hooks
    public $onChanges(changes) {
    }

    public $onInit() {
     }
  }

  class ComponentOptions implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "exam/Upload";
    }
  }
  angular
    .module("pineapples")
    .component("examUploadComponent", new ComponentOptions());

  class confirmController extends Sw.Component.MdDialogController {
    public info: any;
  }
}