﻿// Exams Routes
namespace Pineappples.Exams {

  let routes = function ($stateProvider) {
    //var mapview = 'TeacherMapView';

    // root state for 'teachers' feature
    let state: ng.ui.IState;
    let statename: string;

    // base ndoe state
    state = {
      url: "^/exams",
      abstract: true
    };
    $stateProvider.state("site.exams", state);

    // upload state
    state = {
      url: "^/exams/upload",

      views: {
        "@": {
          component: "examUploadComponent"
        }
      },
    };
    $stateProvider.state("site.exams.upload", state);

    state = {
      url: "^/exams/reports",
      views: {
        "@": "reportPage"       // note this even more shorthand syntax for a component based view
      },
      resolve: {
        folder: () => "Exams",           // not a promise, but to get the automatic binding to the component, make a resolve for folder
        promptForParams: () => "always"
      }
    }
    $stateProvider.state("site.exams.reports", state);

    // reload state for testing
    state = {
      url: '^/exams/reload',
      onEnter: ["$state", "$templateCache", function ($state, $templateCache) {
        $templateCache.remove("exam/upload");
        $state.go("site.exams.upload");
      }]
    };
    statename = "site.exams.reload";
    $stateProvider.state(statename, state);

  }

  angular
    .module('pineapples')
    .config(['$stateProvider', routes])

}