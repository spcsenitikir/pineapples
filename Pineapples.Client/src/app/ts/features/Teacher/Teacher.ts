﻿namespace Pineapples.Teachers {

  // Not in used yet. Here as placeholder for future work
  export class Teacher extends Pineapples.Api.Editable implements Sw.Api.IEditable {

    constructor(teacherData) {
      super();
      this._transform(teacherData);
      angular.extend(this, teacherData);
    }

    // create static method returns the object constructed from the resultset object from the server
    public static create(resultSet: any) {
      let teacher = new Teacher(resultSet);
      return teacher;
    }

    // IEditable implementation
    public _name() {
      return (<any>this).tFullName;
    }
    public _type() {
      return "teacher";
    }
    public _id() {
      return (<any>this).tID
    }

    public _transform(newData) {
      // convert these incoming data values
      this._transformDates(newData, ["tDOB", "tDateRegister", "tDateRegisterEnd", "tDatePSAppointed"]);
      return super._transform(newData);
    }

    public Surveys: any[];
    public Appointments: any[];
    public Qualifications: any[];
    public Payslips: any[];
    public Documents: any[];
    public tPhoto: string;    // the current photo

    public get currentPhoto() {
      return _.find(this.Documents, { docID: this.tPhoto });
    }
  }
}