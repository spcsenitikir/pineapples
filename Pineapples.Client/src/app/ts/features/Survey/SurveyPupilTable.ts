﻿namespace Pineapples.Survey {

  export interface ILookupData {
    codeCode: string;
    codeDescription: string;
  }

  export interface IRowColData {
    row: any;
    col: any;
    M: number;
    F: number;
  }

  export class PupilTable {
    public schoolNo: string;
    public schoolName: string;
    public tableName: string;
    public year: number;
    public rows: ILookupData[];
    public cols: ILookupData[];
    public data: IRowColData[];

    constructor(base: any, rows: ILookupData[], cols: ILookupData[], data: IRowColData[]) {
      this.schoolNo = base.schoolNo;
      this.schoolName = base.schoolName;
      this.year = base.year;
      this.tableName = base.tableName;
      this.rows = rows;
      this.cols = cols;
      this.data = this._makeFat(data);
    }

    private _makeFat = (data: IRowColData[]) => {
      var f = new Array();
      for (var i = 0; i < this.rows.length; i++) {
        f[i] = [];
        for (var j = 0; j < this.cols.length; j++) {
          f[i][j] = {};
        }
      }

      data.forEach((e) => {
        var icol = _.findIndex(this.cols, { "codeCode": e.col });
        var irow = _.findIndex(this.rows, { "codeCode": e.row });
        if (icol >= 0 && irow >= 0) {
          f[irow][icol].M = e.M;
          f[irow][icol].F = e.F;
        }

      });
      return f;
    };

    public reduce = () => {
      this.data = this._makeThin(this.data);
    };

    private _makeThin = (fat) => {
      var thin = [];
      var r = 0;
      fat.forEach((row) => {
        var c = 0;
        row.forEach((col) => {
          if (col.F || col.M) {
            thin.push({ row: this.rows[r].codeCode, col: this.cols[c].codeCode, M: col.M, F: col.F });
          }
          c++;
        });
        r++;
      });
      return thin;
    };
  }

}