﻿// Students Routes
namespace Pineappples.Students {

  let routes = function ($stateProvider) {
    
    // root state for 'students' feature
    let state: ng.ui.IState;
    let statename: string;

    // base student state
    state = {
      url: "^/students",
      abstract: true
    };
    $stateProvider.state("site.students", state);

    state = {
      url: "^/students/reports",
      views: {
        "@": "reportPage"       // note this even more shorthand syntax for a component based view
      },
      resolve: {
        folder: () => "Students",           // not a promise, but to get the automatic binding to the component, make a resolve for folder
        promptForParams: () => "always"
      }
    }
    $stateProvider.state("site.students.reports", state);
       
  }

  angular
    .module('pineapples')
    .config(['$stateProvider', routes])

}