﻿namespace Pineapples { 

  class Service {
    public notify(args) {
      return new PNotifyClass(args);
    }
  }

  angular
    .module("pineapples")
    .service("pinesNotifications", Service);
}