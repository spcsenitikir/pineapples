﻿module Pineapples.Api {

  // ----------------------
  // Restangular config
  // ------------------
  // the base Url 'api' confirms to the web api convention that web api calls
  // are routed under 'api'
  // This allows web api calls to be distinguished from mvc calls

  function restangularConfig(restangularProvider: restangular.IProvider) {
    restangularProvider.setBaseUrl("api");
    restangularProvider.setDefaultHeaders({ 'Content-Type': 'application/json' });

    let interceptorPaged = (data: any, operation: string, what: string, url: string, response: restangular.IResponse, deferred) => {
      if (data.HasPageInfo) {
        let rawPagedData = new Sw.Api.RawPagedData(data as Sw.Api.IRawPagedData);
        return rawPagedData.transform();
      };
      return data;
    };

    let interceptorTagged = (data: any, operation: string, what: string, url: string, response: restangular.IResponse, deferred) => {
      switch (data.Tag) {
        case "pupiltable":
          return new Pineapples.PupilTable(data.ResultSet[0][0],
            data.ResultSet[1],
            data.ResultSet[2],
            data.ResultSet[3]
            );
        case "school":
          return Pineapples.Schools.School.create(data.ResultSet);
        case "resourcelist":
          return new Pineapples.Survey.ResourceList(data.ResultSet); // TODO: refactor like this one
        case "teacher":
          // favour direct use of constructor now over static 'create' method?
          return Pineapples.Teachers.Teacher.create(data.ResultSet);
        case "book":
          return Pineapples.Books.Book.create(data.ResultSet);
        case "quarterlyreport":
          return Pineapples.QuarterlyReports.QuarterlyReport.create(data.ResultSet);
        case "schoolaccreditation":
          return Pineapples.SchoolAccreditations.SchoolAccreditation.create(data.ResultSet);
        case "auditlog":
          return Pineapples.Survey.AuditLog.create(data.ResultSet);
        case "perfassess":
          return Pineapples.PerfAssess.PerfAssess.create(data.ResultSet);


      };
      return data;
    };

    // the save interceptor
    let interceptorBeforeSave = (element: any, operation: string, route: string, url: string) => {
      // if the object has a _beforeSave method, invoke it
      if (element && element._beforeSave)
        return element._beforeSave();

      return element;
    }
    let errorInterceptor = (response, deferred, responseHandler) => {
      switch (response.status) {
        // errors are caught lower down (http Interceptor for 401)
        // or higher up (process in ApiUi for client action)
        // possibly don;t need anything here?
      }
      return true;
    };

    let onElemR = (changedElem, isCollection, route, restangular: restangular.IService) => {
      if (Array.isArray(changedElem) && !isCollection) {
        return restangular.restangularizeCollection(null, changedElem, changedElem.route);
      }
      return changedElem;
    };
    restangularProvider.addRequestInterceptor(interceptorBeforeSave);

    restangularProvider.addResponseInterceptor(interceptorPaged);
    restangularProvider.addResponseInterceptor(interceptorTagged);
    restangularProvider.setOnElemRestangularized(onElemR);
    restangularProvider.setErrorInterceptor(<any>errorInterceptor);

    // point to the roow version field as the default etag
    restangularProvider.setRestangularFields({ etag: "_rowversion" });
  };

  angular
    .module('pineapplesAPI')
    .config(['RestangularProvider', restangularConfig]);

  // ----------------------
  // Restangular config at run - the restangular service is available
  // ------------------

  function restangularRunConfig(restangular) {

    // specify the id field for each type of entity
    // this is how restabgular knows how to construct a rest end point for reading/writing that entity
    restangular.configuration.getIdFromElem = (elem) => {
      let idField = "id";
      switch (elem.route) {
        case "schools":
          idField = "schNo";
          break;
        case "teachers":
          idField = "tID";
          break;
        case "books":
          idField = "bkCode";
          break;
        case "quarterlyreports":
          idField = "qrID";
          break;
        case "schoolaccreditations":
          idField = "saID";
          break;
        case "teacherlinks":
        case "schoollinks":
        case "documentlinks":
          idField = "lnkID";
      };
      if (elem.hasOwnProperty(idField)) {
        return elem[idField];
      };
      if (elem.hasOwnProperty("ID")) {
        // should we change this in lookup queries?
        return elem["ID"];
      }
      if (elem.hasOwnProperty("C")) {
        // used by most lookups that have a string key
        return elem["C"];
      }
      if (elem.hasOwnProperty("Code")) {
        // used by most lookups that have a string key
        return elem["Code"];
      }
      return null;
    };
  };

  angular
    .module("pineapplesAPI")
    .run(["Restangular", restangularRunConfig]);
}
