﻿module Sw {
  class LookupFilter {

    public static lookup(lookups: Sw.Lookups.LookupService) {

      let fcn = (value, lookupset, prop) => {
        // force the return of name
        return lookups.byCode(lookupset, value, prop || "N");
      };
      return fcn;
    }

    public static vocab(lookups: Sw.Lookups.LookupService) {

      let fcn = (value) => {
        // vocab will return the
        return lookups.byCode("vocab", value, "N") || value;;
      };
      return fcn;
    }
    public static sysParam(lookups: Sw.Lookups.LookupService) {

      let fcn = (value) => {
        // force the return of name
        return lookups.byCode("sysParams", value, "N");
      };
      return fcn;
    }
    public static sysParamInt(lookups: Sw.Lookups.LookupService) {

      let fcn = (value) => {
        // force the return of name
        return lookups.byCode("sysParams", value, "I");
      };
      return fcn;
    }

  }

  angular
    .module("sw.common")
    .filter("lookup", ['Lookups', LookupFilter.lookup])
    .filter("vocab", ['Lookups', LookupFilter.vocab])
    .filter("sysParam", ['Lookups', LookupFilter.sysParam])
    .filter("sysParamInt", ['Lookups', LookupFilter.sysParamInt]);
}
