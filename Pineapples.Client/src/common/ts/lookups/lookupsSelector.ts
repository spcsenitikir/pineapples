﻿/**
 * @ngdoc component
 * @name lookupSelector
 *
 * @description
 * Provides a select input that is bound to a lookup list sourced from the Lookups service cache.
 * May use these attributes:
 * lkp: Required. The name of the lookup in the Lookups service cache.
 *
 * bindTo: optional; the property of the lookup entry to use as the option value. May take these values:
 * * C, code - bind to C (the code)
 * * N, name - bind to N ( the name)
 * * row - bind to the lookup entry object
 * * ID - bind to ID (present in some lookups)
 * If not suppplied, default is C
 *
 * displayAs: optional the property of the lookup entry to use as the option label. May take these values:
 * * C, code - bind to C (the code)
 * * N, name - bind to N ( the name)
 * * (other) - bind to the named property. Allows for lookups that have aditional non-standard properties
 * *
 * If not suppplied, default is N
 *
 * groupBy: optional; the property of the lookup entry to use as group by in the option list.
 * If not suppplied, no group is applied
 *
 * ngRequire: optional; but if used should take the form ng-required="true" while ng-required="false"
 * or completely omitting it will not be require
 *
 * Example:
 * <lookup-selector ng-model="vm.district" bind-to="C" display-as="name"></lookup-selector>
 */
module Sw {

  interface IBindings {
    ngModel: string;
    lkp: string;
    ngDisabled: boolean;
    ngRequired: boolean;
    bindTo: string;
    displayAs: string;
  }
  class Controller implements IBindings {
    public ngModel: any;
    public lkp: string;
    public ngDisabled: boolean;
    public ngRequired: boolean;
    public bindTo: string;
    public displayAs: string;

    static $inject = ["Lookups"];
    constructor(public lookups: Sw.Lookups.LookupService) {
      this.bindTo = "C";      // bind to the code
      this.displayAs = "N";
    }

    public static ngOptions(attrs) {

      // bindTo can be C ( default), N, Id or row
      let selectAs: string = "r.C";
      let displayAs: string = "r.N";

      if (attrs.bindTo) {

        switch (attrs.bindTo.toLowerCase()) {
          case "c":
          case "code":
            selectAs = "r.C";
            break;
          case "n":
          case "name":
            selectAs = "r.N";
            break;
          case "row":
            selectAs = "r";
            break;
          case "id":
            selectAs = "r.ID";
            break;
        }
      }
      if (attrs.displayAs) {
        switch (attrs.displayAs.toLowerCase()) {
          case "c":
          case "code":
            displayAs = "r.C";
            break;
          case "n":
          case "name":
            displayAs = "r.N";
            break;
          case "row":
            displayAs = "r";
            break;
          case "id":
            displayAs = "r.ID";
            break;
          default:
            displayAs = "r." + attrs.displayAs;     // allow a (case-sensitive) free-form to reference any custom property
        }
      }
      if (attrs.groupBy) {
        return selectAs + " as " + displayAs + " group by r." + attrs.groupBy + " for r in vm.lookups.cache[vm.lkp]";
      } else {
        return selectAs + " as " + displayAs + " for r in vm.lookups.cache[vm.lkp]";
      }
    }
  }

  class LookupsSelector implements ng.IComponentOptions {
    public bindings: any = {
      ngModel: '=',   // this has to be two-way
      lkp: "@",
      ngDisabled: "<",
      ngRequired: "<",
      bindTo: "@",
      displayAs: "@"
    };
    public controller: any = Controller;
    public controllerAs: string = "vm";

    // angular doesn;t watch ngOptions - so we have to make sure the right string is in the template when
    // it is first rendered
    public template(element, attrs) {
      let template =
        `<select class="form-control" id="{{vm.lkp}}"
                          ng-model="vm.ngModel"
                          ng-disabled="vm.ngDisabled"
                          ng-options="###"
                          ng-required="vm.ngRequired"
                          to-number>
                          <option value=""></option>
                      </select>`;
      // support the to-number directive to enforce a numeric value
      // this has to be declared as an attr on the selector
      if (attrs["number"] !== "true") {
        template = template.replace("to-number", "");
      };
      return template.replace("###", this.controller.ngOptions(attrs));
    };

    constructor() {
      this.template.$inject = ["$element", "$attrs"];
    }
  }

  angular
    .module("sw.common")
    .component("lookupSelector", new LookupsSelector());
}
