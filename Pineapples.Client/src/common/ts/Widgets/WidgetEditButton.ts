﻿namespace sw {

  interface IBindings {
    action;
    isWaiting: boolean;
    form;
    showLabel: boolean;
  }

  class controller implements IBindings{
    public action;
    public isWaiting: boolean = false;
    public form;
    public showLabel: boolean = true;
  }
  class ButtonComponentOptions implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public template: string;

    constructor() {
      this.bindings = {
        action: "&",
        isWaiting: "<",
        form: "=",
        showLabel: "<"
      }
      this.controller = controller;
      this.controllerAs = "vm";
    }
  }

  class editButtonOptions extends ButtonComponentOptions implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public template: string;

    constructor() {
      super();

      this.template =
        `<button class="btn btn-midnightblue btn-sm btn-panel" ng-click="vm.action()">
          <span uib-tooltip="Edit"><i class="fa fa-pencil-square-o" /> <span ng-show="vm.showLabel">Edit</span>
          </span>
        </button>`;
    }
  }

  class saveButtonOptions extends ButtonComponentOptions implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public template: string;

    constructor() {
      super();
      this.template =
        `<button class="btn btn-midnightblue btn-sm btn-panel" 
            ng-class="vm.form.$invalid ? 'invalid' : 'valid'"  ng-disabled="vm.isWaiting || vm.form.$invalid"
            ng-click="vm.action()">
            <span ng-show="vm.isWaiting"><i class="fa fa-spinner spinning" /> Saving...</span>
            <span ng-show="!vm.isWaiting"><i class="fa fa-save" /> Save</span>
         </button>`;
    }
  }

  class undoButtonOptions extends ButtonComponentOptions implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public template: string;

    constructor() {
      super();
      this.template =
        `<button class="btn btn-midnightblue btn-sm btn-panel" ng-disabled="vm.isWaiting" ng-click="vm.action()">
              <span><i class="fa fa-undo" /> Undo changes</span>
        </button>`;
    }
  }

  class editUiButtonOptions extends ButtonComponentOptions implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public template: string;

    constructor() {
      super();
      this.bindings = {
        editAction: "&",
        saveAction: "&",
        undoAction: "&",
        isWaiting: "<",
        isEditing: "<",
        form: "<",
        showLabels: "@"

      }
      this.template =
        `<div>
          <edit-button action="vm.editAction()" ng-show="!vm.isEditing" action=vm.editAction() show-label="vm.showLabels"></edit-button>
          <save-button action="vm.saveAction()" ng-show="vm.isEditing" is-waiting="vm.isWaiting" action=vm.saveAction() form="vm.form"></save-button>
          <undo-button action="vm.undoAction()" ng-show="vm.isEditing" action=vm.undoAction()></save-button>
        </div>
          `;
      this.controller = () => {
        return {};
      };
      this.controllerAs = "vm";
    }
  }

  angular
    .module("sw.common")
    .component("editButton", new editButtonOptions())
    .component("saveButton", new saveButtonOptions())
    .component("undoButton", new undoButtonOptions())
    .component("editUiButtons", new editUiButtonOptions());

}
