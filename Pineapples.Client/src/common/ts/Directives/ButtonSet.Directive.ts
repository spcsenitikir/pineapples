﻿// Button Set
// creates a set of buttons with labels from an array, which are highlighted when their assigned value matches an input from a finite set
// e.g. an 'enum'
// example ['Excellent','Very Good','Good','Poor','Very Poor'], [5,4,3,2,1]

module Sw {

  interface IIsolateScope extends ng.IScope {
    value: any;
  }

  class SwButtonSet implements ng.IDirective {
    public restrict = "EA";
    public require = "ngModel";
    public scope = {
      btnWidth: '@',
      btnNames: '=',
      btnValues: "=",
      btnTooltips: '=',
      btnSelectedClass: '@',
      btnBaseClass: '@'
    };
    public template = '<div class="btn-group"><label class="btn btn-primary" ng-style="{width: btnWidth }" ' +
    'ng-model="value.value" ng-class="btnValues[$index]===value.value? btnSelectedClass : btnBaseClass" ' +
    'ng-repeat="n in btnNames" ' +
    'uib-btn-radio="btnValues[$index]" ' +
    'uib-tooltip="{{btnTooltips[$index]}}" tooltip-placement="bottom" uncheckable> {{n}} </label></div> ';
    public link: any;

    constructor() {
      SwButtonSet.prototype.link = (scope: IIsolateScope, element, attrs, ngModelCtrlr) => {

        scope.value = { value: null };
        scope.$watch("value.value", (newValue) => {
          ngModelCtrlr.$setViewValue(newValue);
        });
        ngModelCtrlr.$render = () => {
          scope.value.value = ngModelCtrlr.$viewValue;
        };
      };
    }

    public static factory() {
      let directive = () => {
        return new SwButtonSet();
      };
      return directive;
    }

  }
  angular
    .module('sw.common')
    .directive('swButtonSet', SwButtonSet.factory());

}
