﻿module Sw {

  /**
   * API Controller
   *
   * very thin controller to allow access to an API
   * effectively this us a ServiceFacadeController
   */
  class ApiController {

    public api: any;
    public lookups: any;
    public state: any;
    public stateParams: any;

    static $inject = ['$state', '$stateParams', 'theAPI', 'Lookups'];

    constructor($state, $stateParams, api, lookups) {
      this.api = api;
      this.lookups = lookups;
      this.state = $state.current;
      this.stateParams = $stateParams;
    }
  }

  angular
    .module("sw.common")
    .controller("APIController", ApiController);
}
