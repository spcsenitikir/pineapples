﻿module Sw.Auth {

  export interface IAuthenticationDialogs {
    showLogin(): void;
    showNoConnection(): ng.IPromise<any>;
    cancelDialogs(): void;
  }


  class AuthenticationDialogs {
    static $inject = ["$mdDialog", "authenticationService", "$state"];

    constructor(private _mdDialog: ng.material.IDialogService, private _authService: IAuthenticationService,
      private _state: angular.ui.IStateService) { }

    private _getIEVersion() {
      let match = navigator.userAgent.match(/(?:MSIE |Trident\/.*; rv:)(\d+)/);
      return match ? parseInt(match[1]) : undefined;
    }

    public showLogin() {
      let dlg: ng.material.IDialogOptions = {
        // Currently making use of same login markup as signin for consistency
        // IDialogOptions' defaults are use
        template:
        '<div class="panel panel-primary">' +
        '    <div class="panel-body">' +
        '        <img src="asset/logo" width="150" />' +
        '        <form class="form-horizontal" name="form" ng-submit="vm.logIn(email, password, rememberMe)" role="form">' +
        '            <div class="form-group">' +
        '                <label for="email" class="control-label col-sm-4" style="text-align: left;">Account Name</label>' +
        '                <div class="col-sm-8">' +
        '                    <input type="text" class="form-control" id="email" placeholder="Account Name" ng-model="email" required>' +
        '                </div>' +
        '            </div>' +
        '            <div class="form-group">' +
        '                <label for="password" class="control-label col-sm-4" style="text-align: left;">Password</label>' +
        '                <div class="col-sm-8">' +
        '                    <input type="password" class="form-control" id="password" placeholder="Password" ng-model="password" required>' +
        '                </div>' +
        '            </div>' +
        '            <div class="clearfix">' +
        '                <div class="pull-right"><label><input type="checkbox" ng-model="rememberMe"> Stay logged in</label></div>' +
        '            </div>' +
        '            <div class="form-actions">' +
        '                <i class="icon-spinner icon-spin icon-large"></i>' +
        '                <button type="submit" class="btn btn-primary btn-block" ng-disabled="form.$invalid || loginInProcess">' +
        '                    {{loginState}} <i class="fa fa-spinner spinning" ng-show="loginInProcess" aria-hidden="true"></i>' +
        '                </button>' +
        '            </div>' +
        '        </form>' +
        '    </div>' +
        '    <div class="panel-footer">' +
        '        <div class="col-md-6">' +
        '            <a ui-sref="forgotpassword">Forgot password?</a>' +
        '        </div>' +
        '        <div class="col-md-6">' +
        '            <div class="text-danger">{{messageText}}</div>' +
        '        </div>' +
        '    </div>' +
        '</div>',
        controller: 'LoginPopupController',
        controllerAs: 'vm'
      };
      // if the mdDialog can't be displayed ( ie9?) then let's fall back to
      if (this._getIEVersion() < 10) {
        // just reroute to signon
        this._state.go("signin");
      } else {
        this._mdDialog.show(dlg);
      }

    }
    // this dialog is displayed if there is a 502 or -1 response from http
    public showNoConnection():ng.IPromise<any> {
      let dlg: ng.material.IDialogOptions = {
        template: `
          <div class="panel panel-primary">
              <div class="panel-body">
		          <div>No response from the server. Check your internet and VPN connection</div>
              <div><button ng-click="vm.closeDialog()">Retry</button></div>
              </div>  
          </div>`,

        controller: Sw.Component.MdDialogController,
        controllerAs: 'vm'
      }
      if (this._getIEVersion() < 10) {
        // just reroute to signon
        this._state.go("signin");
      } else {
        return this._mdDialog.show(dlg);
      }
    }
    public cancelDialogs() {
      this._mdDialog.cancel();
    }
  }
  angular
    .module("sw.common")
    .service("authenticationDialogs", AuthenticationDialogs);
}
