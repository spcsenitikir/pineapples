﻿angular.module('sw.directives', []);

(function () {
    var fitcontent = function ($window, $timeout, $location) {
        return {
            restrict: 'A',

            link: function (scope, element, attr) {

                var setHeight = function () {
                    var diff = $('footer').height() + element.offset().top;

                    var newHeight = ($(window).height() - diff);

                    if (newHeight > 0) {
                        element.css('height', newHeight + 'px');
                    };

                };

                $(window).on('resize', function () {
                    setHeight();
                });
                // also check on a view change, becuase panels may have come in or out
                scope.$on('$viewContentLoaded', function () {
                    $timeout(setHeight,100);
                });

                setHeight();
                //$timeout(, 1000);
            }
        };
    };
    angular
            .module('sw.common')
            .directive('fitContent', ['$window', '$timeout', '$location', fitcontent]);
})();

(function () {
    // jQuery needed, uses Bootstrap classes, adjust the path of templateUrl
    var dir = function () {
        return {
            restrict: 'E',
            //templateUrl: '/path/to/pdfDownload.tpl.html',
            template: '<a href="" class="btn btn-primary" ng-click="downloadPdf()">Download</a>',
            scope: true,
            link: function (scope, element, attr) {
                var anchor = element.children()[0];

                // When the download starts, disable the link
                scope.$on('download-start', function () {
                    $(anchor).attr('disabled', 'disabled');
                });

                // When the download finishes, attach the data to the link. Enable the link and change its appearance.
                scope.$on('downloaded', function (event, data) {
                    $(anchor).attr({
                        href: 'data:application/pdf;base64,' + data,
                        download: attr.filename
                    })
                        .removeAttr('disabled')
                        .text('Save')
                        .removeClass('btn-primary')
                        .addClass('btn-success');

                    // Also overwrite the download pdf function to do nothing.
                    scope.downloadPdf = function () {
                    };
                });
            },
            controller: ['$scope', '$attrs', '$http', function ($scope, $attrs, $http) {
                $scope.downloadPdf = function () {
                    $scope.$emit('download-start');
                    $http.get($attrs.url).then(function (response) {
                        $scope.$emit('downloaded', response.data);
                    });
                };
            }]
        };
    };
    angular
        .module('sw.common')
        .directive('pdfDownload', [dir]);

})();
//http://stackoverflow.com/questions/28114970/angularjs-ng-options-using-number-for-model-does-not-select-initial-value
(function() {
angular
    .module('sw.common')
    .directive('toNumber', function () {
    return {
      require: 'ngModel',
      link: function (scope, element, attrs, ngModel) {
        ngModel.$parsers.push(function (val) {
          return val ? parseInt(val, 10) : null;
        });
        ngModel.$formatters.push(function (val) {
          return val ? '' + val : null;
        });
      }
    };
    });
})();