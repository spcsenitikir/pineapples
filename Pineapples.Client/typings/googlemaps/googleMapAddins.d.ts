﻿
declare module google.maps.ext {
  export type SpidifierOptions = {
    markersWontMove: boolean,
    markersWontHide: boolean,
    keepSpiderfied: boolean
  }

  export type MarkerClustererOptions = {
    gridSize: number,
    maxZoom: number,
    zoomOnClick: boolean
  }

  export type NSEW = {
    n: number,
    s: number,
    e: number,
    w: number
  }

  export class RichMarker extends google.maps.Marker {
    public row: any;
    public title: string;
  }
}
declare class MarkerClusterer {
  constructor(map: google.maps.Map, markers: google.maps.Marker[], mcOptions: google.maps.ext.MarkerClustererOptions);
  clearMarkers(): void;
}
declare class OverlappingMarkerSpiderfier {
  constructor(map: google.maps.Map, options: google.maps.ext.SpidifierOptions);
  addListener(event: string, handler: (marks: google.maps.ext.RichMarker[]) => void);
  clearMarkers(): void;
  addMarker(mark:google.maps.Marker)
}