.. Pacific EMIS Developer Guide documentation master file, created by
   sphinx-quickstart on Wed May 04 10:00:15 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Pacific EMIS Developer Guide
============================

.. toctree::
   :maxdepth: 2

.. _intro:

Introduction
============

The Pacific Education Management Information System (Pacific EMIS) is
an open source application developed to improve the education system
in Pacific nations. It is currently being developed and supported by
the Secretariat of the Pacific Community (SPC) with support from the
Asia Development Bank (ADB). This guide is for any developers tasked
with contributing to this project in one way or another. This guide
documents high level development architectural design, development
environment, development workflow, policies and procedures without
going into lower level details of the source code and APIs which is
documented currently directly within the source code itself.

Project Overview
----------------

The Pacific EMIS is a single page web application built with AngularJS
and ASP.NET technologies. While it is important to know that this web
application is complementary to a desktop application in some
countries, built on the same database, that was first started
in 2002. So there is a lot of functionality in there that hasn't been
ported to the web version; we've concentrated so far on the features
of most use to a broader user base. For example, lookup table
maintenance is done (when needed) through the desktop because there
hasn't been a lot to be gained by porting that. This developer guide
is mostly gear towards the Web application only (which again, make use
of the same database as the legacy desktop application). The web
application project moving forward is likely to eventually obsolete
the legacy MS Access desktop application completely.

Project Structure
-----------------

The Pineapples solution (in Visual Studio parlance) is managed in a
single git repository. The solution is organised into 3 main projects
(which also translates into three folders in the root of the
repository):

* **Pineapples** ASP.NET project: This is the startup executable. It is
  responsible for delivering all objects to the browser, including the
  initial page, all required scripts, stylesheets, and other
  assets. Pineapples implements both WEB API 2 and MVC interfaces. The
  Web API 2 interface delivers data in JSON format to the client on
  request. The MVC interface delivers html to the client including the
  initial page and views inserted into that page.

* **Pineapples.Data** .NET class library: This project exposes "business
  objects" to Pineapples, and is responsible for reading and writing
  data to the SQL server database. This project also includes any
  required JSON serializers used by Web API 2 to construct and tear
  down objects received / sent over the REST interface.

* **Pineapples.Client** .NET Web Site. The client front-end
  (i.e. javascript and css files executed in the browser). As a web
  site, this project has no project file, and Visual Studio build is
  turned off. The project is managed using a "web stack" composed of
  npm, bower and grunt.  Other Projects

Two other projects complete the solution:

* **Pineappples.Reports**: This is an SSRS project, home to report
  definitions and related SSRS objects. These are deployed into the
  SSRS server.
* **Pineapples.Data.Tests**: A VS2015 Test project allowing tests of the
  data layer.

Other folders found in the root of the git repository are:

* **assemblies**: This is home to any additional assemblies that are not
  part of the project, not part of .NET, and not served by
  nuget. Placing these in a common location relative to the solution
  projects makes managing references easier. The assemblies folder
  sits besides the packages folder created by nuget.
* **database**: This folder holds a SQLDelta schema file representing
  the latest schema of the Pineapples database. SQL Delta can compare
  this snapshot against a database and generate a script to resolve
  the differences.
* **docs**: Holds all the documentation for this project including a
  generic user guide, systems administrator guide and this developer
  guide. Instructions for maintaining the documentation is including
  further in this guide.

.. _prog-lang:

Programming Language
--------------------

The programming language on the backend (i.e. Pineapples,
Pineapples.Data, Pineapples.Data.Test) is C# using ASP.NET4.5
framework. The frontend (i.e. Pineapples.Client) is made up of HTML,
CSS, Javascript and TypeScript with the Angular 1 framework. All
Javascript code is being migrated slowly to TypeScript as changes are
done to it or new code is written.

Why not ASP.NET5 and Angular 2? The short answer, those two projects
were not in stable release when this project started. Moving to any of
those is not an upgrade but potentially a significant rewrite. New
code is written in as to ease the transition down the line. This will
be reviewed in mid 2017.

Things to consider regarding ASP.NET4.5 to ASP.NET5 migration path.

+------------+---------------------------+------------+
|**Issue**   |**Pineapples (ASP.NET4.5)**|**ASP.NET5**|
|            |                           |            |
+============+===========================+============+
|Separation  |Separation into 3          |Adds wwwroot|
|of          |projects. Pineapples.Client|and         |
|client-side |Web site is all the client |dependencies|
|SPA from    |material and npm/bower     |folders into|
|server      |depencies. Managed entirely|project for |
|logic.      |by grunt.                  |this        |
|            |                           |purpose.    |
+------------+---------------------------+------------+
|Data Access |Currently ADO.NET Entity   |New EF7 in  |
|            |Framework. EF6 was found to|this release|
|            |be too "heavy" and         |is a        |
|            |restrictive at the time.   |substantial |
|            |                           |rewrite     |
|            |                           |aimed at    |
|            |                           |addressing  |
|            |                           |some of the |
|            |                           |current     |
|            |                           |concerns.   |
+------------+---------------------------+------------+
|Dependency  |Unity framework has adaptor|Dependency  |
|Injection   |for Web Api. These are used|injection   |
|            |in Pineapples.             |bult in to  |
|            |                           |ASP.NET5    |
+------------+---------------------------+------------+
|Confusion   |Separated into two folders:|Merges Web  |
|between Web |Controllers and            |API into    |
|API         |Controllers_Api. Use       |MVC. Only   |
|controllers |specific base custom       |one type of |
|and MVC     |classes.                   |controller. |
|controllers.|                           |            |
+------------+---------------------------+------------+

Similar considerations around Angular 1 vs 2. This is a rewrite, but
tools are being introduced into 1 (see Angular 1.5) to ease this
path. Adhering to best practices will make the transition smoother
down the track:

* Angular 2 is entirely written in Typescript, so should new
  Pineapples code.
* Try not to use scope (store variables on controller not on
  scope). Refer to source code for example of how this works.
* Move to Angular 1.5 components where possible rather than lower
  level directives.


Development Web Stack
---------------------

No web application is built without a web framework (or library). As
was mentioned previously the frameworks used are ASP.NET4.5 and
Angular 1. In addition to the frameworks used for the development of
the application itself a modern web stack is also used to manage
various aspects of the frontend development (i.e. Pineapples.Client)
such as running tests and building the frontend part of the
application:

* `NodeJS <https://nodejs.org/en/>`_ is the platform used to run
  Javascript on the server. It is necessary to run the other tools
  (i.e. npm, grunt, bower). This is not part of the application once
  deployed but used on the developer's workstation only.

* `NPM <https://www.npmjs.com/>`_ will be installed with NodeJS
  above. It is the package manager and is used mainly to install grunt
  plugins and other tools used by the frontend such as testing
  framework. You can see the currently used npm packages in
  ``packages.json`` which you can modify to add new grunt tasks. Or
  use the command line ``> npm install grunt-plugin``. Those packages
  are not part of the actual application in deployment but are used
  mainly on the developer's machine in
  ``Pineapples.Client\node_modules\``.

* `Bower <http://bower.io/>`_ is must be installed globally using
  ``npm``. From the command line you can simply ``> npm insall -g
  bower``. All dependencies on 3rd party components *used in the
  browser* are managed using bower. Those components that do not
  support bower, are still added into ``bower.json`` with a reference
  to their source URL. The ``bower_components`` folder is not in
  source control, since its contents are entirely determined by
  ``bower.json``. Modify ``bower.json`` to add new 3rd party libraries
  to the project or use the command line from within the
  ``Pineapples.Client`` folder (e.g. ``> bower install --save-dev
  angular-cool-module``).

* `GruntJS <http://gruntjs.com/>`_ is used to automate various
  frontend task such as testing, linting, building, etc. The
  ``gruntfile.js`` contains all the configuration for the project
  management tasks. These are accessed in VS2015 from the Task Runner
  Explorer window. The two most important tasks are "build" and
  "deploy" or "_build_deploy" to run one after the other. Careful
  review of the Gruntfile.js must be done to understand what it does
  as this will be constantly improving. Essentially, it gathers all
  files that will go into the browser (i.e. Javascript, CSS, images,
  fonts) and prepares them for being served by the ASP.NET
  application. It puts most of them in a temporary
  ``Pineapples.Client\dist\`` folder that is cleaned and recreated on
  each build.

Database and Operating System
-----------------------------

Only Microsoft SQL Server is supported. However, the application only
requires the freely available `MS SQL Server Express
<http://www.microsoft.com/en-us/server-cloud/products/sql-server-editions/sql-server-express.aspx>`_. This
will likely not change as the application makes heavy use of SQL
Procedures which are not easily portable to other database engines.

Currently the application is developed on windows and must be deployed
on Windows; any recent version of windows, preferably though not
necessarily Windows server. However, there is at least one of the
developers with a strong preference for Linux and a desire to provide
choice and there may very well be work done to get this working on
Linux too.

.. _dev-env:

Setting Up the Development Environment
======================================

The easiest way to get a development environment and start developing
on the Pacific EMIS is on Windows though one of the developers won't
be entirely happy until he can compile the whole project on Linux and
develop using the .NET Execution Environment (DNX) and .NET Core on
for Linux and using the Bash shell and Emacs text editor! Linux fans,
stay tune :) But for now, instructions below are exclusively for
Windows.

Main Development Tools
----------------------

Most developers currently use Visual Studio, ASP.NET4.5 (comes with
Visual Studio), MS SQL Server Express and IIS. Here are the steps
assuming you have a working Windows operating system (version 7, 8, or
10):

1. Download and install latest `Visual Studio 2015
   <https://www.visualstudio.com/en-us/products/visual-studio-community-vs.aspx>`_.
2. Download and install `MS SQL Server Express
   <http://www.microsoft.com/en-us/server-cloud/products/sql-server-editions/sql-server-express.aspx>`_.
3. Enable IIS
4. Download and install `NodeJS <https://nodejs.org/en/>`_. While
   Visual Studio 2015 comes with its own NodeJS it can and should be
   installed on the system by you as well.

Software Configuration Management
---------------------------------

All software is managed through Git (Version Control Management) and
currently Bitbucket (Issue tracking, collaboration, etc.) in a
privately accessible repository. Its location is currently at
`https://bitbucket.org/softwords/pineapples
<https://bitbucket.org/softwords/pineapples>`_ but it will likely
eventually change to a repository of the Secretariat of the Pacific
Community (SPC) and be made public and open source.

Until then you must be given at least read access to retrieve your own
full local clone of the project with Git installed on your
machine. Git can be used with the command line or using a more user
friendly tool such as `SourceTree
<https://www.atlassian.com/software/sourcetree>`_.

.. warning::

   It is preferable to create the pineapples clone into the C:\\ folder
   directly due possible too deep folder structure.

**In SourceTree**

Create the folder ``C:\pineapples`` and follow `this
<https://confluence.atlassian.com/bitbucket/create-and-clone-a-repository-800695642.html#Createandclonearepository-CloningarepositorywithSourceTree>`_
guide to use SourceTree to clone your local copy into ``C:\pineapples``.

**Git command line**

.. code-block:: console
                
   > cd C:\\
   > git clone https://ghachey@bitbucket.org/softwords/pineapples.git

Visual Studio Setup
-------------------

The Visual Studio (VS) solution file is not part of the git
repository, so you'll need to create a new empty solution called
``pineapples``, and add the 3 main pineapples projects to it. Save it
into your pineapples folder. Refer to Create Empty Solution `here
<https://msdn.microsoft.com/en-us/library/zfzh36t7.aspx>`__ if you are
new to VS.

Databases Setup
---------------

You will need to setup two databases: the application's main database
(e.g. MIEMIS, SIEMIS, SIEMIS-TEST, KIEMIS) and the Identify database
(i.e. IdentitiesP). Those should be given to you by one of the
developers with appropriate knowledge and authority until a sample
database and created and available for use by all stored in database
folder.

Restoring a database from a backup is documented `here
<https://msdn.microsoft.com/en-us/library/ms177429.aspx>`__. Take note
of the names of your new databases and also the name of the server
(e.g. LOCALHOST, HOSTNAME\SQLEXPRESS) you will need those next.

In VS, open the file ``Web.config`` from the ``Pineaples``
project. First configure the connection to the IdentitiesP
database. Locate the line.

.. code-block:: xml
   :linenos:
   
   <add name="DefaultConnection" connectionString="Data Source=localhost;Initial Catalog=IdentitiesP;...

And replace ``localhost`` with your server name. And the
``IdentifiesP`` database also is communicated through the Entity
Framework so you will need to alocate the following lines at the
bottom.

.. code-block:: xml
   :linenos:
  
   <entityFramework>

     <defaultConnectionFactory type="System.Data.Entity.Infrastructure.SqlConnectionFactory, EntityFramework">
       <parameters>

         <parameter value="Data Source=keenyah;Initial Catalog=IdentitiesP; Integrated Security=True; MultipleActiveResultSets=True" />
       </parameters>
     </defaultConnectionFactory>
     <providers>
       <provider invariantName="System.Data.SqlClient" type="System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer" />
     </providers>
   </entityFramework>

And change the DataSource ``keenyah`` to your server name. Finally,
configure the connection to your application's database (SIEMIS,
KIEMIS, etc.). Locate the lines below

.. code-block:: xml
   :linenos:

   <appSettings>
     <add key="server" value="localhost" />
     <add key="database" value="kiemis" />
     <add key="useHttps" value="false" />
     <add key="ReportServerUrl" value="http://localhost:8080/reportserver" />
   </appSettings>

And replace ``localhost`` with your own server name and ``kiemis`` with the
name of your database.

Building the Solution
---------------------

This will require the downloading of all the nuget, npm and bower
dependencies.

TODO provide better steps.

From VS menu click on **Tools--NuGetPackage Manager--NugetConsole**
then run **Update-Package** from the NugetConsole. This will ensure
you have the required versions of the Nuget packages.

Next, start with **Pineapples.Data** project. Ensure that the
references to **Softwords.Datatools** and **Softwords.Web** are
present and point to the assemblies folder:

.. image:: images/references.png
   :align: center

Ensure all references are present. Build **Pineapples.Data**, confirm
it Succeeds. Move to **Pineapples** project. Check the references as
above. Also check there is a reference to **Pineapples.Data**, and it
points to the current project in the solution:

.. image:: images/references2.png
   :align: center

Check in particular that reference to **Microsoft.Practices.Unity**
are resolved. These come from Nuget and should be available. Build the
project **Pineapples**, then Build Solution. This should have the
effect of running the gruntfile **_build_deploy** task, which is
linked to the build of **Pineapples.Client** (which otherwise there is
no need to build). You can of course run the gruntfile
**_build_deploy** task on its own, for instance if you develop only on
the client side (i.e. CSS, Javascript, TypeScript).
           
Still getting problems? First thing to do if you run into any errors
is to double check you have done all the steps in here correctly. Then
search the issues database on the online repository as there is a
chance the problem is a known one with a documented solution.

Frontend Development Tools
--------------------------

The application is tested using the Chrome web browser which has good
developer tools out of the box. For more advanced debugging of the
frontend the developers mostly use `Fiddler
<http://www.telerik.com/fiddler>`_

TODO instruction on basic setup of Fiddler.

.. _dev-flow:

Development Work-flow
=====================

The Pacific EMIS project constantly strives to improve its development
operations in order to produce software of higher quality at a more
efficient rate. This part of the developer guide will undoubtebly
change and should be kept close at hand when developing on this
project.

Contribute New Code in Branches Uninterrupted
---------------------------------------------

New development work on a software project is either of maintenance
(fixing bugs, addressing security issues) or construction nature
(adding new features). Regardless of the type of work, all new work
should first be documented through an "Issue" on bitbucket where it
can be discussed by the team first if needed, a new branch **must** be
created based on the branch ``origin/develop`` to do the work and
should take the name of its respective issue (e.g. issue81).

For example, let's say we're tackling issue #3 from the issue tracking
system you should create a branch called ``issue3`` locally on your
machine branches from ``origin/develop``. If you are new to git's
branching and mergin start `here
<http://www.git-scm.com/book/en/Git-Branching-Basic-Branching-and-Merging>`__
like this [PRO-GIT]_.

All branches for new features, bug fixes, maintenance work, etc. are
then merged into ``origin/develop`` which in turn gets rolled into
``origin/master`` for release cycles.

**In SourceTree**

See `SourceTree Create Branch <https://confluence.atlassian.com/sourcetreekb/branch-management-785325799.html#BranchManagement-CreateBranch>`__.

**Git command line**

Branch like this

.. code-block:: console

   > git checkout -b issue3

Work on the issue, add relevant tests so it does not occur again
(proper test will be mandatory in the future for new features or bug
fixing), all the while only committing locally on your branch. After
all this is done you can go ahead with publishing your new fix
following our defined standard procedure.

It is desirable to keep the history of the stable branch ``develop``
as clean and linear as possible for more effective code review and
automatic generation of CHANGELOG.md. There are a few ways to achieve
this with Git, but here the process is established to put the burden
on the developer of a new branch. More specifically the developer of
``issue3`` must squash all local commits from ``issue3`` branch into a
single properly formatted commit before publishing changes and doing a
pull request to ``develop``. If you are new to squashing commits start
`here
<http://www.git-scm.com/book/en/Git-Tools-Rewriting-History#Squashing-Commits>`__
[PRO-GIT]_.

Before going through the steps for doing this a word on the standard
format for the commits. The consolidated commit message must follow
the following conventions adapted from `Google project AngularJS
<https://docs.google.com/document/d/1QrDFcIiPjSLDn3EL15IJygNPiHORgU1_OOAqWjiDU5Y/edit#>`_
which will greatly enhanced the historical information on develop and
allow for automatic generation of the changelog. The format of the
commit message must follow the following convention.

.. highlight:: none

::
   
   <type>(<scope>): <subject>
   <BLANK LINE>
   <body>
   <BLANK LINE>
   <footer>

Any line of the commit message must not be longer than 100
characters. This allows the message to be easier to read on github as
well as in various git tools.

**<type>**

Should be either of the following:

* feat (when working on new feature)
* fix (when fixing a bug or addressing a security vulnerability)
* docs (when working on documentation)
* style (improving formatting, missing semi colons, indentation, etc.)
* refactor (when doing minor or major refactoring work)
* test (when adding missing tests)
* chore (maintain)

**<scope>**

Should specify the location of the commit as succinctly and completely
as possible (e.g. $location, $rootScope, ngHref, ngClick, ngView)

**<subject>**

Subject line contains succinct description of the change. Remember it
must not be longer than 100 characters and this *includes* both the
<type>(<scope>) identified before. Here are some convensions:

* use imperative, present tense: "change" not "changed" nor "changes"
* don't capitalize first letter
* no period full stop (.) at the end

**<body>**

[Optional] Slightly more elaborated description possibly spanning over
several lines never more than 100 characters each.

* just as in <subject> use imperative, present tense
* includes motivation for the change and contrasts with previous behavior

**<footer>**:

[Optional] should include either breaking changes and/or references of
what issues were resolved if any. All breaking changes have to be
mentioned in footer with the description of the change, justification
and migration notes.

The following includes several examples of properly formatted squashed
commit messages.

A new feature commit.

.. highlight:: none

::

   feat($browser): onUrlChange event (popstate/hashchange/polling)

   Added new event to $browser:
   - forward popstate event if available
   - forward hashchange event if popstate not available
   - do polling when neither popstate nor hashchange available

   Breaks $browser.onHashChange, which was removed (use onUrlChange instead)

A fix for browser compatibility commit.

.. highlight:: none
               
::

   fix($compile): couple of unit tests for IE9

   Older IEs serialize html uppercased, but IE9 does not...
   Would be better to expect case insensitive, unfortunately jasmine does
   not allow to user regexps for throw expectations.

   Closes #392
   Breaks foo.bar api, foo.baz should be used instead

A new feature request from issue #351 commit.

.. highlight:: none
               
::

   feat(directive): ng:disabled, ng:checked, ng:multiple, ng:readonly, ng:selected

   New directives for proper binding these attributes in older browsers (IE).
   Added coresponding description, live examples and e2e tests.

   Closes #351, #456

Some cleanup commit.

.. highlight:: none
               
::

   style($location): add couple of missing semi colons

Some documentation work commit.

.. highlight:: none
               
::

   docs(guide): updated fixed docs from Google Docs

   Couple of typos fixed:
   - indentation
   - batchLogbatchLog -> batchLog
   - start periodic checking
   - missing brace

A new feature with major breaking changes.

.. highlight:: none
               
::

   feat($compile): simplify isolate scope bindings

   Changed the isolate scope binding options to:
   - @attr - attribute binding (including interpolation)
   - =model - by-directional model binding
   - &expr - expression execution binding

   This change simplifies the terminology as well as
   number of choices available to the developer. It
   also supports local name aliasing from the parent.

   BREAKING CHANGE: isolate scope bindings definition has changed and
   the inject option for the directive controller injection was removed.

   To migrate the code follow the example below:

   Before:

   scope: {
     myAttr: 'attribute',
     myBind: 'bind',
     myExpression: 'expression',
     myEval: 'evaluate',
     myAccessor: 'accessor'
   }

   After:

   scope: {
     myAttr: '@',
     myBind: '@',
     myExpression: '&',
     // myEval - usually not useful, but in cases where the
     // expression is assignable, you can use '='
     myAccessor: '=' // in directive's template change myAccessor() to myAccessor
   }

   The removed `inject` wasn't generaly useful for directives so there should be no code using it.

For example, you've been working on your branch and made three commit
with vague non-useful messages such as "Work in progress", "Small
fix", etc. You want to wrap up the work with a nice single squashed
commit following the above format. You can use Git's rebase tool

**In SourceTree**

There is a good article on interactive rebase and squashing commits
`here
<http://blogs.atlassian.com/2014/06/interactive-rebase-sourcetree/>`__.

**Git command line**

.. code-block:: console

   > git rebase -i HEAD~3

This will pull open an editor with something like the following.

.. highlight:: none
               
::

   pick f7f3f6d Work on docs
   pick 310154e Work in progress
   pick a5f4a0d Small fix

   # Rebase 710f0f8..a5f4a0d onto 710f0f8
   #
   # Commands:
   #  p, pick = use commit
   #  r, reword = use commit, but edit the commit message
   #  e, edit = use commit, but stop for amending
   #  s, squash = use commit, but meld into previous commit
   #  f, fixup = like "squash", but discard this commit's log message
   #  x, exec = run command (the rest of the line) using shell
   #
   # These lines can be re-ordered; they are executed from top to bottom.
   #
   # If you remove a line here THAT COMMIT WILL BE LOST.
   #
   # However, if you remove everything, the rebase will be aborted.
   #
   # Note that empty commits are commented out
 
To squash the three commits into one you would edit the script to look
like this.

.. highlight:: none
               
::
   
   pick f7f3f6d Work on docs
   squash 310154e Work in progress
   squash a5f4a0d Small fix

   # Rebase 710f0f8..a5f4a0d onto 710f0f8
   #
   # Commands:
   #  p, pick = use commit
   #  r, reword = use commit, but edit the commit message
   #  e, edit = use commit, but stop for amending
   #  s, squash = use commit, but meld into previous commit
   #  f, fixup = like "squash", but discard this commit's log message
   #  x, exec = run command (the rest of the line) using shell
   #
   # These lines can be re-ordered; they are executed from top to bottom.
   #
   # If you remove a line here THAT COMMIT WILL BE LOST.
   #
   # However, if you remove everything, the rebase will be aborted.
   #
   # Note that empty commits are commented out

When saving this you will return to a text editor where you can merge
the commit messages seeying something like this.

.. highlight:: none
               
::

   # This is a combination of 3 commits.
   # The first commit's message is:
   Work on docs

   # This is the 2nd commit message:

   Work in progress

   # This is the 3rd commit message:

   Small fix
 
Those commits are practically useless in the grand scheme of
things. You want to replace it with a single properly formatted
message following above conventions. In this case you would remove the
above from the text editor and replace it with something like the
following.

.. highlight:: none
               
::
   
   docs(developer-guide.rst): update docs with new code base refactory

   What's changed in details:
   * Change backend section to reflect migration to NodeJS
   * Refactor various part of guide with new content
   * Introduce new conventions and standards

Save this nicely formatted commit and then you're ready to publish
your work and do a pull request.

.. code-block:: console
                  
   > git push

Although if you were working entirely on a detached local branch like
I do you would need to push like this instead.

.. code-block:: console

   $ git push --set-upstream origin replace-this-with-branch-name

Do the pull request from bitbucket and use the last commit as the
message.

Contribute New Code in Branches Interrupted
-------------------------------------------

In the ideal scenario, you create your branch, do you work and prepare
a PR all during which time nobody committed their work in
``origin/develop`` since you first branches from it. In real life when
several developers work together at the same time things will get a
little more complicated. The following two scenarios are the most
likely to happen and here is how one can deal with this while still
aiming to keep that nice linear history clean with squashed messages.

TODO - Include scenario where user develops on issueX branch while
other people commmit to develop before issueX is ready. Or issueX is
pushed to origin/issueX to be pull requested and merged but is found
out to be in need of further work.

Database Schema Changes
-----------------------

Database schema migration are currently done more or less manually
with the help of a proprietary tool called SQLDelta. Currently there
is only the most current schema available and it is not easy to revert
to older schemas from previous releases. So it is important to stay
up-to-date. The database schema of the developer can be made
up-to-date by syncing with the master reference database

TODO - Include steps and a simple schema upgrade with screenshots

Note that when you make schema changes, you should run the SP
"common.afterschemaupgrade" which recompiles all views and stored
procs.


Testing Code
------------

Currently not a lot of test is being done, but eventually all new
branches doing into ``origin/develop`` with new features or bug fixes
should contain respective units test at a minimum testing the new
feature or the bug being fix.

TODO develop this section further as we move forward. Here are some
notes to get started:

* karma test infrastructure is in place in the grunt file. unit test
  are added into the test folder. Ad-hoc test pages can be stored in
  testpages. these can be run in IIS Express, which is started with
  the grunt task iisexpress:server.

* establish and start to enforce lint standards in the typescript and
  javascript code (and hopefully in c# somehow)

* move to a test-driven methodology, and accrue regression test suites
  both for the C# code, and the angular code.

.. _high-level:

High Level Architecture
=======================

To recap what was already introduced in :ref:`intro` here is what the
repository of the Pacific EMIS looks like.

.. image:: images/repository.png
  :align: center

.. _data-retrieval-service:

Data Retrieval Service
----------------------

The architecture of the data retrieval service is shown below.

.. image:: images/data-retrieval-architecture.png
  :align: center

And here is a short discussion regarding the use of Entity Framework 6
vs. ADO.NET by the project main software architect, Brian Lewis.

|  I did have a look at that some time back and ended up rolling back,
|  thinking the time is not right. I'm more inclined to wait for EF7 to
|  mature, rather than adopt 6 now and have to deal with another
|  conversion. But – noting the earlier observation that simple CRUD
|  operations on lookups and master tables are not yet in Pineapples
|  and a standalone implementation will need them, I can see benefits
|  in the medium term.
|
|  To dig a bit deeper, although most of the data access is done
|  through ADO.Net connections and command manipulation, at the core ,
|  that connection object comes from a linqToSql class – an earlier
|  .NET ORM . It exposes a ADO.NET connection and that is what is
|  used. But in one or two places it does use LinqToSQl – see
|  PDBPerfAsess.cs – and not coincidentally, this is the only class
|  currently trying to do simple record level writing.
|
|  So the path to EF7 would probably begin with replacing that
|  linqToSql context with an EF7 DBContext, then digging the ADO.NET
|  connection out of that to use in all the existing ADO.NET code: See
|  `Run Stored Procedures in EF <http://stackoverflow.com/questions/28599404/how-to-run-stored-procedures-in-entity-framework-7>`_. Which
|  shows how to get that low-level connection object out of the EF7
|  wrapper. In fact there's an EF6 sketch of this logic still in
|  Pineapples.Data (doing nothing.)
|
|  While I'm generally on board with strongly-typing (hence moving js
|  -> typescript) bear in mind that you do get a bit of overhead with
|  EF in that you need a class to represent the result set of a
|  query. When reading, this class only exists to get turned into JSON
|  – and you'd get the same JSON serialising an ADO.NET
|  DataTable. However, serialising a DataTable means that you can
|  change the query and everything still works without a
|  recompile. This is especially useful in the circumstance where we
|  have Stored Procs that can return a variety of record shapes driven
|  by the "view mode":
|
|  The "Columnset" parameter selected via the dropdown shown in the
|  figure below winds its way right down to the stored proc. This type
|  of thing is pretty painful in a strongly typed ORM, but simple when
|  working with DataTables.

.. image:: images/columnset.png
   :align: center
  
Templates
---------

The architecture of templates is shown below.

.. image:: images/templates-architecture.png
   :align: center

Theme (Look and Feel)
---------------------

Currently a little messy. Started out with a paid theme based on
bootstrap now a lot of it is out and just building with HTML and CSS
"left overs" from the theme. The current idea on the road map would be
to move to a full Angular Material based interface to provide a
professional consistent UI.

Navigation and Module System
----------------------------

The Navigation menu in the side bar is loaded when the user logs in.

.. image:: images/navigation-system1.png
   :align: center

It is part of the packet of data returned with the BearerToken in
response to api/token.

.. image:: images/navigation-system2.png
   :align: center

Both the menu and the ``home_site`` are picked up from the server from
a *MenuKey* associated to the user. For ASP.NET identity accounts,
this is on ``AspNetUsers`` table. For domain accounts, this is set to
the default ``ALL``.  The MenuKey points into the table Navigation in
IndentitiesP (i.e. the ASP.NET Identity database).


.. image:: images/navigation-system3.png
   :align: center

There are 3 types of entries in the IdentitiesP's dbo.Navigation
table

* Those like ALL, which represent a menu that may be assigned to a
  user
* Submenus, that are parents of a collection of function (e.g.
  schools)
* End points that are a specific system function (e.g. schools.list)

And is a brief description of its columns

* **Icon**: this is fontawesome coding for the icon to show on that menu.
* **Label**: text for the menu item
* **State**: for functions, this is the ui-router state to go to when that
  function is selected. Obviously, these need to be defined in the
  Javascript/Typescript code. Most features have a routine that will
  define these routes.
* **Children**: for menu parents, a pipe-separated list of the IDs of its
  children. This can be recursive, but currently, these children are
  all end-points--entries that point to a state.
* **homestate**: the starting state when the user logs in. When you
  navigate to the state site.home, you are redirected to this state.

Roles are sent to the client. For ASP.NET users, these come from the
IdentitiesP database. For domain users, it is just currently a list of
ActiveDirectory groups. A role can be associated to a ui-route on the
client. Access to that route can be dependent on belonging to the
Role. (Note that even if a route is not exposed through the menu, you
can still navigate to it if you know its URL.) This is managed by the
component *angular-permission*.
           
Also sent to the client are *Filters*. These are restrictions on what
a user may select in the searcher pages. For example, a filter of
*District=DC* will mean that the Province dropdowns will be locked to
province *DC* (note that this is ultimately enforced on the server
based on the bearer token.)  Filters are set up in ASP.NET Identity as
Claims:

.. image:: images/navigation-system4.png
   :align: center

Menu filter and roles are sent to the client at login, but are also
encapsulated as Roles and Claims in the bearer token. Their use on the
client is more a courtesy than a security feature: on the client, we
don’t show the user things that they will not be able to do on the
server, where the Claim is enforced based on the token.

TODO We need to better integrate the Roles, Filters and menu that are
retrieved for a user who authenticates against Active Directory. This
is important in Solomon Islands for example, where we will need to
distinguish between users logging in to the the government domain.
Brian suggest that this will be some data-driven mapping between
Active Directory groups, and some abstract ASP.NET Identity account
representing that group. So at login, if you are identified as
belonging to some specified AD group, you will get the MenuKey,
Filters and Roles associated to that group.


Lookups and Configuration
-------------------------

The Pacific EMIS is mainly configured and adapted to different
countries through lookups in the database. Along side this document is
kept a spreadsheet called `PineapplesLookups
</files/PineapplesLookups.xlsx>`_ which details all the lookups. Those
must be understood and customized for any new deployment.

.. _low_level:

Low Level Documentation and API
===============================

The lower level documentation about software design, application
programming interfaces, small gotchas and all other nitty-gritty
details about the source code is written directly inside the source
code. It would be nice to eventually be able to extract and export to
hard copy formats such as HTML or PDF using a tool like `Doxygen
<http://www.stack.nl/~dimitri/doxygen/>`_. Currently the place to
access it is directly inside the source code as this documentation
process is just started off.

Since many things are yet undocumented here are a few things to keep
in mind.

* there's no comprehensive documentation of the Web Api. In the code,
  look in the Api files on the client, and the WebApiConfig.cs class
  in App_Start in Pineapples, which is where most routes are
  defined. Again, Fiddler gives you a look at what is being fired off
  to the server as you perform each operation.
* JSON is the data format used by the Web Api (except for the
  AtAGlance call, see next point).  Parameters passed up to the server
  are converted into objects or variables sent to the method in the
  WebApi controller. This happens automatically, but there is ways to
  dig in to the metadata to control how this happens. Similarly,
  objects returned from the Controller get a default conversion to
  JSON to send back on the response; you can control what this JSON
  looks like if you need to. In many cases, what we send back is a
  "DataResult" object which is one or more named DataTables, together
  with metadata defining the Page read, page size, the total number of
  records, first / last record. Using Fiddler, you'll see both the
  request sent up and the response sent back.
* The AtAGlance pages are built from a static Xml file containing a
  table of aggregated values. Particular values are dug out of this
  Xml using Jquery as a kind of client-side "xpath". Some bulk
  operations in Pineapples pass blocks of XML representing a
  collection of records into the Sql Server. The Enrolment edit page
  does this. So the JSON returned from the client is transformed to
  XML then passed to the SQL server.

.. _esurvey:

eSurvey Technology
==================

The Pacific EMIS makes use of what we call "PDF eSurveys" or
"eSurveys" for short. Essentially, the eSurveys are PDF form documents
used to submit the surveys obsoleting the old paper based process. The
eSurveys are built using a proprietary tool called `CenoPDF
<http://www.lystech.com/>`_, so a relatively not expensive license
must be bought to create the surveys (or edit them in the
future). CenoPDF is a Microsoft Word plugin that adds the ability to
edit, convert and create fillable PDF form files with text box, check
box, button, annotations, actions, javasript, etc. directly in
Microsoft® Word, Microsoft® Publisher and Microsoft® Excel.

Creating/Editing the PDF eSurvey
--------------------------------

The first thing to do for anybody creating/editing the eSurveys is to
read the CenoPDF `User Guide
<http://www.lystech.com/webhelp/default.htm>`_. Once familiar with
CenoPDF one can startting creating a new eSurvey or editing an
existing one to a new revision. When starting from scratch it is a
good idea to get the source word document of Karibati which at the
time of this writing is the most comprehensive eSurvey using this
technology in the Pacific.

These word documents currently could be given from one of the Pacific
EMIS team members but ideally eventually it should be included in the
software repository and version control there as well. Due to the very
large number of data cells to process it is not recomended to try and
creating the whole survey into a single big word document, but one
should break it down into several documents and then re-attached
together into a single PDF once complete.

The process for creating new eSurveys for a given country is something
like this:

1. Go through a given eSurvey (i.e. Primary School Annual Survey) from
   Kiribati and decide what part of the eSurvey applies to you.
2. Collect all the Kiribati broken down word documents for a given
   eSurvey (i.e. Primary School Annual Survey) and download them
   somewhere on your workstation
3. Pick the relevant part that you need for your eSurvey.
4. Edit what needs to be edited (e.g. some countries call education
   levels Class 1, Class 2, etc. others refer to them as Grade 1,
   Grade 2, etc.) Other plain text labels will need to be edited as well such
   as names of provinces
5. Save your word document as another name (e.g. SIEMIS_PRI_TRIN.docx
   for SIEMIS primary schools survey part recording transfers in data)
6. Generate the PDF for this word document using the CenoPDF export to
   PDF feature (not the MS Word export to PDF).
7. Do this for all the word documents to get all the data grid needed
   in the survey effectively constructing the whole survey piece by
   piece.
8. Combine all the generated PDF documents into the final PDF eSurvey
   and call it something like "SIEMIS_2016_PRI_v1.pdf" with the "_v1" as a
   simple means to make it clear what version of the eSurvey this is
   in case it changes in the future. The combine step is done using a
   command line tool by executing the following two commands:

.. code-block:: console

   > "C:\the-path\cenoPostProcess.exe" -d C:\the-path\Pineapples.eSurvey\siemis\PRI\out -c "SIEMIS_2016_PRI.pdf" -f "SIEMIS_PRI*.pdf" --nopause

   > "C:\the-path\cenoPostProcess.exe" -d C:\the-path\Pineapples.eSurvey\siemis\PRI\out -f "SIEMIS_2016_PRI.pdf" --title "Solomon Islands Primary Survey 2016"  -j "C:\the-path\Pineapples.eSurvey\js"

.. warning::

   There is a small issue making the final generated eSurvey missing,
   or more specifically hiding some labels. There is an easy work
   around. Open every CenoPDF generated document into Adobe Acrobat
   and save the document without making any changes. Accept to
   override the file. Run the final step 8. above after doing this
   only.
   
The process for editing would be much simpler.

1. Locate the word document that needs to be edited or if you need to
   create a new document create one.
2. Generated the PDF for the one you changed.
3. Attach all the PDF document together into the new PDF eSurvey and
   increase its revision number
   (e.g. "SIEMIS_2016_PRI_v2.pdf").

PDF eSurvey Manager
-------------------

Currently the PDF eSurvey Manager is a standalone desktop application
that must be installed on a machine to "manage" eSurveys, in other
words, it aims at doing the following:

* Read existing school surveys from a database (production or a test
  database)
* Pre-populate surveys with some data before sending them to schools
* Upload completed surveys into the database (again, production or a
  test database)

The PDF eSurvey Manager repository is available at TODO INCLUDE LINK
TO REPO. But you only need to download the executable unless you are a
developer wanting to fix or improve the tool. Detailed step by step
instructions on how to install, configure and use the PDF eSurvey
Manager belong in the User Guide since they are the target audience.

PDF eSurvey Process
-------------------

The PDF eSurvey compliments the new online web user interface. For
schools with a more reliable Internet connection they can directly
access the survey form online and submit directly in the web Pacific
EMIS application. Other schools without access to a reliable Internet
connection but with access to a laptop or large tablet running the
freely available `Adobe Acrobat Reader
<https://get.adobe.com/reader/>`_ can use the PDF eSurvey. There is no
need for a good Internet connection, the PDF eSurvey can be sent to
the school just like the old paper based survey, on a USB disk for
example, and returned to the data entry officers just as before.

A more detailed user-oriented process is documented in the User Guide,
but in essence it goes like this:

1. Start the eSurvey Manager
2. Create a pre-populated survey for one or more
   schools. Pre-populated eSurveys contains some important information
   such as the school name and ID and some data for previous years as
   a guide for example.
3. Distribute those pre-populated eSurveys to their respective schools
   on a USB stick or email if that have it.
4. School fill up their surveys as before but directly on a
   computer instead of paper.
5. Schools return the filled eSurveys on the same USB stick or email
6. Data Entry Officers look through the eSurveys for any issues and
   make sure it is all correctly completed
7. Start the eSurvey Manager and load the data from the PDF eSurvey
   into the database
8. Verify all data was uploaded successfully by browsing to the
   survey on the Pacific EMIS online web application

.. _security:

Security Considerations
=======================

Security is an increasingly important aspect of any project and the
Pacific EMIS is no exception. This section describes the security
framework of the project.

Authentication
--------------

Currently in progress: User Roles Implementation

ASP.NET Identity includes a concept of "Roles", which are in effect a
specific type of "claim" in the claim-based security model. For users
authenticated by Identity (ie those defined AspNetUsers in
IdentitiesP) these roles are defined in the IdentitesP database. For
users authenticated against the domain the Roles collected are
theDomain groups that the user belongs to. Being Claims, the Roles are
embedded into the Bearer token--so that, every request made to the
server has access to the Role membership of the user.

Web Api lets you decorate methods with a Role attribute to determine
which role(s) can access that method so you can implement server side
security this way. On the client, we send the Role memberships to the
client with the Bearer Token, so they are accessible in the
client-side code. This is the package that comes down in response to
``api/token``.

.. image:: images/roles.png
   :align: center

In particular, the module *angular-permission* gives a simple way to
tie an authorization condition to a state in ui-router so you cannot
navigate to that state if the condition is not met. So the intention
is to set up conditions matched to Role membership, and assign these
on the states on the client, just as we do to the corresponding
functions on the server.

So I think we can use these structures to give us quite fine-grained
control over authorization for various functions. What we need are:

* abstract definitions of rights represented as Roles (e.g. can save a
  school, can read teacher info) and
* a mechanism to map into such Roles from domain groups
    
Other Notes.

* The ASP.NET Identity code was modified to support logins using an
  ASP.NET Identity user (i.e. defined in the identities database) or a
  domain login account – so your simplest way to get in will be to
  supply the user name and password you use to log in to Windows on
  your machine.

Authorization
-------------

The Pacific EMIS uses a granular permissions architecture that divides
the application into topics, and assigns rights to the user in that
topic, example:

* school;
* teacher;
* establishment;
* enrolment;
* survey;
* finance;
* infrastructure; etc.
  
Rights are:

* Read : may read topic data
* ReadX: may read extended topic data
* Write: may write basic topic data
* WriteX: may write extended topic data
* Admin: may administer data in this topic; for example manage lookup lists
* Ops: may perform any specific data manipulation operations in this topic.

Some rights imply others (e.g. ReadX => Read, WriteX => ReadX, WriteX => Write)

In the Pineapples database each Topic/Role is represented as a
**Role** (e.g. pEstablishmentRead). These roles are called
**Functional Roles**. The user (logged in to the database using
Windows Authentication) belongs to a role representing their user
group (e.g. FinanceOffice) and these roles are called **Organisational
Roles**. Each **Organisational Roles** is a member of a set of
**Functional Roles**. These membership bestow database permissions on
the logged in user.

However, the database connection from the web site is made in the name
of the service account running the web site, so this model cannot
work. Using impersonation limits scalability because database
connections cannot be reused. So we need to enforce the user
privileges in other ways.

At the **server** the user must be prevented from connecting to any
API endpoint to which they do not have permission (e.g. cannot edit a
school record if not assigned to pSchoolWrite.) On the **client**, the
user interface should reflect operations that the user is not
permitted to perform (e.g. the edit button in the School Item form is
only available if the user is in pSchoolWrite) .

Note that client side permission are treated as a "courtesy", to allow
the user to understand their role, and not perform operations that
will fail due to authorization restrcitions at the server, but all
authorization is enforced on the server.

Implementing Authorization as Claims
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

These granular permissions will be determined at login time, and
encoded as claims in the bearer token. When an API endpoint is called,
authorization is determined by interrogating the claims in the current
**ClaimsIdentity**. Further, the permissions information is sent to
the client at login time, along with the token. This enables the
client-side code to access this information when configuring the UI.

To keep the data small, the permissions will be hashed into a short
string. Each character of the string will represent a topic. Since
their are 6 possible rights for each topic, there are 64 possible
values for the topic 0--63. Note that the dependencies between rights
mean that in actuality, many of these combinations cannot be
used. Adding 48 to this value produces a displayable character in the
range 48--111. This is the character included in the string to
represent that topic. In this way, the permission grid is rendered as
an 11 character string.

Server-side Implementation
~~~~~~~~~~~~~~~~~~~~~~~~~~

An attribute ``PineapplesPermission(topic, right)`` may be assigned to
any API endpoint (i.e. API controller method) to enforce that
permission on the endpoint.)

Client-side Implementation
~~~~~~~~~~~~~~~~~~~~~~~~~~

Is based on ``angular-permission`` , creating a permission named
``p<topic><right>``, for example:

```<span permission permission-only="pSchoolWrite">Click to edit...</span>```

Assigning Permissions
~~~~~~~~~~~~~~~~~~~~~

For **AspIdentity authentications**, permissions will be assigned
directy into ``AspUserClaims`` table. The hashed string will be stored
as the claim value.

For **AD authentications**, we will map the user to a "proxy" in
``AspNetUsers`` in identity. This proxy will be determined by the
group memberships of the user. The permission claim will be associated
to this proxy. For example, a user that is a member of AD group
ScholarAdmins may be assigned to a proxy AspNetUser account of the
same name. The account will have a permission claim corresponding to
the rights assigned to this group.

  
SSL/TLS Encryption
------------------

The application support encrypted communication (i.e. HTTPS). In
development the embedded web servers already serves pages through
HTTPS. In production it would have to be configured to be served on
HTTPS with redirect from HTTP to HTTPS to encsure this. Details to
achieve this will be documented in the Systems Administrator Guide.

Latest Top 10 Security Risks
----------------------------

The project should always be measured against OWASP's most up-to-date
`Top 10 Security Risks
<https://owasp.org/index.php/Top_10_2013-Table_of_Contents>`_. It is
important to re-assess the project towards this top 10 list on a
regular basic (e.g. every year or whenever a new release comes
out). Any change should be carefully examine to make sure the
application still covers all of them with details reflected in this
documentation.

Integrated Penetration Testing
------------------------------

The above guidelines and procedures should offer an excellent starting
point to ensure a secure web application. Of course, securing a web
application should not stop here. We would like to see a more
integrated penetration testing process. There are a number of tools
that can be used to help support this process. Most of those tools
have a relatelively steep learning curve but they are worth the time
investment.

After some time evaluating several free software tools that were
either recommended by OWASP or considered promising projects we have
come up with a short list of tools to watch:

* `OWASP Zed Attack Proxy Project (ZAP) <https://www.owasp.org/index.php/OWASP_Zed_Attack_Proxy_Project>`_
* `OWASP CSRFTester <https://www.owasp.org/index.php/Category:OWASP_CSRFTester_Project>`_
* `OWASP WebScarab <https://www.owasp.org/index.php/Category:OWASP_WebScarab_Project>`_
* `Vega <http://www.subgraph.com/>`_ a fork of Google Researchers' Skipfish backed up by
  commercial support. A younger but promising project which seem
  easier to use at first glance.

One or more of those tools could eventually be integrated into the
development process. At first only making use of simple features such
as automated scans and slowly integrating more complicated robust
testing processes one by one. As these new processes come to live they
should be clearly documented here with instructions on how to use the
tools.

.. _doc:

Project Documentation
=====================

Higher level documentation (i.e. User Guide, Systems Administrator
Guide and this Developer Guide) is prepared using an excellent tool
developed in the Python world called Sphinx `http://sphinx-doc.org
<http://sphinx-doc.org>`_ which uses the reStructuredText markup
language `http://sphinx-doc.org/rest.html
<http://sphinx-doc.org/rest.html>`_. Sphinx outputs to HTML and PDF
but could also output to other formats.

In the docs folder there is are three folders:

* user-guide
* sysadmin-guide
* developer-guide

Each containing the same folder/file structure. The ``source``
directory contains the source files with the markup content; this is
where the documentation is written. The ``build`` directory is where
the documentation is produced either in PDF, HTML or other supported
format.

If you plan on producing documentation you will need to install
Sphinx. Sphinx is written in `Python <https://www.python.org/>`_ so if
it is not installed on your computer you will need to install it
first. Download it `here <https://www.python.org/downloads/windows/>`__
and add all components at installation including the "Add python.exe
to Path". The installation of Sphinx documentation tool is as easy as
typing the following from the command prompt.

.. code-block:: console
                
   > pip install sphinx

Once correctly install you can edit any of the ``index.rst`` files in
the ``source`` folders and generate the new documentation with the
following command.

.. code-block:: console

   > make latexpdf
   > make html

Or simply type ``make`` to get a list of other options.

**Optional**

If you wish you generate PDF you will need to install the LaTeX type
setting system and also the make tool (GNU Make is a popular one). How
to do this will largely depend on your OS. If you are using Linux
(e.g. On Debian derivatives `sudo apt-get install make texlive-full`)
and you should be good to go. On Windows you can download and install
MikText from \url{http://miktex.org/} and GNUMake from
\url{http://gnuwin32.sourceforge.net/downlinks/make.php}} which should
have the whole type setting system and build tool. On Windows you will
have to add the PATH to the make executable as described in
\url{http://windowsitpro.com/systems-management/how-can-i-add-new-folder-my-system-path}. The
PATH to *add* to the list should be something like `C:\Program Files
(x86)\GnuWin32\bin`.

.. [REST-SERV] Leonard Richardson and Sam Ruby, RESTful Web Services,
               O'Reilly, May 2007.

.. [FHS] Rusty Russell, Daniel Quinlan and Christopher Yeoh,
         Filesystem Hierarchy Standard 2004, freestandards.org

.. [PRO-GIT] Scott Chacon, Pro Git,Available at http://www.git-scm.com/book,
             Apress.
              


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

