

Pineapples Project Structure
----------------------------

Pineapples is a single page web application built with AngularJS 1 and ASP.NET5.

The Pineapples solution is controlled as a single git repository. The solution is organised into 3 main projects:

**Pineapples**
ASP.NET project. This is the startup executable. It is responsible for delivering all objects to the browser, including

 - the initial page
 - all required scripts, stylesheets, and other assets

Pineapples implements both WEB API 2 and MVC interfaces. 
The Web API 2 interface delivers data in JASON format to the client on request.
The MVC interface delivers html to the client - the includes the initial page, and views inserted into that page.

**Pineapples.Data**
.NET class library. This project exposes "business objects" to Pineapples, and is responsible for reading and writing data to the SQL server database.
This project also includes any required JSON serializers used by Web Api 2 to construct  and tear down objects received / sent over the REST interface.

**Pineapples.Client**
.NET Web Site. The client front-end, ie javascript and css files executed in the browser. 
As a _web site_, this project has no project file, and build is turned off. 
The project is managed using the "web stack" - *npm*, *bower*, *grunt*.
***npm***
npm loads grunt and the required grunt plug-ins. Modify ```packages.json``` to add new grunt tasks.
***bower***
_All_ dependencies on 3rd party components are managed using bower. Those components that do not support bower, are still added into ```bower.json``` with a reference to their source url. 
The ```bower_components``` folder is not in source control, since its contents are entirely determined by ```bower.json```.
Modify ```bower.json``` to add new 3rd party libraries to the project.
***grunt***
```gruntfile.js``` contains all the project management tasks for the project. These are accessed in VS2015 from the Task Explorer Window. The two most important tasks are
 - build
 - deploy
***Typescript***
While beginning as a javascript project, Pineapples.Client is being progressively migrated to typescript. New client-side code is being written in typescript. As javascript files require modification, they are ported to typescript.

***Building Pineapples.Client***
The outputs from Pineapples.Client are
 - javascript, assembled into the folder ```scripts```
 - css, assembled into the folder ```styles```
 - other assets ( fonts, images, data) organised into the folder ```assets```
**Javascript** is created from raw javascript in the src/js folder, and javascript compiled from typescript in the src/ts folder. _build_ concatenates the application javascript to a single file, in ```scripts/app```

_build_ also extracts 3rd party javascript dependencies from each bower component, and places these under ```scripts/vendor```

**css**  is concatenated from raw css in src/css folder and placed in ```styles/app```. _build_ also extracts any required css from bower, and places this in ```styles/vendor```.
less tasks are available to build the app css; however, these are not part of the regualr build process. 

**assets** are maintained directly into the ```assets``` folder.

***Deploying Pineapples.Client***
The _deploy_ task simply copies the three folder trees ```scripts``` ```styles``` and ```assets``` into the Pineapples project folder. Note that these three folders in Pineapples are not included in the Pineapples project, and are not added to source control. However, from this location, they are available to ASP.NET to send to the client. 
ASP.NET _Bundling and minification_ is applied separately to the four folders:
- scripts/app
- scripts/vendor
- styles/app
- styles/vendor

***Testing Pineapples.Client***

_karma_ test infrastructure is in place in the grunt file. unit test are added into the ```test``` folder.
Ad-hoc test pages can be stored in ```testpages```. these can be run in IIS Express , which is started with the grunt task _iisexpress:server_.

**Other Projects**

Two other projects complete the solution:
***Pineappples.Reports***

This is an SSRS project, home to report definitions and related SSRS objects. these are deployed into the SSRS server.
***Pineapples.Data.Tests***  
A VS2015 Test project allowing tests of the data layer.  
**Other Folders**  
***Assemblies***  
This is home to any additional assemblies that are not part of the project, not part of DOTNET, and not served by nuget. Placing these in a common location relative to the solution projects makes managing references easier. The asseblies folder sits besides the packages folder created by nuget.
***Database***
This folder holds a SQLDelta schema file representing the latest schema of the Pineapples database. SQL Delta can compare this snapshot against a database and generate a script to resolve the differences.

 - 
 




> Written with [StackEdit](https://stackedit.io/).