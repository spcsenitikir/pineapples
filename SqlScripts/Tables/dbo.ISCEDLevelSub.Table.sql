SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ISCEDLevelSub](
	[ilsCode] [nvarchar](10) NOT NULL,
	[ilCode] [nvarchar](10) NULL,
	[ilsName] [nvarchar](50) NULL,
 CONSTRAINT [aaaaaISCEDLevelSub1_PK] PRIMARY KEY NONCLUSTERED 
(
	[ilsCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[ISCEDLevelSub] TO [pAdminAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[ISCEDLevelSub] TO [pAdminAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ISCEDLevelSub] TO [pAdminAdmin] AS [dbo]
GO
GRANT DELETE ON [dbo].[ISCEDLevelSub] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[ISCEDLevelSub] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[ISCEDLevelSub] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ISCEDLevelSub] TO [pAdminWrite] AS [dbo]
GO
GRANT DELETE ON [dbo].[ISCEDLevelSub] TO [pAdminWriteX] AS [dbo]
GO
GRANT INSERT ON [dbo].[ISCEDLevelSub] TO [pAdminWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ISCEDLevelSub] TO [pAdminWriteX] AS [dbo]
GO
GRANT SELECT ON [dbo].[ISCEDLevelSub] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[ISCEDLevelSub] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[ISCEDLevelSub]  WITH CHECK ADD  CONSTRAINT [ISCEDLevelSub_FK00] FOREIGN KEY([ilCode])
REFERENCES [dbo].[ISCEDLevel] ([ilCode])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ISCEDLevelSub] CHECK CONSTRAINT [ISCEDLevelSub_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'UNESCO Defined sublevels. Each ISCEDLEvel may have multiple sublevels ISCED3A, ISCED3B. The ISCEDLevel is the foreign key. Each level must have at least one sublevel. 
System defined.
The ISCEDSublevel appears on lkpLevels. Since enrolments are clasifed by level, this allows aggregation of enrolment by ISCED Sub Level and ISCEDLevel' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISCEDLevelSub'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Class Levels' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISCEDLevelSub'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'ISCED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISCEDLevelSub'
GO

