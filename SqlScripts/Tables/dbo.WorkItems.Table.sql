SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkItems](
	[witmID] [int] IDENTITY(1,1) NOT NULL,
	[woID] [int] NOT NULL,
	[schNo] [nvarchar](50) NOT NULL,
	[bldID] [int] NULL,
	[witmType] [nvarchar](15) NOT NULL,
	[witmQty] [decimal](18, 2) NULL,
	[witmDesc] [nvarchar](50) NULL,
	[witmEstCost] [money] NULL,
	[witmSourceOfFunds] [nvarchar](15) NULL,
	[witmCostCentre] [nvarchar](10) NULL,
	[witmContractValue] [money] NULL,
	[witmProgress] [nvarchar](15) NULL,
	[witmProgressDate] [datetime] NULL,
	[witmActualCost] [money] NULL,
	[witmInspectedBy] [nvarchar](50) NULL,
	[witmEstBaseCost] [money] NULL,
	[witmEstBaseUnit] [nvarchar](10) NULL,
	[witmBldCreateType] [nvarchar](10) NULL,
	[witmBldCreateNum]  AS (case [witmEstBaseUnit] when 'BLDG' then [witmQty] else (1) end),
	[witmRoomsClass] [int] NULL,
	[witmRoomsOHT] [int] NULL,
	[witmRoomsStaff] [int] NULL,
	[witmRoomsAdmin] [int] NULL,
	[witmRoomsStorage] [int] NULL,
	[witmRoomsDorm] [int] NULL,
	[witmRoomsKitchen] [int] NULL,
	[witmRoomsDining] [int] NULL,
	[witmRoomsLibrary] [int] NULL,
	[witmRoomsSpecialTuition] [int] NULL,
	[witmRoomsHall] [int] NULL,
	[witmRoomsOther] [int] NULL,
	[witmScheduled] [datetime] NULL,
	[witmEstDuration] [int] NULL,
 CONSTRAINT [WorkItems_PK] PRIMARY KEY NONCLUSTERED 
(
	[witmID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[WorkItems] TO [pInfrastructureReadX] AS [dbo]
GO
GRANT DELETE ON [dbo].[WorkItems] TO [pInfrastructureWriteX] AS [dbo]
GO
GRANT INSERT ON [dbo].[WorkItems] TO [pInfrastructureWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[WorkItems] TO [pInfrastructureWriteX] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[WorkItems] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [lkpWorkItemProgressWorkItems] ON [dbo].[WorkItems]
(
	[witmProgress] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [lkpWorkItemTypeWorkItems] ON [dbo].[WorkItems]
(
	[witmType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [woID] ON [dbo].[WorkItems]
(
	[woID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WorkItems] ADD  CONSTRAINT [DF_WorkItems_witmEstQty]  DEFAULT ((1)) FOR [witmQty]
GO
ALTER TABLE [dbo].[WorkItems]  WITH CHECK ADD  CONSTRAINT [BuildingsWorkItems] FOREIGN KEY([bldID])
REFERENCES [dbo].[Buildings] ([bldID])
GO
ALTER TABLE [dbo].[WorkItems] CHECK CONSTRAINT [BuildingsWorkItems]
GO
ALTER TABLE [dbo].[WorkItems]  WITH CHECK ADD  CONSTRAINT [FK_Schools_WorkItems] FOREIGN KEY([schNo])
REFERENCES [dbo].[Schools] ([schNo])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[WorkItems] CHECK CONSTRAINT [FK_Schools_WorkItems]
GO
ALTER TABLE [dbo].[WorkItems]  WITH CHECK ADD  CONSTRAINT [lkpWorkItemProgressWorkItems] FOREIGN KEY([witmProgress])
REFERENCES [dbo].[lkpWorkItemProgress] ([codeCode])
GO
ALTER TABLE [dbo].[WorkItems] CHECK CONSTRAINT [lkpWorkItemProgressWorkItems]
GO
ALTER TABLE [dbo].[WorkItems]  WITH CHECK ADD  CONSTRAINT [lkpWorkItemTypeWorkItems] FOREIGN KEY([witmType])
REFERENCES [dbo].[lkpWorkItemType] ([codeCode])
GO
ALTER TABLE [dbo].[WorkItems] CHECK CONSTRAINT [lkpWorkItemTypeWorkItems]
GO
ALTER TABLE [dbo].[WorkItems]  WITH CHECK ADD  CONSTRAINT [WorkOrdersWorkItems] FOREIGN KEY([woID])
REFERENCES [dbo].[WorkOrders] ([woID])
GO
ALTER TABLE [dbo].[WorkItems] CHECK CONSTRAINT [WorkOrdersWorkItems]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Building ID if the work relates to a specific building' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WorkItems', @level2type=N'COLUMN',@level2name=N'bldID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity required - used in calculating the estimated cost. May determined frm the Work ITem Type and buildng using the Formula for the Work Item Type.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WorkItems', @level2type=N'COLUMN',@level2name=N'witmQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The type of building that will be created by this work item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WorkItems', @level2type=N'COLUMN',@level2name=N'witmBldCreateType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'number of new classrooms that will be created on this workitem' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WorkItems', @level2type=N'COLUMN',@level2name=N'witmRoomsClass'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Specific items of work on an infrastructure Work Order. A Work Order may span multiple schools, but a Work Item realtes to one school only.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WorkItems'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Work Orders' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WorkItems'
GO

