SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SurveyYearCourseYear](
	[sycyID] [int] IDENTITY(1,1) NOT NULL,
	[scrsID] [int] NULL,
	[sycyYear] [int] NULL,
	[sycyCost] [money] NULL,
	[sycyPlaces] [int] NULL,
	[sycyContactT] [int] NULL,
	[sycyContactP] [int] NULL,
 CONSTRAINT [aaaaaSurveyYearCourseYear1_PK] PRIMARY KEY NONCLUSTERED 
(
	[sycyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[SurveyYearCourseYear] TO [pSurveyRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[SurveyYearCourseYear] TO [pSurveyWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[SurveyYearCourseYear] TO [pSurveyWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SurveyYearCourseYear] TO [pSurveyWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[SurveyYearCourseYear] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[SurveyYearCourseYear] ADD  CONSTRAINT [DF__SurveyYea__scrsI__7720AD13]  DEFAULT ((0)) FOR [scrsID]
GO
ALTER TABLE [dbo].[SurveyYearCourseYear]  WITH CHECK ADD  CONSTRAINT [SurveyYearCourseYear_FK00] FOREIGN KEY([scrsID])
REFERENCES [dbo].[SurveyYearCourses] ([scrsID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SurveyYearCourseYear] CHECK CONSTRAINT [SurveyYearCourseYear_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Course details by year for tertiary courses. scrsID is foreign key from SurveyYearCourse.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SurveyYearCourseYear'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Survey' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SurveyYearCourseYear'
GO

