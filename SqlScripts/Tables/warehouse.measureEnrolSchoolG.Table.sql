SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[measureEnrolSchoolG](
	[schNo] [nvarchar](50) NOT NULL,
	[SurveyYear] [int] NOT NULL,
	[Estimate] [int] NOT NULL,
	[SurveyDimensionID] [int] NULL,
	[genderCode] [nvarchar](1) NOT NULL,
	[Enrol] [int] NULL
) ON [PRIMARY]
GO

