SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[schoolTeacherCount](
	[SchNo] [nvarchar](50) NULL,
	[SurveyYear] [int] NOT NULL,
	[GenderCode] [nvarchar](1) NULL,
	[AgeGroup] [varchar](5) NULL,
	[DistrictCode] [nvarchar](5) NULL,
	[AuthorityCode] [nvarchar](10) NULL,
	[SchoolTypeCode] [nvarchar](10) NULL,
	[Sector] [nvarchar](3) NULL,
	[NumTeachers] [int] NULL,
	[Certified] [int] NULL,
	[Qualified] [int] NULL,
	[CertQual] [int] NULL,
	[Estimate] [int] NULL
) ON [PRIMARY]
GO

