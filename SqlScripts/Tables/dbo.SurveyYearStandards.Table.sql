SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SurveyYearStandards](
	[stdID] [int] IDENTITY(1,1) NOT NULL,
	[svyYear] [int] NULL,
	[stCode] [nvarchar](50) NULL,
	[stdSector] [nvarchar](3) NULL,
	[isCode] [nvarchar](10) NULL,
	[isSize] [nvarchar](10) NULL,
	[stdItem] [nvarchar](10) NULL,
	[stdName] [nvarchar](50) NULL,
	[stdValue] [float] NULL,
	[stdValueMax] [float] NULL,
	[stdperPupil] [float] NULL,
	[stdperTeacher] [float] NULL,
	[stdProRateCeiling] [float] NULL,
	[stdValueT] [nvarchar](150) NULL,
	[stdReference] [nvarchar](100) NULL,
	[stdText] [ntext] NULL,
	[stdHelp] [nvarchar](255) NULL,
 CONSTRAINT [aaaaaSurveyYearStandards1_PK] PRIMARY KEY NONCLUSTERED 
(
	[stdID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[SurveyYearStandards] TO [pSurveyRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[SurveyYearStandards] TO [pSurveyWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[SurveyYearStandards] TO [pSurveyWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SurveyYearStandards] TO [pSurveyWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[SurveyYearStandards] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Defines the values for NIS standards in a given year. Pineapples determines the "Applicable Standards" for any school in a given year, based on the year of the standard, and the school Infrastructure Size and Class groupings.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SurveyYearStandards'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'NIS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SurveyYearStandards'
GO

