SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[TeacherQual](
	[tID] [int] NOT NULL,
	[yr] [smallint] NULL,
	[tchQual] [nvarchar](50) NULL
) ON [PRIMARY]
GO

