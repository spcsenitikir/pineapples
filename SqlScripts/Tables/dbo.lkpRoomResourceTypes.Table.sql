SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpRoomResourceTypes](
	[codeCode] [nvarchar](50) NOT NULL,
	[codeDescription] [nvarchar](50) NULL,
	[roomtype] [nvarchar](50) NULL,
	[codeSort] [int] NULL,
	[codeUseNum] [bit] NULL,
 CONSTRAINT [aaaaalkpRoomResourceTypes1_PK] PRIMARY KEY NONCLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpRoomResourceTypes] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpRoomResourceTypes] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpRoomResourceTypes] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpRoomResourceTypes] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpRoomResourceTypes] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[lkpRoomResourceTypes] ADD  CONSTRAINT [DF__lkpRoomRe__codeS__71D1E811]  DEFAULT ((0)) FOR [codeSort]
GO
ALTER TABLE [dbo].[lkpRoomResourceTypes] ADD  CONSTRAINT [DF__lkpRoomRe__codeU__72C60C4A]  DEFAULT ((0)) FOR [codeUseNum]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'List of Resources to be found in a particular room types. OBSELETE - the Resource Category is now stored on lkpRoomtypes.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpRoomResourceTypes'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Resources' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpRoomResourceTypes'
GO

