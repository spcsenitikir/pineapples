SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[tableEnrol](
	[SurveyYear] [int] NOT NULL,
	[Estimate] [int] NOT NULL,
	[ClassLevel] [nvarchar](10) NULL,
	[Age] [int] NULL,
	[GenderCode] [nvarchar](1) NOT NULL,
	[DistrictCode] [nvarchar](5) NULL,
	[AuthorityCode] [nvarchar](10) NULL,
	[SchoolTypeCode] [nvarchar](10) NULL,
	[Enrol] [int] NULL,
	[Rep] [int] NULL,
	[TrIn] [int] NULL,
	[TrOut] [int] NULL,
	[Boarders] [int] NULL,
	[Disab] [int] NULL,
	[Dropout] [int] NULL,
	[PSA] [int] NULL
) ON [PRIMARY]
GO

