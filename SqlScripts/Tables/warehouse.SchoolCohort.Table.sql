SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[SchoolCohort](
	[schNo] [nvarchar](50) NOT NULL,
	[surveyYear] [int] NULL,
	[Estimate] [int] NULL,
	[SurveyDimensionID] [int] NULL,
	[YearOfEd] [int] NULL,
	[Enrol] [int] NULL,
	[Rep] [int] NULL,
	[RepNY] [int] NULL,
	[TroutNY] [int] NULL,
	[EnrolNYNextLevel] [int] NULL,
	[RepNYNextLevel] [int] NULL,
	[TrinNYNextLevel] [int] NULL
) ON [PRIMARY]
GO

