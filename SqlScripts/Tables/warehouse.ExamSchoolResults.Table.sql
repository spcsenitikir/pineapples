SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[ExamSchoolResults](
	[examID] [int] NOT NULL,
	[examCode] [nvarchar](10) NULL,
	[examYear] [int] NULL,
	[examName] [nvarchar](50) NULL,
	[StateID] [nvarchar](5) NULL,
	[State] [nvarchar](50) NULL,
	[schNo] [nvarchar](50) NULL,
	[Gender] [nvarchar](1) NULL,
	[standardID] [int] NULL,
	[standardCode] [nvarchar](20) NULL,
	[standardDesc] [nvarchar](100) NULL,
	[benchmarkID] [int] NULL,
	[benchmarkCode] [nvarchar](20) NULL,
	[achievementLevel] [int] NOT NULL,
	[achievementDesc] [nvarchar](200) NULL,
	[Candidates] [int] NULL
) ON [PRIMARY]
GO

