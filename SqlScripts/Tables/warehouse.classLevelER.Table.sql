SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[classLevelER](
	[SurveyYEar] [int] NOT NULL,
	[ClassLevel] [nvarchar](10) NULL,
	[YearOfEd] [smallint] NULL,
	[OfficialAge] [int] NULL,
	[enrolM] [int] NULL,
	[enrolF] [int] NULL,
	[enrol] [int] NULL,
	[repM] [int] NULL,
	[repF] [int] NULL,
	[rep] [int] NULL,
	[psaM] [int] NULL,
	[psaF] [int] NULL,
	[psa] [int] NULL,
	[intakeM] [int] NULL,
	[intakeF] [int] NULL,
	[intake] [int] NULL,
	[nEnrolM] [int] NULL,
	[nEnrolF] [int] NULL,
	[nEnrol] [int] NULL,
	[nRepM] [int] NULL,
	[nRepF] [int] NULL,
	[nRep] [int] NULL,
	[nIntakeM] [int] NULL,
	[nIntakeF] [int] NULL,
	[nIntake] [int] NULL,
	[popM] [int] NULL,
	[popF] [int] NULL,
	[pop] [int] NULL
) ON [PRIMARY]
GO

