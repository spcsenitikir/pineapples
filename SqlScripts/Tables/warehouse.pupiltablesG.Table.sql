SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[pupiltablesG](
	[schNo] [nvarchar](50) NOT NULL,
	[SurveyYear] [int] NOT NULL,
	[Estimate] [int] NOT NULL,
	[SurveyDimensionID] [int] NULL,
	[ClassLevel] [nvarchar](10) NULL,
	[Age] [int] NULL,
	[genderCode] [nvarchar](1) NOT NULL,
	[pt] [int] NULL,
	[ptCode] [nvarchar](20) NULL
) ON [PRIMARY]
GO

