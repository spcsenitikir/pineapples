SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[enrol](
	[schNo] [nvarchar](50) NOT NULL,
	[surveyYear] [int] NOT NULL,
	[Estimate] [int] NOT NULL,
	[SurveyDimensionID] [int] NULL,
	[ClassLevel] [nvarchar](10) NULL,
	[Age] [int] NULL,
	[GenderCode] [nvarchar](1) NOT NULL,
	[Enrol] [int] NULL,
	[Rep] [int] NULL,
	[Trin] [int] NULL,
	[Trout] [int] NULL,
	[Boarders] [int] NULL,
	[Disab] [int] NULL,
	[Dropout] [int] NULL,
	[PSA] [int] NULL
) ON [PRIMARY]
GO

