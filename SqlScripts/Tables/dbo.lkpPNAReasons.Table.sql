SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpPNAReasons](
	[codeCode] [nvarchar](10) NOT NULL,
	[codeDescription] [nvarchar](50) NULL,
	[codeSeq] [int] NULL,
	[codeDescriptionL1] [nvarchar](50) NULL,
	[codeDescriptionL2] [nvarchar](50) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpPNAReasons_PK] PRIMARY KEY NONCLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpPNAReasons] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpPNAReasons] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpPNAReasons] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpPNAReasons] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpPNAReasons] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[lkpPNAReasons] ADD  CONSTRAINT [DF__lkpPNARea__codeS__344CE440]  DEFAULT ((0)) FOR [codeSeq]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Principal reason for Pupils Not Attending Vanuatu 2010 ff.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpPNAReasons'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Enrolments' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpPNAReasons'
GO

