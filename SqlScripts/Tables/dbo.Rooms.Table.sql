SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rooms](
	[rmID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NULL,
	[rmType] [nvarchar](10) NULL,
	[rmQualityCode] [nvarchar](50) NULL,
	[rmNo] [smallint] NULL,
	[rmTitle] [nvarchar](50) NULL,
	[rmSchLevel] [nvarchar](50) NULL,
	[rmLength] [float] NULL,
	[rmWidth] [float] NULL,
	[rmSize] [float] NULL,
	[rmYear] [int] NULL,
	[rmMaterials] [nvarchar](1) NULL,
	[rmMaterialRoof] [nvarchar](50) NULL,
	[rmMaterialFloor] [nvarchar](50) NULL,
	[rmCondition] [nvarchar](1) NULL,
	[rmUse] [nvarchar](200) NULL,
	[rmSecureDoor] [smallint] NULL,
	[rmSecureWindows] [smallint] NULL,
	[rmBooks] [int] NULL,
	[rmBooksRef] [int] NULL,
	[rmVCR] [int] NULL,
	[rmVideos] [int] NULL,
	[rmComputers] [int] NULL,
	[rmElec] [bit] NULL,
	[rmGas] [bit] NULL,
	[rmWater] [bit] NULL,
	[rmChemStore] [bit] NULL,
	[rmEquipStore] [bit] NULL,
	[rmCapM] [smallint] NULL,
	[rmCapF] [smallint] NULL,
	[rmShowersM] [smallint] NULL,
	[rmShowersF] [smallint] NULL,
	[rmToiletsM] [smallint] NULL,
	[rmToiletsF] [int] NULL,
	[rmLaundry] [smallint] NULL,
	[rmStoveGas] [smallint] NULL,
	[rmStoveWood] [smallint] NULL,
	[rmStoveElec] [smallint] NULL,
	[rmLockedStore] [smallint] NULL,
	[rmRefrig] [smallint] NULL,
	[rmLevel] [nvarchar](10) NULL,
	[rmShareType] [nvarchar](10) NULL,
	[rmSubject] [nvarchar](50) NULL,
	[rmWKTools] [int] NULL,
	[rmWKMaterials] [int] NULL,
	[rmWKEquipment] [int] NULL,
	[rmWKBenches] [int] NULL,
	[rmLunchDay] [int] NULL,
	[rmLunchBoarder] [int] NULL,
	[rmLocation] [nvarchar](50) NULL,
	[rmGUID] [uniqueidentifier] NULL,
	[brevID] [int] NULL,
	[rmKitchen] [smallint] NULL,
	[rmBeds] [int] NULL,
	[rmCupboards] [int] NULL,
	[rmMatresses] [int] NULL,
 CONSTRAINT [aaaaaRooms1_PK] PRIMARY KEY NONCLUSTERED 
(
	[rmID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[Rooms] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[Rooms] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[Rooms] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Rooms] TO [pSchoolWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[Rooms] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_Rooms_SSID_Type] ON [dbo].[Rooms]
(
	[ssID] ASC,
	[rmType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Rooms] ADD  CONSTRAINT [DF__Rooms__ssID__59C55456]  DEFAULT ((0)) FOR [ssID]
GO
ALTER TABLE [dbo].[Rooms] ADD  CONSTRAINT [DF__Rooms__rmNo__5AB9788F]  DEFAULT ((0)) FOR [rmNo]
GO
ALTER TABLE [dbo].[Rooms] ADD  CONSTRAINT [DF__Rooms__rmSize__5BAD9CC8]  DEFAULT ((0)) FOR [rmSize]
GO
ALTER TABLE [dbo].[Rooms] ADD  CONSTRAINT [DF__Rooms__rmYear__5CA1C101]  DEFAULT ((0)) FOR [rmYear]
GO
ALTER TABLE [dbo].[Rooms] ADD  CONSTRAINT [DF__Rooms__rmBooks__5D95E53A]  DEFAULT ((0)) FOR [rmBooks]
GO
ALTER TABLE [dbo].[Rooms] ADD  CONSTRAINT [DF__Rooms__rmBooksRe__5E8A0973]  DEFAULT ((0)) FOR [rmBooksRef]
GO
ALTER TABLE [dbo].[Rooms] ADD  CONSTRAINT [DF__Rooms__rmVCR__5F7E2DAC]  DEFAULT ((0)) FOR [rmVCR]
GO
ALTER TABLE [dbo].[Rooms] ADD  CONSTRAINT [DF__Rooms__rmVideos__607251E5]  DEFAULT ((0)) FOR [rmVideos]
GO
ALTER TABLE [dbo].[Rooms] ADD  CONSTRAINT [DF__Rooms__rmCompute__6166761E]  DEFAULT ((0)) FOR [rmComputers]
GO
ALTER TABLE [dbo].[Rooms] ADD  CONSTRAINT [DF__Rooms__rmElec__625A9A57]  DEFAULT ((0)) FOR [rmElec]
GO
ALTER TABLE [dbo].[Rooms] ADD  CONSTRAINT [DF__Rooms__rmGas__634EBE90]  DEFAULT ((0)) FOR [rmGas]
GO
ALTER TABLE [dbo].[Rooms] ADD  CONSTRAINT [DF__Rooms__rmWater__6442E2C9]  DEFAULT ((0)) FOR [rmWater]
GO
ALTER TABLE [dbo].[Rooms] ADD  CONSTRAINT [DF__Rooms__rmChemSto__65370702]  DEFAULT ((0)) FOR [rmChemStore]
GO
ALTER TABLE [dbo].[Rooms] ADD  CONSTRAINT [DF__Rooms__rmEquipSt__662B2B3B]  DEFAULT ((0)) FOR [rmEquipStore]
GO
ALTER TABLE [dbo].[Rooms]  WITH CHECK ADD  CONSTRAINT [FK_Rooms_BuildingReview] FOREIGN KEY([brevID])
REFERENCES [dbo].[BuildingReview] ([brevID])
ON UPDATE CASCADE
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Rooms] CHECK CONSTRAINT [FK_Rooms_BuildingReview]
GO
ALTER TABLE [dbo].[Rooms]  WITH CHECK ADD  CONSTRAINT [FK_Rooms_SchoolSurvey] FOREIGN KEY([ssID])
REFERENCES [dbo].[SchoolSurvey] ([ssID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Rooms] CHECK CONSTRAINT [FK_Rooms_SchoolSurvey]
GO
ALTER TABLE [dbo].[Rooms]  WITH CHECK ADD  CONSTRAINT [CK_Rooms] CHECK  (([rmLength]>=(0)))
GO
ALTER TABLE [dbo].[Rooms] CHECK CONSTRAINT [CK_Rooms]
GO
ALTER TABLE [dbo].[Rooms]  WITH CHECK ADD  CONSTRAINT [CK_Rooms_1] CHECK  (([rmWidth]>=(0)))
GO
ALTER TABLE [dbo].[Rooms] CHECK CONSTRAINT [CK_Rooms_1]
GO
ALTER TABLE [dbo].[Rooms]  WITH CHECK ADD  CONSTRAINT [CK_Rooms_2] CHECK  (([rmSize]>=(0)))
GO
ALTER TABLE [dbo].[Rooms] CHECK CONSTRAINT [CK_Rooms_2]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Room data collected on surveys. May come frm various components and formats - classroom entry grid, admin rooms list, Dormitory grtid, Kitchen/Dining/Lab etc. 
Room type and ssID are the foreign keys.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Rooms'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Rooms' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Rooms'
GO

