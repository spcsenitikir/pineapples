SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EnrolmentProjection](
	[epID] [int] IDENTITY(1,1) NOT NULL,
	[escnID] [int] NULL,
	[epYear] [int] NULL,
	[dID] [nvarchar](10) NULL,
	[schNo] [nvarchar](50) NULL,
	[epLocked] [bit] NOT NULL,
	[epAggregate] [int] NULL,
 CONSTRAINT [aaaaaEnrolmentProjection1_PK] PRIMARY KEY NONCLUSTERED 
(
	[epID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[EnrolmentProjection] TO [pEnrolmentRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[EnrolmentProjection] TO [pEnrolmentWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[EnrolmentProjection] TO [pEnrolmentWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[EnrolmentProjection] TO [pEnrolmentWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[EnrolmentProjection] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_EnrolmentProjection_SchNo] ON [dbo].[EnrolmentProjection]
(
	[schNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_EnrolmentProjection_Scn_Year] ON [dbo].[EnrolmentProjection]
(
	[escnID] ASC,
	[epYear] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EnrolmentProjection] ADD  CONSTRAINT [DF__Enrolment__escnI__0FB750B3]  DEFAULT ((0)) FOR [escnID]
GO
ALTER TABLE [dbo].[EnrolmentProjection] ADD  CONSTRAINT [DF__Enrolment__epYea__10AB74EC]  DEFAULT ((0)) FOR [epYear]
GO
ALTER TABLE [dbo].[EnrolmentProjection] ADD  CONSTRAINT [DF_EnrolmentProjection_epLocked]  DEFAULT ((0)) FOR [epLocked]
GO
ALTER TABLE [dbo].[EnrolmentProjection]  WITH CHECK ADD  CONSTRAINT [EnrolmentProjection_FK00] FOREIGN KEY([escnID])
REFERENCES [dbo].[EnrolmentScenario] ([escnID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[EnrolmentProjection] CHECK CONSTRAINT [EnrolmentProjection_FK00]
GO
ALTER TABLE [dbo].[EnrolmentProjection]  WITH CHECK ADD  CONSTRAINT [EnrolmentProjection_FK01] FOREIGN KEY([schNo])
REFERENCES [dbo].[Schools] ([schNo])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[EnrolmentProjection] CHECK CONSTRAINT [EnrolmentProjection_FK01]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Enrolment Projection represents a projection for a single year, at the level of school , district or national. escnID is the foreign key , associating the EnrolmentProjection to its parent EnrolmentScenario. Each EnrolmentProjection needs one or more EnrolmentProjectionData, wher ethe projection number are stored.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EnrolmentProjection'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Enrolment Scenario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EnrolmentProjection'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Enrolment Projection' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EnrolmentProjection'
GO

