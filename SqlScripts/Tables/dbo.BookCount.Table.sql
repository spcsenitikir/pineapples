SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BookCount](
	[bkcntID] [int] IDENTITY(1,1) NOT NULL,
	[bkcntDate] [datetime] NOT NULL,
	[bkcntLocType] [nvarchar](10) NOT NULL,
	[bkcntLoc] [nvarchar](50) NOT NULL,
	[bkCode] [nvarchar](20) NOT NULL,
	[bkcntQty] [int] NOT NULL,
	[bkcntReplace] [int] NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [PK_BookCount] PRIMARY KEY CLUSTERED 
(
	[bkcntID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[BookCount] TO [pSchoolRead] AS [dbo]
GO
GRANT INSERT ON [dbo].[BookCount] TO [pSchoolRead] AS [dbo]
GO
GRANT SELECT ON [dbo].[BookCount] TO [pSchoolRead] AS [dbo]
GO
GRANT UPDATE ON [dbo].[BookCount] TO [pSchoolRead] AS [dbo]
GO
ALTER TABLE [dbo].[BookCount]  WITH CHECK ADD  CONSTRAINT [FK_BookCount_Books] FOREIGN KEY([bkCode])
REFERENCES [dbo].[Books] ([bkCode])
GO
ALTER TABLE [dbo].[BookCount] CHECK CONSTRAINT [FK_BookCount_Books]
GO

