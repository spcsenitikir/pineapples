SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[schoolTeachers](
	[SurveyYear] [int] NOT NULL,
	[SchNo] [nvarchar](50) NULL,
	[Role] [nvarchar](50) NULL,
	[GenderCode] [nvarchar](1) NULL,
	[NumTeachers] [int] NULL
) ON [PRIMARY]
GO

