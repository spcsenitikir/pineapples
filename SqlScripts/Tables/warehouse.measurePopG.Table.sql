SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[measurePopG](
	[popYear] [int] NULL,
	[dID] [nvarchar](5) NULL,
	[popAge] [int] NULL,
	[genderCode] [nvarchar](1) NOT NULL,
	[pop] [int] NULL
) ON [PRIMARY]
GO

