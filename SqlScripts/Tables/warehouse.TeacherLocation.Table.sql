SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[TeacherLocation](
	[TID] [int] NOT NULL,
	[SurveyYear] [int] NOT NULL,
	[GenderCode] [nvarchar](1) NULL,
	[BirthYear] [int] NULL,
	[AgeGroup] [varchar](5) NULL,
	[ApptSchNo] [nvarchar](50) NULL,
	[ApptRole] [nvarchar](50) NULL,
	[ApptYear] [int] NULL,
	[ApptSchEstimate] [int] NULL,
	[SurveySchNo] [nvarchar](50) NULL,
	[SurveyRole] [nvarchar](50) NULL,
	[SurveyDataYear] [int] NULL,
	[Estimate] [int] NULL,
	[SurveySector] [nvarchar](3) NULL,
	[SchNo] [nvarchar](50) NULL,
	[Source] [varchar](3) NULL,
	[Role] [nvarchar](50) NULL,
	[Sector] [nvarchar](3) NULL,
	[Certified] [int] NULL,
	[Qualified] [int] NULL,
	[XtraSurvey] [int] NULL
) ON [PRIMARY]
GO

