SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EducationSectors](
	[secCode] [nvarchar](3) NOT NULL,
	[secDesc] [nvarchar](50) NULL,
	[secYears] [int] NULL,
	[secMinAge] [int] NULL,
	[secDescL1] [nvarchar](50) NULL,
	[secDescL2] [nvarchar](50) NULL,
	[secSort] [int] NULL,
	[secGLSalaries] [nvarchar](50) NULL,
 CONSTRAINT [aaaaaEducationSectors1_PK] PRIMARY KEY NONCLUSTERED 
(
	[secCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[EducationSectors] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[EducationSectors] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[EducationSectors] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[EducationSectors] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[EducationSectors] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[EducationSectors] ADD  CONSTRAINT [DF__Education__secYe__2A164134]  DEFAULT ((0)) FOR [secYears]
GO
ALTER TABLE [dbo].[EducationSectors] ADD  CONSTRAINT [DF__Education__secMi__2B0A656D]  DEFAULT ((0)) FOR [secMinAge]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Used to order sectors in reporting; e.g. Establishment Reports where SEC should be before PRI and ECE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EducationSectors', @level2type=N'COLUMN',@level2name=N'secSort'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'GL account code for salaries in this sector. UIsed primarily in Payroll interface.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EducationSectors', @level2type=N'COLUMN',@level2name=N'secGLSalaries'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Education sectors are part of the education system focused on particular curriculum and outcomes. Education Sectos may be chronolgically parallel e.g. Senior Secondary and TVET.
Sectors are the principal means of disagregating finance data.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EducationSectors'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Education System' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EducationSectors'
GO

