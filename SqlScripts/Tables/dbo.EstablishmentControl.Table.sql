SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EstablishmentControl](
	[estcYear] [int] NOT NULL,
	[escnID] [int] NULL,
	[estcBaseYear] [int] NULL,
	[estcSizeFactor] [float] NOT NULL,
	[estcAuthorityDate] [datetime] NULL,
	[estcSnapshotDate] [datetime] NULL,
	[estcIncrementPoints] [int] NOT NULL,
	[estcNominalPointAdjust] [int] NOT NULL,
	[estcRetainUnderpays] [int] NOT NULL,
	[estcAuthorityPositionPer] [int] NOT NULL,
	[estcAuthorityPositionSalaryPoint] [nvarchar](10) NULL,
	[estcAuthorityPositionRoundUpBy] [int] NOT NULL,
	[estcTerminateDate] [datetime] NULL,
 CONSTRAINT [PK_EstablishmentControl] PRIMARY KEY CLUSTERED 
(
	[estcYear] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[EstablishmentControl] TO [pEstablishmentRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[EstablishmentControl] TO [pEstablishmentWriteX] AS [dbo]
GO
GRANT INSERT ON [dbo].[EstablishmentControl] TO [pEstablishmentWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[EstablishmentControl] TO [pEstablishmentWriteX] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[EstablishmentControl] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[EstablishmentControl] ADD  CONSTRAINT [DF_EstablishmentControl_estcSizeFactor]  DEFAULT ((1)) FOR [estcSizeFactor]
GO
ALTER TABLE [dbo].[EstablishmentControl] ADD  CONSTRAINT [DF_EstablishmentControl_estcIncrementPoints]  DEFAULT ((1)) FOR [estcIncrementPoints]
GO
ALTER TABLE [dbo].[EstablishmentControl] ADD  CONSTRAINT [DF_EstablishmentControl_estcNominalPointAdjust]  DEFAULT ((0)) FOR [estcNominalPointAdjust]
GO
ALTER TABLE [dbo].[EstablishmentControl] ADD  CONSTRAINT [DF_EstablishmentControl_estcRetainUnderpays]  DEFAULT ((0)) FOR [estcRetainUnderpays]
GO
ALTER TABLE [dbo].[EstablishmentControl] ADD  CONSTRAINT [DF_EstablishmentControl_estcAuthorityPositionsPer]  DEFAULT ((0)) FOR [estcAuthorityPositionPer]
GO
ALTER TABLE [dbo].[EstablishmentControl] ADD  CONSTRAINT [DF_EstablishmentControl_estcAuthorityPositionRoundUpBy]  DEFAULT ((0)) FOR [estcAuthorityPositionRoundUpBy]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date to use to terminate positions that are closed by this plan. DEfaults to 31/12/(year-1)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EstablishmentControl', @level2type=N'COLUMN',@level2name=N'estcTerminateDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Single record for each year, keyed by year. Holds the varius parameters used in generating and costing an Establishment Plan. ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EstablishmentControl'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessUI', @value=N'frmEstablishmentControl is a modal dialog displayed frm the establishment planner.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EstablishmentControl'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Establishment Planning' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EstablishmentControl'
GO

