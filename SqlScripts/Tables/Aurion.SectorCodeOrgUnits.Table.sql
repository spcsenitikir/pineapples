SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Aurion].[SectorCodeOrgUnits](
	[schNo] [nvarchar](50) NOT NULL,
	[secCode] [nvarchar](50) NOT NULL,
	[orgUnitNumber] [int] NOT NULL,
 CONSTRAINT [PK_SectorCodeOrgUnits] PRIMARY KEY CLUSTERED 
(
	[schNo] ASC,
	[secCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Mapping table for Aurion Org Units to School/Secor code combinations.' , @level0type=N'SCHEMA',@level0name=N'Aurion', @level1type=N'TABLE',@level1name=N'SectorCodeOrgUnits'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Aurion' , @level0type=N'SCHEMA',@level0name=N'Aurion', @level1type=N'TABLE',@level1name=N'SectorCodeOrgUnits'
GO

