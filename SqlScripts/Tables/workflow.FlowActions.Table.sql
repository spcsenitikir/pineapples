SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [workflow].[FlowActions](
	[wfqID] [int] IDENTITY(1,1) NOT NULL,
	[flowID] [int] NOT NULL,
	[wfqStatus] [nvarchar](10) NOT NULL,
	[actID] [int] NOT NULL,
	[wfqSort] [int] NULL,
 CONSTRAINT [metawrkFlowActions_PK] PRIMARY KEY NONCLUSTERED 
(
	[wfqID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [actID] ON [workflow].[FlowActions]
(
	[actID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [flowID] ON [workflow].[FlowActions]
(
	[flowID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [workflow].[FlowActions]  WITH CHECK ADD  CONSTRAINT [FK_FlowActions_Actions] FOREIGN KEY([actID])
REFERENCES [workflow].[Actions] ([actID])
GO
ALTER TABLE [workflow].[FlowActions] CHECK CONSTRAINT [FK_FlowActions_Actions]
GO
ALTER TABLE [workflow].[FlowActions]  WITH CHECK ADD  CONSTRAINT [FK_FlowActions_Flows] FOREIGN KEY([flowID])
REFERENCES [workflow].[Flows] ([flowID])
GO
ALTER TABLE [workflow].[FlowActions] CHECK CONSTRAINT [FK_FlowActions_Flows]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The set of Action that defines a workflow.
At any given statis of the POR , one Action is the next action to perform. 
In FlowActions, an ActionId is associated to a Flow, and Status.

Each action can have a set of Outcomes - each outcome defines a new status for the POR. This makes a new action the next in the sequence.' , @level0type=N'SCHEMA',@level0name=N'workflow', @level1type=N'TABLE',@level1name=N'FlowActions'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessDB', @value=N'linked/bound. Note that the schema is represented by the underscore in the table name:

workflow_FlowActions' , @level0type=N'SCHEMA',@level0name=N'workflow', @level1type=N'TABLE',@level1name=N'FlowActions'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessUI', @value=N'Workflow designer' , @level0type=N'SCHEMA',@level0name=N'workflow', @level1type=N'TABLE',@level1name=N'FlowActions'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Workflow and POR' , @level0type=N'SCHEMA',@level0name=N'workflow', @level1type=N'TABLE',@level1name=N'FlowActions'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'POR' , @level0type=N'SCHEMA',@level0name=N'workflow', @level1type=N'TABLE',@level1name=N'FlowActions'
GO

