SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lists](
	[lstName] [nvarchar](20) NOT NULL,
	[lstDescription] [nvarchar](50) NULL,
	[lstOwner] [nvarchar](20) NULL,
 CONSTRAINT [aaaaaLists1_PK] PRIMARY KEY NONCLUSTERED 
(
	[lstName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[Lists] TO [pSchoolReadX] AS [dbo]
GO
GRANT DELETE ON [dbo].[Lists] TO [pSchoolWriteX] AS [dbo]
GO
GRANT INSERT ON [dbo].[Lists] TO [pSchoolWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Lists] TO [pSchoolWriteX] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[Lists] TO [public] AS [dbo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 16 11 2009
-- Description:	rebuild the cross tab for Lists whever a change is made
-- =============================================
CREATE TRIGGER [dbo].[Trigger_Lists_RebuildListXTab]
   ON  [dbo].[Lists] 
   AFTER INSERT,DELETE,UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- this sp now has EXECUTE AS 'pineapples'
	exec dbo.MakeListXTB

END
GO
ALTER TABLE [dbo].[Lists] ENABLE TRIGGER [Trigger_Lists_RebuildListXTab]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Lists hold the names of adhoc lists of schools. These named are pivoted in ListXtab, which is in turn joined into DimensionSchoolSurveyNoYear view. This means that the names in Lists appear as field names in every pivot with school data.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Lists'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Schools' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Lists'
GO

