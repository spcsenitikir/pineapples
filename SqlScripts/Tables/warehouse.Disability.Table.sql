SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[Disability](
	[schNo] [nvarchar](50) NOT NULL,
	[SurveyYear] [int] NOT NULL,
	[Estimate] [int] NOT NULL,
	[SurveyDimensionID] [int] NULL,
	[ClassLevel] [nvarchar](10) NULL,
	[GenderCode] [nvarchar](1) NOT NULL,
	[disCode] [nvarchar](50) NULL,
	[Disab] [int] NULL,
	[Disability] [nvarchar](50) NULL
) ON [PRIMARY]
GO

