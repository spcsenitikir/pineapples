SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Switchboard Items](
	[SwitchboardID] [int] NOT NULL,
	[ItemNumber] [smallint] NOT NULL,
	[ItemText] [nvarchar](255) NULL,
	[Command] [smallint] NULL,
	[Argument] [nvarchar](255) NULL,
	[PermissionGroup] [nvarchar](50) NULL,
	[ItemTextL1] [nvarchar](255) NULL,
	[ItemTextL2] [nvarchar](50) NULL,
 CONSTRAINT [aaaaaSwitchboard Items1_PK] PRIMARY KEY NONCLUSTERED 
(
	[SwitchboardID] ASC,
	[ItemNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[Switchboard Items] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[Switchboard Items] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Switchboard Items] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[Switchboard Items] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[Switchboard Items] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[Switchboard Items] ADD  CONSTRAINT [DF__Switchboa__ItemN__1CBC4616]  DEFAULT ((0)) FOR [ItemNumber]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'May hold a user defined menu structure. Practice now is not to use this iother than to point to the Default menu in the local table "PineapplesSwitchboardDefs"' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Switchboard Items'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Configuration' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Switchboard Items'
GO

