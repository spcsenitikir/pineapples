SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [audit].[AuditRow](
	[arowID] [int] IDENTITY(1,1) NOT NULL,
	[auditID] [int] NOT NULL,
	[arowKeyS] [nvarchar](50) NULL,
	[arowKey] [int] NULL,
	[arowName] [nvarchar](100) NULL,
	[arowSendType] [nvarchar](10) NULL,
	[arowSendBatch] [int] NULL,
 CONSTRAINT [PK_Table1] PRIMARY KEY CLUSTERED 
(
	[arowID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [audit].[AuditRow]  WITH CHECK ADD  CONSTRAINT [FK_AuditRow_AuditLog] FOREIGN KEY([auditID])
REFERENCES [audit].[AuditLog] ([auditID])
GO
ALTER TABLE [audit].[AuditRow] CHECK CONSTRAINT [FK_AuditRow_AuditLog]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This is used for DELETES, when the primary key may not help us much to know what got deleted.' , @level0type=N'SCHEMA',@level0name=N'audit', @level1type=N'TABLE',@level1name=N'AuditRow', @level2type=N'COLUMN',@level2name=N'arowName'
GO

