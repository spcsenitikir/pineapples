SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[bestSurvey](
	[SurveyYear] [int] NOT NULL,
	[schNo] [nvarchar](50) NOT NULL,
	[surveyDimensionID] [int] NULL
) ON [PRIMARY]
GO

