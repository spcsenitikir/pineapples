SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[textbookCounts](
	[SchNo] [nvarchar](50) NOT NULL,
	[SurveyYear] [int] NOT NULL,
	[Resource] [nvarchar](50) NULL,
	[Number] [int] NULL,
	[Condition] [smallint] NULL,
	[ClassLevel] [nvarchar](50) NULL,
	[Estimate] [int] NULL
) ON [PRIMARY]
GO

