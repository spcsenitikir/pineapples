SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[SurveyYearRank](
	[SurveyYear] [int] NOT NULL,
	[schNo] [nvarchar](50) NOT NULL,
	[rankDistrict] [nvarchar](10) NULL,
	[rankSchType] [nvarchar](10) NOT NULL,
	[Enrol] [int] NULL,
	[EnrolM] [int] NULL,
	[EnrolF] [int] NULL,
	[Estimate] [bit] NULL,
	[rankDistrictSchoolType] [int] NULL,
	[rankDistrictSchoolTypeDec] [nvarchar](2) NULL,
	[rankDistrictSchoolTypeQtl] [nvarchar](2) NULL,
	[rankSchoolType] [int] NULL,
	[rankSchoolTypeDec] [nvarchar](2) NULL,
	[rankSchoolTypeQtl] [nvarchar](2) NULL
) ON [PRIMARY]
GO

