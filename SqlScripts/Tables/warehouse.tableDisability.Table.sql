SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[tableDisability](
	[SurveyYear] [int] NOT NULL,
	[Estimate] [int] NOT NULL,
	[ClassLevel] [nvarchar](10) NULL,
	[GenderCode] [nvarchar](1) NOT NULL,
	[DistrictCode] [nvarchar](5) NULL,
	[AuthorityCode] [nvarchar](10) NULL,
	[SchoolTypeCode] [nvarchar](10) NULL,
	[DisCode] [nvarchar](50) NULL,
	[Disability] [nvarchar](50) NULL,
	[Disab] [int] NULL
) ON [PRIMARY]
GO

