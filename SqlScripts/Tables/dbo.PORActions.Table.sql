SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PORActions](
	[pactID] [int] IDENTITY(1,1) NOT NULL,
	[porID] [int] NOT NULL,
	[pactDate] [datetime] NULL,
	[pactUser] [nvarchar](50) NULL,
	[actID] [int] NULL,
	[pactOutcome] [nvarchar](50) NULL,
	[pactNote] [ntext] NULL,
 CONSTRAINT [PORActions_PK] PRIMARY KEY NONCLUSTERED 
(
	[pactID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[PORActions] TO [pEstablishmentRead] AS [dbo]
GO
GRANT INSERT ON [dbo].[PORActions] TO [pEstablishmentRead] AS [dbo]
GO
GRANT UPDATE ON [dbo].[PORActions] TO [pEstablishmentRead] AS [dbo]
GO
GRANT SELECT ON [dbo].[PORActions] TO [pTeacherReadX] AS [dbo]
GO
GRANT DELETE ON [dbo].[PORActions] TO [pTeacherWriteX] AS [dbo]
GO
GRANT INSERT ON [dbo].[PORActions] TO [pTeacherWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[PORActions] TO [pTeacherWriteX] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[PORActions] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [actID] ON [dbo].[PORActions]
(
	[actID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [POR_FK] ON [dbo].[PORActions]
(
	[porID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PORActions] ADD  CONSTRAINT [DF__PORAction__actID__480696CE]  DEFAULT ((0)) FOR [actID]
GO
ALTER TABLE [dbo].[PORActions]  WITH CHECK ADD  CONSTRAINT [FK_PORActions_Actions] FOREIGN KEY([actID])
REFERENCES [workflow].[Actions] ([actID])
GO
ALTER TABLE [dbo].[PORActions] CHECK CONSTRAINT [FK_PORActions_Actions]
GO
ALTER TABLE [dbo].[PORActions]  WITH CHECK ADD  CONSTRAINT [FK_PORActions_POR] FOREIGN KEY([porID])
REFERENCES [dbo].[POR] ([porID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PORActions] CHECK CONSTRAINT [FK_PORActions_POR]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The hsitory of actions - or steps - performed in processing a POR. The outcome of each step may change the status of the POR , determining the next step.  PORID is a foreign key.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PORActions'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'POR' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PORActions'
GO

