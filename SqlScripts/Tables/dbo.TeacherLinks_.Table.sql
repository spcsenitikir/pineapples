SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeacherLinks_](
	[lnkID] [int] IDENTITY(1,1) NOT NULL,
	[tID] [int] NOT NULL,
	[docID] [uniqueidentifier] NOT NULL,
	[lnkFunction] [nvarchar](20) NULL,
	[lnkHidden] [int] NOT NULL,
	[pCreateTag] [uniqueidentifier] NULL,
 CONSTRAINT [PK_TeacherLinks_] PRIMARY KEY CLUSTERED 
(
	[lnkID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_TeacherLinks_] ON [dbo].[TeacherLinks_]
(
	[tID] ASC,
	[docID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TeacherLinks_] ADD  CONSTRAINT [DF_Table_2_dlnkHidden]  DEFAULT ((0)) FOR [lnkHidden]
GO
ALTER TABLE [dbo].[TeacherLinks_] ADD  DEFAULT (newid()) FOR [pCreateTag]
GO
ALTER TABLE [dbo].[TeacherLinks_]  WITH CHECK ADD  CONSTRAINT [FK_TeacherLinks__lkpTeacherLinkTypes] FOREIGN KEY([lnkFunction])
REFERENCES [dbo].[lkpTeacherLinkTypes] ([codeCode])
GO
ALTER TABLE [dbo].[TeacherLinks_] CHECK CONSTRAINT [FK_TeacherLinks__lkpTeacherLinkTypes]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Link Key' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherLinks_', @level2type=N'COLUMN',@level2name=N'lnkID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Teacher Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherLinks_', @level2type=N'COLUMN',@level2name=N'tID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Document identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherLinks_', @level2type=N'COLUMN',@level2name=N'docID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Category of this document' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherLinks_', @level2type=N'COLUMN',@level2name=N'lnkFunction'
GO

