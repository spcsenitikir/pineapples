SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[EdLevelAltER](
	[SurveyYear] [int] NULL,
	[edLevelCode] [nvarchar](50) NULL,
	[enrolM] [int] NULL,
	[enrolF] [int] NULL,
	[enrol] [int] NULL,
	[repM] [int] NULL,
	[repF] [int] NULL,
	[rep] [int] NULL,
	[intakeM] [int] NULL,
	[intakeF] [int] NULL,
	[intake] [int] NULL,
	[nEnrolM] [int] NULL,
	[nEnrolF] [int] NULL,
	[nEnrol] [int] NULL,
	[nRepM] [int] NULL,
	[nRepF] [int] NULL,
	[nRep] [int] NULL,
	[nIntakeM] [int] NULL,
	[nIntakeF] [int] NULL,
	[nIntake] [int] NULL,
	[popM] [int] NULL,
	[popF] [int] NULL,
	[pop] [int] NULL,
	[firstYear] [int] NULL,
	[lastYear] [int] NULL,
	[numYears] [int] NULL
) ON [PRIMARY]
GO

