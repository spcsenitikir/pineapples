SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[measureSchoolCount](
	[NumSchools] [int] NULL,
	[SurveyYear] [int] NOT NULL,
	[AuthorityCode] [nvarchar](10) NULL,
	[DistrictCode] [nvarchar](5) NULL,
	[SchoolTypeCode] [nvarchar](10) NULL,
	[Region] [nvarchar](50) NULL
) ON [PRIMARY]
GO

