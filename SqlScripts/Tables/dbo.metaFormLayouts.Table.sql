SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[metaFormLayouts](
	[formName] [nvarchar](20) NOT NULL,
	[formDescription] [nvarchar](200) NULL,
	[formLayout] [xml] NULL,
 CONSTRAINT [PK_metaFormLayouts] PRIMARY KEY CLUSTERED 
(
	[formName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[metaFormLayouts] TO [pSurveyAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[metaFormLayouts] TO [pSurveyAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[metaFormLayouts] TO [pSurveyAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[metaFormLayouts] TO [pSurveyAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[metaFormLayouts] TO [pSurveyRead] AS [dbo]
GO

