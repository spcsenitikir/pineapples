SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Authorities](
	[authCode] [nvarchar](10) NOT NULL,
	[authName] [nvarchar](100) NULL,
	[authType] [nvarchar](1) NULL,
	[authContact] [nvarchar](50) NULL,
	[authAddr1] [nvarchar](50) NULL,
	[authAddr2] [nvarchar](50) NULL,
	[authAddr3] [nvarchar](50) NULL,
	[authAddr4] [nvarchar](50) NULL,
	[authPh1] [nvarchar](30) NULL,
	[authPh2] [nvarchar](30) NULL,
	[authFax] [nvarchar](30) NULL,
	[authEmail] [nvarchar](30) NULL,
	[authNameL1] [nvarchar](50) NULL,
	[authNameL2] [nvarchar](50) NULL,
	[authBank] [nvarchar](10) NULL,
	[authBSB] [nvarchar](10) NULL,
	[authAccountNo] [nvarchar](20) NULL,
	[authID] [int] IDENTITY(1,1) NOT NULL,
	[authOrgUnitNumber] [int] NULL,
	[authSuperOrgUnitNumber] [int] NULL,
 CONSTRAINT [aaaaaAuthorities1_PK] PRIMARY KEY NONCLUSTERED 
(
	[authCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[Authorities] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[Authorities] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Authorities] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[Authorities] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[Authorities] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[Authorities]  WITH CHECK ADD  CONSTRAINT [Authorities_FK00] FOREIGN KEY([authType])
REFERENCES [dbo].[lkpAuthorityType] ([codeCode])
GO
ALTER TABLE [dbo].[Authorities] CHECK CONSTRAINT [Authorities_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Authorities are organisations that run schools. Authorities may be the receipients of grants.
Authorities are grouped by type, and then as Govt/Non-govt. 
schAuth is a foreign key on the school table. 
Authority of a school may change over time - ssAuth is the Authority reported on the survey for any year.
Planned future changes to the Authority of a school can be recorded on SchoolEstablishment.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Authorities'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Authority Hierarchy' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Authorities'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Authorities' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Authorities'
GO

