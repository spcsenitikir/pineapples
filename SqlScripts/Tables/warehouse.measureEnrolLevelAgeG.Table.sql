SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[measureEnrolLevelAgeG](
	[SurveyYear] [int] NOT NULL,
	[Estimate] [int] NOT NULL,
	[ClassLevel] [nvarchar](10) NULL,
	[Age] [smallint] NULL,
	[genderCode] [nvarchar](1) NOT NULL,
	[Enrol] [int] NULL
) ON [PRIMARY]
GO

