SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ISCEDLevel](
	[ilCode] [nvarchar](10) NOT NULL,
	[ilName] [nvarchar](50) NULL,
 CONSTRAINT [aaaaaISCEDLevel1_PK] PRIMARY KEY NONCLUSTERED 
(
	[ilCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[ISCEDLevel] TO [pAdminAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[ISCEDLevel] TO [pAdminAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ISCEDLevel] TO [pAdminAdmin] AS [dbo]
GO
GRANT DELETE ON [dbo].[ISCEDLevel] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[ISCEDLevel] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[ISCEDLevel] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ISCEDLevel] TO [pAdminWrite] AS [dbo]
GO
GRANT DELETE ON [dbo].[ISCEDLevel] TO [pAdminWriteX] AS [dbo]
GO
GRANT INSERT ON [dbo].[ISCEDLevel] TO [pAdminWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ISCEDLevel] TO [pAdminWriteX] AS [dbo]
GO
GRANT SELECT ON [dbo].[ISCEDLevel] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[ISCEDLevel] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'UNSECO-defined ISCED Levels ISCED0, ISCED1, ISCED2, ISCED3 etc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISCEDLevel'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Class Levels' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISCEDLevel'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'ISCED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISCEDLevel'
GO

