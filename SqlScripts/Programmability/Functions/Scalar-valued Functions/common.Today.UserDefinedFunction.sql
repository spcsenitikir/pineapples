SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 24 4 2010
-- Description:	Return the current date without time part
-- =============================================
CREATE FUNCTION [common].[Today]
()
RETURNS datetime
AS
BEGIN


	-- Return the result of the function
	RETURN common.dropTime(getDate())

END
GO

