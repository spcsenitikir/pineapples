SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 18 10 2009
-- Description:	Date only from datetime
-- =============================================
CREATE FUNCTION [common].[dropTime]
(
	-- Add the parameters for the function here
	@theDate datetime = null
)
RETURNS datetime
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result datetime

	if (@theDate is null)
		select @theDate = getDate()

	-- Add the T-SQL statements to compute the return value here
	SELECT @Result = DATEADD(dd, DATEDIFF(dd,0,@theDate), 0)

	-- Return the result of the function
	RETURN @Result

END
GO

