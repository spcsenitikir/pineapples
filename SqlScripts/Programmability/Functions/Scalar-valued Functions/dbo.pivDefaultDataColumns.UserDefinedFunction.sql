SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 23 10 2013
-- Description:	Helper function fr pivot tables -
-- determines a default setting for 'DataColumns' parameter if non is supplied
-- =============================================
CREATE FUNCTION [dbo].[pivDefaultDataColumns]
(
	-- Add the parameters for the function here
	@DataColumns nvarchar(40)
)
RETURNS nvarchar(40)


AS
BEGIN
	-- Declare the return variable here
if (@DataColumns is null) begin
	Select @DataColumns = case when paramText in ('SLEMIS','CSEMIS','PLEMIS') then 'SOMALIA'
							else null end
	from SysParams
	WHERE paramName = 'APP_NAME'
end


	-- Return the result of the function
	RETURN @DataColumns

END
GO

