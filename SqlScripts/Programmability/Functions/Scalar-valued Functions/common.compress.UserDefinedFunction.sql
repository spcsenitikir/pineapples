SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 14 8 2015
-- Description:	compress field (used for teacher payslips to save space)
-- =============================================
CREATE FUNCTION [common].[compress]
(
	-- Add the parameters for the function here
	@in nvarchar(max)

)
RETURNS nvarchar(max)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @cx nvarchar(max)
	select @cx = replace(@in,'-----------------------------------------------------------------------------',char(1) + '-')
	select @cx = replace(@cx,space(21),char(1) + char(21))
	select @cx = replace(@cx,space(11),char(1) + char(11))
	select @cx = replace(@cx,space(6),char(1) + char(6))
	select @cx = replace(@cx,space(3),char(1) + char(3))
	return @cx

END
GO

