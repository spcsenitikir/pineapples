SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2009-09-12
-- Description:
-- =============================================
CREATE FUNCTION [dbo].[makeTeacherChangeAudit]
(
	-- Add the parameters for the function here
	@DSurname nvarchar(50), @ISurname nvarchar(50)
		,@DGiven nvarchar(50), @IGiven nvarchar(50)
		,@DDOB datetime, @IDOB datetime
		,@DSex nchar(1), @ISex nchar(1)
		,@DPayroll nvarchar(50), @IPayroll nvarchar(50)
		,@DProvident nvarchar(50), @IProvident nvarchar(50)
		,@DRegister nvarchar(50), @IRegister nvarchar(50)
		,@DDateRegister datetime, @IDateRegister datetime
		,@DDateRegisterEnd datetime, @IDateRegisterEnd datetime
		,@DSalaryPoint nvarchar(10), @ISalaryPoint nvarchar(10)
		,@DDatePSAppointed datetime,@IDatePSAppointed datetime
		,@DDatePSClosed datetime,@IDatePSClosed datetime


)
RETURNS xml
AS
BEGIN
	-- Declare the return variable here
	declare @xml xml

	declare @stubDate datetime
	select @StubDate = '1800-01-01'
	declare  @edits table
	(
		FieldName sysname
		, vbefore nvarchar(50)
		, vafter nvarchar(50)
	)

	IF (isnull(@DSurname,'') <> isnull(@ISurname,''))
		INSERT INTO @Edits
		SELECT 'tSurname', @DSurname, @ISurname

	IF (isnull(@DGiven,'') <> isnull(@IGiven,''))
		INSERT INTO @Edits
		SELECT 'tGiven', @DGiven, @IGiven

	IF (isnull(@DDoB,@StubDate) <> isnull(@IDoB,@StubDate))
		INSERT INTO @Edits
		SELECT 'tDOB', @DDOB, @IDOB

	IF (isnull(@DSex,'') <> isnull(@ISex,''))
		INSERT INTO @Edits
		SELECT 'tSex', @DSex, @ISex

	IF (isnull(@DPayroll,'') <> isnull(@IPayroll,''))
		INSERT INTO @Edits
		SELECT 'tPayroll', @DPayroll, @IPayroll

	IF (isnull(@DProvident,'') <> isnull(@IProvident,''))
		INSERT INTO @Edits
		SELECT 'tProvident', @DProvident, @IProvident

	IF (isnull(@DRegister,'') <> isnull(@IRegister,''))
		INSERT INTO @Edits
		SELECT 'tRegister', @DRegister, @IRegister

	IF (isnull(@DDateRegister,@StubDate) <> isnull(@IDateRegister,@StubDate))
		INSERT INTO @Edits
		SELECT 'tDateRegister', @DDateRegister, @IDateRegister

	IF (isnull(@DSalaryPoint,'') <> isnull(@ISalaryPoint,''))
		INSERT INTO @Edits
		SELECT 'tSalaryPoint', @DSalaryPoint, @ISalaryPoint

	IF (isnull(@DDateRegisterEnd,@StubDate) <> isnull(@IDateRegisterEnd,@StubDate))
		INSERT INTO @Edits
		SELECT 'tDateRegisterEnd', @DDateRegisterEnd, @IDateRegisterEnd

	IF (isnull(@DDatePSAppointed,@StubDate) <> isnull(@IDatePSAppointed,@StubDate))
		INSERT INTO @Edits
		SELECT 'tDatePSAppointed', @DDatePSAppointed, @IDatePSAppointed

	IF (isnull(@DDatePSClosed,@StubDate) <> isnull(@IDatePSClosed,@StubDate))
		INSERT INTO @Edits
		SELECT 'tDatePSClosed', @DDatePSClosed, @IDatePSClosed


	set @xml =
	(Select FieldName as [@Field]
	, vbefore as [@before]
	, vafter as [@after]
	from @edits
	FOR XML Path('Edit')
	)

		Set @xml =
		(
		Select @xml
		for XML PAth('Edits')
		)
	RETURN @xml

END
GO

