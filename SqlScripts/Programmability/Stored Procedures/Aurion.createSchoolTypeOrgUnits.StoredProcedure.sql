SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Aurion].[createSchoolTypeOrgUnits]
	-- Add the parameters for the stored procedure here
	@SendAll int = 0
	, @Nested int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

begin try

	If @Nested = 0
		create table #OrgUnits
		(
				OrgUnitNumber int
				, OrgUnitLevel int
				, OrgUnitName nvarchar(50)
				, OrgUnitShortName nvarchar(50)
				, OrgUnitLongDescription nvarchar(100)
				, OrgUnitParent int
				, OrgUnitReference nvarchar(12)
		)

	exec Aurion.createAuthorityOrgUnits @SendAll, 1   -- nested


	begin transaction
	-- list the schools that need an org unit number
		Select DISTINCT schAuth, schType
		INTO #tmpSTO
		FROM Establishment E
			INNER JOIN Schools S
				ON E.schNo = S.SchNo
			LEFT JOIN Aurion.SchoolTypeOrgUnits STOrg
				ON S.schAuth = STOrg.authCode
				AND S.schType = STOrg.stCode
		WHERE
			schAuth is not null
			AND schType is not null
			AND STOrg.OrgUnitNumber is null


		declare @ToAllocate int

		-- this is the number of org unit numbers we need
		Select @ToAllocate = count(*)
		from #tmpSTO

		-- this is where we'll put them
		declare @counters table
		(
		counter int
		, maxCounters int
		, seq int
		, char nvarchar(6)
		)

		IF (@ToAllocate > 0 ) begin
		-- get the numbers

			insert into @counters
			exec getCounter 'Aurion_OrgUnit', @ToAllocate


			INSERT INTO Aurion.SchoolTypeOrgUnits
			SELECT
				schAuth
				, schType
				, counter

			FROM
				(Select row_number() over (ORDER BY schAuth, schType) Pos
				,schAuth, schType
				from #tmpSTO
				) ORDERED
				INNER JOIN @counters C
					ON ORDERED.Pos = C.seq

		end

	commit transaction

	declare @TopOrgUnit int
	declare @TopOrgLevel int

	Select @TopOrgUnit = common.SysParamInt('Aurion_TopLevelOrgNumber',0)
	Select @TopOrgLevel = common.SysParamInt('Aurion_TopLevelOrgLevel',4)


	INSERT INTO #OrgUnits
	(
		OrgUnitNumber
		, OrgUnitLevel
		, OrgUnitName
		, OrgUnitShortName
		, ORgUnitLongDescription
		, OrgUnitParent
		, OrgUnitReference
	)
	SELECT
		STORg.OrgUnitNumber
		, @TopOrgLevel + 3
		, left(stDescription,25)
		, STOrg.stCode
		, stDEscription
		, Auth.authOrgUnitNumber
		, STOrg.stCode

	FROM  Aurion.SchoolTypeOrgUnits STOrg
		LEFT JOIN Authorities Auth
			ON STOrg.authCode = Auth.authCode
		LEFT JOIN SchoolTypes
			ON SchoolTypes.stCode = STORg.stCode
		LEFT JOIN #tmpSTO
			ON #tmpSTO.schAuth = STOrg.authCode
			AND #tmpSTO.schType = STOrg.stCode
	WHERE
--		STOrg.OrgUnitNumber is not null
--		AND
		(#tmpSTO.schAuth is not null
			OR @SendAll = 1)


	-- return the ones we added
	If (@Nested = 0) begin
		Select *
		FROM
			#OrgUnits
		ORDER BY orgUnitLevel
				, orgUnitNumber
		drop table #OrgUnits
	end

	drop table #tmpSTO

end try

--- catch block
	begin catch
		DECLARE @err int,
			@ErrorMessage NVARCHAR(4000),
			@ErrorSeverity INT,
			@ErrorState INT;

		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()

		print @ErrorMessage

		if @@trancount > 0
			begin
				rollback transaction
				select @errorMessage = @errorMessage + ' The transaction was rolled back.'
			end
		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		return @err
	end catch


END
GO

