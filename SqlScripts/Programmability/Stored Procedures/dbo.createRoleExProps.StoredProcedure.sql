SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[createRoleExProps]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @Role sysname
	declare @LastRole sysname
	select TOP 1 @Role = U.name
		from sys.sysusers u
		cross apply dbo.ParsePineapplesRoleName(U.name) RN
		WHERE RN.DataArea is not null
		and u.name like 'p%'
		ORDER BY U.NAme


	while @Role is not null
		begin
			select @LastRole = @Role
			begin try
				EXEC sys.sp_addextendedproperty
				@name = N'PineapplesUser',
				@value = N'CoreRole',
				@level0type = N'USER', @level0name = @Role
			end try
			begin catch
			end catch
			select @Role = null
			select TOP 1 @Role = U.name
				from sys.sysusers u
				cross apply dbo.ParsePineapplesRoleName(U.name) RN
				WHERE RN.DataArea is not null
				and u.name > @LastRole
				and u.name like 'p%'

				ORDER BY U.NAme

		end


END
GO

