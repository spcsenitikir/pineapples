SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brian Lewis	Name
-- Create date:   6 12 2007
-- Description:	 Creates a view that crosstabs all the resources in a given category
-- used for e.g. Laboratory pivot table
-- =============================================
CREATE PROCEDURE [dbo].[MakeResourceXTabView]
	-- Add the parameters for the stored procedure here
(
@ResourceCategory nvarchar(50),
@ViewName sysname
)
AS
BEGIN

	DECLARE @lists varchar(2000)
	DECLARE @listfields varchar(2000)

	declare @defs table
	(
		defcode nvarchar(50),
		defName nvarchar(50)
	)

	INSERT INTO @defs
	SELECT mresCode, mresName
	FROM TRMetaResourceDefs
	WHERE mresCat = @resourceCategory


	SET @lists = ''
	set @listfields = ''
	--// Get a list of the pivot columns that are important to you.

	SELECT @lists = @lists + '[' + convert(varchar(50),defName,101) + '],'
	FROM (SELECT Distinct defName FROM @defs) l
	SELECT @listfields = @listfields + '[' + convert(varchar(50),defName,101) + '] nvarchar(50),'
	FROM (SELECT Distinct defName FROM @defs) l

declare @cmd nvarchar(2000)
	--// Remove the trailing comma
if len(@lists) > 0
	begin
		SET @lists = LEFT(@lists, LEN(@lists) - 1)
		set @listfields = LEFT(@listfields, LEN(@listfields) - 1)
		--// Now execute the Select with the PIVOT and dynamically add the list
		--// of dates for the columns

		set @cmd = 'CREATE VIEW ' + @ViewName +' AS SELECT ssID, ' + @lists +
			' FROM (Select ssID, resName, resSplit,
			case when resAvail = 0 then 0 else 1 end Available
			from Resources) R
			PIVOT (MAX(Available) FOR resSplit IN (' + @lists + ')) p
			WHERE resName = ''' + @ResourceCategory + ''''
	end
else
	begin
		set @cmd = 'CREATE VIEW ' + @ViewName +' AS SELECT ssID from Resources'


	end
execute sp_executesql  @cmd
END
GO

