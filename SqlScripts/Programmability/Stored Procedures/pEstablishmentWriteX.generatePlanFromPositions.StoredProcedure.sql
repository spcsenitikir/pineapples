SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Create basic stored procedure template
-- =============================================


-- =============================================
-- Author:		Brian Lewis
-- Create date: 25 10 2009
-- Description:	Align an establishment pan to actual positions
-- =============================================
-- there are 4 things we may want to do here:
-- project only the current positions
-- project only the current filled positions

-- substitute a planned position for an existing position of the same role
-- (filled - would you want to do this if unfilled?)

-- substitute an existing position for one of the same salary band range
--
CREATE PROCEDURE [pEstablishmentWriteX].[generatePlanFromPositions]
	@SchoolNo nvarchar(50)
	, @Year int
	, @FilledOnly bit = 0
	, @PreserveRoleTotals bit = 0


AS

-- get the positions at the date


-- group them into totals by rolegrade

-- insert any rolegrades not in the current establishment plan

-- update all the rolegrades with the manual total (including 0)


BEGIN

DECLARE @err int,
	@ErrorMessage NVARCHAR(4000),
	@ErrorSeverity INT,
	@ErrorState INT;

declare @pos table
(
	estpNo nvarchar(20)
	, tID int
)

declare @roleGradeTotals table
(
	rgCode nvarchar(10)
	, NumPositions int

)

declare @roleTotalsPos table
(
	roleCode nvarchar(10)
	, NumPositions int

)

declare @roleTotalsPlan table
(
	roleCode nvarchar(10)
	, NumPositions int

)
declare @AsAtDate datetime
declare @estID int
	begin try


		begin transaction

	-- find the snapshot date for the etablishment year
		Select @AsAtDate = estcSnapshotDate
		FROM
			EstablishmentControl
			WHERE estcYear = @Year

	-- retrieve the position list on the snapshot date
		INSERT INTO @pos
		EXEC	[pEstablishmentRead].[EstablishmentFilter]
				@SummaryOnly = 2
				,@SchoolNo = @SchoolNo
				,@AsAtDate =@AsAtDate


		SELECT pos.*
			, RG.rgCode
		FROM
			@POS POS
			INNER JOIN Establishment E
			ON POS.estpNo = E.estpNo
			INNER JOIN RoleGrades RG
			on E.estpRoleGrade = RG.rgCode

		INSERT INTO @RoleGradeTotals
		SELECT RG.rgCode
			, count(POS.estpNo)
		FROM
			@POS POS
			INNER JOIN Establishment E
			ON POS.estpNo = E.estpNo
			INNER JOIN RoleGrades RG
			on E.estpRoleGrade = RG.rgCode
		WHERE
			(POS.tID is not null or @FilledOnly = 0)
		GROUP BY
			RG.rgCode


		SELECT @estID = estID
		FROM
			SchoolEstablishment
		WHERE
			schNo = @schoolNo
			AND estYear = @Year


		if (@estID is null) begin

			INSERT INTO SchoolYearHistory
			(schNo, syYear, systCode, syAuth, syParent, syDormant, syLocked)
			Select schNo
			, @Year
			, schType
			, schAuth
			, schPArent
			, schDormant
			, 1
			FROM Schools
			WHERE schNo = @SchoolNo


			select @estID = scope_identity()
			end
		else
			UPDATE SchoolEstablishment
			SET estLocked = 1
			WHERE estID = @estID


		If (@PreserveRoleTotals = 0) begin
			INSERT INTO SchoolEstablishmentRoles
				(estID , estrRoleGrade, estrCountUser)
			SELECT
				@estID
				, RGtot.rgCode
				, RGtot.NumPositions
			FROM @RoleGRadeTotals RGtot
			WHERE RGTot.rgCode not in
				(Select estrRoleGrade
					from SchoolEstablishmentRoles
					where estID = @estID)


			-- update the exisitng allocations
			UPDATE SchoolEstablishmentRoles
			SET
				estrCountUser = isnull(RGtot.NumPositions,0)
			FROM
				SchoolEstablishmentRoles
				LEFT JOIN @RoleGRadeTotals RGtot
				ON SchoolEstablishmentRoles.estrRoleGRade = RGtot.rgCode
			WHERE
				SchoolEstablishmentRoles.estId = @estID
		end

		if (@PreserveRoleTotals = 1) begin


			-- preserve role totals

			-- find the role totals for all rolegrades in positions that are not in plan
			-- totals by role
			INSERT INTO @RoleTotalsPos
			SELECT RG.roleCode
				, count(POS.estpNo)
			FROM
				@POS POS
				INNER JOIN Establishment E
				ON POS.estpNo = E.estpNo
				INNER JOIN RoleGrades RG
				on E.estpRoleGrade = RG.rgCode
			WHERE
				(POS.tID is not null or @FilledOnly = 0)
				and E.estpRoleGrade not in
					(Select estrRoleGrade
						from SchoolEstablishmentRoles
						where estID = @estID)

			GROUP BY
				RG.roleCode


			-- find the role totals for all rolegrades in the plan with no positions
			INSERT INTO @RoleTotalsPlan
			SELECT RG.roleCode
				, sum(SER.estrQuota)
			FROM
				SchoolEstablishmentRoles SER
				INNER JOIN RoleGrades RG
				on SER.estrRoleGrade = RG.rgCode
			WHERE
				SER.estID = @estID
				AND SER.estrRoleGrade not in
					(Select estpRoleGrade
						from @POS POS
							INNER JOIN Establishment
								ON Establishment.estpNo = POS.estpNo)

			GROUP BY
				RG.roleCode

			--inner join these on role when they are are equal
			Select TPlan.*
			INTO #tmp
			FROM
			@RoleTotalsPlan TPlan
			INNER JOIN @RoleTotalsPos TPos
				ON TPlan.roleCode = TPos.roleCode
				AND TPLan.NumPositions = TPos.NumPositions

			Select * from @RoleTotalsPos

			Select * from @RoleTotalsPlan
			Select * from #tmp

			UPDATE SchoolEstablishmentRoles
			SET estrCountUser = 0
			FROM SchoolEstablishmentRoles
				INNER JOIN RoleGrades RG
					ON SchoolEstablishmentRoles.estrRoleGrade = RG.rgCode
				INNER JOIN #tmp
					ON RG.roleCode = #tmp.RoleCode

			INSERT INTO SchoolEstablishmentRoles
			(estID, estrRoleGrade, estrCountUser)
			SELECT @estID
				, E.estpRoleGrade
				, count(POS.estpNo)
			FROM
				@POS POS
				INNER JOIN Establishment E
				ON POS.estpNo = E.estpNo
				INNER JOIN RoleGrades RG
				on E.estpRoleGrade = RG.rgCode
				INNER JOIN #tmp
					ON #tmp.roleCode = RG.roleCode
			GROUP BY
				E.estpRoleGrade
		end
		-- clear the planned ones and add the positions in


		commit transaction

	end try

--- catch block
	begin catch

		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()


		if @@trancount > 0
			begin
				rollback transaction
				select @errorMessage = @errorMessage + ' The transaction was rolled back.'
			end

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		return @err
	end catch


	-- commit open transaction if any
	if @@trancount > 0
		commit transaction
END
GO

