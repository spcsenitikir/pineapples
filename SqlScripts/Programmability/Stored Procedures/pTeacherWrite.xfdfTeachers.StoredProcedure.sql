SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 8 1 2015
-- Description:	update list of teachers at the school.
-- this procedure handles both the existing teachers list
-- ( where the survey is prepopulated, so each row already holds the tID
-- and the new teachers list
-- New teachers are identified either by a lookup of TeacherIdentity
-- on name or payroll no, or by looking in the TeacherUnidentified table
-- which allows some manula intervention in the load process
-- ie All teachers unidentified are written to this table
-- the tId can then be added to this table manually

-- =============================================
CREATE PROCEDURE [pTeacherWrite].[xfdfTeachers]
	@SurveyID int
	, @xml xml
AS
BEGIN
/*


*/
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

print @SurveyID
print 'SurveyID'
print '----------'

declare @rc int				-- for @@rowcount
	-- data n the grid
	declare @data TABLE
	(
		seq					int
		, PayrollNo			nvarchar(50)
		, FamilyName		nvarchar(50)
		, FirstName			nvarchar(50)
		, OnStaff			nvarchar(1)			-- y or n
		, Gender			nvarchar(1)
		, DoB				date
		, MaritalStatus		nvarchar(50)
		, Dep				int
		, Citizenship		nvarchar(50)
		, HomeIsland		nvarchar(2)
		, House				nvarchar(1)
		, Role				nvarchar(50)
		, Duties			nvarchar(2)
		, ClassMin			nvarchar(50)
		, ClassMax			nvarchar(50)
		, Qual				nvarchar(20)
		, QualEd			nvarchar(20)
		, YearStarted		int
		, YearsTeaching		int
		, InserviceYear		int
		, Inservice			nvarchar(50)
		, FP				nvarchar(1)
		, Days				int
		, EmpStatus			nvarchar(2)
		, PaidBy			nvarchar(50)
		, Salary			money
		, tID				int
		, UpdateIdentity	int					-- use with TeacherUnidentified
	)

declare @messages TABLE
(
	num	int
	, code nvarchar(20)
	, message nvarchar(200)
)

	declare @codeType nvarchar(10)			-- the ptCode assocaited to this table
	declare @idoc int

	-- the xfdf file has the default adobe namespace
	-- we have to alias this namespace ( as 'x' ) so as to be able to use
	-- the xpath expressions
	declare @xmlns nvarchar(500) = '<root xmlns:x="http://ns.adobe.com/xfdf/"/>'

	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml, @xmlns


/*
Format of the Teacher list block
   <field name="TL">
      <field name="00">
        <field name="PayrollNo">
          <value>2002024</value>
        </field>
        <field name="FamilyName">
          <value>TIMEON</value>
        </field>
        <field name="FirstName">
          <value>Mamao</value>
        </field>
        <field name="OnStaff">
          <value>Y</value>
        </field>
        <field name="Gender">
          <value>F</value>
        </field>
        <field name="DoB">
          <value>1968-05-10</value>
        </field>
        <field name="Role">
          <value>HD</value>
        </field>
        <field name="Duties">
          <value>T</value>
        </field>
        <field name="FP">
          <value>F</value>
        </field>
        <field name="House">
          <value>Y</value>
        </field>
        <field name="tID">
          <value>-1811765968</value>
        </field>
      </field>
*/
-- determine if we are loading from the exisitng teachers or new teacher list
declare @listType nvarchar(10)

declare @section nvarchar(200)


Select @listType = listType
	FROM OPENXML(@idoc,'/x:field',2) -- base is the row node
	WITH
	(
		listType				nvarchar(10)			'@name'			-- 00 01 02 etc
	)

Select @section = case @ListType when 'TL' then 'Teachers' else 'Teachers (New)' end
Select @listType, @Section

-- populate the table of grid values
	INSERT INTO @data
	( seq
		, PayrollNo
		, FamilyName
		, FirstName
		, OnStaff 			-- y or n
		, Gender
		, DoB
		, MaritalStatus
		, Dep
		, Citizenship
		, HomeIsland
		, House
		, Role
		, Duties
		, ClassMin
		, ClassMax
		, Qual
		, QualEd
		, YearStarted
		, YearsTeaching
		, InserviceYear
		, Inservice
		, FP
		, Days
		, EmpStatus
		, PaidBy
		, Salary
		, tID
		, UpdateIdentity
	)
	SELECT
	 seq
		, PayrollNo
		, FamilyName
		, FirstName
		, OnStaff 			-- y or n
		, Gender
		, DoB
		, nullif(MaritalStatus ,'00')
		, Dep
		, nullif(Citizenship ,'00')
		, nullif(HomeIsland,'00')
		, House
		, Role
		, Duties
		, nullif(ClassMin,'00')
		, nullif(ClassMax,'00')
		, nullif(Qual,'00')
		, nullif(QualEd,'00')
		, YearStarted
		, YearsTeaching
		, InserviceYear
		, Inservice
		, FP
		, Days
		, nullif(EmpStatus,'00')
		, nullif(PaidBy,'00')
		, Salary
		, tID
		-- UpdateIdentity: update if using existing teachers list
		, case when tID is not null then 1 else 0 end

	FROM OPENXML(@idoc,'/x:field/x:field',2) -- base is the row node
	WITH
	(
		seq				nvarchar(2)			'@name'			-- 00 01 02 etc
		, PayrollNo		nvarchar(50)		'x:field[@name="PayrollNo"]/x:value'
		, FamilyName	nvarchar(50)		'x:field[@name="FamilyName"]/x:value'
		, FirstName		nvarchar(50)		'x:field[@name="FirstName"]/x:value'
		, OnStaff		nvarchar(1)			'x:field[@name="OnStaff"]/x:value'
		, Gender		nvarchar(1)			'x:field[@name="Gender"]/x:value'
		, DoB			date				'x:field[@name="DoB"]/x:value'
		, MaritalStatus nvarchar(50)		'x:field[@name="MaritalStatus"]/x:value'
		, Dep			int					'x:field[@name="Dep"]/x:value'
		, Citizenship	nvarchar(50)		'x:field[@name="Citizenship"]/x:value'
		, HomeIsland	nvarchar(2)			'x:field[@name="HomeIsland"]/x:value'
		, Role			nvarchar(50)		'x:field[@name="Role"]/x:value'
		, Duties		nvarchar(2)			'x:field[@name="Duties"]/x:value'
		, ClassMin		nvarchar(50)		'x:field[@name="Class"]/x:field[@name="Min"]/x:value'
		, ClassMax		nvarchar(50)		'x:field[@name="Class"]/x:field[@name="Max"]/x:value'
		, Qual			nvarchar(20)		'x:field[@name="Qual"]/x:value'
		, QualEd		nvarchar(20)		'x:field[@name="QualEd"]/x:value'
		, YearStarted	int					'x:field[@name="YearStarted"]/x:value'
		, YearsTeaching int					'x:field[@name="YearsTeaching"]/x:value'
		, InserviceYear int					'x:field[@name="Inservice"]/x:field[@name="Year"]/x:value'
		, Inservice		nvarchar(50)		'x:field[@name="Inservice"]/x:field[@name="Year"]/x:value'
		, FP			nvarchar(1)			'x:field[@name="FP"]/x:value'
		, House			nvarchar(1)			'x:field[@name="House"]/x:value'
		, tID			int					'x:field[@name="tID"]/x:value'
		, Days			int					'x:field[@name="Days"]/x:value'
		, EmpStatus		nvarchar(2)			'x:field[@name="EmpStatus"]/x:value'
		, PaidBy		nvarchar(50)		'x:field[@name="PaidBy"]/x:value'
		, Salary		money				'x:field[@name="Salary"]/x:value'
		)
		WHERE FamilyName is not null


select * from @data

/*

This is a kludeg to deal with bad values in the Role field for new teachers,
in the original Kiribati forms. A better solution is needed medium term

*/
declare @SchoolType nvarchar(50)
declare @SurveyYear int

Select @SchoolType = isnull(ssSchType, schType)
, @SurveyYEar = svyYear
from SchoolSurvey SS
INNER JOIN Schools S
	ON SS.schNo = S.schNo

UPDATE @data
SET Role =
case role
	when '0' then 'P'
	when '1' then 'DP'
	when '2' then 'HD'
	when '3' then 'CT'
	else role
end
FROM @data
WHERE @ListType = 'T'
AND @SchoolType in ('JS','SS')


UPDATE @data
SET Role =
case role
	when '0' then 'HT1'
	when '1' then 'HT2'
	when '2' then '1A'
	when '3' then 'CT'
	else role
end
FROM @data
WHERE @ListType = 'T'
AND @SchoolType in ('P')

-- we only want to add the ones identified as being at the school
-- ie ListType T or ListType = TL and OnStaff = Y

DELETE
FROM @Data
WHERE @ListType = 'TL'
and isnull(OnStaff,'N') = 'N'


begin transaction


begin try

	-- clear any exisitng entries
	DELETE from TeacherSurvey
	WHERe ssId = @SurveyID
	AND tcheSource = @listType

	Select @rc = @@rowcount
	exec audit.xfdfInsert @SurveyID, 'Teacher surveys cleared',@section,@rc
	print @rc
	print 'List type delete: ' + @listType
	print '------------------------'


	-- we have to find the best tID to match this teacher
	-- uniditified teacher s get writtens to a holding table wehre they can be reviewed an manually matched
	-- so these may already	be available

	UPDATE @data
	SET tID = TU.tID
	, UpdateIdentity = TU.tuUpdateIdentity
	FROM @data
	INNER JOIN TeacherUnidentified TU
		ON [@data].seq = TU.seq
		AND [@data].FamilyName = TU.FamilyName
		AND isnull([@data].PayrollNo,'') = isnull(TU.PayrollNo,'')
	WHERE [@data].tID is null
	AND TU.ssID = @surveyID

	print @@rowcount
	print 'Matches with unidentified list'
	print '------------------------------'


-- now search for resolutions of the others
	UPDATE @data
	SET tID = TI.tID
	FROM @data
	INNER JOIN TeacherIdentity TI
		ON FamilyName = tSurname
		AND FirstName = tGiven
		AND dob = tDOB
		AND Gender = tSex
	WHERE [@data].tID is null

	print @@rowcount
	print 'name dob gender match'
	print '---------------------'

	-- payroll number and name
	UPDATE @data
	SET tID = TI.tID
	FROM @data
	INNER JOIN TeacherIdentity TI
		ON PayrollNo = tPayroll
		AND FamilyName = tSurname
		AND FirstName = tGiven
	WHERE [@data].tID is null

	print @@rowcount
	print 'name payroll match'
	print '---------------------'

	-- payroll number and date of birth
	UPDATE @data
	SET tID = TI.tID
	FROM @data
	INNER JOIN TeacherIdentity TI
		ON PayrollNo = tPayroll
		AND dob = tDOB
		AND Gender = tSex
	WHERE [@data].tID is null

	print @@rowcount
	print 'payroll dob gender match'
	print '---------------------'


-- now do some cleanups to report on missing codes
-- These can be reported, or fixed manually or the codes corrected and the load rerun

declare @counter int

INSERT INTO @messages
SELECT count(*)
	, role
	, 'undefined teacher role code'
FROM @data
WHERE Role not in (Select codecode from lkpTeacherRole)
and nullif(Role, '00') is not null			-- its null
GROUP BY role

UPDATE @data
SET Role = null
WHERE Role not in (Select codecode from lkpTeacherRole)


-- we may get an error if this teacher is appearing somewhere else ie a duplicate TeacherSurvey for the year...
INSERT INTO @messages

Select 1, FamilyName + ', ' + FirstName, 'teacher on another survey'
FROM @Data
WHERE tId is not null
AND tID in
(Select tID
from TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
	WHERE ss.ssID <> @SurveyID
	AND ss.svyYEar = @SurveyYear
)
if @@ROWCOUNT > 0 begin
	DELETE
	FROM @Data
		WHERE tId is not null
		AND tID in
		(Select tID
		from TeacherSurvey TS
		INNER JOIN SchoolSurvey SS
			ON TS.ssID = SS.ssID
			WHERE ss.ssID <> @SurveyID
			AND ss.svyYEar = @SurveyYear
		)

end

-- the survey filler may enter an exisitng teacher again in the new teacher section
-- we need to see this and prevent it

INSERT INTO @messages

Select 1, FamilyName + ', ' + FirstName, 'teacher already in survey'
FROM @Data
WHERE tId is not null
AND tID in
(Select tID
from TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
	WHERE ss.ssID = @SurveyID
)
if @@ROWCOUNT > 0 begin
	DELETE
	FROM @Data
		WHERE tId is not null
		AND tID in
		(Select tID
		from TeacherSurvey TS
		INNER JOIN SchoolSurvey SS
			ON TS.ssID = SS.ssID
			WHERE ss.ssID = @SurveyID
		)

end

	-- write a teacher survey record for each teacher that can be identified
	INSERT INTO TeacherSurvey
	(
		ssID
		, tcheSource

		, tchSort
		, tchEmplNo
		, tchFamilyName
		, tchFirstName
		, tchGender
		, tchDoB

		, tchCivilStatus
		, tchNumDep
		, tchCitizenship
		, tchIsland

		, tchRole
		, tchTAM
		, tchFullPart
		, tchFTE

		, tchHouse
		, tID

		, tchClass
		, tchClassMax
		, tchQual
		, tchEdQual

		, tchYearStarted
		, tchYears

		, tchInserviceYear
		, tchInservice

		, tchStatus
		, tchSponsor
		, tchSalary


	)
	Select
	@surveyID
	, @listType
	 , case when @listtype = 'T' then 100 else 0 end +  seq + 1
		, PayrollNo
		, FamilyName
		, FirstName
		, Gender
		, DoB

		, MaritalStatus
		, Dep
		, Citizenship
		, HomeIsland

		, Role
		, case Duties when 'TA' then 'M' else Duties end		-- uses mixed in the database 'TA' on the form
		, FP
		-- fte can set to 1 if full time
		, case FP when 'F' then 1 else Days/5 end
		, case House when 'Y' then 1 when 'N' then 0 else null end
		, tID

		, ClassMin
		, ClassMax
		, Qual
		, QualEd

		, YearStarted
		, YearsTeaching

		, InserviceYear
		, Inservice

		, EmpStatus
		, PaidBy
		, Salary

	FROM @Data
	WHERE tID is not null
	Select @rc = @@rowcount
	exec audit.xfdfInsert @SurveyID, 'Teacher surveys inserted',@section,@rc
	print @rc
	print 'TeacherSurvey added'

	-- update the identity where flagged to do so
	-- there are two cases -
	-- 1) from the existing teachers list (we are able to change only gender and dob)
	-- 2) from the Unidetnified list - teacher is undientified anf flagged to update identity
	-- ie this survey data is correct and should update the identity record

	UPDATE TeacherIdentity
	SET tPayroll = PayrollNo
	, tSurname = FamilyName
	, tGiven = FirstName
	, tDoB = Dob
	, tSex = coalesce(Gender, tSex)
	FROM TeacherIdentity
	INNER JOIN @data D
		ON teacherIdentity.tID = D.tID
	WHERE D.UpdateIdentity = 1

	Select @rc = @@rowcount
	exec audit.xfdfInsert @SurveyID, 'Teacher identity updated',@section,@rc

	-- finally, write any unditified teachers to TeacherUnidentified for manual review
	if (@listType = 'T') begin
		DELETE From TeacherUnidentified
		WHERE ssID = @SurveyID

		INSERT INTO Teacherunidentified
		  ([ssID]
			   ,[seq]
			   ,[PayrollNo]
			   ,[FamilyName]
			   ,[FirstName]
			   ,[Gender]
			   ,[DoB]
			   ,[MaritalStatus]
			   ,[Dep]
			   ,[Citizenship]
			   ,[HomeIsland]
			   ,[House]
			   ,[Role]
			   ,[Duties]
			   ,[ClassMin]
			   ,[ClassMax]
			   ,[Qual]
			   ,[QualEd]
			   ,[YearStarted]
			   ,[YearsTeaching]
			   ,[InserviceYear]
			   ,[Inservice]
			   ,[FP]
			   ,[Days]
			   ,[EmpStatus]
			   ,[PaidBy]
			   ,[Salary]
			   )
		   Select
			@SurveyID
			,[seq]
			   ,[PayrollNo]
			   ,[FamilyName]
			   ,[FirstName]
			   ,[Gender]
			   ,[DoB]
			   ,[MaritalStatus]
			   ,[Dep]
			   ,[Citizenship]
			   ,[HomeIsland]
			   ,[House]
			   ,[Role]
			   ,[Duties]
			   ,[ClassMin]
			   ,[ClassMax]
			   ,[Qual]
			   ,[QualEd]
			   ,[YearStarted]
			   ,[YearsTeaching]
			   ,[InserviceYear]
			   ,[Inservice]
			   ,[FP]
			   ,[Days]
			   ,[EmpStatus]
			   ,[PaidBy]
			   ,[Salary]
			FROM @data
			WHERE tID is null
	end			-- @listtype = 'T'

	Select @rc = @@rowcount
	if (@rc > 0)
		exec audit.xfdfInsert @SurveyID, 'Teachers unidentified',@section,@rc,null, null, 1
	if (@rc = 0)
		exec audit.xfdfInsert @SurveyID, 'All Teachers identified',@section
	print @@rowcount
	print 'Teacher Unidentified updated'
	print '---------------------'


-- a custom more detailed infomration set is written to the log
--
	INSERT INTO audit.xfdfAudit
	( ssID, svyYear, schNo,
	sxaDesc, sxaSection,
	sxaNum, sxaData,
	sxaDate, sxaUser,
	sxaErrorFlag)

	Select @SurveyID, svyYear, schNo
	, message, @section,
	num, code,
	getdate(), original_login(),
	1
	FROM @messages
		LEFT JOIN SchoolSurvey
			ON SchoolSurvey.ssID = @surveyID
end try

begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end
	print '*** ERROR ****'
	print @errorMessage
	exec audit.xfdfError @SurveyID, @ErrorMessage,@section,@listType
    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch

-- and commit
if @@trancount > 0
	commit transaction


END
GO

