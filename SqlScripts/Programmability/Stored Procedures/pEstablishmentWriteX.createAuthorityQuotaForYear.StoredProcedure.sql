SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [pEstablishmentWriteX].[createAuthorityQuotaForYear]
	-- Add the parameters for the stored procedure here
	@Year int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


begin try

	begin transaction

	Select estYear
	, estAuth
	, sum(estrQuota) QuotaSum
	, sum(estrCount) CountSum
	, case estcAuthorityPositionPer
			when 0 then 0
			else
				floor((cast(isnull(sum(estrCount),0) as float) + estcAuthorityPositionRoundUpBy) / estcAuthorityPositionPer)
		end AuthQuota

	INTO #tmpSuperPos

	from SchoolEstablishment SE
	INNER JOIN SchoolEstablishmentRoles SER
	ON SE.estID = SER.estID
	INNER JOIN EstablishmentControl EC
	ON SE.estYear = EC.estcYear
	WHERE Se.estYEar = @Year
		AND SE.estAuth is not null
	group by estYEar, estAuth
	,estcAuthorityPositionRoundUpBy
	,estcAuthorityPositionPer


	UPDATE EstablishmentSuperPos
	set eppQuota = isnull(Q.AuthQuota,0)
	FROM EstablishmentSuperPos
		LEFT JOIN #tmpSuperPos Q
			ON EstablishmentSuperPos.estcYear = Q.estYEar
			AND EstablishmentSuperPos.eppAuth = Q.estAuth
	WHERE EstablishmentSuperPos.estcYear = @Year


    INSERT INTO EstablishmentSuperPos
    ( estcYear
		, eppScope
		, eppAuth
		, eppQuota
	)
	Select estYear
	, 'A'
	, estAuth
	, AuthQuota
	FROM #tmpSuperPos Q
		LEFT JOIN EstablishmentSuperPos
			ON EstablishmentSuperPos.estcYear = Q.estYEar
			AND EstablishmentSuperPos.eppAuth = Q.estAuth
	WHERE EstablishmentSuperPos.eppID is null


	commit transaction

end try

/****************************************************************
Generic catch block
****************************************************************/
begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch

END
GO

