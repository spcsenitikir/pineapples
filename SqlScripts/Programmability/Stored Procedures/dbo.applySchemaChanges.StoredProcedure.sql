SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[applySchemaChanges]

AS


BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @ThisVersion nvarchar(50)
	declare @CurrentVersion nvarchar(50)

BEGIN TRY
	exec getSchemaInfo @CurrentVersion OUTPUT
END TRY
BEGIN CATCH
	Select @CurrentVersion = '00000000'
END CATCH
	select @CurrentVersion

if @CurrentVersion is null
	Select @CurrentVersion = '00000000'
if @CurrentVersion < '20071218A'
	BEGIN

begin try
	-- change to schema tracking mechanism
	CREATE TABLE [dbo].[schemaVersion](
		[schemaID] [nvarchar](50) NULL,
		[schemaDate] [datetime] NULL,
		[schemaApplied] [datetime] NULL
	) ON [PRIMARY]
end try
begin catch
end catch

-- earlier versions had an incorrect version of this table
begin try
ALTER TABLE dbo.Exams ADD
	exScoringModel nvarchar(50)  NULL,
	exsmNumScores int NULL,
	exsmMandatory nvarchar(255)  NULL,
	exsmExcluded nvarchar(255)  NULL,
	exExportDate datetime NULL,
	exExporterVersion nvarchar(20) NULL
end try
begin catch
end catch


-- add the index on year on SchoolSurvey
begin try ALTER TABLE [dbo].[SchoolSurvey] DROP CONSTRAINT [UK_SchoolSurvey_School_Year] end try
begin catch end catch

ALTER TABLE [dbo].[SchoolSurvey] ADD  CONSTRAINT [UK_SchoolSurvey_School_Year] UNIQUE NONCLUSTERED
(
	[schNo] ASC,
	[svyYear] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
-- index on survey year
CREATE NONCLUSTERED INDEX [IX_SchoolSurvey_Year] ON [dbo].[SchoolSurvey]
(
	[svyYear] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

------------------------------------------------------------------
-- SchoolAlias
-- index on schNo
CREATE NONCLUSTERED INDEX IX_SchoolAlias_schNo ON dbo.SchoolAlias
	(
	schNo
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


--------------------------------------------------------------------
-- SchoolSurveyQuality
-- index on dataitem/subitem and level
CREATE NONCLUSTERED INDEX IX_SchoolSurveyQuality_ITem_Sub_Level ON dbo.SchoolSurveyQuality
	(
	ssqDataItem,
	ssqSubItem,
	ssqLevel
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

-- on ssID, data and sub is unique
-- mustbe an index not a key becuase of possible nulls
ALTER TABLE dbo.SchoolSurveyQuality ADD CONSTRAINT
	UK_SchoolSurveyQuality_SSID_Item_sub UNIQUE NONCLUSTERED
	(
	ssID,
	ssqDataItem,
	ssqSubItem
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


-- PupilTables
CREATE NONCLUSTERED INDEX IX_PupilTables_ssID_Code ON dbo.PupilTables
	(
	ssID,
	ptCode
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

/**********************************************************************************************
 Resources
**********************************************************************************************/

delete from Resources where ssID not in (Select ssID from SchoolSurvey)

-- resources did not have the foreien key relationship prior to this version
ALTER TABLE dbo.Resources ADD CONSTRAINT
	FK_Resources_SchoolSurvey FOREIGN KEY
	(
	ssID
	) REFERENCES dbo.SchoolSurvey
	(
	ssID
	) ON UPDATE  NO ACTION
	 ON DELETE  CASCADE

CREATE NONCLUSTERED INDEX IX_Resources_ssID_Category ON dbo.Resources
	(
	ssID ASC,
	resName ASC
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


/**********************************************************************************************
 ResourceProvision
**********************************************************************************************/

delete from ResourceProvision where ssID not in (Select ssID from SchoolSurvey)

-- resources did not have the foreien key relationship prior to this version
ALTER TABLE dbo.ResourceProvision ADD CONSTRAINT
	FK_ResourceProvision_SchoolSurvey FOREIGN KEY
	(
	ssID
	) REFERENCES dbo.SchoolSurvey
	(
	ssID
	) ON UPDATE  NO ACTION
	 ON DELETE  CASCADE

CREATE NONCLUSTERED INDEX IX_ResourceProvision_ssID_Type ON dbo.ResourceProvision
	(
	ssID ASC,
	rpType ASC
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

/**********************************************************************************************
Furniture
**********************************************************************************************/
-- have to do furniture before rooms to get the rooms relationship to work

-- furniture
-- index by ssID
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Furniture_FK03]') AND parent_object_id = OBJECT_ID(N'[dbo].[Furniture]'))
ALTER TABLE [dbo].[Furniture] DROP CONSTRAINT [Furniture_FK03]
-- ad this constraint without cascade
ALTER TABLE [dbo].[Furniture]  WITH CHECK ADD  CONSTRAINT [Furniture_FK03] FOREIGN KEY([rmID])
REFERENCES [dbo].[Rooms] ([rmID])
ALTER TABLE [dbo].[Furniture] CHECK CONSTRAINT [Furniture_FK03]
-- and an index
CREATE NONCLUSTERED INDEX [IX_Furniture_SSID] ON [dbo].[Furniture]
(
	[ssID] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

--- index by room id
CREATE NONCLUSTERED INDEX [IX_Furniture_Room] ON [dbo].[Furniture]
(
	[rmID] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

/**********************************************************************************************
SurveyFunds
**********************************************************************************************/
-- no primary key
ALTER TABLE dbo.SurveyFunds ADD CONSTRAINT
	PK_SurveyFunds PRIMARY KEY CLUSTERED
	(
	svfID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


delete from SurveyFunds where ssID not in (Select ssID from SchoolSurvey)

-- resources did not have the foreien key relationship prior to this version
ALTER TABLE dbo.SurveyFunds ADD CONSTRAINT
	FK_SurveyFunds_SchoolSurvey FOREIGN KEY
	(
	ssID
	) REFERENCES dbo.SchoolSurvey
	(
	ssID
	) ON UPDATE  NO ACTION
	 ON DELETE  CASCADE


CREATE NONCLUSTERED INDEX IX_SurveyFunds_SSID ON dbo.SurveyFunds
	(
	ssID ASC
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


/**********************************************************************************************
Rooms
**********************************************************************************************/

delete from Rooms where ssID not in (Select ssID from SchoolSurvey)

-- resources did not have the foreien key relationship prior to this version
ALTER TABLE dbo.Rooms ADD CONSTRAINT
	FK_Rooms_SchoolSurvey FOREIGN KEY
	(
	ssID
	) REFERENCES dbo.SchoolSurvey
	(
	ssID
	) ON UPDATE  NO ACTION
	 ON DELETE  CASCADE


CREATE NONCLUSTERED INDEX IX_Rooms_SSID_Type ON dbo.Rooms
	(
	ssID,
	rmType
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

-- enrolments
CREATE NONCLUSTERED INDEX [IX_Enrollments_SSID] ON [dbo].[Enrollments]
(
	[ssID] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]


--disabilityvillage
CREATE NONCLUSTERED INDEX [IX_DisabilityVillage_SSID] ON [dbo].[DisabilityVillage]
(
	[ssID] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

--primaryclass
CREATE NONCLUSTERED INDEX [IX_PrimaryClass_SSID] ON [dbo].[PrimaryClass]
(
	[ssID] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

------ tables related to primaqryclass
CREATE NONCLUSTERED INDEX [IX_PrimaryClassLevel_PCID] ON [dbo].[PrimaryClassLevel]
(
	[pcID] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]


CREATE NONCLUSTERED INDEX [IX_PrimaryClassTeacher_PCID] ON [dbo].PrimaryClassTeacher
(
	[pcID] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
/**********************************************************************************************
TeacherSurvey
**********************************************************************************************/
--ssID index
CREATE NONCLUSTERED INDEX [IX_TeacherSurvey_SSID] ON [dbo].[TeacherSurvey]
(
	[ssID] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--tid index
CREATE NONCLUSTERED INDEX [IX_TeacherSurvey_TID] ON [dbo].[TeacherSurvey]
(
	[tID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

/**********************************************************************************************
toilets
**********************************************************************************************/

CREATE NONCLUSTERED INDEX [IX_Toilets_SSID] ON [dbo].[Toilets]
(
	[ssID] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

--vehicles
CREATE NONCLUSTERED INDEX [IX_Vehicles_SSID] ON [dbo].Vehicles
(
	[ssID] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

--activities


---- relation activities does not cascade - this should get fixed
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Activities_FK00]') AND parent_object_id = OBJECT_ID(N'[dbo].[Activities]'))
ALTER TABLE [dbo].[Activities] DROP CONSTRAINT [Activities_FK00]
ALTER TABLE dbo.Activities ADD CONSTRAINT
	Activities_FK00 FOREIGN KEY
	(
	ssID
	) REFERENCES dbo.SchoolSurvey
	(
	ssID
	) ON UPDATE  NO ACTION
	 ON DELETE  CASCADE
CREATE NONCLUSTERED INDEX [IX_Activities_SSID] ON [dbo].Activities
(
	[ssID] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

--DistanceTransport
CREATE NONCLUSTERED INDEX [IX_DistanceTransport_SSID] ON [dbo].DistanceTransport
(
	[ssID] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

--PreSchoolSurvey
CREATE NONCLUSTERED INDEX [IX_PreSchoolSurvey_SSID] ON [dbo].PreSchoolSurvey
(
	[ssID] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

--SchoolBanks
CREATE NONCLUSTERED INDEX [IX_SchoolBanks_SSID] ON [dbo].SchoolBanks
(
	[ssID] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

--Streams
CREATE NONCLUSTERED INDEX [IX_Streams_SSID] ON [dbo].Streams
(
	[ssID] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]


--SurveyYearCourses
CREATE NONCLUSTERED INDEX [IX_SurveyYearCourses_SSID] ON [dbo].SurveyYearCourses
(
	[ssID] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

--Faculty
CREATE NONCLUSTERED INDEX [IX_Faculty_SSID] ON [dbo].Faculty
(
	[ssID] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

--Faculty
CREATE NONCLUSTERED INDEX [IX_Faculty_SSID] ON [dbo].Faculty
(
	[ssID] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]


--FinancialInternalAudit
CREATE NONCLUSTERED INDEX [IX_FinancialInternalAudit_SSID] ON [dbo].FinancialInternalAudit
(
	[ssID] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]


--FinancialResponsibility
CREATE NONCLUSTERED INDEX [IX_FinancialResponsibility_SSID] ON [dbo].FinancialResponsibility
(
	[ssID] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]


--FinancialResponsibility
CREATE NONCLUSTERED INDEX [IX_FinancialResponsibility_SSID] ON [dbo].FinancialResponsibility
(
	[ssID] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]


--ManagingBody
CREATE NONCLUSTERED INDEX [IX_ManagingBody_SSID] ON [dbo].ManagingBody
(
	[ssID] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

/*************************************************************************************************
Foreign key indexes related to schools
*************************************************************************************************/

--schoolnotes
--- index by school
CREATE NONCLUSTERED INDEX IX_SchoolNotes_SchNo ON dbo.SchoolNotes
	(
	schNo ASC
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

--Schoollinks
CREATE NONCLUSTERED INDEX [IX_SchoolLinks_SchNo] ON [dbo].[SchoolLinks]
(
	[schNo] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

--SchoolEstablishment
CREATE NONCLUSTERED INDEX [IX_SchoolEstablishment_SchNo] ON [dbo].SchoolEstablishment
(
	[schNo] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]


--TeacherAppointment
CREATE NONCLUSTERED INDEX [IX_TeacherAppointment_SchNo] ON [dbo].TeacherAppointment
(
	[schNo] ASC,
	taDate ASC

)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [IX_TeacherAppointment_Date] ON [dbo].TeacherAppointment
(
	taDate ASC,
	taID

)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]


--TeacherAttendance
CREATE NONCLUSTERED INDEX [IX_TeacherAttendance_SchNo] ON [dbo].TeacherAttendance
(
	[schNo] ASC

)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

---- on tID as well
CREATE NONCLUSTERED INDEX [IX_TeacherAttendance_tID] ON [dbo].TeacherAttendance
(
	tID ASC

)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

--SchoolLedger
--- include svyYear in this index as well
CREATE NONCLUSTERED INDEX [IX_SchoolLedger_SchNo_Year] ON [dbo].SchoolLedger
(
	schNo ASC,
	svyYear ASC

)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

--EnrolmentProjection
CREATE NONCLUSTERED INDEX [IX_EnrolmentProjection_SchNo] ON [dbo].EnrolmentProjection
(
	schNo ASC

)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [IX_EnrolmentProjection_Scn_Year] ON [dbo].EnrolmentProjection
(
	escnID ASC,
	epYear ASC

)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

----------------------------------------------------------------------------------
-- drop some unused tables
----------------------------------------------------------------------------------
/****** Object:  Table [dbo].[EFAPoint]    Script Date: 12/18/2007 10:20:32 ******/
DROP TABLE [dbo].[EFAPoint]
DROP TABLE [dbo].[EFAPointSchools]


	END			-- version 20071217
if @CurrentVersion <= '20071220A'
	BEGIN
-- make a cascade delete on Resources
		IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Resources_SchoolSurvey]') AND parent_object_id = OBJECT_ID(N'[dbo].[Resources]'))
		ALTER TABLE [dbo].[Resources] DROP CONSTRAINT [FK_Resources_SchoolSurvey]

		ALTER TABLE [dbo].[Resources]  WITH CHECK ADD  CONSTRAINT [FK_Resources_SchoolSurvey] FOREIGN KEY([ssID])
		REFERENCES [dbo].[SchoolSurvey] ([ssID])
		ON DELETE CASCADE

		ALTER TABLE [dbo].[Resources] CHECK CONSTRAINT [FK_Resources_SchoolSurvey]

-- and on activities
begin try
		IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Activities_FK00]') AND parent_object_id = OBJECT_ID(N'[dbo].[Activities]'))
		ALTER TABLE [dbo].[Activities] DROP CONSTRAINT [Activities_FK00]

		ALTER TABLE [dbo].[Activities]  WITH CHECK ADD  CONSTRAINT [Activities_FK00] FOREIGN KEY([ssID])
		REFERENCES [dbo].[SchoolSurvey] ([ssID])
		ON DELETE CASCADE

		ALTER TABLE [dbo].[Activities] CHECK CONSTRAINT [Activities_FK00]
end try
begin catch
end catch

	END
if @currentVersion < '20080115A'
	begin
	-- add smisID to support pineapples@school
		ALTER TABLE dbo.TeacherSurvey ADD
			tchsmisID int NULL
	end
if @currentVersion < '20080418A'
	begin
	begin try

		-- paypoint codes added in si
		CREATE TABLE [dbo].[PayPointCodes](
			[payptCode] [nvarchar](10) NOT NULL,
			[payptDesc] [nvarchar](50) NULL,
			[payptDescL1] [nvarchar](50) NULL,
			[payptDescL2] [nvarchar](50) NULL,
		 CONSTRAINT [PK_PayPointCodes] PRIMARY KEY CLUSTERED
		(
			[payptCode] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

		ALTER TABLE dbo.TeacherIdentityExternal ADD
			xtpayptCode nvarchar(10) NULL

		ALTER TABLE dbo.TeacherIdentity ADD
			tpayptCode nvarchar(10) NULL

		ALTER TABLE dbo.Schools ADD
			schpayptCode nvarchar(10) NULL

	end try
	begin catch
	end catch
	end


if @currentVersion < '20080502B'
	begin
	begin try
	-- teacher links to support teacher image library
	CREATE TABLE [dbo].[TeacherLinks](
	[lnkID] [int] NOT NULL CONSTRAINT [DF__TeacherLi__lnkID__3B0E2511]  DEFAULT ((0)),
	[tID] [int] NOT NULL,
	[lnkDate] [datetime] NULL,
	[lnkTitle] [nvarchar](80) NULL,
	[lnkSummary] [ntext] NULL,
	[lnkSrc] [nvarchar](50) NULL,
	[lnkPath] [nvarchar](255) NULL,
	[lnkImg] [bit] NULL CONSTRAINT [DF__TeacherLi__lnkIm__3C02494A]  DEFAULT ((0)),
	[lnkKeywords] [nvarchar](255) NULL,
 CONSTRAINT [aaaaaTeacherLinks_PK] PRIMARY KEY NONCLUSTERED
(
	[lnkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


ALTER TABLE [dbo].[TeacherLinks]  WITH CHECK ADD  CONSTRAINT [FK_TeacherLinks_TeacherIdentity] FOREIGN KEY([tID])
REFERENCES [dbo].[TeacherIdentity] ([tID])

ALTER TABLE [dbo].[TeacherLinks] CHECK CONSTRAINT [FK_TeacherLinks_TeacherIdentity]

-- default image
		ALTER TABLE dbo.TeacherIdentity ADD
			tImage int NULL

	end try
	begin catch
	end catch
	end

if @currentVersion < '20080507A'
	begin
		begin try

-- active flag added to teacher external data
			ALTER TABLE dbo.TeacherIdentityExternal ADD
				xtActive bit NOT NULL CONSTRAINT DF_TeacherIdentityExternal_xtActive DEFAULT 1
		end try
		begin catch
		end catch
	end


END
GO

