SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 05 05 2010
-- Description:	Change a position number
-- =============================================
CREATE PROCEDURE [pEstablishmentOps].[ChangePositionNumber]
	-- Add the parameters for the stored procedure here
	@OldNo nvarchar(20)
	, @NewNo nvarchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

begin try
	begin transaction
	DECLARE @PositionNumbers TABLE
	( posNo nvarchar(50)
	, counter int)

	if (@NewNo is null)	begin
		declare @SchoolNo nvarchar(50)
		Select @SchoolNo = schNo
			FROM Establishment
			WHERE estpNo = @OldNo

		INSERT INTO @PositionNumbers
		EXEC pEstablishmentWriteX.MakeEstablishmentNumbers @SchoolNo,1
		Select @NewNo = posNo FROM @PositionNumbers
	end

	INSERT INTO Establishment
	(
		[estpNo]
	   ,[schNo]
	   ,[estpScope]
	   ,[estpAuth]
	   ,[estpRoleGrade]
	   ,[estpActiveDate]
	   ,[estpClosedDate]
	   ,[estpClosedReason]
	   ,[estpTitle]
	   ,[estpDescription]
	   ,[estpSubject]
	   ,[estpFlag]
	   ,[estpCreateDate]
	   ,[estpCreateUser]
	   ,[estpEditDate]
	   ,[estpEditUser]
	   ,[estpConfirmed]
	   ,[estpSendDate]
	   ,[estpSendUser]
	   ,[estpSendBatch]
	   ,[estpNote]
	 )
	 Select
		@NewNo
		, schNo
		, estpScope
		,[estpAuth]
	   ,[estpRoleGrade]
	   ,[estpActiveDate]
	   ,[estpClosedDate]
	   ,[estpClosedReason]
	   ,[estpTitle]
	   ,[estpDescription]
	   ,[estpSubject]
	   ,[estpFlag]
	   ,[estpCreateDate]
	   ,[estpCreateUser]
	   ,[estpEditDate]
	   ,[estpEditUser]
	   ,[estpConfirmed]
	   ,[estpSendDate]
	   ,[estpSendUser]
	   ,[estpSendBatch]
	   ,[estpNote]
	 FROM Establishment
	 WHERE estpNo = @oldNo

	 -- next move any appointments

	 UPDATE TeacherAppointment
		SET estpNo = @NewNo
	 WHERE EstpNo = @oldNo

	 DELETE
	 FROM Establishment
	 WHERE estpNo = @oldNo

	 commit transaction
end try
/****************************************************************
Generic catch block
****************************************************************/
begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch


END
GO

