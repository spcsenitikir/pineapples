SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 24 1 2009
-- Description:	REad XML for Classes
-- =============================================
CREATE PROCEDURE [pEnrolmentRead].[readClasses]
	-- Add the parameters for the stored procedure here
	@SurveyID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

declare @minYearOfEd int

select @minYEarOfEd = min(lvlYear)
from metaSchooltypelevelmap M
inner join SchoolSurvey SS
on M.stCode = SS.ssSchType
inner join lkpLevels L
on M.tlmLevel = L.codeCode
where SS.ssID = @surveyID


declare @xml xml

Select @xml =
(

Select
Tag
, Parent
, ssID as [Classes!1!SurveyID]

, pcID as [Class!2!ID]
, pcHrsWeek as [Class!2!weeklyHours]
, pcLang as [Class!2!langCode]
, subjCode as [Class!2!subjectCode]
, shiftCode as [Class!2!shiftCode]
, rmID as [Class!2!roomID]

, Teachers as [Teachers!3!ID!Hide]
, tchsID as [Teacher!4!tchsID]
, tID as [Teacher!4!ID]
, pctHrsWeek as [Teacher!4!HoursPerWeek]
, TMajor as [Teacher!4!Major]
, TMinor as [Teacher!4!Minor]

, Levels as [Levels!5!ID!Hide]
, levelCode as [Level!6!Level]
, offset as [Level!6!yearOffset]
, yearOfEd as [Level!6!yearOfEd]
, pclNum as [Level!6!Enrol]
, pclM as [Level!6!EnrolM]
, pclF as [Level!6!EnrolF]
from
(
Select DISTINCT 1 as Tag
, NULL as Parent
, ssID as ssID

, null as pcID
, null as pcHrsWeek
, null as rmID
, null as pcLang
, null as subjCode
, null as shiftCode

, null as Teachers
, null as tchsID
, null as tID
, null as pctHrsWeek
, null as TMajor			-- teacher major
, null as TMinor			-- teacher major

, null as Levels
, null as levelCode
, null as offset
, null as yearofEd
, null as pclNum
, null as pclM
, null as pclF
from Classes
union all
Select 2 as Tag
, 1 as Parent
, ssID
, pcID
, pcHrsWeek
, rmID
, pcLang
, subjCode
, shiftCode

, null
, null
, null
, null
, null as TMajor			-- teacher major
, null as TMinor			-- teacher major

, null
, null
, null
, null
, null
, null
, null

from Classes
union all
Select 3 as Tag
, 2 as Parent
, ssID
, pcID
, null
, null
, null
, null
, null as shiftCode

, pcID
, null
, null
, null
, null as TMajor			-- teacher major
, null as TMinor			-- teacher major


, null
, null
, null
, null
, null
, null
, null

from Classes

union all
-- levels header
Select 5 as Tag
, 2 as Parent
, ssID
, pcID
, null
, null
, null
, null
, null as shiftCode

, null
, null
, null
, null
, null as TMajor			-- teacher major
, null as TMinor			-- teacher major

, pcID
, null
, null
, null
, null
, null
, null

from Classes


union all

Select 6 as Tag
, 5
, ssID
, C.pcID
, null
, null
, null
, null
, null as shiftCode

, null
, null
, null
, null
, null as TMajor			-- teacher major
, null as TMinor			-- teacher major

, CL.pcID
, pclLevel
, lvlYear - @minYearOfEd
, lvlYear
, pclNum
, pclM
, pclF

from Classes C Inner join ClassLevel CL
on C.pcID = CL.pcID
inner join lkpLevels L
on CL.pclLevel = l.codeCode

union all

Select 4 as Tag
, 3 as Parent
, C.ssID
, C.pcID
, null
, null
, null
, null
, null as shiftCode

, Ct.pcID
, CT.tchsID
, CT.tID
, pctHrsWeek
, tchSubjectMajor as TMajor			-- teacher major
, tchSubjectMinor as TMinor			-- teacher major

, null
, null
, null
, null
, null
, null
, null

from Classes C Inner join ClassTeacher CT
on C.pcID = CT.pcID
LEFT JOIN TeacherSurvey TCH
	ON CT.tID = TCH.tID
	AND C.ssID = TCH.ssID

) sub

where ssId = @surveyID

ORDER By
[Classes!1!SurveyID]
,[Class!2!ID]
, [Teachers!3!ID!Hide]
, [Teacher!4!ID]
, [Levels!5!ID!Hide]
, [Level!6!Level]
, Parent
, Tag


for xml explicit
)

SELECT @xml

END
GO

