SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 12 2010
-- Description:	Nation-level summary of enrolment ratios at the class level, rather than by level of education
-- =============================================
CREATE PROCEDURE [warehouse].[_vermEdLevelER]
	-- Add the parameters for the stored procedure here
	@SendASXML int = 0
	, @xmlOut xml = null OUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


declare @schoolNo nvarchar(50)
declare @SurveyYear int

declare @popModCode nvarchar(10)			-- the pop model we will use
declare @popModName nvarchar(50)
declare @popModDefault int
DECLARE @edLevelsU TABLE
(
S int
, codeCode nvarchar(10)
)

INSERT INTO @edLevelsU
Select S, codeCode
FROM (
	Select *, row_number() over (PARTITION BY codeCode ORDER BY S ) R
	from
	(
	Select 0 S, codeCode from lkpEducationLevels EL
	UNION
	Select 1, codeCode from lkpEducationLevelsAlt EL1
	UNION
	Select 2, codeCode from lkpEducationLevelsAlt2 EL2
	)	U
) UU
WHERE R = 1

if @SendASXML <> 0  begin

   declare @XML xml
   declare @XML2 xml
   SELECT @XML =
   (
   Select
   L.SurveyYear		[@year]
   , edLevelCode	[@edLevelCode]
   , firstYear		[@firstYear]
   , lastYear		[@lastYear]
   , numYears		[@numYears]
   , firstYear - 1 + S.svyPSAge [@startAge]
   , L.popM
   , L.popF
   , L.pop
   , L.enrolM
   , L.enrolF
   , L.enrol
   , L.nEnrolM
   , L.nEnrolF
   , L.nEnrol
   , L.repM
   , L.repF
   , L.rep
   , L.nRepM
   , L.nRepF
   , L.nRep
   , L.intakeM
   , L.intakeF
   , L.intake
   , L.nIntakeM
   , L.nIntakeF
   , L.nIntake

   , cast(convert(float,isnull(L.enrolM,0))/ L.popM  as decimal(8,5)) gerM
   , cast(convert(float,isnull(L.enrolF,0))/L.popF as decimal(8,5)) gerF
   , cast(convert(float,isnull(L.enrol,0))/L.pop as decimal(8,5)) ger
   , cast(convert(float,isnull(L.nEnrolM,0))/L.popM as decimal(8,5)) nerM
   , cast(convert(float,isnull(L.nEnrolF,0))/L.popF as decimal(8,5)) nerF
   , cast(convert(float,isnull(L.nEnrol,0))/L.pop as decimal(8,5)) ner

   , cast(convert(float,isnull(L.intakeM,0))/C.popM as decimal(8,5)) girM
   , cast(convert(float,isnull(L.intakeF,0))/C.popF as decimal(8,5)) girF
   , cast(convert(float,isnull(L.intake,0))/C.pop as decimal(8,5)) gir
   , cast(convert(float,isnull(L.nIntakeM,0))/C.popM as decimal(8,5)) nirM
   , cast(convert(float,isnull(L.nIntakeF,0))/C.popF as decimal(8,5)) nirF
   , cast(convert(float,isnull(L.nIntake,0))/C.pop as decimal(8,5)) nir


   FROM
	( SELECT E.* from	warehouse.edLevelER E
	  UNION
	  SELECT E.* from	warehouse.edLevelAltER E
		INNER JOIN @EdLevelsU U
			ON E.edLevelCode = U.codeCode
			AND U.S = 1
	  UNION
	  SELECT E.* from	warehouse.edLevelAlt2ER E
		INNER JOIN @EdLevelsU U
			ON E.edLevelCode = U.codeCode
			AND U.S = 2

	) L
	LEFT JOIN Survey S
		ON L.SurveyYEar = S.svyYear
	LEFT JOIN
		( Select surveyYear, yearOfEd,
		avg(popM) popM, avg(popF) popF, avg(pop) pop		-- do this becuase we have the same population on possibly multiple records in here
		-- ( ie these are classlevels at the same yearofed)
		from warehouse.classLevelER
		group by SurveyYear, yearOfEd) C		-- need this for the population of intake year
		ON L.SurveyYear = C.SurveyYear
		AND L.firstYear = C.YearOfEd

 	ORDER BY L.SurveyYear, firstYear
	FOR XML PATH('ER')
	)

	SElect @xmlOut =
		(Select @xml
			FOR XML PATH('ERs')
		)
	-- send the xml as the result set
	if @SendAsXML = 1
		SELECT @xmlOut LevelNers
end

END
GO

