SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 22 10 2013
-- Description:	data for grounds - create the temp table and passes to the EXEC
-- =============================================
CREATE PROCEDURE [dbo].[PIVRooms_Admin]
	-- Add the parameters for the stored procedure here
	@DimensionColumns nvarchar(20) = null,
	@DataColumns nvarchar(20) = null,
	@group nvarchar(40) = null,
	@SchNo nvarchar(50) = null,
	@SurveyYear int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


-- 14 11 2009 SRVU0017
print 'Rooms Admin @group=' + isnull(@group,'(null)')

SELECT *
INTO #tmpPivCols
FROM PIVColsRoomsAdmin R
WHERE
	R.Code in
		(Select rmapCode from metaSchoolTypeRoomMap
			WHERE rMapCode is not null
				AND
					(	rmapGroup = @group
						OR (@group is null ANd rmapGroup not in ('CLASS', 'REC'))
					)
		)


exec dbo.PIVRooms_EXEC
   			@DimensionColumns,
			@DataColumns,
			@Group,
			@SchNo,
			@SurveyYear

END
GO

