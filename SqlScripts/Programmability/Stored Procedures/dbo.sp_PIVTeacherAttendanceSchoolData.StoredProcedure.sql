SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 23 11 2007
-- Description:	for teacher attendance pivot table
-- =============================================
CREATE PROCEDURE [dbo].[sp_PIVTeacherAttendanceSchoolData]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;
-- dump the estimate best survey data

SELECT *
INTO #ebse
FROM dbo.tfnESTIMATE_BestSurveyEnrolments()

SELECT TERM.tmYear AS [Year],
TERM.tmNo AS Term,
TERM.tmWeek AS Week,
TERM.tmDate AS WeekStartDate,
TERM.tmPay AS PayWeek,
ATT.schNo,
count(ATT.tID) as TeachersReported,
sum(ATT.tattAbsentDays) AS Absent,
sum(ATT.tattLateDays) AS Late,
sum(TERM.tmDays) AS TeacherDays,
sum(tattAbsentDays) + sum(tattLateDays) AS Noncompliant,
E.surveyDimensionssID
FROM TermStructure TERM
	INNER JOIN TeacherAttendance ATT ON TERM.tmID = ATT.tmID
	INNER JOIN TeacherIdentity  ON TeacherIdentity.tID = ATT.tID
	INNER JOIN #ebse E ON TERM.tmYear = E.LifeYear and ATT.schNo = E.schNo
group by
	TERM.tmYear,
	TERM.tmNo,
	TERM.tmWeek,
	TERM.tmDate,
	TERM.tmPay,
	ATT.schNo,
	E.surveyDimensionssID

drop table #ebse
END
GO
GRANT EXECUTE ON [dbo].[sp_PIVTeacherAttendanceSchoolData] TO [pTeacherReadX] AS [dbo]
GO

