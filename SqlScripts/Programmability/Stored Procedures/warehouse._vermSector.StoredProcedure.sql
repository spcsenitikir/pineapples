SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 30 12 2016
-- Description:	Calculate the teacher indicators from warehouse
-- =============================================
CREATE PROCEDURE [warehouse].[_vermSector]
	@SendASXML int = 0
	, @xmlOut xml = null OUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @xml xml

	Select @xml =
	(
		Select
		SurveyYear [@year]
		, Sector   [@sectorCode]
		, sum(teachersM) teachersM
		, sum(teachersF) teachersF
		, sum(teachers) teachers
		, sum(CertifiedM) certM
		, sum(CertifiedF) certF
		, sum(Certified) [cert]
		, sum(QualifiedM) qualM
		, sum(qualifiedF) qualF
		, sum(qualified) qual
		, sum(CertQualM) certQualM
		, sum(CertQualF) certQualF
		, sum(CertQual) certQual
		, sum(enrolM) enrolM
		, sum(enrolF) enrolF
		, sum(enrol) enrol
		, convert(decimal(8,3) , cast(sum(certifiedM) as float)/ sum(teachersM)) certPercM
		, convert(decimal(8,3) , cast(sum(certifiedF) as float)/ sum(teachersF)) certPercF
		, convert(decimal(8,3) , cast(sum(certified) as float)/ sum(teachers)) certPerc

		, convert(decimal(8,3) , cast(sum(qualifiedM) as float)/ sum(teachersM)) qualPercM
		, convert(decimal(8,3) , cast(sum(qualifiedF) as float)/ sum(teachersF)) qualPercF
		, convert(decimal(8,3) , cast(sum(qualified) as float)/ sum(teachers)) qualPerc

		, convert(decimal(8,3) , cast(sum(certQualM) as float)/ sum(teachersM)) certQualPercM
		, convert(decimal(8,3) , cast(sum(certQualF) as float)/ sum(teachersF)) certQualPercF
		, convert(decimal(8,3) , cast(sum(certQual) as float)/ sum(teachers)) certQualPerc

		, convert(decimal(8,3) , cast(sum(enrol) as float)/ sum(teachers)) PTR
		, case when isnull(sum(certified),0) = 0 then null
			else convert(decimal(8,3) , cast(sum(enrol) as float)/ sum(certified)) end certPTR
		, case when isnull(sum(qualified),0) = 0 then null
			else convert(decimal(8,3) , cast(sum(enrol) as float)/ sum(qualified)) end qualPTR
		, case when isnull(sum(certqual),0) = 0 then null
			else convert(decimal(8,3) , cast(sum(enrol) as float)/ sum(certQual)) end certQualPTR

		FROM
		(
		Select SurveyYear
		, Sector
		, sum(case when GenderCode = 'M' then 1 end)  teachersM
		, sum(case when GenderCode = 'F' then 1 end)  teachersF
		, count(tID) teachers
		, sum(case when Gendercode = 'M' then Certified end) CertifiedM
		, sum(case when Gendercode = 'F' then Certified end) CertifiedF
		, sum(Certified) Certified
		, sum(case when Gendercode = 'M' then Qualified end) QualifiedM
		, sum(case when Gendercode = 'F' then Qualified end) QualifiedF
		, sum(Qualified) Qualified
		, sum(case when Gendercode = 'M' then Qualified * Certified end) CertQualM
		, sum(case when Gendercode = 'F' then Qualified * Certified end) CertQualF
		, sum(Qualified * Certified ) CertQual
		, null enrolM
		, null enrolF
		, null enrol
		from warehouse.teacherLocation
		GROUP BY SurveyYear, sector
		UNION
		Select SurveyYear
		, SectorCode
		, null teachersM
		, null teachersF
		, null teachers
		, null certifiedM
		, null certifiedF
		, null certified
		, null qualifiedM
		, null qualifiedF
		, null qualified
		, null certQualM
		, null certQualF
		, null certQual
		, sum(case when Gendercode = 'M' then enrol end) enrolM
		, sum(case when Gendercode = 'F' then enrol end) enrolF
		, sum(enrol) enrol
		From  warehouse.SectorEnrol SE
		GROUP BY SurveyYear, SectorCode
		) U
		GROUP BY SurveyYear, Sector
		ORDER BY SurveyYear, Sector
		FOR XML PATH('Sector')
	)
   	SElect @xmlOut =
		(Select @xml
			FOR XML PATH('Sectors')
		)
	-- send the xml as the result set
	if @SendAsXML = 1
		SELECT @xmlOut Sectors
END
GO

