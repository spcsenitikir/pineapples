SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 18 6 2016
-- Description:	Read a book
-- =============================================
CREATE PROCEDURE [pSchoolRead].[BookReadEx]
	-- Add the parameters for the stored procedure here

	@BookCode nvarchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from Books
	WHERE bkCode = @BookCode

	-- next is any curriculum in which the book appears
	Select cuYear
	, cuLevel
	, bkSubject
	FROM SetBooks C
		INNER JOIN Books B
			ON C.bkCode = B.bkCode
	WHERE B.bkCode = @BookCode


	declare @dd TABLE
	(
		d datetime,
		lt nvarchar(20),
		l	nvarchar(50)
	)

	INSERT INTO @dd
	Select max(bkcntDate),
	bkcntLocType,
	bkcntLoc
	FRom BookCount
	WHERE bkCode = @BookCode
	GROUP BY bkcntLocType, bkCntLoc

	-- next holdings of the book
	-- this ultimately needs to be smarter than this....
	Select bkcntDate asatDate
	, bkcntQty qty
	, bkcntReplace qtyToReplace
	, bkcntLocType locType
	, bkcntLoc	   location
	, schName		locationName

	from BookCount BC
		INNER JOIN @dd d
			ON BC.bkcntDate = d.d
			AND BC.bkcntLocType = d.lt
			AND BC.bkcntLoc = d.l
	LEFT JOIN Schools S
		ON bkcntLocType = 'SCHOOL'
		AND bkcntLoc = S.schNo
	WHERE bkCode = @BookCode
END
GO

