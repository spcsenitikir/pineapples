SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 20 2 2010
-- Description:	Audit the Teacher Quota for a given enrolment projection
-- =============================================
CREATE PROCEDURE [pEstablishmentRead].[QuotaForScenario]
	-- Add the parameters for the stored procedure here
	@ScenarioID int
	, @estYear int = null
	, @schoolNo nvarchar(50) = null
	, @PivotOutput int = 1

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


-- for now, much of this is cut-and-paste from
-- [pEstablishmentWrite].[CreateSchoolQuotasForYear]

	-- begin by pulling out the schools enrolment projection

	begin try
	-- 20 2 2010 allow a ScenarioId to be passed to this routine to
	-- overwrite the one assocaited with the establishment plan
	--
		if (@ScenarioID is null) begin
			Select @ScenarioID = escnID
			FROM
				EstablishmentControl
			WHERE
				estcYear = @EstYear
			-- all in a trnasaction
		end


		If (@ScenarioID is null)
			Raiserror('No projection scenario is defined for this establishment year',16,1)

		begin transaction

		-- first get the total of the projection by level, for each school
		-- note allow ,multiple years
		Select EP.SchNo, epYear, epdLevel levelCode,
		sum(epdSum) Enrol

		into #enrol
		from
			EnrolmentScenario S
			INNER JOIN EnrolmentProjection EP on S.escnId = EP.escnID
			INNER JOIN EnrolmentProjectionData EPD on EP.epID = EPD.epID
		WHERE
			S.escnID = @ScenarioID
			and (EP.schNo = @SchoolNo or @SchoolNo is null)
			and (EP.epYEar = @EstYear or @estYear is null)

		group by EP.SchNo, epYear, epdLevel

		-- first we have to work out what formula tables apply
		-- here's the strategy
		-- get all possible combinations of schools and tables
		-- rank the tables in their groups according to the number of levels - smallest is best
		-- find all combinations of school and table WHERE
		-- there exists at least one level for the school in the group
		-- there are no levels for the school in the group that are not in this table
		-- then, because the tables may be subsets of each other, we take the smallest one by rank

		-- a simple helper query
		-- gives us all the levels that may apply in a group
		Select efGroup, eflLevel
		into #groupLevels
		from EFTAbles INNER JOIN EFTableLevels on EFTables.efTable = EFTableLevels.efTable
		group by efGRoup, eflLevel


		Select schNo, epYear, efTable
		into #SchoolEFTables
		from
		(
		-- schools and applicable tables
		-- the tables in each table group are ranked from smallest to largest: we only want the smallest one
		Select SchNo, epYear, T.efGRoup, T.EFTable, row_number() OVER(PARTITION BY schNo, epYear, T.efGroup ORDER BY TableLevels) NUM
		from
			-- this query returns the school and all the tables that apply to it
			-- at this stage there may be multiple tables in each table group
			-- this cross join schools and EFTables
			(Select DISTINCT schNo , epYear from #enrol) E,
			-- subquery T is the number of level in each table in each group
			-- at the top level, we rank by the number of levels to select the smallest applicable table
			(Select efGRoup, EFTables.efTable, count(eflLevel) TableLevels
				from EFTAbles
				INNER JOIN EFTableLevels ON efTables.efTable = efTableLevels.efTable
				GROUP BY efGRoup, EFTables.efTable) T

			WHERE
				-- this condition exlcudes any table in T , where that Table
				-- does not contain a Class Level for which this school has an enrolment
				-- where that class level is included in the grop of table T
				-- e.g. if the Table is a Secondary Table
				-- it must include all the Secondary levels for which this class has an enrolment
				-- Becuae the T may be nested, there may be multiple tables matching this condition for any Group
				not exists
				(Select levelCode from #enrol EE
					inner join
					#groupLevels MM on MM.eflLevel = EE.levelCode


					WHERE levelCode not in (Select eflLevel from EFTableLevels WHERE efTable = T.EFTable)
					and E.schNo = EE.schNo
					and E.epYear = EE.epYear
					and T.efGroup = MM.efGroup
				)
			and exists
				-- we want to be sure that the school is represented in the group
				-- ie without this clause , we would get a secondary Table from the previous condition if the
				-- school had no secondary enrolments
				(Select levelCode from #enrol EEE
					inner join
					#groupLevels MMM on MMM.eflLevel = EEE.levelCode
					and E.schNo = EEE.schNo
					and E.epYear = EEE.epYear
					and T.efGroup = MMM.efGroup

				)
			) sub

		WHERE Num = 1


		-- after the above, #schoolEFTables contains each school , Year in the projection
		-- and the tables that apply to it. There is at most one table from each table group,
		-- and that is the smallest table in the table group to contain all the levels for the school


-- the update strategy is to preserve the Current User values by writing them first into the temp table
-- then the calculated values are written into the temp table
-- all the SchooleEstablishmentRole records for the year are deleted and re-added


		create table #tempSER
		(
		schNo nvarchar(50)
		, epYear int
		, rgCode nvarchar(10)
		, Quota int
		, CountUser int
		)


		INSERT INTO #tempSER

		Select schNo, epYear, rgCode, sum(efaAlloc) Quota, null
		from EFAllocations
			inner join EFTableBands on EFAllocations.efbID = EFTableBands.efbID

		-- for each selected table
			-- get the total enrolment for the levels in that table
		inner join
		-- subquery TableEnrol finds the total enrol at the school for all level included in the table
		(Select #enrol.schNo, #enrol.epYear, #SchoolEFTables.efTable,  sum(Enrol) TotEnrol
		from #enrol
			INNER JOIN EFTableLevels on EFTAbleLevels.eflLevel = #enrol.levelCode
			INNER JOIN #SchoolEFTables
				ON #SchoolEFTables.schNo = #enrol.SchNo
				AND #SchoolEFTables.epYear = #enrol.epYear
			WHERE #SchoolEFTables.efTable = efTableLevels.efTable
		group by #enrol.schNo, #enrol.epYear, #SchoolEFTables.efTable
		) TableEnrol on TableEnrol.efTable = EFTableBAnds.efTable

		-- the table band that applies is the one that includes the school total enrolment
		WHERE TableEnrol.TotEnrol between EFTableBands.efbMin and EFTableBands.efbMax

		group by SchNo, epYear, rgCode


if @PivotOutput = 0 begin
	Select * from #tempSER
end


if @PivotOutput = 1 begin


	-- the following queries collect the required field names for the pivots - class levels, role grades and salary points
			declare @s nvarchar(4000)
			declare @s2 nvarchar(4000)
			declare @s3 nvarchar(4000)
			Select @s = ''
			Select @s = case @s when '' then quotename(rgCode) else @s + ', ' + quotename(rgCode) end
			FRom RoleGRades
			WHERE rgCode in (Select rgCode from EFAllocations)
			ORDER BY rgSort, rgSalaryLevelMax DESC, rgSalaryLevelMin DESC

			------------Select schNo, estrRoleGrade, estrCount, rgSalaryPointMedian
			------------FROM
			------------ SchoolEstablishmentRoles SER
			------------INNER JOIN SchoolEstablishment SE
			------------	ON SER.estID = SE.estID
			------------INNER JOIN RoleGRades
			------------	ON RoleGrades.rgCode = SER.estrRoleGrade
			------------WHERE SE.estYear = 2010

			declare @sql nvarchar(4000)

			--------select @sql = 'Select schNo, epYear' + @s +
			--------' FROM (Select schNo, epYear, rgCode, Quota from #tempSER) S
			--------PIVOT ( sum(Quota) FOR rgCode In (' + @s + ')) p'

			--------print @sql

			--------EXECUTE sp_executesql @sql

			-- now do the same trick to get the class levels

			Select @s2 = ''
			Select @s2 = case @s2 when '' then quotename(codeCode) else @s2 + ', ' + quotename(codeCode) end
			FRom lkpLevels
			WHERE codeCode in (Select levelCode from #enrol)
			ORDER BY lvlYear, ilsCode


			-- now do the same trick to get the class levels

			Select @s3 = ''
			Select @s3 = case @s3 when '' then quotename(spCode) else @s3 + ', ' + quotename(spCode) end
			FRom lkpSalaryPoints
			WHERE spCode in (Select rgSalaryPointMedian from roleGrades)
			ORDER BY spSort DESC, spCode DESC


	-- this query joins 3 pivots - on role grade, class level and salary point using the field names collected in @s, @s2, @s3
			SELECT @sql = 'Select E.schNo, schName, E.epYear, ' + @s2 + ', ' + @s + ', ' + @s3 +
					' FROM (Select schNo, epYear, ' + @s2 +
								' FROM #enrol
								PIVOT ( sum(Enrol) FOR levelCode In (' + @s2 + ')) p
							) E
					 INNER JOIN Schools ON E.schNo = Schools.schNo
					LEFT JOIN (Select schNo, epYear, ' + @s +
								'  FROM #tempSER S
									PIVOT ( sum(Quota) FOR rgCode In (' + @s + ')) p2
							) Q
						ON Q.schNo = E.schNo AND Q.epYear = E.epYear
					LEFT JOIN (Select schNo, epYear, ' + @s3 +
								' FROM (Select #tempSER.schNo, epYear, rgSalaryPointMedian, sum(Quota) Quota
										FROM #tempSER INNER JOIN roleGRades ON #tempSER.rgCode = RoleGrades.rgCode
										GROUP BY schno, epYear, rgSalaryPointMedian) TT
									PIVOT ( sum(Quota) FOR rgSalaryPointMedian in (' + @s3 + ')) p3
							) SP
						ON SP.schNo = E.schNo AND SP.epYear = E.epYear'

	print @sql

			EXECUTE sp_executesql @sql
end

	DROP TABLE #tempSER
	DROP TABLE #enrol


		commit transaction			-- does it need a transation?
	end try


		begin catch
	    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;

		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()


		if @@trancount > 0
			begin
				rollback transaction
				select @errorMessage = @errorMessage + ' The transaction was rolled back.'
			end

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		return @err
	end catch

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.


END
GO

