SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 12/11/2007
-- Description:	Return the estimated enrolments
-- =============================================
-- Consolidation values:
-- 0 - full detail with all Dimension fields

-- Consolidation     DISTRICT   AUTHORITY  SCHOOL TYPE    LEVEL    AGE    GENDER    RANK	SCHOOL ESTIMATE  EDLevelMin/Max
--        1             x          x            x	         x      x        x                        x
--        2             x                       x	         x      x        x                        x
--        3             x                       x	         x               x                        x
--        4             x                       x	         x               x        x               x
--        5             x                       x	         x               x                x       x
--        6             x                       x	         x      x        x                        x             x
--        7             x                       x	         x      x        x        x               x
--        8                                     	         x      x        x        x               x
--        9                                      	         x               x                        x
--		 10													 x      x


CREATE PROCEDURE [dbo].[PIVEnrolment]
	(@Consolidation int = 0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;


select *
into #ebse
from dbo.tfnESTIMATE_BestSurveyEnrolments()

if @Consolidation in (0,4,5,7,8) -- these use Rank
	BEGIN
		exec BuildRank

		SELECT     EE.schNo
					, EE.LifeYear AS [Survey Year]
					, dbo.Enrollments.enAge
					, dbo.Enrollments.enLevel
					, enM
					, enF
					, EE.bestssID
					, EE.surveyDimensionssID
					, EE.Estimate
					, EE.Offset AS [Age of Data]
					, EE.bestYear AS [Year of Data]
					, EE.bestssqLevel AS DataQualityLevel
					, EE.ActualssqLevel AS SurveyYearQualityLevel
					, dbo.Enrollments.enAge - (dbo.Survey.svyPSAge + dbo.lkpLevels.lvlYear - 1) AS AgeOffset
					, dbo.Survey.svyYear - dbo.Enrollments.enAge AS YearOfBirth

						,	DR.[District Rank]
						,	DR.[District Decile]
						,	DR.[District Quartile]
						,	DR.[Rank]
						,	DR.Decile
						,	DR.Quartile


		into #eRank
		FROM         #ebse EE INNER JOIN
							  dbo.Enrollments ON EE.bestssID = dbo.Enrollments.ssID INNER JOIN
							  dbo.lkpLevels ON dbo.Enrollments.enLevel = dbo.lkpLevels.codeCode INNER JOIN
							  dbo.Survey ON EE.LifeYear = dbo.Survey.svyYear
								LEFT JOIN DimensionRank DR
								on EE.LifeYear = DR.svyYEar
								and EE.schNo = DR.schNo

	END
else
	SELECT     EE.schNo, EE.LifeYear AS [Survey Year], dbo.Enrollments.enAge, dbo.Enrollments.enLevel,
						enM, enF,
						EE.bestssID,
						EE.surveyDimensionssID,
						EE.Estimate,
						  EE.Offset AS [Age of Data], EE.bestYear AS [Year of Data],
						  EE.bestssqLevel AS DataQualityLevel, EE.ActualssqLevel AS SurveyYearQualityLevel,
						  dbo.Enrollments.enAge - (dbo.Survey.svyPSAge + dbo.lkpLevels.lvlYear - 1) AS AgeOffset, dbo.Survey.svyYear - dbo.Enrollments.enAge AS YearOfBirth


	into #e
	FROM         #ebse EE INNER JOIN
						  dbo.Enrollments ON EE.bestssID = dbo.Enrollments.ssID INNER JOIN
						  dbo.lkpLevels ON dbo.Enrollments.enLevel = dbo.lkpLevels.codeCode INNER JOIN
						  dbo.Survey ON EE.LifeYear = dbo.Survey.svyYear

-- school survey data
-- at school level, or higher
if @consolidation = 0 or @Consolidation = 5
		BEGIN
			print @Consolidation
			Select * INTO #ds
				from DimensionSchoolSurveyNoYear
				ORDER BY [Survey ID]

		END
else

--
--we make a leaner subset of DimensionSchoolSurvey
	Select ssId [Survey ID]
		, ssSchType SchoolTypeCode
		, ssAuth AuthorityCode
		, iGroup [District Code]
	INTO #dsSmall

	from DimensionSchoolSurveySub S
		INNER JOIN Islands I
		on s.iCode = I.iCode;


-- take a subset of these
SELECT
	LevelCode
	, [Level]
	, [Year of Education]
	, SectorCode
	, Sector
	, edLevelCode
	, [Education Level]
	, edLevelAltCode
	, [Education Level Alt]
	, edLevelAlt2Code
	, [Education Level Alt2]
	, [ISCED Level Name]
	, [ISCED Level]
	, [ISCED SubClass]
	, LevelGV

into #dl
FROM        DimensionLevel ORDER BY [Year of Education],levelCode


select * into #G
from DimensionGender

--if @Consolidation = 0
	-- this is equivalent to the full detail as assembled by the Pineapples client
	-- from vtblEnrolmentEst, DimensionSchoolSurvey, DimensionLevel, DimensionGender

-- all agregations present District, School Type and Gender
-- and possibly Age, Level, Authority


if @Consolidation = 0
	BEGIN
-- this option will return the full detail that we currently get from
-- pivEnrolmentEst, but without client-side assembly


		Select E.[Survey Year]

			, e.Estimate
			, E.[Age of Data]
			, e.[Year of Data]
			, e.DataQualityLevel
			, e.SurveyYearQualityLevel

-- age
			, e.enAge as Age
			, e.AgeOffset
			, e.YearOfBirth
-- gender
			, G.*
-- level
			, DL.*
-- school/survey
			, DS.*
-- rank

			,	E.[District Rank]
			,	E.[District Decile]
			,	E.[District Quartile]
			,	E.[Rank]
			,	E.Decile
			,	E.Quartile

			, e.bestssID
			, e.surveyDimensionssID


		, (case when G.GenderCode = 'M' then enM else enF end) Enrol
		, (enM) EnrolM
		, (enF) EnrolF


	from #eRank E
		INNER JOIN #DL DL
		on E.enLevel = DL.[LevelCode]
		INNER JOIN #DS DS
		on DS.[Survey ID] = e.surveyDimensionssID

		CROSS JOIN #G G

	END			-- consolidation = 0


IF @Consolidation = 1
	BEGIN
-- aggregates up to age, level, authority

	Select * into #da
	from DimensionAuthority;

	WITH X AS
	(
		Select #E.[Survey Year]
			, #E.Estimate
			, enAge as Age
			, enLevel
			, DS.SchoolTypeCode
			, DS.[District Code]
			, DS.AuthorityCode


			, sum(enM) EnrolM
			, sum(enF) EnrolF


		from #E
			INNER JOIN #dsSmall DS
			on DS.[Survey ID] = #e.surveyDimensionssID


		group by [Survey Year], enAge, enLevel
			,#E.Estimate
			, DS.SchoolTypeCode
			, DS.[District Code]
			, DS.AuthorityCode

	)
		Select X.[Survey Year]
			, X.Estimate
			, X.Age
			, G.*
			, X.SchoolTypeCode
			, ST.stDescription SchoolType
			, X.[District Code]
			, D.dName District
			, DL.*
			, DA.*

			, (case when G.GenderCode = 'M' then X.enrolM else null end) EnrolM
			, (case when G.GenderCode = 'F' then X.enrolF else null end) EnrolF
			, (case when G.GenderCode = 'M' then X.enrolM else X.enrolF end) Enrol
			, (case when G.GenderCode = 'M' then X.enrolM else X.enrolF end)*X.Age Age_x_Enrol
		from X
			INNER JOIN #DL DL
			on X.enLevel = DL.[LevelCode]
			INNER JOIN #da DA
			on DA.AuthorityCode = X.AuthorityCode
			-- -- a nit of experimentation shows that while these joins
			-- generate lots of reads compared to using DimensionSchoolSurvey to get these values
			--- CPU and Duration are both nonetheless smaller
			INNER JOIN Districts D
			on X.[District Code] = D.dID
			INNER JOIN SchoolTypes ST
			on X.SchoolTypeCode = ST.stcode
			CROSS JOIN #G G
	END

IF @Consolidation = 2
-- aggregates up to age, level
	BEGIN
-- aggregates up to level
	WITH X AS
	(
		Select #E.[Survey Year]
			, #E.Estimate
			, enAge as Age
			, enLevel
			, DS.SchoolTypeCode
			, DS.[District Code]


			, sum(enM) EnrolM
			, sum(enF) EnrolF


		from #E
			INNER JOIN #dsSmall DS
			on DS.[Survey ID] = #e.surveyDimensionssID

		group by [Survey Year], enAge, enLevel
			,#E.Estimate
			, DS.SchoolTypeCode
			, DS.[District Code]

	)
		Select X.[Survey Year]
			, X.Estimate
			, X.Age
			, G.*
			, X.SchoolTypeCode
			, ST.stDescription SchoolType
			, X.[District Code]
			, D.dName District
			, DL.*

			, (case when G.GenderCode = 'M' then X.enrolM else null end) EnrolM
			, (case when G.GenderCode = 'F' then X.enrolF else null end) EnrolF
			, (case when G.GenderCode = 'M' then X.enrolM else X.enrolF end) Enrol
			, (case when G.GenderCode = 'M' then X.enrolM else X.enrolF end)*X.Age Age_x_Enrol

		from X
			INNER JOIN #DL DL
			on X.enLevel = DL.[LevelCode]
			INNER JOIN Districts D
			on X.[District Code] = D.dID
			INNER JOIN SchoolTypes ST
			on X.SchoolTypeCode = ST.stcode
			CROSS JOIN #G G
	END
IF @Consolidation = 3
-- aggregates up to level
	BEGIN

	WITH X AS
	(
		Select #E.[Survey Year]
			, #E.Estimate
			, enLevel
			, DS.SchoolTypeCode
			, DS.[District Code]


			, sum(enM) EnrolM
			, sum(enF) EnrolF


		from #E
			INNER JOIN #dsSmall DS
			on DS.[Survey ID] = #e.surveyDimensionssID

		group by [Survey Year]
			, enLevel
			,#E.Estimate
			, DS.SchoolTypeCode
			, DS.[District Code]

	)
		Select X.[Survey Year]
			, X.Estimate
			, G.*
			, X.SchoolTypeCode
			, ST.stDescription SchoolType
			, X.[District Code]
			, D.dName District
			, DL.*

			, (case when G.GenderCode = 'M' then X.enrolM else null end) EnrolM
			, (case when G.GenderCode = 'F' then X.enrolF else null end) EnrolF
			, (case when G.GenderCode = 'M' then X.enrolM else X.enrolF end) Enrol

		from X
			INNER JOIN #DL DL
			on X.enLevel = DL.[LevelCode]
			INNER JOIN Districts D
			on X.[District Code] = D.dID
			INNER JOIN SchoolTypes ST
			on X.SchoolTypeCode = ST.stcode
			CROSS JOIN #G G
	END

IF @Consolidation = 4
-- aggregates up to level and rank
	BEGIN
-- aggregates up to level

	WITH X AS
	(
		Select E.[Survey Year]
			, E.Estimate
			, enLevel
			, DS.SchoolTypeCode
			, DS.[District Code]


			, E.[District Decile]
			,	E.[District Quartile]

			,	E.Decile
			,	E.Quartile

			, sum(enM) EnrolM
			, sum(enF) EnrolF


		from #eRank E
			INNER JOIN #dsSmall DS
			on DS.[Survey ID] = e.surveyDimensionssID
		group by [Survey Year]
			, enLevel
			,E.Estimate
			, DS.SchoolTypeCode
			, DS.[District Code]

			,	E.[District Decile]
			,	E.[District Quartile]

			,	E.Decile
			,	E.Quartile

	)
		Select X.[Survey Year]
			, X.Estimate
			, G.*
			, X.SchoolTypeCode
			, ST.stDescription SchoolType
			, X.[District Code]
			, D.dName District
			, DL.*

			, X.[District Decile]
			, X.[District Quartile]

			, X.Decile
			, X.Quartile

			, (case when G.GenderCode = 'M' then X.enrolM else null end) EnrolM
			, (case when G.GenderCode = 'F' then X.enrolF else null end) EnrolF
			, (case when G.GenderCode = 'M' then X.enrolM else X.enrolF end) Enrol

		from X
			INNER JOIN #DL DL
			on X.enLevel = DL.[LevelCode]
			INNER JOIN Districts D
			on X.[District Code] = D.dID
			INNER JOIN SchoolTypes ST
			on X.SchoolTypeCode = ST.stcode
			CROSS JOIN #G G
	END

if @Consolidation = 5
	BEGIN

	WITH X AS
	(
		Select E.[Survey Year]
			, E.Estimate
			, enLevel
			, DS.SchoolTypeCode
			, DS.[District Code]

			, DS.[School No]
			, DS.[School Name]
			, DS.SchoolID_Name
			, DS.SchoolID_Name_Type

			, E.[District Rank]
			, E.[District Decile]
			, E.[District Quartile]

			, E.[Rank]
			, E.Decile
			, E.Quartile

			, sum(enM) EnrolM
			, sum(enF) EnrolF


		from #eRank E
			INNER JOIN #ds DS
			on DS.[Survey ID] = e.surveyDimensionssID
		group by [Survey Year]
			, enLevel
			,E.Estimate
			, DS.SchoolTypeCode
			, DS.[District Code]

			, DS.[School No]
			, DS.[School Name]
			, DS.SchoolID_Name
			, DS.SchoolID_Name_Type

			, E.[District Rank]
			, E.[District Decile]
			, E.[District Quartile]

			, E.[Rank]
			, E.Decile
			, E.Quartile


	)
		Select X.[Survey Year]
			, X.Estimate
			, X.[School No]
			, X.[School Name]
		--	, X.SchoolID_Name
		--	, X.SchoolID_Name_Type

			, X.SchoolTypeCode
			, ST.stDescription SchoolType
			, X.[District Code]
			, D.dName District
			, G.*
		--	, DL.*
			, DL.LevelCode
			, DL.Level
		--	, X.[District Rank]
		--	, X.[District Decile]
		--	, X.[District Quartile]
		--	, X.[Rank]
		--	, X.Decile
		--	, X.Quartile

		--	, (case when G.GenderCode = 'M' then X.enrolM else null end) EnrolM
		--	, (case when G.GenderCode = 'F' then X.enrolF else null end) EnrolF
			, (case when G.GenderCode = 'M' then X.enrolM else X.enrolF end) Enrol

		from X
			INNER JOIN #DL DL
			on X.enLevel = DL.[LevelCode]
			INNER JOIN Districts D
			on X.[District Code] = D.dID
			INNER JOIN SchoolTypes ST
			on X.SchoolTypeCode = ST.stcode
			CROSS JOIN #G G
	END

IF @Consolidation = 6
	BEGIN
-- this option returns school  level data, including ages,
-- and a restricted set of columns
-- needed for AtAGlance
	WITH X AS
	(
		Select E.[Survey Year]
			, E.Estimate
			, enAge as Age
			, enLevel
			, DS.SchoolTypeCode
			, DS.[District Code]


			, sum(enM) EnrolM
			, sum(enF) EnrolF


		from #e E
			INNER JOIN #dsSmall DS
			on DS.[Survey ID] = e.surveyDimensionssID

		group by [Survey Year], enAge, enLevel
			,E.Estimate
			, DS.SchoolTypeCode
			, DS.[District Code]

	)
		Select X.[Survey Year]
			, X.Estimate
			, X.Age
			, G.*
			, X.SchoolTypeCode
			, ST.stDescription SchoolType
			, X.[District Code]
			, D.dName District
			, DL.*

			, (case when G.GenderCode = 'M' then X.enrolM else null end) EnrolM
			, (case when G.GenderCode = 'F' then X.enrolF else null end) EnrolF
			, (case when G.GenderCode = 'M' then X.enrolM else X.enrolF end) Enrol
			, (case when G.GenderCode = 'M' then X.enrolM else X.enrolF end)*X.Age Age_x_Enrol

		from X
			INNER JOIN DimensionLevel DL
			on X.enLevel = DL.[LevelCode]
			INNER JOIN Districts D
			on X.[District Code] = D.dID
			INNER JOIN SchoolTypes ST
			on X.SchoolTypeCode = ST.stcode
			CROSS JOIN #G G
	END
IF @Consolidation = 7
	BEGIN
-- this option returns school  level data, including ages,
-- and a restricted set of columns
-- needed for AtAGlance
	WITH X AS
	(
		Select E.[Survey Year]
			, E.Estimate
			, enAge as Age
			, enLevel
			, DS.SchoolTypeCode
			, DS.[District Code]
			, E.[District Decile]
			, E.[District Quartile]

			, E.Decile
			, E.Quartile


			, sum(enM) EnrolM
			, sum(enF) EnrolF


		from #eRank E
			INNER JOIN #dsSmall DS
			on DS.[Survey ID] = e.surveyDimensionssID

		group by [Survey Year], enAge, enLevel
			,E.Estimate
			, DS.SchoolTypeCode
			, DS.[District Code]
			, E.[District Decile]
			, E.[District Quartile]

			, E.Decile
			, E.Quartile

	)
		Select X.[Survey Year]
			, X.Estimate
			, X.Age
			, G.*
			, X.SchoolTypeCode
			, ST.stDescription SchoolType
			, X.[District Code]
			, D.dName District
			, DL.*
			, X.[District Decile]
			, X.[District Quartile]

			, X.Decile
			, X.Quartile
			, (case when G.GenderCode = 'M' then X.enrolM else null end) EnrolM
			, (case when G.GenderCode = 'F' then X.enrolF else null end) EnrolF
			, (case when G.GenderCode = 'M' then X.enrolM else X.enrolF end) Enrol
			, (case when G.GenderCode = 'M' then X.enrolM else X.enrolF end)*X.Age Age_x_Enrol

		from X
			INNER JOIN DimensionLevel DL
			on X.enLevel = DL.[LevelCode]
			INNER JOIN Districts D
			on X.[District Code] = D.dID
			INNER JOIN SchoolTypes ST
			on X.SchoolTypeCode = ST.stcode
			CROSS JOIN #G G
	END

--- 8 -------------------------------------------------
IF @Consolidation = 8
	BEGIN
-- no district or school type
-- for fast reporting servies
	WITH X AS
	(
		Select E.[Survey Year]
			, E.Estimate
			, enAge as Age
			, enLevel

			, E.Decile
			, E.Quartile


			, sum(enM) EnrolM
			, sum(enF) EnrolF


		from #eRank E
			INNER JOIN #dsSmall DS
			on DS.[Survey ID] = e.surveyDimensionssID

		group by [Survey Year], enAge, enLevel
			,E.Estimate

			, E.Decile
			, E.Quartile

	)
		Select X.[Survey Year]
			, X.Estimate
			, X.Age
			, G.*
			, DL.*

			, X.Decile
			, X.Quartile
			, (case when G.GenderCode = 'M' then X.enrolM else null end) EnrolM
			, (case when G.GenderCode = 'F' then X.enrolF else null end) EnrolF
			, (case when G.GenderCode = 'M' then X.enrolM else X.enrolF end) Enrol
			, (case when G.GenderCode = 'M' then X.enrolM else X.enrolF end)*X.Age Age_x_Enrol

		from X
			INNER JOIN DimensionLevel DL
			on X.enLevel = DL.[LevelCode]
			CROSS JOIN #G G
	END

--- 9 -------------------------------------------------
IF @Consolidation = 9
	BEGIN
-- no district or school type, age or rank
-- for fast reporting servies
	WITH X AS
	(
		Select E.[Survey Year]
			, E.Estimate
			, enLevel


			, sum(enM) EnrolM
			, sum(enF) EnrolF


		from #e E
			INNER JOIN #dsSmall DS
			on DS.[Survey ID] = e.surveyDimensionssID

		group by [Survey Year],  enLevel
			,E.Estimate


	)
		Select X.[Survey Year]
			, X.Estimate
			, G.*
			, DL.*

			, (case when G.GenderCode = 'M' then X.enrolM else null end) EnrolM
			, (case when G.GenderCode = 'F' then X.enrolF else null end) EnrolF
			, (case when G.GenderCode = 'M' then X.enrolM else X.enrolF end) Enrol

		from X
			INNER JOIN DimensionLevel DL
			on X.enLevel = DL.[LevelCode]
			CROSS JOIN #G G
	END


--- 10 -------------------------------------------------
IF @Consolidation = 10
	BEGIN
-- no district or school type, age or rank
-- for fast reporting servies

	Select E.[Survey Year]
	, enLevel levelCode
	, [Year of Education]
	, enAge Age
	, sum(isnull(enM,0) + isnull(enF,0)) Enrol
	, sum(enM) enrolM
	, sum(enF) enrolF
	FROM #E E
	INNER JOIN #dl DL
	On DL.levelCode = E.enLevel
	GROUP BY [Survey Year]
	, enLevel
	, [Year of Education]
	, enAge


	END
END
GO
GRANT EXECUTE ON [dbo].[PIVEnrolment] TO [pEnrolmentRead] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[PIVEnrolment] TO [public] AS [dbo]
GO

