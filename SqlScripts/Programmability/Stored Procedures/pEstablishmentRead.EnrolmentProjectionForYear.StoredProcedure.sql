SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 19 10 2009
-- Description:	retrieve the relevant enrolment projection data
-- =============================================
CREATE PROCEDURE [pEstablishmentRead].[EnrolmentProjectionForYear]
	-- Add the parameters for the stored procedure here

	@EstYear int
	, @SchoolNo nvarchar(50) = null
	, @Authority nvarchar(10) = null
	, @SchoolType nvarchar(10) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @ScenarioID int		-- if this flag is on, positions currently underpaid will be assumed to stay underpaid

	-- get the parameter values set up on the establishment control

	begin try
		SELECT
			@ScenarioID = escnID
		FROM
			EstablishmentControl
		WHERE
			estcYear = @EstYear

		SELECT EP.schNo
				, epYear
				, EPD.*
				, epdSum epdTotal			-- eodSum added to table, but we'l preserve this name
				, epLocked
		FROM
			EnrolmentPRojection EP
			INNER JOIN  Schools
			ON EP.schNo = Schools.schNo
			INNER JOIN EnrolmentProjectionData EPD
			ON EP.epID = EPD.epID
			INNER JOIN TRLevels L
			ON EPD.epdLevel = L.codeCode

			WHERE
				EP.escnID = @ScenarioID
				AND (EP.SchNo = @SchoolNo or @SchoolNo = null)
				AND epYEar = @EstYear

-- second recordset is aggregate projection for the parent

-- if this school has an extension, or is an extension, calcuate the totals for its establishment point


		SELECT SE.estEstablishmentPoint schNo
				, epYear
				, epdLevel
				, sum(epdM) epdM
				, sum(epdF) epdF
				, sum(epdU) epdU
				, sum(epdSum) epdSum
		FROM
			SchoolEstablishment SE
			INNER JOIN SchoolEstablishment SE2
				ON SE.estYear = SE2.estYear
				AND SE.estEstablishmentPoint = SE2.estEstablishmentPoint
			INNER JOIN EnrolmentPRojection EP
				ON Se2.schNo = EP.schNo
				AND SE2.estYEar = EP.epYear
			INNER JOIN  Schools
			ON EP.schNo = Schools.schNo
			INNER JOIN EnrolmentProjectionData EPD
			ON EP.epID = EPD.epID
			INNER JOIN TRLevels L
			ON EPD.epdLevel = L.codeCode

			WHERE
				EP.escnID = @ScenarioID
				-- we don't want to duplicate everything here
				-- we just want thise where its interesting - a SE.schNo is a parent of child
				AND
				(SE.schNo = @SchoolNo or @SchoolNo = null)		-- the school/schools we are considering
				AND (
					-- it's a parent....
					SE.schNo in
						(Select estParent from SchoolEstablishment WHERE estYear = @EstYear)
					OR
					-- it's a child
					SE.estParent is not null
					)

				AND SE.estYEar = @EstYear
			GROUP BY SE.estEstablishmentPoint, epYear, epdLevel


	-- a third record set assist in editing when it is a school
	-- this is the available levels for the school type
	-- it is actually the full set of levels in the defualt path, but, if there is a level at the
	-- school type not in the default path, that level appears for its level year
	-- the deault path in other words is the default set of levels

	If @schoolNo is not null begin
		select @SchoolType = schType
		From SChools
		WHERE schNo = @SchoolNo
	end

	CREATE TABLE #tmp
	 (
	 lvlYEar int
	 , levelCode nvarchar(10)
	 , levelDesc nvarchar(20)
	 )

	 -- if @SChoolType is null, we'll just get teh default Education Path
	 INSERT INTO #tmp
	 Select lvlYear, codeCode, codeDescription
	 FROM lkpLevels L
	 INNER JOIN metaSchoolTypeLevelMap TM
	 ON TM.tlmLevel = L.codecode
	 WHERE stCode = @SchoolType

	 INSERT INTO #tmp
	 Select lvlYear, codeCode, codeDescription
	 FROM lkpLevels L
	 INNER JOIN EducationPathLevels
		ON L.codeCode = EducationPathLevels.levelCode
	 INNER JOIN EducationPaths
	 ON EducationPAthLevels.pathCode = EducationPaths.pathCode
	 WHERE pathDefault = 1
	 AND lvlYear not in (Select lvlYear from #tmp)

	 SELECT * from #tmp ORDER BY lvlYEar

	 DROP TABLE #tmp


	end try
	begin catch

		DECLARE @err int,
			@ErrorMessage NVARCHAR(4000),
			@ErrorSeverity INT,
			@ErrorState INT;
		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()


		if @@trancount > 0
			begin
				rollback transaction
				select @errorMessage = @errorMessage + ' The transaction was rolled back.'
			end

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		return @err
	end catch

    -- Insert statements for procedure here
END
GO

