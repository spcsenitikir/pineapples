SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 3 6 2016
-- Description:	returns a string representing the
-- permissions available to the nominated user
-- =============================================
CREATE PROCEDURE [dbo].[permissionHash]
	-- Add the parameters for the stored procedure here
	@username nvarchar(200),
	@grp0 nvarchar(200) = null,
	@grp1 nvarchar(200) = null,
	@grp2 nvarchar(200) = null,
	@grp3 nvarchar(200) = null,
	@grp4 nvarchar(200) = null,
	@grp5 nvarchar(200) = null,
	@grp6 nvarchar(200) = null,
	@grp7 nvarchar(200) = null,
	@grp8 nvarchar(200) = null,
	@grp9 nvarchar(200) = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- set up the table of permssion areas
	declare @areas TABLE
	(
		area nvarchar(50)
	)
	INSERT INTO @Areas VALUES('Enrolment'),
	('Establishment'),
	('Exam'),
	('Finance'),
	('Infrastructure'),
	('Inspection'),
	('IST'),
	('PA'),
	('School'),
	('Survey'),
	('Teacher')

	-- table of levels for each area

	declare @roles TABLE
	(
		rolename nvarchar(200)
	)
	declare @levels TABLE
	(
		a nvarchar(100),
		level int

	)

	-- collect the roles
	;
	with rm(r)
	AS
	(
		SELECT r.name
		FROM sys.database_role_members AS m
			INNER JOIN sys.database_principals AS r
				ON m.role_principal_id = r.principal_id
			INNER JOIN sys.database_principals AS u
				ON u.principal_id = m.member_principal_id
		WHERE u.name in (@username, @grp0, @grp1, @grp2, @grp3, @grp4, @grp5, @grp6, @grp7, @grp8, @grp9)

		UNION ALL
		SELECT r.name
		FROM sys.database_role_members AS m
			INNER JOIN sys.database_principals AS r
				ON m.role_principal_id = r.principal_id
			INNER JOIN sys.database_principals AS u
				ON u.principal_id = m.member_principal_id
			INNER JOIN rm
				ON u.name = rm.r
	)

	INSERT INTO @roles
	Select DISTINCT r
	FROM rm

	INSERT INTO @Levels
	select area
	, sum(
	case (replace(r.rolename, 'p' + area, ''))
		when 'Admin' then 32
		when 'Ops' then 16
		when 'WriteX' then 8
		when 'Write' then 4
		when 'ReadX' then 2
		when 'Read' then 1
	end
	) Level
	from
	@Roles R
	INNER JOIN @Areas A
		ON r.rolename like 'p' + A.area + '%'
	group by area
	order by area

----------	Select * from @roles
----------	Select * from @levels


	-- finally collate the string

	declare @s nvarchar(50) = ''
	Select @s = @s + char(isnull(level,0) + 48)
	from
		@areas A
		left join @levels L
		ON A.area = L.a
	ORDER BY A.area

select @s as p
END
GO

