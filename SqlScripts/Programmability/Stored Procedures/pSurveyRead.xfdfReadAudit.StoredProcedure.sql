SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 25 4 2015
-- Description:	detail audit record
-- =============================================
CREATE PROCEDURE [pSurveyRead].[xfdfReadAudit]
	-- Add the parameters for the stored procedure here
	@schNo nvarchar(50)
	, @svyYear int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT
	sxaSection Section,
	sxaDesc Info,
	sxaNum Number,
	sxaData Code,
	case sxaErrorFlag when 2 then 'Error' when 1 then 'Warning' else '' end Status
	FROM SchoolSurvey SS
		LEFT JOIN audit.xfdfAudit A
	ON SS.ssID = A.ssID
	WHERE SS.schNo = @schNo AND SS.svyYEar = @svyYear
	ORDER BY sxaID
	-- summary
	Select SS.ssID
	, max(sxaDate) Loaded
	, sum(case when sxaErrorflag = 2 then 1 else null end) Errors
	, sum(case when sxaErrorflag = 1 then 1 else null end) Warnings
	FROM SchoolSurvey SS
		LEFT JOIN audit.xfdfAudit A
	ON SS.ssID = A.ssID
	WHERE SS.schNo = @schNo AND SS.svyYear = @svyYear
	GROUP BY SS.ssID
END
GO

