SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:
-- =============================================
CREATE PROCEDURE [pSurveyWrite].[AddToSurveyControl]
	-- Add the parameters for the stored procedure here
	@SurveyYear int,
	@SchoolNo   nvarchar(50) = null,
	@SchoolName nvarchar(50) = null,
    @fSearchAlias int = 0, -- 1 = search 2 = search for exclusion
    @AliasSource nvarchar(50) = null,
    @SchoolType nvarchar(10) = null,
	@SchoolClass nvarchar(10) = null,
	@District nvarchar(10) = null,
    @Island nvarchar(10) = null,
	@ElectorateN nvarchar(10) = null,
	@ElectorateL nvarchar(10) = null,
	@Authority nvarchar(10) = null,
	@Language nvarchar(10) = null,
	@InfrastructureClass nvarchar(10) = null,
	@InfrastructureSize nvarchar(10) = null,
	@ShipService nvarchar(50) = null,
	@BankService nvarchar(50) = null,
	@PostService nvarchar(50) = null,
	@AirService nvarchar(50) = null,
	@Clinicservice nvarchar(50) = null,
	@HospitalService nvarchar(50) = null,
	@SearchIsExtension int = null,
	@PArentSchool nvarchar(50) = null,
	@SEarchHasExtension int = null,
	@ExtensionReferenceYear int = null,	-- look on school year history - if null, use the Active Year
	@YearEstablished int = null,
	@RegistrationStatus nvarchar(10) = null,	-- see case for allowed values
	@SearchReg int = 0,				--search for the school name text in the reg no too
	@CreatedAfter datetime = null,
	@EditedAfter datetime = null,
	@IncludeClosed int = 0


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;


	declare @sql nvarchar(2000)
	declare @Params nvarchar(500)
	declare @TablesToUse int
	select @TablesToUse = 0

-- rules/cases for alias search
-- no name specified, search alias flag on no action (return any school, or any school with any alias)
-- name specified, alias flag on - match schname, or any alias
-- name specified alias flag on, specific item - match name or alias in the specific list
-- no name specified, alias flag on, specific item - any item in specific alias list
-- name specified, alias flag on, specific item, NOT : any school with name like name' not incuded in the specified alias list
-- no name specified alias flag on, specific item, NOT :any school not in the specific alias list
declare @AliasMode int
select @AliasMode = 0
declare @fForceAlias int
select @fForceAlias = 0


declare @UseSurvey int
select @UseSurvey = 0


create table #alias
(
	schNo nvarchar(50)
)

create table #survey
(
	schNo nvarchar(50)
)


if (@fSearchAlias = 1)
	select @AliasMode = 4
if (@fSearchAlias = 2)
	Select @AliasMode = 8
if (@SchoolName is not null)
	Select @AliasMode =@AliasMode + 2
if (@AliasSource is not null)
	select @AliasMode = @AliasMode +1


if (@AliasMode = 9 or @AliasMode = 11)
	-- name is blank, not in specified list
	-- SELECTED school MUST be in #ALIAS
BEGIN
	insert into #alias
	select schNo
	From Schools WHERE
		schNo not in (Select schNo from SchoolAlias WHERE saSrc = @AliasSource)
	select @fForceAlias = 1
END

if @AliasMode = 5
	-- search , in specific alias list, no name
	--
BEGIN

	insert into #alias
	select schNo
	From SchoolAlias
	WHERE saSrc = @AliasSource
	select @fForceAlias = 1
END


if ((@ShipService is not null) or
	(@BankService is not null) or
	(@PostService is not null) or
	(@AirService is not null) or
	(@ClinicService is not null) or
	(@InfrastructureClass is not null) or
	(@InfrastructureSize is not null))		BEGIN

			INSERT INTO #survey
			Select S.schNo
			from Schools S
				inner join SchoolSurvey SS on S.schNo = ss.SchNo
			WHERE
				SS.svyYear = (Select max(svyYear) from SchoolSurvey ss2 where ss2.schno = S.schNo)
				AND (ssSvcAir = @AirService or @AirService is null)
				AND (ssSvcPost = @PostService or @PostService is null)
				AND (ssSvcShip = @ShipService or @ShipService is null)
				AND (ssSvcBank = @BankService or @BankService is null)
				AND (ssSvcClinic = @ClinicService or @ClinicService is null)
				AND (ssSvcHospital = @HospitalService or @HospitalService is null)
				AND (ssISClass = @InfrastructureClass or @InfrastructureClass is null)
				AND (ssISSize = @InfrastructureClass or @InfrastructureClass is null)

			select @UseSurvey = 1
end

INSERT INTO SchoolYearHistory
( schNo
, syYEar
, syAuth
, systCode
, syDormant
, syParent
)
SELECT S.schNo
, @SurveyYear
, S.schAuth
, S.schType
, S.schDormant
, S.schParent

from Schools S
	LEFT OUTER JOIN Islands I
		on S.iCode = I.iCode
	LEFT OUTER JOIN Districts D
		on D.dID = I.iGroup
	LEFT OUTER JOIN lkpElectorateN N
		ON S.schElectN = N.codeCode
	LEFT OUTER JOIN lkpElectorateL L
		ON S.schElectL = L.codeCode
	LEFT OUTER JOIN (Select syParent, count(schNo) NumExtensions from SchoolYearHistory
						WHERE syYear = isnull(@ExtensionReferenceYear, common.ActiveSurveyYear())
						GROUP BY syParent) Extensions
		ON S.schNo = Extensions.syParent
	LEFT OUTER JOIN (Select schNo PSchNo, syParent
						FROM SchoolYearHistory
						WHERE syYear = isnull(@ExtensionReferenceYear, common.ActiveSurveyYear())
					) Parent
					ON S.schNo = Parent.PschNo
WHERE
	(S.schNo = @SchoolNo or @SchoolNo is null)
	AND
		(


			(@SchoolName is null or
				(schName like @SchoolName + '%' or
					(@fSearchAlias = 1 and
						S.SchNo in
							(Select schNo from SchoolAlias WHERE saAlias like (@SchoolName + '%')
								AND (saSrc=@AliasSource OR @AliasSource is null))
					) or
					(@SearchReg = 1 and
						S.SchReg = @SchoolName
					)


				)
			)
		)

	AND (schType = @schoolType or @SchoolType is null)
	AND (SchClass = @SchoolClass or @SchoolClass is null)
	AND (S.iCode = @Island or @Island is null)
	AND (I.iGroup = @District or @District is null)
	AND (schElectN = @ElectorateN or @ElectorateN is null)
	AND (schElectL = @ElectorateL or @ElectorateL is null)
	AND (schAuth = @Authority or @Authority is null)
	AND (schLang = @Language or @Language is null)

	AND (schEst = @YearEstablished or @YearEstablished is null)
	AND (schClosed = 0 or schClosed > year(getdate()) or @IncludeClosed = 1)		-- fixed 26 6 2010

	AND (schNo = any (Select schNo from #survey)
			OR @UseSurvey = 0)
	AND (schNo = any (Select schNo from #alias)
			OR @fForceAlias = 0)

	----AND ( ( schNo in (Select schParent from Schools)
	----		AND @SearchHasExtension = 1)
	----	OR (schNo <> any (Select schParent from Schools)
	----		AND @SearchHasExtension = 0)
	----	OR @SearchHasExtension is null
	----	)

	AND (( Extensions.syParent is not null
			AND @SearchHasExtension = 1)
		OR (Extensions.syParent is null
			AND @SearchHasExtension = 0)
		OR @SearchHasExtension is null
		)

	AND ( (Parent.syParent is not null
			AND @SearchIsExtension = 1)
		OR (Parent.syParent is  null
			AND @SearchIsExtension = 0)
		OR @SearchIsExtension is null
		)

	AND (
			Parent.syParent =@ParentSchool

		OR @ParentSchool is null
		)

	AND ( @RegistrationStatus is null or
			case @RegistrationStatus
				when 'NULL' then
					case when schRegStatus is null then 1 end
				when 'ANY' then
					case when schRegStatus is not null then 1 end
				when 'PXX' then
					case when schRegStatus in ('PX','X') then 1 end
				when 'PRR' then
					case when schRegStatus in ('PR','R') then 1 end
				when 'P' then
					case when schRegStatus in ('PX','PR') then 1 end
				when 'RX' then
					case when schRegStatus in ('X','R') then 1 end
				else
					case when schRegStatus =@RegistrationStatus then 1 end
			end = 1
		)
	AND ( pEditDateTime >= @EditedAfter or @EditedAfter is null)
	AND ( pCreateDateTime >= @CreatedAfter or @CreatedAfter is null)

AND S.schNo not in (Select schNo from SchoolYearHistory WHERE syYEar = @SurveyYear)
-- for now we'll produce a very simple summary recordset

SELECT @@ROWCOUNT NumSchools
drop table #alias
drop table #survey

END
GO

