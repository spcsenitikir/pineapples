SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13/11/2007
-- Description:	Data for schools pivot table
-- =============================================
CREATE PROCEDURE [dbo].[PIVSchools]
	-- Add the parameters for the stored procedure here
	@Consolidation int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- collect the estimate temp table
Select * into
#ebse
from dbo.tfnESTIMATE_BestSurveyEnrolments() E

-- rebuild the rank table
	exec buildRank

		SELECT
			EE.LifeYear [Survey Year],
			EE.Estimate,
			EE.Offset AS [Age of Data],
			EE.bestYear AS [Year of Data],
			EE.bestssqLevel AS [Data Quality Level],
			EE.ActualssqLevel AS [Survey Year Quality Level],
			EE.SurveyDimensionssID,
			SS.ssEnrol as Enrol,
			1 as NumberOfSchools,

		-- from pEnrolmentRead.ssIDEnrolmentLevelsummary
			ELS.NumLevels,
			ELS.RangeYearOfEd,
			ELS.HighestLevel,
			ELS.LowestLevel,
			ELS.HighestYearOfEd,
			ELS.LowestYearOfEd,
--			ELS.MaxLevelEnrol,
			ELS.LevelOfMaxEnrol,
			ELS.FullRangeYN,
			ELS.FullRange,

		-- from vtblSizeEst
			VSIZE.ssSizeSite as [Site Size],
			VSIZE.ssSizeFarm as [Farm Size],
			VSIZE.ssSizePlayground as [Playground Size],
			case when VSIZE.ssSizeSite > 0 then 1 else 0 end SiteSizeSupplied,
			case when VSIZE.ssSizeFarm > 0 then 1 else 0 end FarmSizeSupplied,
			case when VSIZE.ssSizePlayground > 0 then 1 else 0 end PlaygroundSizeSupplied,
			VSIZE.[Year of Data] as [Year of School Size Data],
			VSIZE.Estimate as [School Size Estimate],
			VSIZE.[Age Of Data] as [School Size Age of Data],

		-- from DimensionRank
			DRANK.[School Enrol],
			DRANK.[District Rank],
			DRANK.[District Decile],
			DRANK.[District Quartile],
			DRANK.[Rank],
			DRANK.[Decile],
			DRANK.[Quartile]
		INTO #school


		FROM
			#ebse EE INNER JOIN SchoolSurvey SS
				on EE.bestssID = SS.ssID
			INNER JOIN pEnrolmentRead.ssIDEnrolmentLevelSummary ELS
				on EE.bestssID = ELS.ssID
			LEFT JOIN vtblSizeEst VSIZE
				on EE.schNo = VSIZE.schNo and EE.LifeYear = VSIZE.[Survey Year]
			LEFT JOIN DimensionRank DRANK
				on EE.schNo = DRANK.schNo and EE.LifeYear = DRANK.svyYear

if @consolidation = -1
	BEGIN
		-- now the output
		SELECT *
		from #school


	END

if @consolidation = 0
	BEGIN
		-- now the output
		SELECT *
		from #school
		INNER JOIN DimensionSchoolSurveyNoYear DS
			ON #school.SurveyDimensionssID = DS.[Survey ID]


	END

if @consolidation = 1

	BEGIN

	--we make a leaner subset of DimensionSchoolSurvey
		Select ssId [Survey ID]
			, ssSchType SchoolTypeCode
			, stDescription [SchoolType]
			, ssAuth AuthorityCode
			, iGroup [District Code]
			, D.dName District
		INTO #dsSmall

		from DimensionSchoolSurveySub S
			INNER JOIN Islands I
			on s.iCode = I.iCode
			INNER JOIN Districts D
			on i.iGroup = D.dID
			INNER JOIN SchoolTypes ST
			on ST.stCode = S.ssSchType;

-- consolidation of the data query


		SELECT
			S.[Survey Year],
			S.Estimate,

		-- from DimensionSurvey
			DS.[SchoolTypeCode],
			DS.SchoolType,
			DS.[District Code],
			DS.District,

		-- from Dimension Autority
			DA.AuthorityCode,
			DA.Authority,
			DA.AuthorityTypeCode,
			DA.AuthorityType,
			DA.AuthorityGRoupCode,
			DA.AuthorityGRoup,
		-- from pEnrolmentRead.ssIDEnrolmentLevelsummary
			S.NumLevels,
			S.RangeYearOfEd,
			S.HighestLevel,
			S.LowestLevel,
			S.HighestYearOfEd,
			S.LowestYearOfEd,
--			S.MaxLevelEnrol,
			S.LevelOfMaxEnrol,
			S.FullRangeYN,
			S.FullRange,

		-- from vtblSizeEst
			sum(S.[Site Size]) [Site Size],
			sum(S.[Farm Size]) [Farm Size],
			sum(S.[Playground Size]) [Playground Size],
			sum(S.SiteSizeSupplied) SiteSizeSupplied,
			sum(S.FarmSizeSupplied) FarmSizeSupplied,
			sum(S.PlaygroundSizeSupplied) PlaygroundSizeSupplied,
			S.[School Size Estimate],

		-- from DimensionRank
			S.[District Decile],
			S.[District Quartile],
			S.[Decile],
			S.[Quartile],

			sum(S.Enrol) as Enrol,
			sum(S.NumberOfSchools) as NumberOfSchools

		FROM
			#School S
			inner join #dsSmall DS
			on S.SurveyDimensionssID = DS.[Survey ID]
			inner join DimensionAuthority DA
			on DA.AuthorityCode = DS.AuthorityCode
		GROUP BY
			S.[Survey Year],
			S.Estimate,


		-- from DimensionSurvey
			DS.[SchoolTypeCode],
			DS.SchoolType,
			DS.[District Code],
			DS.District,

			DA.AuthorityCode,
			DA.Authority,
			DA.AuthorityTypeCode,
			DA.AuthorityType,
			DA.AuthorityGRoupCode,
			DA.AuthorityGRoup,
		-- from pEnrolmentRead.ssIDEnrolmentLevelsummary
			S.NumLevels,
			S.RangeYearOfEd,
			S.HighestLevel,
			S.LowestLevel,
			S.HighestYearOfEd,
			S.LowestYearOfEd,
--			S.MaxLevelEnrol,
			S.LevelOfMaxEnrol,
			S.FullRangeYN,
			S.FullRange,

		-- from vtblSizeEst
			S.[School Size Estimate],

		-- from DimensionRank
			S.[District Decile],
			S.[District Quartile],
			S.[Decile],
			S.[Quartile]
	END
END
GO

