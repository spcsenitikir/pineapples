SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 28 5 2010
-- Description:	Finalise processing for payslip import
-- =============================================
CREATE PROCEDURE [pTeacherWrite].[PaySlipImportBatchFinalise]
	-- Add the parameters for the stored procedure here
	@PeriodEnd datetime = 0
	WITH EXECUTE AS 'pineapples'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- this tsakes too long, so, do it one by one by way of trigger
		----UPDATE TeacherPayslips
		----SET tpsXML = pTeacherReadX.fnParsePayslip(tpsPayslip )
		----WHERE tpsPeriodEnd = @PeriodEnd


		INSERT INTO TeacherPayslipItemTypes
		(payItemType)
		Select
		value
		FRom
		(
		SELECT DISTINCT i.value('./@pc', 'nvarchar(20)') value
		FROM TeacherPayslips
		CROSS APPLY
		TPSXML.nodes('Payslip/ThisPay/Item') as T(i)
		WHERE tpsPeriodEnd = @PeriodEnd
		) Items
		WHERE Value not in (Select payItemType from TeacherPayslipItemTypes)
END
GO

