SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 15 10 2009
-- Description:
-- =============================================
CREATE PROCEDURE [pEstablishmentRead].[PlannedEstablishmentData]
	-- Add the parameters for the stored procedure here
	@EstYear int

	, @UseAuthorityDate int
-- the next fields are filtering fields

	, @SchoolNo nvarchar(50) = null
	, @Authority nvarchar(10) = null
	, @SchoolType nvarchar(10) = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @AsAtDate datetime		-- date for reporting the current position
	declare @NumPointIncrements int		-- number of points to increment current positions when estimating future salary
	declare @AdjustNominalSalaryPoint int
	declare @RetainUnderPays bit 		-- if this flag is on, positions currently underpaid will be assumed to stay underpaid
									-- if it is off (default), underpaid positions are recosted at the nomiual cost for the future


	SELECT
		@AsAtDate = case when @UseAuthorityDate = 1 then estcAuthorityDate else estcSnapshotDate end
		, @NumPointIncrements = estcIncrementPoints
		, @AdjustNominalSalaryPoint = estcNominalPointAdjust
		, @RetainUnderPays = estcRetainUnderpays
	FROM
		EstablishmentControl
	WHERE
		estcYear = @EstYear

declare @MaxSurveyYear int

Select @maxSurveyYear = max(svyYear)
from
	Survey
/*********************************************
Temporary Table Definitions
***********************************************/
-- temp table #data holds the planned establishment and totals drawn from Establishment (ie poisition count)
create table #data
(
	schNo nvarchar(50)
	, roleGrade nvarchar(10)
	, numPlanned int
	, numPositions int
	, numFilled int
	, numFilledSalaryPointComplies int
	, numFilledUnderPay int
	, NumUnfilled int
	, NumRequired int
	, FilledPayFN money
	, FuturePay money
	, FuturePaySalaryPointComplies money		-- the future pay of those who are not currently underpaid


)

-- rates applicable to each paylevel at the date fo the establishment
create table #rates
(
	spCode nvarchar(10)
	, salLevel nvarchar(5)
	, spSalary money
	, spHAllow money
	, AnnualTotal money
	, spEffective datetime
	, pointOrder int
)

-- the minium pointOrder for any salary level - used for checking if a point is in a range
create table #salaryLevelPointOrder
(
	salLevel nvarchar(5)
	, levelMinPointOrder int
)


create table #projectedEnrolStats
(
	schNo nvarchar(50)
	, ProjectedEnrol int
	, minLevelCode nvarchar(10)
	, maxLevelCode nvarchar(10)
	, NumLocations int				-- number of schools that are consolidated - ie including extension schools 1 if no extensions

)


/**************************************************************************
Populate temporary Tables
***************************************************************************/
-- get the salary rates that will be in effect for the establishment date

-- #rates

INSERT INTO #rates
SELECT
	spCode
	, salLevel
	, spSalary
	, spHAllow
	, isnull(spSalary,0) + isnull(spHAllow,0) AnnualTotal
	, spEffective
	-- this row number based on sort is so we can do an increment
	, row_number() OVER (ORDER BY spSort)
FROM
	(
	SELECT
		SalaryPointRates.*
		, spSort
		, salLevel
		-- row number to get the first pay effective date desc
		, row_number() over( PARTITION BY lkpSalaryPoints.spCode ORDER BY spEffective DESC)  RowNum
	FROM
		lkpSalaryPoints
		INNER JOIN SalaryPointRates
			ON LkpSalaryPoints.spCode = SalaryPointRates.spCode
		, EstablishmentControl
	WHERE
		EstablishmentControl.estcYear = @EstYear
		AND spEffective <= estcAuthorityDate
	) sub
	WHERE RowNum = 1

--


-- #SalaryLevelPointOrder

-- what is the minimum point order for each level?
-- we need this to know if an appointment is underpaid

INSERT INTO #salaryLevelPointOrder
SELECT
	salLevel
	, min(PointOrder)
FROM
	#rates
GROUP BY
	salLevel


-- #data

-- the crux of the SP is to build the data combining the plan with the current positions
-- and appoint/costing info
-- this is done by using a union to bring them together, then sum that union

INSERT INTO #data
SELECT
	schNo
	, RoleGrade
	, sum(Planned)
	, sum(Positions)
	, sum(Filled)
	, isnull(sum(Filled),0) - isnull(sum(FilledUnderPay),0)
	, sum(FilledUnderPay)
	, isnull(sum(Positions),0) - isnull(sum(Filled),0)
	, isnull(sum(Planned),0) - isnull(sum(Positions),0)
	, sum(PayFN)
	, sum(FuturePay)
	, sum(FuturePayCorrectLevel)

FROM
	(
	-- first part of the union is the plan from SchoolEstablishment
	SELECT
		SE.schNo
		, estrRoleGrade RoleGrade
		, estrCount Planned
		, null	Positions
		, null	Filled
		, null FilledUnderPay
		, null	PayFN
		, null FuturePay
		, null FuturePayCorrectLevel

		FROM SchoolEstablishment SE
			INNER JOIN SchoolEstablishmentRoles SER
				on SE.estID = SER.estID
			LEFT JOIN Schools S
				ON S.SchNo = SE.SchNo
		WHERE
			estYear = @EstYear

		-- if we are after just one school, cut to the chase
			AND (@schoolNo = SE.schNo or @SchoolNo is null)
			AND (@Authority = S.schAuth or @Authority is null)
			AND (@SchoolType = S.schType or @SchoolType is null)

		UNION ALL

	-- second part gets the current actuals from Establishment and TeacherAppointments etc
	SELECT
		E.schNo
		, estpRoleGrade
		, null
		, 1
		--, case when slips.tpsID is null then 0 else 1 end NumFilled
		, case when Appts.taID is null then 0 else 1 end NumFilled
		-- num filled under pay
		-- the teacher is underpaid if the pointorder of their current salary point
		-- is less than the minimum point order for the minimum level of the RoleGrade
		, case when RateNow.PointOrder < LvlPtOrder.levelMinPointOrder then 1
				else 0
		  end numFilledUnderPay
		, slips.tpsGross
		, RateFuture.AnnualTotal
		, case when RateNow.PointOrder > LvlPtOrder.levelMinPointOrder then RateFuture.AnnualTotal
				else RateFuture.AnnualTotal
		  end FuturePayCorrectPay

		FROM
			Establishment E
			LEFT JOIN Schools S
				ON S.SchNo = E.schNo
			INNER JOIN RoleGRades RG
				ON E.estpRoleGRade = RG.rgCode
			LEFT JOIN #salaryLevelPointOrder LvlPtOrder
				ON LvlPtOrder.salLevel = RG.rgSalaryLevelMin
			LEFT JOIN
					( Select *
						from TeacherAppointment  Appt
						WHERE (Appt.taDate <= @asAtDate)
						AND (Appt.taEndDate >= @AsAtDate or Appt.taEndDate is null)
					) Appts
				ON Appts.estpNo = E.estpNo
			LEFT JOIN TEacherIdentity TI
				ON TI.tID = Appts.tID
			LEFT JOIN
					( Select *
						from TeacherPaySlips
						WHERE tpsPeriodStart <= @AsAtDate
							AND tpsPeriodEnd >= @AsAtDate
					) Slips
				   ON Slips.tpsPayroll = TI.tPayroll
			-- use two copies of #rates
			-- the first is the current salarypaypoint, the join increments that
			LEFT JOIN #rates RateNow
				ON RateNow.spCode = slips.tpsSalaryPoint
			LEFT JOIN #rates RateFuture
				ON RateNow.pointOrder + @NumPointIncrements = RateFuture.pointOrder
		WHERE
			estpActiveDate <=@AsAtDate
			AND (@AsAtDate <= estpClosedDate or estpClosedDate is null)

			-- if we are after just one school, cut to the chase
			AND (@schoolNo = E.schNo or @SchoolNo is null)
			AND (@Authority = S.schAuth or @Authority is null)
			AND (@SchoolType = S.schType or @SchoolType is null)


	) U


	GROUP BY schNo, roleGrade


-- #projectedEnrolStats

declare @escnID int				-- the establishemtns scenario ID

SELECT @escnID = escnID
FROM
	EstablishmentControl
WHERE
	estcYear = @estYEar


-- 17 04 2010
-- support for Extension/PArent
-- this table now need to hold the sum across all schools that consolidate to the same parent
-- This may be different in the future year than current, as a school may be promoted from Exension to standalone
-- So , we have to get the EstablishmentPoint from SchoolEstablishment
INSERT INTO #projectedEnrolStats

SELECT
	PE.schNo
	, PE.ProjectedEnrol
	, min(case when lvlYEar = PE.lvlMinYr then EPD.epdLevel else null end) MinLevelCode
	, max(case when lvlYEar = PE.lvlMaxYr then EPD.epdLevel else null end) MaxLevelCode
	, PE.NumLocations
FROM
	(Select
		estEstablishmentPoint schNo
		, @escnID escnID			-- use the variable gets around the group by
		, isnull(sum(epdM),0) +  isnull(sum(epdF),0) + isnull(sum(epdU),0) ProjectedEnrol
		, min(lkpLevels.lvlYear) lvlMinYr
		, max(lkpLevels.lvlYear) lvlMaxYr
		, count(DISTINCT SE.schNo) NumLocations
	from
		SchoolEstablishment SE
		INNER JOIN EnrolmentProjection
				ON SE.estYear = EnrolmentProjection.epYear
				AND SE.schNo = EnrolmentProjection.schNo

		INNER JOIN EnrolmentProjectionData
			ON enrolmentProjection.epID = enrolmentProjectionData.epID
		INNER JOIN lkpLevels
			ON lkpLevels.codeCode = EnrolmentProjectionData.epdLevel
	WHERE
		EnrolmentProjection.escnID = @escnID
		AND EnrolmentProjection.epYear = @EstYear
	group by
		estEstablishmentPoint
	) PE
	INNER JOIN EnrolmentProjection
		ON EnrolmentProjection.escnID = PE.escnID
		AND EnrolmentProjection.schNo = PE.schNo
	INNER JOIN enrolmentProjectionData EPD
		ON enrolmentProjection.epID = EPD.epID
	INNER JOIN lkpLevels
		ON lkpLevels.codeCode = EPD.epdLevel
WHERE
	lkpLevels.lvlYear IN (PE.lvlMinYr, PE.lvlMaxYr)
GROUP BY
	PE.schNo, PE.ProjectedEnrol, PE.NumLocations


Select * from #data

END
GO

