SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis and Ghislain Hachey
-- Create date: 18 6 2016
-- Description:	Filter quarterly report IDs
-- =============================================
CREATE PROCEDURE [pSurveyRead].[QuarterlyReportFilterIDs]
	-- Add the parameters for the stored procedure here

	@NumMatches int OUTPUT,
	@PageSize int = 0,
	@PageNo int = 1,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0,

	@QRID int = null,
    @School nvarchar(50) = null, -- e.g. 'Geek High School'
	@InspQuarterlyReport nvarchar(50) = null, --- e.g. '2016/Q4'

	@xmlFilter xml = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;

-- if filter params come from XML
if (@xmlFilter is not null) begin

	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xmlFilter

	Select
	    @QRID = isnull(@QRID, qrID),
		@School = isnull(@School, School),
		@InspQuarterlyReport = isnull(@InspQuarterlyReport, InspQuarterlyReport)
	FROM OPENXML(@idoc,'//Filter',2)
	WITH
	(
	qrID int '@QRID',
	School nvarchar(50) '@School',
	InspQuarterlyReport nvarchar(50) '@InspQuarterlyReport'
	)
end

	DECLARE @keysAll TABLE
	(
	ID int,
	recNo int IDENTITY PRIMARY KEY
	)

INSERT INTO @KeysAll (ID)
SELECT qrID
from pInspectionRead.QuarterlyReports QR-- view
	LEFT JOIN Schools S
		ON QR.schNo = S.schNo
WHERE
(qrID = @QRID OR @QRID IS NULL)
AND (InspQuarterlyReport = @InspQuarterlyReport OR @InspQuarterlyReport IS NULL)
AND (S.schName like '%' +  @School + '%' OR @School IS NULL)

OPTION(RECOMPILE)


SELECT @NumMatches = @@ROWCOUNT

-- now return the page, sorted in the right sequence and direction

If @SortDir = 1 begin


		SELECT ID
		, RecNo
		FROM
		(
			Select ID
			, @NumMatches - RecNo + 1 RecNo
			FROM @KeysAll
		) S
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
		ORDER BY RecNo

	end
	else begin


		SELECT ID
		, RecNo
		FROM @KeysAll
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
	end

END
GO

