SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 18 11 2007
-- Description:	data for population pivot table
-- =============================================
CREATE PROCEDURE [common].[PIVPopulation]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- a simple select statment to get district, island and electorate names
-- doesn;t really need to be an sp
-- exept we'll just return the default model

declare @defModCode nvarchar(10)

Select TOP 1 @DefModCode = popModCode
FROM POPulationModel
WHERE popModDefault = 1

SELECT
	Population.popYear AS [Projection Year],
	Population.popmodCode AS [Population Model Code],
	PopulationModel.popmodName AS [Population Model Name],
	case populationModel.popModDefault
		when 1 then 'Y'
		else 'N' end as					[Default Model],
	case populationModel.popModEFA
		when 1 then 'Y'
		else 'N' end as 				[EFA Model],
	Population.dID AS DistrictCode,
	Districts.dName AS District,
	Population.iCode AS IslandCode,
	Islands.iName AS Island,
	Population.elL AS [Local Electorate No],
	lkpElectorateL.codeDescription AS [Local Electorate],
	Population.elN AS [National Electorate No],
	lkpElectorateN.codeDescription AS [National Electorate],
	Population.popAge AS Age,
	Population.popM,
	Population.popF,
	ELAR.AgeEdLevelCode,
	ELAR.[Age Expected Education Level],
	ELAR.AgeEdLevelAltCode,
	ELAR.[Age Expected Education Level Alt],
	ELAR.AgeEdLevelAlt2Code,
	ELAR.[Age Expected Education Level Alt2],
	G.codeCode as GenderCode,
	G.codeDescription as Gender,
	case G.codeCode when 'M' then population.popM when 'F' then population.popF end [Population]

FROM

	dbo.Population LEFT JOIN Districts
				ON Population.dID = Districts.dID
			LEFT JOIN Islands
				ON Population.iCode = Islands.iCode
			LEFT JOIN lkpElectorateL
				ON Population.elL = lkpElectorateL.codeCode
			LEFT JOIN lkpElectorateN
				ON Population.elN = lkpElectorateN.codeCode
			LEFT JOIN EducationLevelAgeRange ELAR
				on Population.popYear = ELAR.populationYear
					AND population.popAge = ELAR.Age
			LEFT JOIN PopulationModel
				ON Population.popmodCode = PopulationModel.popmodCode
	CROSS JOIN lkpGender G
	WHERE (PopulationModel.popModCode = @DefModCode or @DefModCode is null)
	and G.codeCode in ('M','F')

END
GO

