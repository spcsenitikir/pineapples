SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 10 09 2011
-- Description:	SimpleXML of school counts to feed into PAF
-- this version assumes that warehouse.SurveyYearRan already exists
-- =============================================
CREATE PROCEDURE [warehouse].[_vermSchoolCountsDistrict]
	-- Add the parameters for the stored procedure here
	@districtCode nvarchar(10)
	, @SendAsXML int
	, @xmlOut xml OUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


    DECLARE @xml xml

    SELECT @xml =
    (
    Select SurveyYear [@year]
	, rankSchType	[@schoolType]
	, count(schNo)  [count]
	, sum(Enrol) [enrol]
	, sum(EnrolM) [enrolM]
	, sum(EnrolF) [enrolF]
	, convert(decimal(7,4),convert(float,sum(case when Estimate = 1 then isnull(Enrol,0) else 0 end)) / sum(Enrol)) [est]

	FROM warehouse.SurveyYearRank
		WHERE rankDistrict = @districtCode
	GROUP BY SurveyYear
	, rankSchType
	ORDER by SurveyYear, rankSchType
	FOR XML PATH('SchoolCount')
	)

	SELECT @xmlOut=
	(
	SELECT
	@districtCode [@districtCode]
	, @xml
	FOR XML PATH('SchoolCounts')
    )
END
GO

