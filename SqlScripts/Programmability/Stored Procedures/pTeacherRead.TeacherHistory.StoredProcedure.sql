SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2 10 2009
-- Description:	Return the set of survbeys for a selected teacher
-- =============================================
CREATE PROCEDURE [pTeacherRead].[TeacherHistory]
	-- Add the parameters for the stored procedure here
	@tID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT TeacherSurvey.*
	, SchoolSurvey.svyYear
	, Schools.schNo
	, Schools.schName
	, tRegister
	, tPayroll
	, tDOB
	, tSex
	, tNamePrefix
	, tGiven
	, tMiddleNames
	, tSurname
	, tNameSuffix
	, tSrcID
	, tSrc
	, tComment

	FROM TeacherSurvey
		INNER JOIN TeacherIdentity ON TeacherSurvey.tID = TeacherIdentity.tID
		INNER JOIN SchoolSurvey ON TeacherSurvey.ssID = SchoolSurvey.ssID
		INNER JOIN Schools ON SchoolSurvey.schNo = Schools.schNo
		WHERE TeacherIdentity.tID = @tID
		ORDER BY svyYear DESC

END
GO

