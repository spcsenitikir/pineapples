SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 6 2009
-- Description:	TEacher at a selected school in a given year, and their whereabouts in a future year
-- =============================================
CREATE PROCEDURE [dbo].[sp_whereAreTheyNow]
	-- Add the parameters for the stored procedure here
	@schNo nvarchar(50),
	@srcYear int = 0,
	@targetYear int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

with X as
(
SELECT
TeacherIdentity.tID
, TeacherSurvey.ssID
, TeacherSurvey.tchsID
, SchoolSurvey.svyYear
, SchoolSurvey.schNo
, TeacherIdentity.tRegister
, TeacherIdentity.tPayroll
, TeacherIdentity.tDOB
, TeacherIdentity.tSex
, TeacherIdentity.tSurname
, TeacherIdentity.tGiven
, TeacherSurvey.tchSort
, TeacherSurvey.tchFirstName
, TeacherSurvey.tchFamilyName
, TeacherSurvey.tchSalaryScale
, TeacherSurvey.tchSalary
, TeacherSurvey.tchCitizenship
, TeacherSurvey.tchSponsor
, TeacherSurvey.tchYears
, TeacherSurvey.tchYearsSchool
, TeacherSurvey.tchEdQual
, TeacherSurvey.tchQual
, TeacherSurvey.tchCivilStatus
, TeacherSurvey.tchNumDep
, TeacherSurvey.tchHouse
, TeacherSurvey.tchSubjectMajor, TeacherSurvey.tchSubjectMinor
, TeacherSurvey.tchClass, TeacherSurvey.tchClassMax
, TeacherSurvey.tchJoint, TeacherSurvey.tchComposite
, TeacherSurvey.tchStatus, TeacherSurvey.tchRole
, Schools.schName
FROM TeacherIdentity
	INNER JOIN TeacherSurvey ON TeacherIdentity.tID = TeacherSurvey.tID
	INNER JOIN SchoolSurvey ON SchoolSurvey.ssID = TeacherSurvey.ssID
	Inner JOIN Schools on SchoolSurvey.schNo = Schools.schNo
)
select
SRC.*
, Target.ssID as NextssID
, Target.tchsID as NextTeacherSurvey
, Target.svyYear as NextYear
, Target.SchNo as NextSchNo
, Target.SchName as NextSchoolName
, Target.tchRole as NextRole
from (Select * from X WHERE svyYear = @SrcYear) SRC
left join (Select * from X WHERE svyYear = @TargetYear) Target
on SRC.tID = Target.tID
WHERE SRC.schNo = @Schno
END
GO
GRANT EXECUTE ON [dbo].[sp_whereAreTheyNow] TO [pTeacherRead] AS [dbo]
GO

