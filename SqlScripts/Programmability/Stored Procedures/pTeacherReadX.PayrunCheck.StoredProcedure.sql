SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 19 04 2010
-- Description:	Report on teachers in pay run not known in Pineapples, or without an appointment
-- =============================================
CREATE PROCEDURE [pTeacherReadX].[PayrunCheck]
	-- Add the parameters for the stored procedure here
	@AsAtDate datetime = null,
	@PayPtCode nvarchar(10) = null,
	@ShowExceptions int = 0,		-- 1 = tpf not known, 2 = no appointment at all, 3 = appointment to wrong position
	@PayItemType nvarchar(20) = null,
	@PayAllowanceException int = 0	-- if 1 - get allowance, shouldn;t 2 = don;t get allowance should, 3 both
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- find the applicable pay period for the date in question
if (@AsAtDate) is null
	Select @AsAtDate = common.today()

declare @PayPeriodStart datetime

SELECT @PayPeriodStart = max(tpsPeriodStart)
FROM TeacherPayslips
WHERE tpsPeriodStart <= @AsAtDate


SELECT
	tpsID
	, tpsPeriodStart
	, tpsPEriodEnd
	, tpsPayroll
	, tpsShortName
	, tpsNamePrefix
	, tpsGiven
	, tpsMiddleNames
	, tpsSurname
	, tpsNameSuffix
	, tpsPosition
	, tpsTitle
	, tpsPayPoint
	, tpsSalaryPoint
	, tpsGross
	, TRPayPointCodes.payptDesc
	, TI.tID
	, TI.tFullName
	, TI.tSurname
	, TI.tGiven
	, TA.taID
	, TA.taDate
	, TA.taEndDate
	, TA.estpNo
	, E.schNo
	, E.estpRoleGrade
	, ApptSchool.schName ApptSchName
	, RG.roleCode
	, RG.rgSalaryLevelMin
	, RG.rgSalaryLevelMax
	, RG.rgSalaryPointMedian
	, TeacherLastSurvey.schNo LastSurveySchNo
	, TeacherLastSurvey.svyYear LastSurveyYear
	, TeacherLastsurvey.schName LastSurveySchName
	, case when @PayItemType is null then null
		when tpsXML.exist('/Payslip/ThisPay/Item[@pc=sql:variable("@PayItemType")]') = 1 then 1	-- @PayITemType
		else null
	  end PaidAllowance
from TeacherPayslips
	LEFT JOIN TeacherIdentity TI
		ON TeacherPayslips.tpsPayroll = TI.tPayroll
	LEFT JOIN TeacherAppointment TA
		ON TI.tID = TA.tID
		AND TA.taDate <=@AsAtDate
		AND (TA.TAEndDate is null or TA.taEndDate >= @AsAtDate)
	LEFT JOIN Establishment E
		ON TA.estpNo = E.estpNo
	LEFT JOIN Schools ApptSchool
		ON isnull(TA.schNo, E.schNo) = ApptSchool.schNo
	LEFT JOIN RoleGRades RG
		ON E.estpRoleGrade = RG.rgCode
	LEFT JOIN
		--- this contruction finds the last teacher survey for each tID
		(
			Select *
			FROM
			(
			Select svyYear
			, S.schNo
			, SchName
			, tID
			, tchRole
			, row_number() OVER (PARTITION BY tID ORDER BY svyYEar DESC) Num
			from SchoolSurvey SS
			INNER JOIN TeacherSurvey TS
				ON SS.ssID = TS.ssID
			INNER JOIN Schools S
				ON SS.schNo = S.schNo
			) sub
			WHERE Num = 1
		) TeacherLastSurvey
		ON TeacherLastSurvey.tID = TI.tID

	LEFT JOIN TRPayPointCodes
		ON TeacherPayslips.tpsPayPoint = TRPayPointCodes.payptCode

WHERE tpsPeriodStart = @PayPeriodStart


AND (@ShowExceptions = 0
		OR (@ShowExceptions = 1 and TI.tID is null)
		OR (@ShowExceptions = 2 and TI.tID is not null and TA.taID is null)
		OR (@ShowExceptions = 3 and TA.taID is not null and TA.estpNo <> TeacherPayslips.tpsPosition)
	)
AND (tpsPaypoint = @PayPtcode or @PayPtCode is null)


AND (
		(@PayAllowanceException = 0
			AND (tpsXML.exist('/Payslip/ThisPay/Item[@pc=sql:variable("@PayItemType")]') = 1
				OR @PayItemType is null)
		)
		OR
		(@PayItemType is not null
			and
			(
				(  @PayAllowanceException in (1,3)
					AND tpsXML.exist('/Payslip/ThisPay/Item[@pc=sql:variable("@PayItemType")]') = 1	-- get the allowance
					AND ApptSchool.schNo not in
							(

								Select schNo from ListSchools LST
									INNER JOIN EstablishmentAllowance EA
										ON Lst.lstName = EA.lstName
									INNER JOIN lkpPayAllowance
										ON lkpPayAllowance.codeCode = EA.estaCode
									WHERE lkpPayAllowance.payItemType = @PayItemType
							)

				)
				OR
				(  @PayAllowanceException in (2,3)
					AND tpsXML.exist('/Payslip/ThisPay/Item[@pc=sql:variable("@PayItemType")]') = 0	-- get the allowance
					AND ApptSchool.schNo in
							(

								Select schNo from ListSchools LST
									INNER JOIN EstablishmentAllowance EA
										ON Lst.lstName = EA.lstName
									INNER JOIN lkpPayAllowance
										ON lkpPayAllowance.codeCode = EA.estaCode
									WHERE lkpPayAllowance.payItemType = @PayItemType
							)

				)
			)
		)
	)

-- tack the summary on the end
exec pTeacherReadX.payrunSummary @PayPeriodStart
END
GO

