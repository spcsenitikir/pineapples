SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 14/11/07
-- Description:	InputDataForEnrolmentPIVsubjectTable
-- =============================================
CREATE PROCEDURE [dbo].[PIVEnrolmentSubject]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT *
INTO #ebse
FROM dbo.tfnESTIMATE_BestSurveyenrolments()

    -- Insert statements for procedure here
	SELECT DG.*,
	case DG.genderCode when 'M' then SE.ptM
			when 'F' then SE.ptF
	end Enrol,
	DSS.*,
	E.surveyDimensionssID,
	E.Estimate,
	E.Lifeyear as [Survey Year],
	E.Offset as [Age of Data],
	S.subjName,
	S.subjCode
	from
	vtblSubjectEnrol SE
	INNER JOIN #EBSE E
	ON SE.ssID = E.bestssID
	INNER JOIN TRsubjects S
	ON SE.subjCode = S.subjcode
	INNER JOIN DimensionSchoolSurveyNoYear DSS
		ON DSS.[Survey ID] = E.surveyDimensionssID
	CROSS JOIN DimensionGender DG


drop TABLE #ebse
END
GO

