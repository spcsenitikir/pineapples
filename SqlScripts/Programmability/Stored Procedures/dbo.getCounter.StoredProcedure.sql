SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2009-09-11
-- Description:	Return an incremented value for the next available value of a counter
-- =============================================
CREATE PROCEDURE [dbo].[getCounter]
(
	-- Add the parameters for the function here
	@counterName nvarchar(20)
	, @AllocateNumber int = 1
	, @SupressSelect int = 0			-- this is a convenience to handle server-side consumers
)

AS
BEGIN
	-- Declare the return variable here

	declare @Result int

	begin try
		begin transaction

			if (@AllocateNumber < 1 ) begin
				raiserror('Invalid increment',16,0)
			end
			-- null makes lots of irritations
			Select @AllocateNumber = isnull(@allocateNumber,1)
			Select @SupressSelect = isnull(@SupressSelect,0)

			INSERT INTO sysCounters (counterName, counterValue)
			SELECT @counterName, 0
			WHERE not exists (Select counterName from sysCounters WHERE counterName = @counterName)

			Select @Result = isnull(counterValue ,0)
			from sysCounters
				WHERE counterName = @counterName

			UPDATE sysCounters
				SET counterValue = @Result + @AllocateNumber
				WHERE counterName = @counterName

		commit transaction
		If (@SupressSelect = 0)
		Select @Result + num , @AllocateNumber, num, cast(@Result + num as nvarchar(6))
		from metaNumbers WHERE num between 1 and @AllocateNumber
		RETURN @Result + @AllocateNumber
	end try

	begin catch
		DECLARE @err int,
			@ErrorMessage NVARCHAR(4000),
			@ErrorSeverity INT,
			@ErrorState INT;
		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()


		if @@trancount > 0
			begin
				rollback transaction
				select @errorMessage = @errorMessage + ' The transaction was rolled back.'
			end

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		return @err
	end catch


	-- Return the result of the function

END
GO
GRANT EXECUTE ON [dbo].[getCounter] TO [pTeacherWriteX] AS [dbo]
GO

