SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis and Ghislain Hachey
-- Create date: 27 04 2017
-- Description:	Filter school accreditation IDs
-- =============================================
CREATE PROCEDURE [pSurveyRead].[SchoolAccreditationFilterIDs]
	-- Add the parameters for the stored procedure here

	@NumMatches int OUTPUT,
	@PageSize int = 0,
	@PageNo int = 1,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0,

	@SAID int = null,
    @School nvarchar(50) = null, -- e.g. 'Geek High School'
	@InspYear nvarchar(50) = null, --- e.g. '2016'

	@xmlFilter xml = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;

-- if filter params come from XML
if (@xmlFilter is not null) begin

	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xmlFilter

	Select
	    @SAID = isnull(@SAID, saID),
		@School = isnull(@School, School),
		@InspYear = isnull(@InspYear, InspYear)
	FROM OPENXML(@idoc,'//Filter',2)
	WITH
	(
	saID int '@SAID',
	School nvarchar(50) '@School',
	InspYear nvarchar(50) '@InspYear'
	)
end

	DECLARE @keysAll TABLE
	(
	ID int,
	recNo int IDENTITY PRIMARY KEY
	)

INSERT INTO @KeysAll (ID)
SELECT saID
from pInspectionRead.SchoolAccreditations SA -- view
	LEFT JOIN Schools S
		ON SA.schNo = S.schNo
WHERE
(saID = @SAID OR @SAID IS NULL)
AND (InspYear = @InspYear OR @InspYear IS NULL)
AND (S.schName like '%' +  @School + '%' OR @School IS NULL)

OPTION(RECOMPILE)


SELECT @NumMatches = @@ROWCOUNT

-- now return the page, sorted in the right sequence and direction

If @SortDir = 1 begin


		SELECT ID
		, RecNo
		FROM
		(
			Select ID
			, @NumMatches - RecNo + 1 RecNo
			FROM @KeysAll
		) S
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
		ORDER BY RecNo

	end
	else begin


		SELECT ID
		, RecNo
		FROM @KeysAll
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
	end

END
GO

