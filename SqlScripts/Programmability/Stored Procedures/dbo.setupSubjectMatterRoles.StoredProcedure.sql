SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[setupSubjectMatterRoles]
(
	@subjectMatter nvarchar(50)
)

AS

BEGIN

declare @theRole sysname
declare @theRoleParent sysname
declare @sql nvarchar(255)

-- create all the roles
select @theRole = ('p' + @subjectMatter + 'Read')
IF  NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = @theRole AND type = 'R')
select @sql = 'CREATE ROLE ' + quotename(@theRole,']') + ' AUTHORIZATION [dbo]'; exec (@sql)

select @theRole = ('p' + @subjectMatter + 'Write')
IF  NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = @theRole AND type = 'R')
select @sql = 'CREATE ROLE ' + quotename(@theRole,']') + ' AUTHORIZATION [dbo]'; exec (@sql)

select @theRole = ('p' + @subjectMatter + 'ReadX')
IF  NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = @theRole AND type = 'R')
select @sql = 'CREATE ROLE ' + quotename(@theRole,']') + ' AUTHORIZATION [dbo]'; exec (@sql)

select @theRole = ('p' + @subjectMatter + 'WriteX')
IF  NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = @theRole AND type = 'R')
select @sql = 'CREATE ROLE ' + quotename(@theRole,']') + ' AUTHORIZATION [dbo]'; exec (@sql)

select @theRole = ('p' + @subjectMatter + 'Ops')
IF  NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = @theRole AND type = 'R')
select @sql = 'CREATE ROLE ' + quotename(@theRole,']') + ' AUTHORIZATION [dbo]'; exec (@sql)

select @theRole = ('p' + @subjectMatter + 'Admin')
IF  NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = @theRole AND type = 'R')
select @sql = 'CREATE ROLE ' + quotename(@theRole,']') + ' AUTHORIZATION [dbo]'; exec (@sql)

-- set up the nesting
select @theRole = ('p' + @subjectMatter + 'Write')
select @theRoleParent = ('p' + @subjectMatter + 'Read')

select @sql = ('EXEC sp_addrolemember ' + @theRoleParent + ', ' + @theRole); exec(@sql)

select @theRole = ('p' + @subjectMatter + 'WriteX')
select @theRoleParent = ('p' + @subjectMatter + 'ReadX')

select @sql = ('EXEC sp_addrolemember ' + @theRoleParent + ', ' + @theRole); exec(@sql)


-- global admin gets all other admins
select @theRole = ('pAdmin')
select @theRoleParent = ('p' + @subjectMatter + 'Admin')

select @sql = ('EXEC sp_addrolemember ' + @theRoleParent + ', ' + @theRole); exec(@sql)


END
GO

