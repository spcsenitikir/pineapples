SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: Armageddon + 1 (10 11 2016)
-- Description:	This produces the flat table that can be used to produce the LevelER nodes in WERMPAF
-- =============================================
CREATE PROCEDURE [warehouse].[buildLevelER]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	-- enrolment related pupil tables
	IF OBJECT_ID('warehouse.classLevelER', 'U') IS NOT NULL
		DROP TABLE warehouse.classLevelER;
Select S.*
, POP.popM
, POP.popF
, POP.pop
INTO warehouse.classLevelER
FROM
(
	Select SurveyYEar
		, ClassLevel
		, lvlYear YearOfEd
		, svyPSAge - 1 + lvlYear OfficialAge
		, sum(enrolM) enrolM
		, sum(enrolF) enrolF
		, sum(enrol) enrol

		, sum(repM) repM
		, sum(repF) repF
		, sum(rep) rep


		, sum(psaM) psaM
		, sum(psaF) psaF
		, sum(psa) psa

		, sum(intakeM) intakeM
		, sum(intakeF) intakeF
		, sum(intake) intake

		, sum(nEnrolM) nEnrolM
		, sum(nEnrolF) nEnrolF
		, sum(nEnrol) nEnrol

		, sum(nRepM) nRepM
		, sum(nRepF) nRepF
		, sum(nRep) nRep

		, sum(nIntakeM) nIntakeM
		, sum(nIntakeF) nIntakeF
		, sum(nIntake) nIntake
	FROM
	(
	Select SurveyYear
		, ClassLevel
		, Enrol enrolM
		, null enrolF
		, Enrol enrol

		, Rep repM
		, null repF
		, Rep rep

		, psa psaM
		, null psaF
		, psa psa

		, isnull(Enrol,0) - isnull(Rep,0) intakeM
		, null intakeF
		, isnull(Enrol,0) - isnull(Rep,0) intake

		, case when svyPSAge - 1 + lvlYear = Age then Enrol end nEnrolM
		, null nEnrolF
		, case when svyPSAge - 1 + lvlYear = Age then Enrol end nEnrol

		, case when svyPSAge - 1 + lvlYear = Age then Rep end nRepM
		, null nRepF
		, case when svyPSAge - 1 + lvlYear = Age then Rep end nRep

		, case when svyPSAge - 1 + lvlYear = Age then isnull(Enrol,0) - isnull(Rep,0)  end nIntakeM
		, null nIntakeF
		, case when svyPSAge - 1 + lvlYear = Age then isnull(Enrol,0) - isnull(Rep,0)  end nIntake

		FROM warehouse.enrol
		INNER JOIN lkpLevels L
			ON ClassLevel = L.codeCode
		INNER JOIN Survey S
			ON SurveyYEar = S.svyYear
		WHERE GenderCode = 'M'
		UNION ALL
		Select SurveyYear
		, ClassLevel
		, null enrolM
		, Enrol enrolF
		, Enrol enrol

		, null repM
		, Rep repF
		, Rep rep

		, null psaM
		, psa psaF
		, psa psa

		, null intakeM
		, isnull(Enrol,0) - isnull(Rep,0) intakeF
		, isnull(Enrol,0) - isnull(Rep,0) intake

		, null nEnrolM
		, case when svyPSAge - 1 + lvlYear = Age then Enrol end nEnrolF
		, case when svyPSAge - 1 + lvlYear = Age then Enrol end nEnrol

		, null nRepM
		, case when svyPSAge - 1 + lvlYear = Age then Rep end nRepF
		, case when svyPSAge - 1 + lvlYear = Age then Rep end nRep

		, null nIntakeM
		, case when svyPSAge - 1 + lvlYear = Age then isnull(Enrol,0) - isnull(Rep,0)  end nIntakeF
		, case when svyPSAge - 1 + lvlYear = Age then isnull(Enrol,0) - isnull(Rep,0)  end nIntake


		FROM warehouse.enrol
		INNER JOIN lkpLevels L
			ON ClassLevel = L.codeCode
		INNER JOIN Survey S
			ON SurveyYEar = S.svyYear
		WHERE GenderCode = 'F'
	) U
	INNER JOIN lkpLevels L
		ON U.ClassLevel = L.codeCode
	INNER JOIN Survey S
		ON U.SurveyYear = S.svyYear
	GROUP BY SurveyYear, ClassLevel, lvlYEar, svyPSAge
) S
LEFT join
(
	--population totals
	SELECT popYear
	, popAge
	, sum(case when GenderCode = 'M' then pop end) PopM
	, sum(case when GenderCode = 'F' then pop end) PopF
	, sum(pop) Pop
	from warehouse.measurePopG
	GRoup by popYear, popAge
) POP
	ON S.SurveyYEar = POP.PopYear
	AND S.OfficialAge = POP.popAge
ORDER BY SurveyYear, YearOfEd
END
GO

