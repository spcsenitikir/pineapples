SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 3/6/2009
-- Description:
-- =============================================
CREATE PROCEDURE [pSurveyRead].[SurveyControlFilter]
	-- Add the parameters for the stored procedure here
	@SummaryOnly int
	, @paramYear int
	, @paramDistrict nvarchar(10) = null
	, @paramIsland nvarchar(10)  = null
	, @paramType nvarchar(10) = null
	, @paramAuth nvarchar(10)= null
	, @paramFile nvarchar(30)= null
	, @paramStatus int = -1
	, @paramSort nvarchar(50) = 'schName'
	, @paramSortORder nvarchar(30) = 'ASC'

AS
BEGIN
	SET NOCOUNT ON;

SELECT
	SchoolSurvey.ssID

	, Schools.schName
	, Schools.iCode
	, Schools.schType, Schools.schAuth

	, Survey.svyYear
	, Survey.svyCensusDate
	, Survey.svyCollectionDate
	, SYH.syID
	, SYH.syYear
	, SYH.schNo

	, SYH.saSent
	, SYH.saCollected
	, SYH.saReceived
	, SYH.saReceivedTarget
	, SYH.saCompletedby
	, SYH.saEntered
	, SYH.saAudit
	, SYH.saAuditDate
	, SYH.saAuditBy
	, SYH.saAuditResult
	, SYH.saAuditNote
	, SYH.saComment
	, SYH.saFileLocn
	, SYH.syDormant
	, SYH.saReceivedD
	, SYH.saReceivedDTarget

	, SYH.systCode
	, SYH.syAuth
	, SYH.syParent


	, Islands.iGroup
	, D.dName

	, SchoolSurvey.ssReview

	, SchoolSurvey.ssMissingData, SchoolSurvey.ssMissingDataNotSupplied

	, QUAL.NumIssues, QUAL.NumAlerts, QUAL.NumBlocks, QUAL.MaxLevel

	, Schools.schClosed, Schools.schEst

INTO #tmp

FROM
	Islands
		INNER JOIN Schools ON Islands.iCode = Schools.iCode
		INNER JOIN SchoolYearHistory SYH on Schools.schNo = SYH.schNo
		INNER JOIN Survey  ON Survey.svyYear = SYH.syYear
		LEFT JOIN SchoolSurvey ON (SYH.schNo = SchoolSurvey.schNo) AND (SYH.syYear = SchoolSurvey.svyYear)
		LEFT JOIN pSurveyRead.ssIDQuality QUAL ON SchoolSurvey.ssID = QUAL.ssID
		LEFT JOIN Districts D
			ON Islands.iGRoup = D.dID

WHERE
		(syYear = @paramYear or @paramYear is null)
	and (iGroup = @paramDistrict or @paramDistrict is null)
	and (Schools.iCode = @paramIsland or @paramIsland is null)
	and (schType = @paramType or @paramType is null)
	and (schAuth = @paramAuth or @paramAuth is null)
	and (saFileLocn = @paramFile or @paramFile is null)

	and (
			(@paramStatus <= 0)
		or	(@paramStatus = 1 and saSent is null )
		or	(@paramStatus = 2 and saReceived is null )
		or	(@paramStatus = 3 and saEntered is null )
		or	(@paramStatus = 4 and saAudit = 1  )
		or	(@paramStatus = 5 and saAudit = 1  and saAuditDate is null )
		or	(@paramStatus = 6 and saAuditDate is not null )
		or	(@paramStatus = 7 and saAuditResult = 'F' )
		or	(@paramStatus = 8 and MaxLevel in (1,2) )
		or	(@paramStatus = 9 and MaxLevel > 0 )
		or	(@paramStatus = 10 and saSent is not null )
		or	(@paramStatus = 11 and saReceived is not null )
		or	(@paramStatus = 12 and saEntered is not null )
		or	(@paramStatus = 13 and saEntered is null and SchoolSurvey.ssID is not null )
		or	(@paramStatus = 14 and syDormant = 1 )

		)


	declare @Summary table
	(
		schNo nvarchar(50)
		, svyYear int
		, SchoolType nvarchar(5)
		, Authority nvarchar(10)
		, Enrol int
		, EnrolM int
		, EnrolF int
		, ssID int
		, prinFirstName nvarchar(50)
		, prinSurname nvarchar(50)
		, note ntext
		, minClassLevel nvarchar(10)
		, maxClassLevel nvarchar(10)
		, maxLevelEnrol int
		, maxEnrolLevel nvarchar(10)
		, TeacherSurveyCount int
		, ClassroomCount int
		, PTR float

	)

	if (@SummaryOnly = 2) begin
		SELECT syID
		, schNo
		, syYear
		FROM #tmp
		drop Table #tmp
		return
	end

	--if (is_member('pEnrolmentRead') = 1 or  begin
	-- only attempt this if the user has permission
	-- otherwise leave the summary table blank
	-- 5 2 2010 just try anyway for the benefit of sysadmins etc
	begin try
		INSERT INTO @Summary
		EXEC pEnrolmentRead.SchoolAnnualSummary null,@ParamYear
	end try
	begin catch
	end catch

	SELECT #Tmp.*
		, S.Enrol
		, S.EnrolM
		, S.EnrolF
		, S.minClassLevel
		, S.maxClassLevel
		, S.TeacherSurveyCount
		, S.ClassroomCount
		, S.PTR
	INTO #tmp2
	FROM
		#tmp
		LEFT JOIN @Summary S
		on #tmp.syYear = S.svyYear
		AND #tmp.schNo = S.schNo

select @paramSort = isnull(@paramSort,'schName')
select @paramSortOrder = isnull(@paramSortOrder,'ASC')

-- ingenious!
-- client side sorting is probably better under ADO 17 11 2009
	if (@SummaryOnly = 0) begin
		IF @paramSortOrder= 'ASC'
			begin
				If @paramSort =  'schNo' Select * from #tmp2 ORDER BY schNo
				If @paramSort =  'schName' Select * from #tmp2 ORDER BY schName
				If @paramSort =  'saSent' Select * from #tmp2 ORDER BY saSent
				If @paramSort =  'saCollected' Select * from #tmp2 ORDER BY saCollected
				If @paramSort =  'saReceived' Select * from #tmp2 ORDER BY saReceived
				If @paramSort =  'saReceivedTarget' Select * from #tmp2 ORDER BY saReceivedTarget
				If @paramSort =  'saCompletedby' Select * from #tmp2 ORDER BY saCompletedby
				If @paramSort =  'saEntered' Select * from #tmp2 ORDER BY saEntered
				If @paramSort =  'saAudit' Select * from #tmp2 ORDER BY saAudit
				If @paramSort =  'saAuditDate' Select * from #tmp2 ORDER BY saAuditDate
				If @paramSort =  'saAuditBy' Select * from #tmp2 ORDER BY saAuditBy
				If @paramSort =  'saAuditResult' Select * from #tmp2 ORDER BY saAuditResult
				If @paramSort =  'saAuditNote' Select * from #tmp2 ORDER BY saAuditNote
				If @paramSort =  'saComment' Select * from #tmp2 ORDER BY saComment
				If @paramSort =  'saFileLocn' Select * from #tmp2 ORDER BY saFileLocn
				If @paramSort =  'saDormant' Select * from #tmp2 ORDER BY saDormant
				If @paramSort =  'saReceivedD' Select * from #tmp2 ORDER BY saReceivedD
				If @paramSort =  'saReceivedDTarget' Select * from #tmp2 ORDER BY saReceivedDTarget

			end
		IF @paramSortOrder= 'DESC'
			begin
				If @paramSort =  'schNo' Select * from #tmp2 ORDER BY schNo DESC
				If @paramSort =  'schName' Select * from #tmp2 ORDER BY schName DESC
				If @paramSort =  'saSent' Select * from #tmp2 ORDER BY saSent DESC
				If @paramSort =  'saCollected' Select * from #tmp2 ORDER BY saCollected DESC
				If @paramSort =  'saReceived' Select * from #tmp2 ORDER BY saReceived DESC
				If @paramSort =  'saReceivedTarget' Select * from #tmp2 ORDER BY saReceivedTarget DESC
				If @paramSort =  'saCompletedby' Select * from #tmp2 ORDER BY saCompletedby DESC
				If @paramSort =  'saEntered' Select * from #tmp2 ORDER BY saEntered DESC
				If @paramSort =  'saAudit' Select * from #tmp2 ORDER BY saAudit DESC
				If @paramSort =  'saAuditDate' Select * from #tmp2 ORDER BY saAuditDate DESC
				If @paramSort =  'saAuditBy' Select * from #tmp2 ORDER BY saAuditBy DESC
				If @paramSort =  'saAuditResult' Select * from #tmp2 ORDER BY saAuditResult DESC
				If @paramSort =  'saAuditNote' Select * from #tmp2 ORDER BY saAuditNote DESC
				If @paramSort =  'saComment' Select * from #tmp2 ORDER BY saComment DESC
				If @paramSort =  'saFileLocn' Select * from #tmp2 ORDER BY saFileLocn DESC
				If @paramSort =  'saDormant' Select * from #tmp2 ORDER BY saDormant DESC
				If @paramSort =  'saReceivedD' Select * from #tmp2 ORDER BY saReceivedD DESC
				If @paramSort =  'saReceivedDTarget' Select * from #tmp2 ORDER BY saReceivedDTarget DESC

			end
	end

-- now get the second rs of totals

	Select count(saReceived) as Received
	, count(SchNo) as Total
	, count(saSent) as Sent
	, count(saEntered) as Entered
	, sum(CASE WHEN (saAudit <> 0) then 1 else 0 end) as AuditReq
	, count(saAuditDate) as Auditcomplete
	, sum(case when (saCollected <= svyCollectionDate) then 1 else 0 end) as CollectedOnTime
	, sum(case when (saReceived <= saReceivedTarget) then 1 else 0 end) as ReceivedOnTime
	, sum(case when (saAuditResult = 'F') then 1 else 0 end) as AuditFail
	, sum(Enrol) Enrol
	, sum(EnrolM) EnrolM
	, sum(EnrolF) EnrolF
	, sum(TeacherSurveyCount) TeacherSurveyCount
	, sum(ClassroomCount) ClassRoomCount
	from #tmp2


DROP TABLE #tmp
DROP TABLE #tmp2

END
GO

