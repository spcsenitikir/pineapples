SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 11 1 2010
-- Description:	Retireve Work Orders based on Criteria
-- =============================================
CREATE PROCEDURE [pInfrastructureRead].[WorkOrderFilter]
	-- Add the parameters for the stored procedure here


@Status nvarchar(15) = null
, @SupplierCode nvarchar(10) = null
, @FundSource nvarchar(15) = null
, @ManagedBy int = null


, @PlannedDateStart datetime = null, @PlannedDateEnd datetime = null
, @WorkApprovedDateStart datetime = null, @WorkApprovedDateEnd datetime = null
, @FinanceApprovedDateStart datetime = null, @FinanceApprovedDateEnd datetime = null
, @TenderCloseDateStart datetime = null, @TenderCloseDateEnd datetime = null
, @ContractDateStart datetime = null, @ContractDateEnd datetime = null
, @CommencedDateStart datetime = null, @CommencedDateEnd datetime = null
, @PlannedCompletionDateStart datetime = null, @PlannedCompletionDateEnd datetime = null
, @CompletionDateStart datetime = null, @CompletionDateEnd datetime = null
, @SignOffDateStart datetime = null, @SignOffDateEnd datetime = null

, @BudgetCostMin money = null, @BudgetCostMax money = null
, @ContractCostMin money = null, @ContractCostMax money = null
, @InvoiceCostMin  money = null, @InvoiceCostMax money = null
, @VarianceMin  money = null, @VarianceMax money = null

, @SchoolNo nvarchar(50) = null
, @WorkItemType nvarchar(15) = null
, @Progress nvarchar(15) = null
, @BuildingID int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET ANSI_WARNINGS OFF;
begin try

	CREATE TABLE #wo
	(
		ID int
		, SelectedItemCount int
		, SelectedItemEst int
		, SelectedItemInProgress int
		, SelectedItemSchools int			-- distinct schools on the selected workitems

	)


INSERT INTO #wo
(
	ID
	, SelectedITemCount
	, SelectedItemEst
	, SelectedItemInProgress
	, SelectedItemSchools
)
Select WorkOrders.woID
	, count(WorkItems.witmID)
	, sum(witmEstCost)
	, Sum(case witmProgress when 'INSP' then 1 else 0 end)
	, count(distinct workitems.schNo)

FROM WorkOrders
	LEFT JOIN WorkITems
		ON WorkOrders.woID = WorkItems.woID


WHERE
	(woStatus = @Status or @Status is null)
	AND (supCode = @SupplierCode or @SupplierCode is null)


	AND (woSourceOfFunds = @FundSource or @FundSource is null)

	AND (@ManagedBy = woDonorManaged or @ManagedBy is null)

	AND (woPlanned >= @PlannedDateStart or @PlannedDateStart is null)
		AND (woPlanned <= @PlannedDateEnd or @PlannedDateEnd is null)

	AND (woWorkApproved >= @WorkApprovedDateStart or @WorkApprovedDateStart is null)
		AND (woWorkApproved <= @WorkApprovedDateEnd or @WorkApprovedDateEnd is null)

	AND (woFinanceApproved >= @FinanceApprovedDateStart or @FinanceApprovedDateStart is null)
		AND (woFinanceApproved <= @FinanceApprovedDateEnd or @FinanceApprovedDateEnd is null)

	AND (woTenderClose >= @TenderCloseDateStart or @TenderCloseDateStart is null)
		AND (woTenderClose <= @TenderCloseDateEnd or @TenderCloseDateEnd is null)

	AND (woContracted >= @ContractDateStart or @ContractDateStart is null)
		AND (woContracted <= @ContractDateEnd or @ContractDateEnd is null)

	AND (woCommenced >= @CommencedDateStart or @CommencedDateStart is null)
		AND (woCommenced <= @CommencedDateEnd or @CommencedDateEnd is null)

	AND (woCompleted >= @CompletionDateStart or @CompletionDateStart is null)
		AND (woCompleted <= @CompletionDateEnd or @CompletionDateEnd is null)

	AND (woPlannedCompletion >= @PlannedCompletionDateStart or @PlannedCompletionDateStart is null)
		AND (woPlannedCompletion <= @PlannedCompletionDateEnd or @PlannedCompletionDateEnd is null)

	AND (woSignOff >= @SignOffDateStart or @SignOffDateStart is null)
		AND (woSignOff <= @SignOffDateEnd or @SignOffDateEnd is null)

-- costs

	AND (woBudget >= @BudgetCostMin or @BudgetCostMin is null)
		AND (woBudget <= @BudgetCostMax or @BudgetCostMax is null)

-- and look for a subitem
	AND (schNo = @schoolNo or @SchoolNo is null)
		AND (witmType = @WorkITemType or @WorkITemType is null)
		AND (bldID = @buildingID or @BuildingID is null)
		AND (witmProgress = @Progress or @Progress is null)

GROUP BY WorkOrders.woID


-- the first recordset is the work orders
SELECT WorkOrders.woID
	, woRef
	, woStatus
	, woDesc
	, woPlanned
	, woBudget
	, woWorkApproved
	, woFinanceApproved
	, woSourceOfFunds
	, woDonorManaged
	, woCostCentre, woTenderClose, woContracted, supCode, woContractValue
	, woCommenced, woPlannedCompletion, woCompleted, woInvoiceValue
	, woSignoff, woSignOffUser
	, Count(DISTINCT WorkItems.schNo) as NumSchools
	, Count(WorkItems.witmID) AS NumItems
	, Sum(case witmProgress when 'INSP' then 1 else 0 end) AS ItemsInspected
	, Sum(WorkItems.witmEstCost) AS EstCost
	, sum(#wo.SelectedItemEst) as SelectedItemEstCost			-- sum or group by doesn;t matter, only one per woID anyway
	, sum(#wo.SelectedItemSchools) as SelectedItemNumSchools
	, [woInvoiceValue]-[woContractValue] AS Variance
FROM WorkOrders
	INNER JOIN #wo
		ON WorkOrders.woID = #wo.ID
	LEFT JOIN WorkITems
		ON WorkOrders.woID = WorkItems.woID


GROUP BY WorkOrders.woID, woRef, woStatus, woDesc
	, woPlanned, woBudget, woWorkApproved, woFinanceApproved
	, woSourceOfFunds, woDonorManaged, woCostCentre, woTenderClose, woContracted
	, supCode, woContractValue, woCommenced, woPlannedCompletion, woCompleted, woInvoiceValue, woSignoff, woSignOffUser


-- the second is the summary
declare @TotalInvoice as money
declare @TotalContract as money
declare @TotalBudget as money

declare @SelectedItemEst as money
declare @SelectedITemNumSchools as int
declare @SelectedITemNumItems as int

declare @NumWO int


SELECT
	@NumWO = count(woID)
	,@TotalBudget =  sum(woBudget)
	,@TotalInvoice =  sum([woInvoiceValue])
	,@TotalContract =  sum([woContractValue])


FROM WorkOrders
	INNER JOIN #wo
		ON WorkOrders.woID = #wo.ID


SELECT
	@SelectedItemEst = sum(witmEstCost)
	,@SelectedItemNumSchools = count(distinct schNo)
	,@SelectedItemNumItems = count(witmID)

FROM #wo
	INNER JOIN WorkITems
		ON #wo.ID = WorkItems.woID
	WHERE (schNo = @schoolNo or @SchoolNo is null)
		AND (witmType = @WorkITemType or @WorkITemType is null)
		AND (bldID = @buildingID or @BuildingID is null)
		AND (witmProgress = @Progress or @Progress is null)


SELECT @NumWO as NumWO
	, Count(DISTINCT WorkItems.schNo) as NumSchools
	, Count(WorkItems.witmID) AS NumItems
	, Sum(case witmProgress when 'INSP' then 1 else 0 end) AS ItemsInspected
	, Sum(WorkItems.witmEstCost) AS TotalEstCost
	, @TotalBudget as TotalBudget
	, @TotalInvoice as TotalInvoice
	, @totalContract AS TotalContract
	, @SelectedItemEst as SelectedItemTotalEstCost
	, @SelectedItemNumSchools as SelectedItemNumSchools
	, @SelectedItemNumItems as SelectedItemNumItems
FROM #wo
	INNER JOIN WorkITems
		ON #wo.ID = WorkItems.woID


DROP TABLE #wo

end try

/****************************************************************
Generic catch block
****************************************************************/
begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
END
GO

