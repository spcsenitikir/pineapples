SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 14 2 2010
-- Description:	Generate School Grants for a year/payment no
-- =============================================
CREATE PROCEDURE [pFinanceWriteX].[setGrantStatus]
	-- Add the parameters for the stored procedure here
	@GrantYear int,
	@PaymentNo int,
	@SchoolNo nvarchar(50) = null,
	@SchoolType nvarchar(10) = null,
	@Authority nvarchar(10) = null,
	@NewStatus nvarchar(20)
AS
BEGIN
SET Nocount on;

-- first collect the enrolment data

UPDATE SchoolGrant
SET sgStatus = @NewStatus
FROM SchoolGrant
	INNER JOIN Schools S
		ON SchoolGrant.schNo = S.schNo
	LEFT JOIN SchoolYearHistory SEst
		ON S.schNo = SEst.schNo
		AND Sest.syYear = @GrantYear

WHERE
	sgYear = @GrantYear
	AND sgPaymentNo = @PaymentNo
	AND (SchoolGrant.schno = @SchoolNo or @SchoolNo is null)
	AND (isnull(SEST.systcode, S.schType) = @SchoolType or @SchoolType is null)
	AND (isnull(SESt.syAuth, S.schauth) = @Authority or @Authority is null)
	AND sgStatus is null

print @@ROWCOUNT


END
GO

