SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 10 6 2016
-- Description:	Upload a resource list entered via the web interface
-- =============================================
CREATE PROCEDURE [pSurveyWrite].[ResourceListSave]
	@list xml,
	@user nvarchar(100) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @schoolNo nvarchar(50)
	declare @surveyYear int
	declare @category nvarchar(100)
		declare @ssId int

	-- ensure there is a survey to write to
    select @user = isnull(@user, original_login())
	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @list

	--
	declare @resourceDefs TABLE
	(
		category nvarchar(100),
		name nvarchar(100)
	)

	Select @SchoolNo = schoolNo
	, @surveyYear = surveyYear
	, @category = Category
	from OPENXML (@idoc, '/ResourceList',2)
	WITH (
		category nvarchar(100) 'Category'
		, schoolNo nvarchar(50) 'SchoolNo'
		, surveyYear int		'Year'
	)
	INSERT @resourceDefs
	Select * from OPENXML (@idoc, '/ResourceList/Rows/ResourceDef',2)
	WITH (
		category nvarchar(100) '../../Category'
		, name nvarchar(100) 'name'
	)


	DECLARE @Survey TABLE
	(
		ssID int
	)
	Select @ssID = ssID
	from SchoolSurvey
	WHERE schNo = @schoolNo
	AND svyYear = @SurveyYear

	if (@ssId is null) begin
		INSERT INTO @Survey
		exec pSurveyWrite.CreateSchoolSurvey @SchoolNo, @SurveyYear
		Select @ssID = ssId from @Survey
	end


	--- debug  report the variables

--------	Select @SchoolNo SchoolNo
--------	, @surveyYear Year
--------	, @ssID ssID
--------	, @category category

----------		Select * from @ResourceDefs

--------	--SELECT @list


--------	Select
--------	resourceCode
--------	, case available when 'true' then 1 when 'false' then 0 else null end Available
--------	 from OPENXML (@idoc, '/ResourceList/Data/ResourceData',2)
--------	WITH (
--------		resourceCode nvarchar(50) 'ResourceCode'
--------		, available nvarchar(10) 'Available'
--------		, adequate nvarchar(10) 'Adequate'
--------		, functioning nvarchar(10) 'Functioning'
--------		, num		int				'Num'
--------		, qty		int				'Qty'
--------	)
--------	WHERE resourceCode is not null
--------	AND
--------	-- some values must be provided
--------	( available = 'true'
--------	OR adequate = 'true'
--------	OR functioning = 'true'
--------	OR isnull(Qty,0) <> 0
--------	OR isnull(num,0) <> 0
--------	)

	---- end debug

	DELETE from Resources
	WHERE ssID = @ssID
	AND resName = @category

	INSERT INTO Resources
	(
	ssID
	, resName
	, resSplit
	, resAvail
	, resAdequate
	, resNumber
	, resQty
	, resCondition
	, resFunctioning
	)
		Select
	@ssID
	, @category
	, resourceCode
	, case available when 'true' then -1 when 'false' then 0 else null end Available
	, case adequate when 'true' then -1 when 'false' then 0 else null end Adequate
	, num
	, qty
	, condition
	, case functioning when 'true' then -1 when 'false' then 0 else null end Functioning
	 from OPENXML (@idoc, '/ResourceList/Data/ResourceData',2)
	WITH (
		resourceCode nvarchar(50) 'ResourceCode'
		, available nvarchar(10) 'Available'
		, adequate nvarchar(10) 'Adequate'
		, functioning nvarchar(10) 'Functioning'
		, num		int				'Num'
		, qty		int				'Qty'
		, condition int				'Condition'
	)
		WHERE resourceCode is not null
	AND
	-- some values must be provided
	( available = 'true'
	OR adequate = 'true'
	OR functioning = 'true'
	OR isnull(Qty,0) <> 0
	OR isnull(num,0) <> 0
	)

	exec pSurveyRead.ResourceList @SchoolNo, @SurveyYear, @category
END
GO

