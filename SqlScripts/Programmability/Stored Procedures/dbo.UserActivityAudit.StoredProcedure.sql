SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 18 10 2009
-- Description:	report on user activity
-- =============================================
CREATE PROCEDURE [dbo].[UserActivityAudit]
	-- Add the parameters for the stored procedure here
	@UserLogin sysname = null
	, @StartDate datetime = null
	, @EndDate datetime = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
if (@startDate is null)
	Select @startDate = common.Today()
if (@EndDate is null)
	Select @EndDate = @StartDate
    -- Insert statements for procedure here
Select ActivityLogin, dateadd(dd,num,@startDate) ActivityDate
, min(
case when ActivityTask = 'Login' then ActivityTime end) FirstLogin
, max(
case when ActivityTask = 'Login' then ActivityTime end) LastLogin
, min(
case when ActivityTask = 'Logout' then ActivityTime end) FirstLogout
, max(
case when ActivityTask = 'Logout' then ActivityTime end) LastLogout

, count(DISTINCT case when ActivityTask = 'ReadSurvey' then ActivityReference end) SurveyReadsDistinct
, count( case when ActivityTask = 'ReadSurvey' then ActivityReference end) SurveyReads
, count(DISTINCT case when ActivityTask = 'UpdateEnrolment' then ActivityReference end) EnrolmentUpdatesDistinct
, count( case when ActivityTask = 'UpdateEnrolment' then ActivityReference end) EnrolmentUpdates
from MetaNumbers
LEFT JOIN (Select *
			from ActivityLog
			WHERE
				(ActivityLogin = @UserLogin or @UserLogin is null)
			) AL
	ON DateAdd(dd,num,@StartDate) = AL.activityDate
WHERE

			(num between 0 and datediff(dd, @StartDate ,@endDate))

group by ActivityLogin, num

END
GO

