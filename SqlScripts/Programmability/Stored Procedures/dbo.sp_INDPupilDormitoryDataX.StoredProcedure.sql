SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 26 11 2007
-- Description:	Data for pupil  dormitory ratio
-- =============================================
CREATE PROCEDURE [dbo].[sp_INDPupilDormitoryDataX]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- there are 3 data sources - enrolment, boarders and dormitories
-- attemtped this with outer join, union-and-sum is much faster


create table #data
(
LifeYear int,
schNo nvarchar(50),
GenderCode nvarchar(2),		-- 2 is becuase of the <> placeholder
Gender nvarchar(10),
[Dorm Data Estimate] int,
[Year of Dorm Data] int,
[Age of Dorm Data] int,
[Dorm Data Quality Level] int,
[Enrol Estimate] int,
[Year of Enrol Data] int,
[Age of Enrol Data] int,
[Enrol Data Quality Level] int,
SurveyDimensionssID  int,
DormSurveyDimensionssID  int,		-- surveydimensionssid as supplied from the dorm estimate
									-- only use it when there is no enrolment estimate

NumRooms int,
TotSize float,
MinSize float,
[MaxSize] float,
SizeSupplied int,
Capacity int,
Showers int,
Toilets int,
Enrol int,
Boarders int

)

SELECT *
INTO #ebse
FROM dbo.tfnESTIMATE_BestSurveyEnrolments()

SELECT *
INTO #ebr
FROM dbo.tfnESTIMATE_BestSurveyRoomType('DORM')

-- first get the enrolments
INSERT INTO #data
(LifeYear, SchNo,
GenderCode,
Gender,
[Enrol Estimate],
[Year of Enrol Data],
[Age of Enrol Data],
surveyDimensionssID,
Enrol
)
SELECT
	E.LifeYear,
	E.schNo,
	G.GenderCode,
	G.Gender,
	E.Estimate,
	E.BestYear,
	E.Offset,
	E.surveyDimensionssID,
	case
		when GenderCode = 'M' then SS.ssEnrolM else ssEnrolF
	end
FROM #ebse E
	INNER JOIN SchoolSurvey SS
		ON E.bestssID = ss.ssID
	CROSS JOIN DimensionGender G


-- boarders
INSERT INTO #data
(LifeYear, SchNo,
GenderCode,
Gender,
[Enrol Estimate],
[Year of Enrol Data],
[Age of Enrol Data],
surveyDimensionssID,
Boarders
)
SELECT
	E.LifeYear,
	E.schNo,
	G.GenderCode,
	G.Gender,
	E.Estimate,
	E.BestYear,
	E.Offset,
	E.surveyDimensionssID,
	case
		when GenderCode = 'M' then brdM else brdF
	end
FROM #ebse E
	INNER JOIN (Select
						ssID,
						sum(ptM) brdM,
						sum(ptF) brdF
					FROM vtblBoarders
					GROUP BY ssID
					) B
		ON E.bestssID = B.ssID
	CROSS JOIN DimensionGender G


-- next is the dorm data that can be split by gender
INSERT INTO #data
(LifeYear, SchNo,
GenderCode,
Gender,
[Dorm Data Estimate],
[Year of Dorm Data],
[Age of Dorm Data],
DormsurveyDimensionssID,
Capacity,
Showers,
Toilets
)
Select
	E.LifeYear,
	E.schNo,
	G.GenderCode,
	G.Gender,
	E.Estimate,
	E.BestYear,
	E.Offset,
	E.surveyDimensionssID,
	case
		when GenderCode = 'M' then capM else capF
	end,
	case
		when GenderCode = 'M' then showersM else showersF
	end,
	case
		when GenderCode = 'M' then toiletsM else toiletsF
	end

FROM #ebr E
	INNER JOIN ssIDDormitories D
		ON E.bestssID = D.ssID

CROSS JOIN DimensionGender G

-- finally, add the room data that cannot be disaggregated by gender
INSERT INTO #data
(LifeYear, SchNo,
GenderCode,
Gender,
[Dorm Data Estimate],
[Year of Dorm Data],
[Age of Dorm Data],
DormsurveyDimensionssID,
NumRooms,
TotSize,
MinSize,
[MaxSize],
SizeSupplied
)
Select
	E.LifeYear,
	E.schNo,
	'<>',			-- place holders in gender column
	'<>',
	E.Estimate,
	E.BestYear,
	E.Offset,
	E.surveyDimensionssID,
	NumRooms,
	TotSize,
	MinSize,
	[MaxSize],
	SizeSupplied

FROM #ebr E
	INNER JOIN ssIDDormitories D
		ON E.bestssID = D.ssID


-- now group and total

Select LifeYear [Survey Year],
schNo,
GenderCode,
Gender,
min([Dorm Data Estimate]) [Dorm Data Estimate],		-- all values are the same except for NULL
min([Year of Dorm Data]) [Year of Dorm Data],
min([Age of Dorm Data]) [Age of Dorm Data],

min([Enrol Estimate]) [Enrol Estimate],		-- all values are the same except for NULL
min([Year of Enrol Data]) [Year of Enrol Data],
min([Age of Enrol Data]) [Age of Enrol Data],

case min(SurveydimensionssID)
	when null then min(DormSurveyDimensionssID)
	else min(SurveydimensionssID)
end surveydimensionssID,

sum(NumRooms) NumRooms,
sum(TotSize) TotSize,
sum(minSize) minSize,
sum([MaxSize]) [MaxSize],
min(SizeSupplied) SizeSupplied,			-- only one roww

sum(capacity) Capacity,
sum(showers) Showers,
sum(toilets) Toilets,
sum(Enrol) Enrol,
sum(Boarders) Boarders

from #data D
GROUP BY LifeYear,schNo, Gender, Gendercode

END
GO
GRANT EXECUTE ON [dbo].[sp_INDPupilDormitoryDataX] TO [pSchoolRead] AS [dbo]
GO

