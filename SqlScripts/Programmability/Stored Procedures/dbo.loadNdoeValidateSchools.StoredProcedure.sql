SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 11 10 2017
-- Description:	accepts a set of school number and returns those that are not valid
-- =============================================
CREATE PROCEDURE [dbo].[loadNdoeValidateSchools]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here


	declare @all table
	(
		School_No nvarchar(50)
		, Sheet_Name nvarchar(50)
		, rowID int
	)

	INSERT INTO @all
	Select
		nullif(v.value('.', 'nvarchar(50)'),'') School_No
		--, null
		--, null
		, v.value('@Sheet', 'nvarchar(50)') Sheet_Name
		, v.value('@Index', 'int') rowID
	FROM @xml.nodes('/Schools/Sheet/School_No') as V(v)

	DELETE FROM @all
	FROM @all
	INNER JOIN Schools S
		ON [@all].School_No = S.schNo

	-- bit kludgey - we are allowed to have the PNIDOE on the staff page
	DELETE FROM @all
	WHERE Sheet_Name = 'SchoolStaff'
	AND School_No like '%%%DOE'

	Select Sheet_Name
	, rowID
	, isnull(School_No,'(missing)') School_No
	from @all
	ORDER BY Sheet_Name, rowID


END
GO

