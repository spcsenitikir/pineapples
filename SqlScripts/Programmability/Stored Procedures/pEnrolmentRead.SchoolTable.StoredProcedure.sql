SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 14 04 2016
-- Description:	quick Pivot for schools
-- =============================================
CREATE PROCEDURE [pEnrolmentRead].[SchoolTable]
	-- Add the parameters for the stored procedure here
	@RName nvarchar(50) = 'School Type'		-- identifier of row
	, @CName nvarchar(50) = 'Authority'	-- identifier of column
	, @xmlFilter xml = null				-- xml filter of selected data
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @ids TABLE
(
	schNo nvarchar(50)
	, recNo int
)

declare @NumM int =0


declare @currentSurvey int
Select @currentSurvey = max(svyYear)
from SchoolSurvey

if @xmlFilter is not null begin

	INSERT INTO @ids
	EXEC pSchoolRead.SchoolFilterIDs @NumMatches = @NumM OUT, @xmlFilter = @xmlFilter

end
    -- Insert statements for procedure here
declare @district nvarchar(100)

declare @electN nvarchar(100)

declare @electL nvarchar(100)

Select @electN = vocabTerm
from sysVocab
WHERE vocabName = 'National Electorate'

Select @electL = vocabTerm
from sysVocab
WHERE vocabName = 'Local Electorate'

Select @district = vocabTerm
from sysVocab
WHERE vocabName = 'District'
declare @output TABLE
(
	R nvarchar(200) NULL,
	C nvarchar(200) NULL,
	RC nvarchar(200) NULL,
	Num int NULL,
	E int NULL,
	F int NULL,
	S int NULL,
	RName nvarchar(400) NULL,
	CName nvarchar(400) NULL
)
INSERT INTO @output

Select

	isnull(R,'<>') R
	, isnull(C,'<>') C
	, isnull(RC,'<>') RC
	, count(*) Num
	, sum (Enrol) E
	, sum(EnrolF) F
	, sum (Estimate) S

	, @RName Rname
	, @CName Cname
FROM
(

Select
	convert(nvarchar(100),
	case @RName
				when 'School Type' then ST.stDescription
				when 'SchoolType' then ST.stDescription
				when 'Authority' then A.authName
				when 'District' then D.dName
				when @district then D.dName
				when @electN then S.schElectL
			end) R
,
	convert(nvarchar(100),
	case @RName
				when 'School Type' then schType
				when 'SchoolType' then schType
				when 'Authority' then schAuth
				when 'District' then D.dID
			end) RC

	,
	case @CName
				when 'School Type' then ST.stDescription
				when 'SchoolType' then ST.stDescription
				when 'Authority' then A.authName
				when 'District' then D.dName

			end  C
	, IDE.Enrol Enrol
	, IDE.EnrolF EnrolF
	, case when E.Estimate = 1 then E.bestEnrol else null end Estimate
From Schools S
	LEFT JOIN SchoolTypes ST
		ON S.schType = ST.stCode
	LEFT JOIN Authorities A
		ON S.schAuth = A.authCode
	LEFT JOIN Islands I
		ON S.iCode = I.iCode
	LEFT JOIN Districts D
		ON I.iGroup = D.dID
	LEFT JOIN lkpElectorateN N
		ON S.schElectN = N.codeCode
	LEFT JOIN lkpElectorateL L
		ON S.schElectL = L.codeCode
	LEFT JOIN dbo.tfnEstimate_BestSurveyEnrolments() E
		ON E.LifeYear = @currentSurvey
		AND E.schNo = S.schNo
	LEFT JOIN pEnrolmentRead.ssIDEnrolment IDE
		ON E.bestssId = IDE.ssID


WHERE (@xmlFilter is null or S.schNo in (Select schNo from @Ids))
) SUB
GROUP BY R,C,RC

declare @summaryRows int


Select R,C, RC, RName, CName

, Num, E, F, S
from @Output
ORDER BY R,C

Select @summaryRows = @@rowcount -- = count(*) from @output

-- finally the summary
                SELECT @NumM NumMatches
                , 1 PageFirst
                , @NumM PageLast
                , 0 PageSize
                , 1 PageNo
                , 0 columnSet
                , @summaryRows SummaryRows

END
GO

