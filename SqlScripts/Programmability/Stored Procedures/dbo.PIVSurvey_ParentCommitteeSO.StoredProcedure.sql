SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PIVSurvey_ParentCommitteeSO]
	-- Add the parameters for the stored procedure here
	@DimensionColumns nvarchar(20) = null,
	@DataColumns nvarchar(20) = null,
	@Group nvarchar(30) ,
	@SchNo nvarchar(50) = null,
	@SurveyYear int = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	print 'CTC'
   Select *
   INTO #tmppivcols
   FROM PIVColsCECSO

	exec dbo.PIVSurvey_EXEC

   			@DimensionColumns,
			@DataColumns,
			@Group,
			@SchNo,
			@SurveyYear
END
GO

