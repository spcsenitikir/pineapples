SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 26 04 2009
-- Description:	Return a raw xml for a schoo table item
-- =============================================
CREATE PROCEDURE [dbo].[xmlSchoolTableItem]
	-- Add the parameters for the stored procedure here
	@tdiCode nvarchar(30) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	declare @sql nvarchar(2000)
	declare @xml xml
	create table #codeTmp
	(
		code nvarchar(20),
		codedesc nvarchar(1000)
	)
	select @sql = isnull(tdiSrcSQL, tdiSrc) from metaSchoolTableItems WHERE tdiCode = @tdiCode
	insert into #codeTmp
	exec (@sql)

    -- Insert statements for procedure here
	begin try
		set @xml = (SELECT * from #codeTmp for XML RAW)
	end try
	begin catch
		set @xml = (select null as code, null as codeDescription FOR XML RAW)
	end catch
	drop table #codeTmp
	select @xml
END
GO
GRANT EXECUTE ON [dbo].[xmlSchoolTableItem] TO [pSchoolRead] AS [dbo]
GO

