SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 25 10 2016
-- Description:
-- =============================================
CREATE PROCEDURE [warehouse].[buildWarehouse]
	-- Add the parameters for the stored procedure here
	with EXECUTE AS 'pineapples'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here


	---- diemensions
	begin transaction
	IF OBJECT_ID('warehouse.dimensionSchoolSurvey', 'U') IS NOT NULL
		DROP TABLE warehouse.dimensionSchoolSurvey;

	SELECT *
	INTO warehouse.dimensionSchoolSurvey
	FROM DimensionSchoolSurveyNoYear
	commit

	begin transaction
	IF OBJECT_ID('warehouse.bestSurvey', 'U') IS NOT NULL
		DROP TABLE warehouse.bestSurvey;

	SELECT LifeYear SurveyYear
	, schNo
	, surveyDimensionssID surveyDimensionID
	INTO warehouse.bestSurvey
	FROM tfnESTIMATE_BestSurveyEnrolments()
	commit

	-- warehouse rebuild script

	begin transaction
	IF OBJECT_ID('warehouse.measureEnrolSchoolG', 'U') IS NOT NULL
		DROP TABLE warehouse.measureEnrolSchoolG
	-- begin with the enrolments

	SELECT *
		INTO warehouse.measureEnrolSchoolG
		FROM measureEnrolSchoolG

	commit


	-- enrolment related pupil tables
	IF OBJECT_ID('warehouse.PupilTablesG', 'U') IS NOT NULL
		DROP TABLE warehouse.PupilTablesG;


		Select
		schNo
		, LifeYear SurveyYear
		, Estimate
		, SurveyDimensionssID SurveyDimensionID

		, ptLevel ClassLevel
		, ptAge Age
		, G.genderCode
		, case when G.genderCode = 'M' then ptM
			when G.genderCode = 'F' then ptF
		 end pt
		, ptCode
		INTO warehouse.pupiltablesG
		from dbo.tfnESTIMATE_BestSurveyEnrolments() B
			INNER JOIN PupilTables PT
				ON B.bestssID = PT.ssID
			CROSS JOIN DimensionGender G
		WHERE ptCode in ('BRD', 'REP', 'TRIN','TROUT', 'DIS', 'DROP')
		AND isnull(case when G.genderCode = 'M' then ptM
			when G.genderCode = 'F' then ptF
		 end ,0) <> 0
--- pre school attenders


	declare @yr1 nvarchar(10)
	select @yr1 = levelCode
		FROM ListDefaultPathLevels
		WHERE yearOfEd = 1

	If @yr1 is null
		Select top 1 @yr1 =  codeCode
		from lkpLevels

		INSERT INTO warehouse.PupilTablesG

		Select
		schNo
		, LifeYear SurveyYear
		, Estimate
		, SurveyDimensionssID SurveyDimensionID

		, @yr1 ClassLevel
		, ptAge Age
		, G.genderCode
		, sum(case when G.genderCode = 'M' then ptM
			when G.genderCode = 'F' then ptF
		 end) pt
		, 'PSA'
		from dbo.tfnESTIMATE_BestSurveyEnrolments() B
		-- in vermpaf, psa is only shown if there are Enrolments in yearofEd = 1
		-- (the psa are a subset of these)
		-- so this join will exclude any psa that do not have an enrolment record,
		-- even if that psa is in PupilTables
		-- see [dbo].[EnrolmentRatiosByYearOfEd]
--			INNER JOIN pEnrolmentRead.ssIDEnrolmentLevelN N
--				ON B.bestssID = N.ssID
--				AND N.YearOfEd = 1
			INNER JOIN PupilTables PT
				ON B.bestssID = PT.ssID
			CROSS JOIN DimensionGender G
		WHERE ptCode in ('KINDER')
		GROUP BY
		schNo
		, LifeYear
		, Estimate
		, SurveyDimensionssID
		, ptAge
		, G.genderCode
		HAVING isnull(sum(case when G.genderCode = 'M' then ptM
			when G.genderCode = 'F' then ptF
		 end) ,0) <> 0

		-- combine with enrolments and cross tab the measures
		-- OUTPUT: warehouse.enrol


		IF OBJECT_ID('warehouse.enrol', 'U') IS NOT NULL
			DROP TABLE warehouse.enrol

			Select schNo
			, surveyYear
			, Estimate
			, SurveyDimensionID
			, ClassLevel
			, nullif(Age,0) Age
			, GenderCode
			, sum(case when ptCode = 'E' then Enrol end) Enrol
			, sum(case when ptCode = 'REP' then Enrol end) Rep
			, sum(case when ptCode = 'TRIN' then Enrol end) Trin
			, sum(case when ptCode = 'TROUT' then Enrol end) Trout
			, sum(case when ptCode = 'BRD' then Enrol end) Boarders
			, sum(case when ptCode = 'DIS' then Enrol end) Disab
			, sum(case when ptCode = 'DROP' then Enrol end) Dropout
			, sum(case when ptCode = 'PSA' then Enrol end) PSA
			INTO warehouse.enrol
			FROM
			(
			Select *
			, 'E' ptCode from measureEnrolG

			UNION ALL
			Select *
			from warehouse.pupilTablesG

			) U
			GROUP BY
			schNo
			, surveyYear
			, Estimate
			, SurveyDimensionID
			, ClassLevel
			, Age
			, GenderCode

		-- group by key dimension keys
		-- OUTPUT:  warehouse.tableEnrol
		begin transaction
		IF OBJECT_ID('warehouse.tableEnrol', 'U') IS NOT NULL
			DROP TABLE warehouse.tableEnrol

			Select E.SurveyYear
			, E.Estimate
			, ClassLevel
			, nullif(Age, 0) Age
			, GenderCode

			, DSS.[District Code] DistrictCode

			, [AuthorityCode]
			, [SchoolTypeCode]
			, sum(Enrol) Enrol
			, sum(Rep) Rep
			, sum(Trin) TrIn
			, sum(TROUT) TrOut
			, sum(Boarders) Boarders
			, sum(Disab) Disab
			, sum(Dropout) Dropout
			, sum(PSA) PSA
			INTO warehouse.tableEnrol
			from warehouse.enrol E
			INNER JOIN warehouse.dimensionSchoolSurvey DSS
			ON E.surveyDimensionID = DSS.[Survey ID]

			GROUP BY
			E.SurveyYear
			, E.Estimate
			, ClassLevel
			, Age
			, GenderCode

			, DSS.[District Code]
			, [AuthorityCode]
			, [SchoolTypeCode]

		commit transaction
			-- population, based on the default model
			-- Output: warehouse.popG
		begin transaction
			IF OBJECT_ID('warehouse.measurePopG', 'U') IS NOT NULL
				DROP TABLE warehouse.measurePopG

			SELECT *
			INTO warehouse.measurePopG
			FROm dbo.measurePopG
		commit transaction

		--- School counts
		IF OBJECT_ID('warehouse.measureSchoolCount', 'U') IS NOT NULL
			DROP TABLE warehouse.measureSchoolCount

		Select count(DISTINCT E.SchNo) NumSchools
			, SurveyYear
			, AuthorityCode
			, [District Code] DistrictCode
			, SchoolTypeCode
			, Region
			INTO warehouse.measureSchoolCount
			FROM
			(
			Select DISTINCT schNo
				, SurveyYear
				, SurveyDimensionID
			from measureEnrolSchool
			) E
			INNER JOIN warehouse.DimensionSchoolSurvey DSS
			ON E.SurveyDimensionID = DSS.[Survey ID]
			GROUP BY
				SurveyYear
				, AuthorityCode
				, [District Code]
				, SchoolTypeCode
				, Region
		--- disability

		IF OBJECT_ID('warehouse.disability', 'U') IS NOT NULL
			DROP TABLE warehouse.disability

		Select  G.*
			, D.codeDescription Disability
			INTO warehouse.Disability
			FROM
				measureDisabilityG G
			LEFT JOIN TRDisabilities D
				ON G.disCode = D.codeCode
			WHERE isnull(G.disab,0) <> 0

		begin transaction
		IF OBJECT_ID('warehouse.tableDisability', 'U') IS NOT NULL
			DROP TABLE warehouse.tableDisability

		Select D.SurveyYear
			, D.Estimate
			, ClassLevel
			, GenderCode
			, DSS.[District Code] DistrictCode
			, [AuthorityCode]
			, [SchoolTypeCode]
			, DisCode
			, Disability
			, sum(Disab) Disab
			INTO warehouse.tableDisability
			from warehouse.disability D
			INNER JOIN warehouse.dimensionSchoolSurvey DSS
			ON D.surveyDimensionID = DSS.[Survey ID]

			GROUP BY
			D.SurveyYear
			, D.Estimate
			, ClassLevel
			, GenderCode
			, DSS.[District Code]
			, [AuthorityCode]
			, [SchoolTypeCode]
			, DisCode
			, Disability

			commit transaction
--- Cohort -- aggregated
		begin transaction
		IF OBJECT_ID('warehouse.cohort', 'U') IS NOT NULL
			DROP TABLE warehouse.cohort

			Select SurveyYear
			, avg(Estimate) Estimate -- one valueis non-null
			, YearOfEd
			, GenderCode
			, DistrictCode
			, AuthorityCode
			, sum(Enrol) Enrol
			, sum(Rep) Rep
			, sum(RepNY) RepNY
			, sum(TroutNY) TroutNY
			, sum(EnrolNYNextLevel) EnrolNYNextLevel
			, sum(RepNYNextLevel) RepNYNextLevel
			, sum(TrinNYNextLevel) TrinNYNextLevel
			INTO warehouse.cohort
			FROM
			(
			Select
			SurveyYear
			, Estimate
			, lvlYear YearOfEd
			, GenderCode
			, DistrictCode
			, AuthorityCode
			, Enrol
			, Rep
			, null RepNY
			, null TroutNY
			, null EnrolNYNextLevel
			, null RepNYNextLevel
			, null TrinNYNextLevel

			from warehouse.tableenrol E
				LEFT JOIN lkpLevels L
					on E.ClassLevel = L.codeCode
			WHERE (Enrol is not null or Rep is not null)
			UNION ALL
			-- data for next year, same class level
			Select
			SurveyYear - 1
			, null Estimate
			, lvlYear YearOfEd
			, GenderCode
			, DistrictCode
			, AuthorityCode
			, null Enrol
			, null Rep
			, Rep RepNY
			, Trout TroutNY
			, null EnrolNYNextLevel
			, null RepNYNextLevel
			, null TrinNYNextLevel
			from warehouse.tableenrol E
				LEFT JOIN lkpLevels L
					on E.ClassLevel = L.codeCode
			WHERE (Rep is not null or Trout is not null)
			UNION ALL
			-- data for next year, same class level
			Select
			SurveyYear - 1
			, null Estimate
			, lvlYear - 1
			, GenderCode
			, DistrictCode
			, AuthorityCode
			, null Enrol
			, null Rep
			, null RepNY
			, null TroutNY
			, Enrol EnrolNYNextLevel
			, Rep RepNYNextLevel
			, Trin TrinNYeExtLevel
			from warehouse.tableenrol E
				LEFT JOIN lkpLevels L
					on E.ClassLevel = L.codeCode
					WHERE (Enrol is not null or Rep is not null or Trin is not null)
			) U
			GROup BY
			SurveyYear
			, YearOfEd
			, GenderCode
			, DistrictCode
			, AuthorityCode

			commit transaction

---- school level flow model

		begin transaction
		IF OBJECT_ID('warehouse.schoolCohort', 'U') IS NOT NULL
			DROP TABLE warehouse.schoolCohort

			-- this query
			Select schNo
			, surveyYear
			, avg(Estimate) Estimate
			, min(SurveyDimensionID) SurveyDimensionID

			, YearOfEd
			, sum(Enrol) Enrol
			, sum(Rep) Rep
			, sum(RepNY) RepNY
			, sum(TroutNY) TroutNY
			, sum(EnrolNYNextLevel) EnrolNYNextLevel
			, sum(RepNYNextLevel) RepNYNextLevel
			, sum(TrinNYNextLevel) TrinNYNextLevel
			into warehouse.SchoolCohort
			FROM
			(
				select schNo
				, SurveyYear
				, Estimate
				, SurveyDimensionID
				, lvlYear YearOfEd
				, GenderCode
				, Enrol
				, Rep
				, null RepNY
				, null TroutNY
				, null EnrolNYNextLevel
				, null RepNYNextLevel
				, null TrinNYNextLevel
				From warehouse.enrol E
					LEFT JOIN lkpLevels L
						ON E.ClassLevel = L.codeCode
				UNION ALL
				select schNo
				, SurveyYear - 1
				, null Estimate
				, null SurveyDimensionID
				, lvlYear YearOfEd
				, GenderCode
				, null Enrol
				, null Rep
				, Rep RepNY
				, Trout TroutNY
				, null EnrolNYNextLevel
				, null RepNYNextLevel
				, null TrinNYNextLevel
				From warehouse.enrol E
					LEFT JOIN lkpLevels L
						ON E.ClassLevel = L.codeCode
				WHERE Rep is not null or Trout is not null
				UNION ALL
				select schNo
				, SurveyYear - 1
				, null Estimate
				, null SurveyDimensionID
				, lvlYear - 1 YearOfEd
				, GenderCode
				, null Enrol
				, null Rep
				, null RepNY
				, null TroutNY
				, Enrol EnrolNYNextLevel
				, Rep RepNYNextLevel
				, Trin TrinNYNextLevel
				From warehouse.enrol E
					LEFT JOIN lkpLevels L
						ON E.ClassLevel = L.codeCode
					WHERE (Enrol is not null or Rep is not null or Trin is not null)
			) U
			GROUP BY
			U.schNo
			, U.surveyYear
			, U.YearOfEd
		commit transaction

--- other data
exec warehouse.buildTeacherLocation
exec warehouse.buildRoomCounts
exec warehouse.buildTextbooks


---- vermpaf support
	exec warehouse.buildLevelER
	exec warehouse.buildEdLevelER
	exec warehouse.BuildRank
--- some sanity checks

Select SurveyYear
, sum(Enrol) tableEnrol
from warehouse.tableEnrol
GROUP BY SurveyYear ORDER BY surveyYear

Select SurveyYear
, sum(Enrol) cohort
from warehouse.cohort
GROUP BY SurveyYear ORDER BY surveyYear

SELECT SurveyYear
, sum(Enrol) enrol
from warehouse.enrol
GROUP BY SurveyYear ORDER BY surveyYear

SELECT SurveyYear
, sum(Enrol) enrol
from warehouse.enrol
GROUP BY SurveyYear ORDER BY surveyYear

Select SurveyYear
, sum(Enrol) surveyYearRank
from warehouse.SurveyYearRank
GROUP BY SurveyYear ORDER BY surveyYear

END
GO

