SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[PIVColsPupilTablesRowLevelGender]
AS
Select
ssId,
ptCode Category,
ptRow Code,
genderCode,
Gender,
ptLevel LevelCode,
Level,
[Year Of Education],
LEvelGV GeneralVocational,
edLevelCode,
[Education Level],
SectorCode,
Sector,
[ISCED Level],
COALESCE(DT.codeDescription, CP.cpDESC) Type,
case genderCode when 'M' then ptM when 'F' then ptF end NumChildren
From PupilTables PT
	CROSS JOIN DimensionGender
LEFT JOIN TRDisabilities DT
	ON PT.ptRow = DT.codeCode
	AND ptCode = 'DIS'
LEFT JOIN TRChildProtection CP
	ON PT.ptRow = CP.cpCode
	AND ptCode = 'CHILDPROT'
LEFT JOIN DimensionLevel LL
	ON ptLevel = LL.levelCode
GO

