SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [warehouse].[NationFlow]
WITH VIEW_METADATA
AS

Select S.*
, 1 - RepeatRate - PromoteRate DropoutRate
, 1 - RepeatRateTR - PromoteRateTR DropoutRateTR
, case when RepeatRate = 1 then null else PromoteRate / (1 - RepeatRate) end SurvivalRate
, case when RepeatRateTR = 1 then null else PromoteRateTR / (1 - RepeatRateTR) end SurvivalRateTR
FROM
(
	Select C.*
	, convert(float,RepNY) /Enrol RepeatRate
	, convert(float,RepNY) / (Enrol - isnull(TroutNY,0)) RepeatRateTR
	, ( convert(float,EnrolNYNextLevel) - RepNYNextLevel)  / Enrol PromoteRate
	, ( convert(float,EnrolNYNextLevel) - RepNYNextLevel - - isnull(TrinNYNextLevel,0))  / ( Enrol - isnull(TroutNY,0)) PromoteRateTR
	from warehouse.NationCohort C

) S
GO

