SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[measureEnrolLevelAge]
AS
SELECT
LifeYear SurveyYear
, Estimate
, enLevel ClassLevel
, enAge Age
, sum(enM) EnrolM
, sum(enF) EnrolF
from dbo.tfnESTIMATE_BestSurveyEnrolments() B
	INNER JOIN Enrollments E
		ON B.bestssID = E.ssID
	INNER JOIN DimensionSchoolSurvey DSS
		ON DSS.[Survey ID] = B.surveyDimensionssID
GROUP BY LifeYear, Estimate, enAge, enLevel
GO

