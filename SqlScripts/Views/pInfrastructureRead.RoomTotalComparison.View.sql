SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- get all the building ids for each survey date, and add the room type
CREATE VIEW [pInfrastructureRead].[RoomTotalComparison]
AS

Select censusDate
	, 'BuildingInventory' Source
	, schNo
	, max(inspStart) LastReviewed
	, sum(brevRoomsClass) RoomsClass
	, sum(brevRoomsOHT) RoomsOHT
	, sum(brevRoomsStaff) RoomsStaff
	, sum(brevRoomsAdmin) RoomsAdmin
	, sum(brevRoomsStorage) RoomsStorage
	, sum(brevRoomsDorm) RoomsDorm
	, sum(brevRoomsDining) RoomsDining
	, sum(brevRoomsKitchen) RoomsKitchen
	, sum(brevRoomsLibrary) RoomsLibrary
	, sum(brevRoomsHall) RoomsHall
	, sum(brevRoomsSpecialTuition) RoomsSpecialTuition
	, sum(brevRoomsOther) RoomsOther
from
(
Select brevID, B.schNo, inspStart, svyCensusDate censusDate, row_number() OVER (PARTITION BY BR.bldID, svyCensusDate ORDER by inspStart DESC) POS
		from
			BuildingREview BR
				INNER JOIN Buildings B
					ON BR.bldID = B.bldID
				INNER JOIN SchoolInspection
					ON SchoolInspection.inspID = BR.inspID
				INNER JOIN Survey
					ON SchoolInspection.inspStart <=Survey.svyCensusDate
) U
INNER JOIN BuildingReview BR
	on BR.brevID = U.brevID
	AND U.Pos = 1
GROUP BY CensusDate, schNo

--
-- totals from the survey
--
UNION ALL

Select svyCensusDate CensusDate
, 'Survey' Source
, schNo
, svyYear
, sum(roomsClass) RoomsClass
, sum(roomsOHT) RoomsOHT
, sum(RoomsStaff) RoomsStaff
, sum(RoomsAdmin) RoomsAdmin
, sum(RoomsStorage) RoomsStorage
, sum(RoomsDorm) RoomsDorm
, sum(RoomsDining) RoomsDining
, sum(RoomsKitchen) RoomsKitchen
, sum(RoomsLibrary) RoomsLibrary
, sum(RoomsHall) RoomsHall
, sum(RoomsSpec) RoomsSpecialTuition
, sum(RoomsOther) RoomsOther


FROM
(
Select svyCensusDate, schNo, svyYear
	, case rfcnCode when 'Class' then NumRooms end RoomsClass
	, case rfcnCode when 'OHT' then NumRooms end RoomsOHT
	, case rfcnCode when 'Staff' then NumRooms end RoomsStaff
	, case rfcnCode when 'Admin' then NumRooms end RoomsAdmin
	, case rfcnCode when 'Storage' then NumRooms end RoomsStorage
	, case rfcnCode when 'Dorm' then NumRooms end RoomsDorm
	, case rfcnCode when 'Dining' then NumRooms end RoomsDining
	, case rfcnCode when 'Kitchen' then NumRooms end RoomsKitchen
	, case rfcnCode when 'Library' then NumRooms end RoomsLibrary
	, case rfcnCode when 'Hall' then NumRooms end RoomsHall
	, case rfcnCode when 'Spec' then NumRooms end RoomsSpec
	, case rfcnCode when 'Other' then NumRooms end RoomsOther
FROM
(
Select svyCensusDate, schNo, svyYear, rfcnCode, count(*) NumRooms
FROM
(
Select ssID, schNo, SS.svyYear, svyCensusDate
	, row_number() OVER (PARTITION BY schNo , svyCEnsusDate ORDER BY SS.svyYear DESC) POS
FROM
	SchoolSurvey SS
		INNER JOIN Survey
			ON SS.svyYear <= Survey.svyYEar
	WHERE SS.ssID = any(Select ssID from Rooms)
) U
	INNER JOIN Rooms
		ON Rooms.ssID = U.ssID
		AND U.POS = 1
	INNER JOIN lkpRoomTypes RT
		ON Rooms.rmType = RT.codeCode
GROUP BY svyCEnsusDate, schNo, svyYear, rfcnCode
) U2
) U3
GROUP BY svyCensusDate, schNo, svyYear
GO

