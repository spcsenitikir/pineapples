SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[measureRepeatersG]
AS
SELECT schNo
, SurveyYear
, Estimate

, SurveyDimensionID
, ClassLevel
, Age
, G.genderCode
, case G.genderCode
	when 'M' then RepM
	when 'F' then RepF
end Rep
from measureRepeaters R
	CROSS JOIN DimensionGender G
GO

