SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [warehouse].[NationTeacherCount]
AS
Select SurveyYear
, GenderCode
, AgeGroup
, Sector
, sum(NumTeachers) NumTeachers
, sum(Certified) Certified
, sum(Qualified) Qualified
, sum(CertQual) CertQual
from warehouse.schoolTeacherCount
GROUP BY SurveyYear,  GenderCode, Sector, AgeGroup
GO

