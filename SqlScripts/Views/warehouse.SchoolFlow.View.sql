SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [warehouse].[SchoolFlow]
WITH VIEW_METADATA
AS

Select S.*
, 1 - RepeatRate - PromoteRate DropoutRate
, 1 - RepeatRateTR - PromoteRateTR DropoutRateTR
, case when RepeatRate = 1 then null else PromoteRate / (1 - RepeatRate) end SurvivalRate
, case when RepeatRateTR = 1 then null else PromoteRateTR / (1 - RepeatRateTR) end SurvivalRateTR
FROM
(
	Select C.*
	, case when Enrol is null then null
		else convert(float,RepNY) /Enrol end RepeatRate
	, case when ( Enrol - isnull(TroutNY,0)) is null then null
		else convert(float,RepNY) / Enrol - isnull(TroutNY,0) end RepeatRateTR
	, case when Enrol is null then null
		else ( convert(float,EnrolNYNextLevel) - RepNYNextLevel)  / Enrol end PromoteRate
	, case when ( Enrol - isnull(TroutNY,0)) is null then null
			else ( convert(float,EnrolNYNextLevel) - RepNYNextLevel - - isnull(TrinNYNextLevel,0))  / ( Enrol - isnull(TroutNY,0)) end PromoteRateTR
	from warehouse.SchoolCohort C

) S
GO

