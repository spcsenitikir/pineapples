SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[measureRepeaters]
AS
SELECT schNo
, LifeYear SurveyYear
, Estimate
, SurveyDimensionssID SurveyDimensionID
, ptLevel ClassLevel
, ptAge Age
, ptM RepM
, ptF RepF
from dbo.tfnESTIMATE_BestSurveyEnrolments() B
	INNER JOIN vtblRepeaters R
		ON B.SurveyDimensionssID = R.ssID
GO

