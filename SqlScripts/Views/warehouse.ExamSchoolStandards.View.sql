SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [warehouse].[ExamSchoolStandards]
AS
Select examID
, examCode
, examYear
, examName
, StateID
, State
, schNo
, Gender
, standardID
, standardCode
, standardDesc
, achievementLevel
, achievementDesc
, sum(Candidates) Candidates
from warehouse.ExamSchoolResults
GROUP BY examID
, examCode
, examYear
, examName
, StateID
, State
, schNo
, Gender
, standardID
, standardCode
, standardDesc
, achievementLevel
, achievementDesc
GO

