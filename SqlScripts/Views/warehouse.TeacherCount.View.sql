SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [warehouse].[TeacherCount]
AS
Select SurveyYear
, DistrictCode
, SchoolTypeCode
, AuthorityCode
, GenderCode
, AgeGroup
, Sector
, sum(NumTeachers) NumTeachers
, sum(Certified) Certified
, sum(Qualified) Qualified
, sum(CertQual) CertQual
from warehouse.schoolTeacherCount
GROUP BY SurveyYear,  DistrictCode, SchoolTypeCode, AuthorityCode, GenderCode, Sector, AgeGroup
GO

