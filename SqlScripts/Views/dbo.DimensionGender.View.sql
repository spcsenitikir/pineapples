SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[DimensionGender]
AS
SELECT     codeCode AS GenderCode, codeDescription AS Gender
FROM         dbo.TRGender
GO

