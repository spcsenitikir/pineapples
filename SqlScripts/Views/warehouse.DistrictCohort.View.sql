SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [warehouse].[DistrictCohort]
WITH VIEW_METADATA
AS
Select *
	from
	(
	Select SurveyYear
	, YearOfEd
	, GenderCode
	, DistrictCode
	, sum(Enrol) Enrol
	, sum(Rep) Rep
	, sum(RepNY) RepNY
	, sum(TroutNY) TroutNY
	, sum(EnrolNYNextLevel) EnrolNYNextLevel
	, sum(RepNYNextLevel) RepNYNextLevel
	, sum(TrinNYNextLevel) TrinNYNextLevel
	From warehouse.Cohort
	GROUP BY SurveyYear
	, YearOfEd
	, GenderCode
	, DistrictCode
	UNION ALL
	Select SurveyYear
	, YearOfEd
	, null GenderCode
	, DistrictCode
	, sum(Enrol) Enrol
	, sum(Rep) Rep
	, sum(RepNY) RepNY
	, sum(TroutNY) TroutNY
	, sum(EnrolNYNextLevel) EnrolNYNextLevel
	, sum(RepNYNextLevel) RepNYNextLevel
	, sum(TrinNYNextLevel) TrinNYNextLevel
	From warehouse.Cohort
	GROUP BY SurveyYear
	, YearOfEd
	, DistrictCode
	) U
GO

