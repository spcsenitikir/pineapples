SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRshifts]
AS
SELECT     shiftcode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN shiftName
	WHEN 1 THEN shiftNameL1
	WHEN 2 THEN shiftNameL2

END,shiftName) AS shiftName
, shiftSeq

FROM         dbo.lkpShifts
GO

