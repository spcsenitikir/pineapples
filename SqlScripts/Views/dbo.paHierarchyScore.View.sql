SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[paHierarchyScore]
WITH VIEW_METADATA
AS
select PA.paID
, H.*
, case H.Level
	when 'F' then PA.paScore
	when 'C' then C.comAvg
	when 'E' then E.elmAvg
	when 'I' then I.paplValue
end Score
from paAssessment PA
	INNER JOIN paHierarchy H
		ON PA.pafrmCode = H.pafrmCode
	LEFT JOIN paCompetencyAvg C
		ON H.LEvel = 'C'
		AND H.FullID = C.comFullID
		AND C.paID = PA.paID
	LEFT JOIN paElementAvg E
		ON H.LEvel = 'E'
		AND H.FullID = E.elmFullID
		AND E.paID = PA.paID
	LEFT JOIN paAssessmentLine I
		ON H.LEvel = 'I'
		AND H.ID = I.paindID
		AND I.paID = PA.paID
GO

