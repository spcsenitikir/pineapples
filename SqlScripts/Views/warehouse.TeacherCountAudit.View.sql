SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [warehouse].[TeacherCountAudit]
AS
Select SurveyYear
, count(*) NumTeachers
, sum (case when Source = 'S' then 1 end ) S
, sum (case when Source = 'XS' then 1 end ) XS
, sum (case when Source = 'C' then 1 end ) C
, sum (case when Source in ('S','XS', 'C') then 1 end ) Actual
, sum (case when Source = 'E' then 1 end ) E
, sum (case when Source = 'XE' then 1 end ) XE
, sum (case when Source = 'CE' then 1 end ) CE
, sum (case when Source in ('E','XE', 'CE', 'XA', 'XA?') then 1 end ) Estimate
, sum (case when Source = 'A' then 1 end ) A
, sum (case when Source = 'XA' then 1 end ) XA
, sum (case when Source = 'A?' then 1 end ) [A?]
, sum (case when Source = 'XA?' then 1 end ) [XA?]
, sum (case when Source in ('A', 'A?') then 1 end ) Appt
, sum(XtraSurvey) Duplicates
from warehouse.TeacherLocation
GROUP by SurveyYear
GO

