SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [warehouse].[ExamNationStandards]
AS
Select examID
, examCode
, examYear
, examName
, Gender
, standardID
, standardCode
, standardDesc
, achievementLevel
, achievementDesc
, sum(Candidates) Candidates
from warehouse.ExamStateResults
GROUP BY examID
, examCode
, examYear
, examName
, Gender
, standardID
, standardCode
, standardDesc
, achievementLevel
, achievementDesc
GO

