SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[DimensionLanguage]
AS
SELECT		LangCode LanguageCode,
			langName [Language],
			langGrp LanguageGroupCode,
			lnggrpName LanguageGroup

FROM
	dbo.TRLanguage
	Inner JOIN TRLanguageGroup
		ON dbo.TRLanguage.langGrp = dbo.TRLanguageGroup.lngGrp
GO
GRANT SELECT ON [dbo].[DimensionLanguage] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[DimensionLanguage] TO [public] AS [dbo]
GO

