SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[schoolYearHasDataTeachers]
AS
SELECT     S.schNo, S.svyYear,
			S.ssID EnrolssID,
			Q.ssqLevel
FROM         dbo.SchoolSurvey AS S LEFT OUTER JOIN
                      dbo.tfnQualityIssues('Teachers', null) AS Q ON S.ssID = Q.ssID
WHERE     (S.ssID IN
                          (SELECT     ssID
                            FROM          dbo.TeacherSurvey AS D))
			and (q.ssqLevel is null or q.ssqLevel <2)
GO

