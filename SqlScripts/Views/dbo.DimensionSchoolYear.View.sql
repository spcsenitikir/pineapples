SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[DimensionSchoolYear]
AS
SELECT
SYH.syYear as LifeYear,
Survey.svyYear,
S.schNo AS [School No],
S.schName AS [School Name],
S.SchNo + ': ' + S.SchName as [SchoolID_Name],
S.SchNo + ': ' + S.SchName + ' (' + SYH.systCode + ')' as [SchoolID_Name_Type],
S.SchNo + ': ' + S.SchName + ' (' + Districts.dShort + ')' as [SchoolID_Name_District],

S.schReg AS [Registration No],
S.schRegStatus as [Registration Status],
S.schRegYear as [Registration Year],
S.schXNo AS [School External ID],
Islands.iGroup AS [District Code],
Districts.dName AS District,
Districts.dShort as [District Short],
Islands.iName AS Island,
Islands.iOuter AS Region,
lkpElectorateL.codeDescription AS [Local Electorate],
S.schElectL AS [Local Electorate No],
lkpElectorateN.codeDescription AS [National Electorate],
S.schElectN AS [National Electorate No],
Auth.*,
Lang.*,
DSS.ssID		[Survey ID],
DSS.svyYEar [Survey Data Year],
DSS.ssISClass AS InfrastructureClass,
DSS.ssISSize AS InfrastructureSizeClass,
SchoolTypes.stDescription AS [Current School Type],
S.schType AS CurrentSchoolTypeCode,
SYH.systCode AS SchoolTypeCode,
SchoolTypes_1.stDescription AS SchoolType,
S.schClass AS [School Class],
DSS.ssAccess AS AccessCode,
dbo.TRAccess.codeDescription AS Access,
Case
	When DSS.ssParentCommittee = 0 Then 'N'
	Else 'Y'
End AS  HasParentCommitteeYN,

PC.codeDescription AS ParentCommitteeSupport,
S.schEst AS [Year Established],
S.schClosed AS [Year Closed],
1 AS NumberOfSchools,
dbo.lkpElectorateL.elgisID,
dbo.lkpElectorateN.engisID,
dbo.Islands.igisID,
dbo.Districts.dgisID,
ListXTab.*,
SYH.syParent AS ParentSchool,
CASE
	When syParent is null Then 'N'
	Else 'Y'
End As HasParentSchool

FROM Schools S
----CROSS JOIN Survey
----INNER JOIN MetaNumbers NUMS
----	ON num >= schEst
----	AND (num <=schClosed or schClosed = 0)
----	AND num = Survey.svyYEar
----LEFT JOIN SchoolYearHistory SYH
----	ON S.schNo = SYH.schNo
----	AND NUMS.num = SYH.syYEar
INNER JOIN SchoolYearHistory SYH
	ON S.schNo = SYH.schNo
----LEFT JOIN SchoolSurvey DSS
----	ON SYH.syBestssID = DSs.ssID
LEFT JOIN dbo.tfnEStimate_BestSurveyEnrolments() EE
	ON SYH.syYEar = EE.LifeYear
	AND SYH.schNo = EE.schNo
LEFT JOIN SchoolSurvey DSS
	ON EE.BestssID = DSs.ssID
LEFT JOIN TRSchoolTypes AS SchoolTypes
	ON S.schType = SchoolTypes.stCode
LEFT JOIN TRSchoolTypes AS SchoolTypes_1
		ON SYH.systCode = SchoolTypes_1.stCode
LEFT JOIN Islands
	ON S.iCode = Islands.iCode
LEFT JOIN Districts
	ON Islands.iGroup = Districts.dID
LEFT JOIN lkpElectorateN
	ON S.schElectN = dbo.lkpElectorateN.codeCode
LEFT JOIN lkpElectorateL
	ON S.schElectL = dbo.lkpElectorateL.codeCode
LEFT JOIN dbo.ListXTab
	ON S.schNo = ListXtab.schNo
LEFT JOIN dbo.qsubEnrolmentPartition
	ON DSS.ssID = dbo.qsubEnrolmentPartition.ssID
LEFT JOIN dbo.DimensionAuthority Auth
	ON SYH.syAuth = Auth.AuthorityCode
LEFT JOIN dbo.DimensionLanguage Lang
	ON DSS.ssLang = Lang.LanguageCode
LEFT JOIN dbo.PIVParentCommitteeSub PC
	ON DSS.ssID = PC.ssID
LEFT JOIN dbo.TRAccess
	ON DSS.ssAccess = dbo.TRAccess.codeCode
LEFT JOIN Survey
	ON SUrvey.svyYear = SYH.syYear
GO

