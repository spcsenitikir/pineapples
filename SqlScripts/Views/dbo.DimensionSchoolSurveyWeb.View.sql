SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[DimensionSchoolSurveyWeb]
AS
SELECT [Survey ID] SurveyID
      ,[School No] SchoolID
      ,[School Name] SchoolName
      ,[District]
      ,[Authority]
      ,[SchoolType]
      , 1 NumSchools
  FROM [DimensionSchoolSurveyNoYear]
GO

