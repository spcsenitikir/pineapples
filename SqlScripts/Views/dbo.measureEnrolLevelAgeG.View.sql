SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW measureEnrolLevelAgeG
AS
SELECT
SurveyYear
, Estimate
, ClassLevel
, Age
, G.genderCode
, case genderCode
	when 'M' then EnrolM
	when 'F' then EnrolF
 end Enrol

from measureEnrolLevelAge M
	CROSS JOIN dimensionGender G
GO

