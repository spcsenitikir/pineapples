SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[measureEnrolSchoolG]
AS
SELECT schNo
, SurveyYear
, Estimate

, SurveyDimensionID
, G.genderCode
, case G.genderCode
	when 'M' then EnrolM
	when 'F' then EnrolF
end Enrol
from measureEnrolSchool E
	CROSS JOIN DimensionGender G
GO

