SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 7 12 2007
-- Description:	standards that apply to a school in a given year
-- =============================================
CREATE VIEW [dbo].[SchoolYearApplicableStandards]
as

Select schNo,
LifeYear ,
std.svyYear as stdYear,
std.*

from


(SELECT EBSE.schNo,
EBSE.LifeYear, EBSE.bestssID,
EBSE.bestYear, EBSE.Estimate,

max(convert(char(4), std.svyYear)
+ case when isCode is null then '0' else '1' end
+ case when stCode is null then '0' else '1' end
+ case when isSize is null then '0' else '1' end
+ case when stdSector is null then '0' else '1' end
+ convert(char(20), std.stdId)
) MatchT,
substring(
max(convert(char(4), std.svyYear)
+ case when isCode is null then '0' else '1' end
+ case when stCode is null then '0' else '1' end
+ case when isSize is null then '0' else '1' end
+ case when stdSector is null then '0' else '1' end
+ convert(char(20), std.stdId)
),9,20) beststdID
FROM
	SurveyYearStandards Std,
	dbo.tfnESTIMATE_BestSurveyEnrolments()  EBSE
	INNER JOIN SchoolSurvey AS SS ON EBSE.bestssID = SS.ssID
WHERE
	Std.svyYear<=EBSE.lifeyear
	AND (Std.stCode Is Null Or Std.stCode=ssSchtype)
	AND (Std.isCode Is Null Or Std.isCode=ssISClass)
	AND (Std.isSize Is Null Or Std.isSize=ssISSize)
GROUP BY EBSE.schNo, EBSE.LifeYear, EBSE.bestssID, EBSE.bestYear, EBSE.Estimate,
SS.svyYear, Std.stdName, Std.stdItem
) subQ
Inner join surveyYearStandards Std
on subQ.bestStdID = std.stdID
GO
GRANT SELECT ON [dbo].[SchoolYearApplicableStandards] TO [pInfrastructureRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[SchoolYearApplicableStandards] TO [pInfrastructureWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[SchoolYearApplicableStandards] TO [pInfrastructureWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SchoolYearApplicableStandards] TO [pInfrastructureWrite] AS [dbo]
GO

