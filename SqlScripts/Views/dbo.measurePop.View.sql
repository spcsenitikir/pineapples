SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[measurePop]
AS
Select popYear
, dID
, popAge
, popM
, popF
from Population P
	INNER JOIN PopulationModel PM
		ON P.popmodCode= PM.popModCode
WHERE PM.popmodDefault=1
GO

