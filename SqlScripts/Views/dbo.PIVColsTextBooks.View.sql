SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[PIVColsTextBooks]
AS
-- 22 03 2009
-- some aliases created
SELECT
Resources.resID,
Resources.ssID,
Resources.resName Category,
Resources.resLevel LevelCode,
Resources.resSplit [SubjectCode],
-- this handles somalias
case
	when resSplit = 'O1' then SS.ssCoreSubject1
	when resSplit = 'O2' then SS.ssCoreSubject2
	else S.subjName
end [Subject],
Resources.resNumber Number,
case when resCondition = 1 then 'Good'
	 when resCondition = 2 then 'Fair'
	 when resCondition = 3 then 'Poor'
end Condition,
case
	when Resources.resAvail = -1 then 1
	else Resources.resAvail
end Available,
case
	when Resources.resAvail = -1 then 'Y'
	when Resources.resAvail = 0 then 'N'
	else null
end AvailableYN,
case
	when Resources.resAdequate = -1 then 1
	else Resources.resAdequate
end Adequate,
case
	when Resources.resAdequate = -1 then 'Y'
	when Resources.resAdequate = 0 then 'N'
	else null
end AdequateYN,
L.codeDescription [Level],
L.lvlYear YearOfEducation


FROM dbo.Resources
	LEFT JOIN TRLevels L
		ON Resources.resLevel = L.codeCode
	LEFT JOIN TRSubjects S
		ON Resources.resSplit = S.subjCode
	INNER JOIN SchoolSurvey SS
		on Resources.ssID = SS.ssID
WHERE Resources.resName in ('Text books', 'Teacher Guides', 'Readers')
GO

