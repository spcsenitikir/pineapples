SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRWorkorderstatus] AS
SELECT codeCode,
isnull(CASE dbo.userLanguage()
    WHEN 0 THEN codeDescription
    WHEN 1 THEN codeDescriptionL1
    WHEN 2 THEN codeDescriptionL2
 END,codeDescription) AS codeDescription
 FROM dbo.lkpWorkorderstatus
GO
GRANT SELECT ON [dbo].[TRWorkorderstatus] TO [public] AS [dbo]
GO

