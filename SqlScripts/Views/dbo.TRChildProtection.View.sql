SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRChildProtection]
AS
SELECT     cpCode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN cpDesc
	WHEN 1 THEN cpDescL1
	WHEN 2 THEN cpDescL2

END,cpDesc) AS cpDesc,
cpGrp,
cpseq

FROM         dbo.lkpChildProtection
GO
GRANT SELECT ON [dbo].[TRChildProtection] TO [public] AS [dbo]
GO

