SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[RecursiveRoleMembership]
AS
WITH RoleMembers(RoleName, MemberName, Depth) AS
(
	Select convert(sysname,N'ROLE') as RoleName, m.name as MemberName, 0 as Depth
	from
		sys.sysusers M

		WHERE issqlRole = 1
	UNION ALL

select case WHEN cte.Depth = 0 then CTE.MemberName else CTE.RoleName end, m.name as MemberName, CTE.Depth+1 as Depth
	from sys.sysusers R
	INNER JOIN sys.database_role_members RM
	ON RM.role_principal_ID = R.uid
	INNER JOIN sys.sysusers M
	ON RM.member_principal_ID = M.uid
	INNER JOIN RoleMembers CTE
		On CTE.MemberName = R.name


)
SELECT RoleName
, MemberName
, P.Type
, suser_sName(P.sID) LoginName
, min(Depth) Depth
FROM RoleMembers
INNER JOIN sys.database_principals P
ON P.name = RoleMembers.MemberName

WHERE RoleMembers.Depth <> 0
GROUP BY RoleName, MemberName
, P.Type, p.sID
GO

