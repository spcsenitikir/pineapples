SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ddIsland]
AS
SELECT     TOP 100 PERCENT Island, SortOrder
FROM         (SELECT     '(all)' AS Island, '' AS SortOrder
                       FROM          dbo.metaNumbers
                       WHERE      (num = 0)
                       UNION
                       SELECT     iName, iName AS SortOrder
                       FROM         dbo.Islands) AS L
ORDER BY SortOrder
GO
GRANT DELETE ON [dbo].[ddIsland] TO [pAdminAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[ddIsland] TO [pAdminAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ddIsland] TO [pAdminAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[ddIsland] TO [public] AS [dbo]
GO

