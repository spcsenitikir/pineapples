SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [warehouse].[ExamStateStandards]
AS
Select examID
, examCode
, examYear
, examName
, StateID
, State
, Gender
, standardID
, standardCode
, standardDesc
, achievementLevel
, achievementDesc
, sum(Candidates) Candidates
from warehouse.ExamStateResults
GROUP BY examID
, examCode
, examYear
, examName
, StateID
, State
, Gender
, standardID
, standardCode
, standardDesc
, achievementLevel
, achievementDesc
GO

