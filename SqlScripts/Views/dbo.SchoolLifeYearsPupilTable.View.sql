SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[SchoolLifeYearsPupilTable]
AS
SELECT
schoolLifeYears.*,
metaPupilTableDefs.tdefCode
FROM
schoolLifeYears,
metaPupilTableDefs
GO

