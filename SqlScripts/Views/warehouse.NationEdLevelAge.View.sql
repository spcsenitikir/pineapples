SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [warehouse].[NationEdLevelAge]
AS
Select surveyYear, ClassLevel, yearOfEd, EdLevel, GenderCode
, sum(case when EdLevelOfficialAge = 'UNDER' then Enrol else null end ) UnderAge
, sum(case when EdLevelOfficialAge = '=' then Enrol else null end ) OfficialAge
, sum(case when EdLevelOfficialAge = 'OVER' then Enrol else null end ) OverAge
, sum(Enrol) Enrol
, sum(case when Estimate = 1 then Enrol else null end ) EstimatedEnrol
From Warehouse.enrolmentRatios
GROUP BY
surveyYear, ClassLevel, yearOfEd, EdLevel, GenderCode
GO

