SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblRepeaters]
as
/*

	Created: 11 10 2007

	Repeaters data extracted from the pupiltables table
	For historical reasons, checks type REP1 as well as type REP
	In Kiri up to 2004 we only differentiated repeaters by age in class 1
	(to calculate efa2)

	Everywhere else repeaters is disaggregated by age at all levels

	No Estimates used in this query

*/


SELECT
	PupilTables.ptID,
	PupilTables.ssID,
	PupilTables.ptCode,
	PupilTables.ptAge,
	PupilTables.ptLevel,
	PupilTables.ptM,
	PupilTables.ptF
FROM
	lkpLevels INNER JOIN PupilTables
		ON lkpLevels.codeCode=PupilTables.ptLevel
WHERE
	-- this where clause picks up anything disaggregated by age
	-- or anything not in year 1
	-- so for early kiri data, we get the disaggregated year 1 and aggregated data for all other year
	(ptCode='REP' Or ptCode='REP1') AND
	(ptAge<>0 OR lkpLevels.lvlYear<>1)
GO

