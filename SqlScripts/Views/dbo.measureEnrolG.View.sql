SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[measureEnrolG]
AS
SELECT schNo
, SurveyYear
, Estimate

, SurveyDimensionID
, ClassLevel
, Age
, G.genderCode
, case G.genderCode
	when 'M' then EnrolM
	when 'F' then EnrolF
end Enrol
from measureEnrol E
	CROSS JOIN DimensionGender G
GO

