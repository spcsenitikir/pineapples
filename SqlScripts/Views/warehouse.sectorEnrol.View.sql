SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [warehouse].[sectorEnrol]
AS
Select SurveyYear
, Estimate
, GenderCode
, L.secCode SectorCode
, sum(Enrol) Enrol
, sum(Rep) Rep
From warehouse.tableEnrol E
INNER JOIN lkpLevels L
ON E.ClassLevel = L.codeCode
GROUP BY SurveyYear, Estimate, L.secCode, GenderCode
GO

