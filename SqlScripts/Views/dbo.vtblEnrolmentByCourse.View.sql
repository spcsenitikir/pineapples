SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblEnrolmentByCourse]
AS
SELECT
PupilTables.ptID,
PupilTables.ssID,
PupilTables.ptCode,
PupilTables.ptAge,
PupilTables.ptLevel,
PupilTables.ptPage AS Course,
PupilTables.ptRow AS AgeGroup,
PupilTables.ptM,
PupilTables.ptF
FROM dbo.PupilTables
WHERE (((PupilTables.ptCode)='ENCRS'))
GO

