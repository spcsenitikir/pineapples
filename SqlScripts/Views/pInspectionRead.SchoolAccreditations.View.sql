SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pInspectionRead].[SchoolAccreditations]
WITH  VIEW_METADATA
AS
SELECT        SI.inspID, SI.schNo, SI.inspStart AS StartDate, SI.inspEnd AS EndDate, SI.inspNote AS Note, SI.inspBy AS InspectedBy, SA.saID, SA.saL1 AS L1, SA.saL2 AS L2, SA.saL3 AS L3, SA.saL4 AS L4, SA.saT1 AS T1,
                         SA.saT2 AS T2, SA.saT3 AS T3, SA.saT4 AS T4, SA.saD1 AS D1, SA.saD2 AS D2, SA.saD3 AS D3, SA.saD4 AS D4, SA.saN1 AS N1, SA.saN2 AS N2, SA.saN3 AS N3, SA.saN4 AS N4, SA.saF1 AS F1, SA.saF2 AS F2,
                         SA.saF3 AS F3, SA.saF4 AS F4, SA.saS1 AS S1, SA.saS2 AS S2, SA.saS3 AS S3, SA.saS4 AS S4, SA.saCO1 AS CO1, SA.saCO2 AS CO2, SA.saLT1 AS LT1, SA.saLT2 AS LT2, SA.saLT3 AS LT3, SA.saLT4 AS LT4,
                         SA.saT AS T, SA.saSchLevel AS SchLevel, ISET.inspsetName AS InspYear, ISET.inspsetType, SA.pEditUser, SA.pEditDateTime, SA.pCreateUser, SA.pCreateDateTime, SA.pRowversion, SA.pCreateTag
FROM            dbo.SchoolInspection AS SI LEFT OUTER JOIN
                         dbo.InspectionSet AS ISET ON SI.inspsetID = ISET.inspsetID LEFT OUTER JOIN
                         dbo.SchoolAccreditation AS SA ON SI.inspID = SA.saID LEFT OUTER JOIN
                         dbo.lkpInspectionTypes AS IT ON ISET.inspsetType = IT.intyCode
WHERE        (ISET.inspsetType = 'SCHACCR')
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 28 04 2017
-- Description:	Trigger on [pInspectionRead].[SchoolAccreditationsUpdate]
-- =============================================
CREATE TRIGGER [pInspectionRead].[SchoolAccreditationsUpdate]
   ON  [pInspectionRead].[SchoolAccreditations]
   INSTEAD OF INSERT, UPDATE, DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- INSERT ------- 
	

	-- can we assume that the inspection set already exists?
	-- for sake of generality assume not for now, 
	-- but may be better data management or fail if the user supplies an invalid inspection set

	INSERT INTO InspectionSet
	(
		inspsetName
		, inspsetType
		, inspsetYear
	)
	SELECT DISTINCT INSERTED.inspYear
	, 'SCHACCR'
	, convert(int, inspYear) --- Not sure here?
	From INSERTED
	LEFT JOIN InspectionSet INSPSET
		ON INSERTED.inspYear = INSPSET.inspsetName
	WHERE INSPSET.inspsetID is null

	-- create the school inspection record

	INSERT INTO SchoolInspection
	 ( schNo
           ,inspsetID
		   ,inspStart
           ,inspEnd
           ,inspNote
           ,inspBy
	 )
	 SELECT 
		INSERTED.schNo
		,INSPSET.inspsetID
		,INSERTED.StartDate
        ,INSERTED.EndDate
        ,INSERTED.Note
        ,INSERTED.InspectedBy
	From INSERTED
		INNER JOIN InspectionSet INSPSET
		ON INSERTED.inspYear = INSPSET.inspsetName
		LEFT JOIN SchoolInspection SI
			ON INSPSET.inspsetID = SI.inspsetID
			AND INSERTED.schNo = SI.schNo

	WHERE INSERTED.saID is null
		AND SI.inspID is null			-- don't create a dup here

	-- now we have all the inspIDs -- we can insert into SchoolAccreditation
	INSERT INTO SchoolAccreditation
	(
	    saID
		,saL1
        ,saL2
        ,saL3
        ,saL4
        ,saT1
        ,saT2
        ,saT3
        ,saT4
        ,saD1
        ,saD2
        ,saD3
        ,saD4
        ,saN1
        ,saN2
        ,saN3
        ,saN4
        ,saF1
        ,saF2
        ,saF3
        ,saF4
        ,saS1
        ,saS2
        ,saS3
        ,saS4
        ,saCO1
        ,saCO2
        ,saLT1
        ,saLT2
        ,saLT3
        ,saLT4
        ,saT
		,saSchLevel

        ,pCreateUser
        ,pCreateDateTime
        ,pEditUser
        ,pEditDateTime
        ,pCreateTag
		)
	SELECT 
		SI.inspID
        ,L1
        ,L2
        ,L3
        ,L4
        ,T1
        ,T2
        ,T3
        ,T4
        ,D1
        ,D2
        ,D3
        ,D4
        ,N1
        ,N2
        ,N3
        ,N4
        ,F1
        ,F2
        ,F3
        ,F4
        ,S1
        ,S2
        ,S3
        ,S4
        ,CO1
        ,CO2
        ,LT1
        ,LT2
        ,LT3
        ,LT4
        ,T
		,SchLevel
  
      ,pEditUser
      ,pEditDateTime
      ,pCreateUser
      ,pCreateDateTime
      ,pCreateTag
  FROM INSERTED    -- Insert statements for trigger here
	INNER JOIN InspectionSet INSPSET
		ON INSERTED.inspYear = INSPSET.inspsetName
	INNER JOIN SchoolInspection SI
		ON Si.inspsetID = INSPSET.inspsetID
		AND SI.schNo = INSERTED.schNo
	WHERE INSERTED.saID is null

 -- end insert

 -- update
 -- the only thing we can update on SchoolInspection is the inspsetID - if we change the SA to a different year ( a correction)
	UPDATE SchoolInspection
		SET inspsetID = INSPSET.inspsetID
		,inspStart = INSERTED.StartDate
        ,inspEnd = INSERTED.EndDate
        ,inspNote = INSERTED.Note
        ,inspBy = INSERTED.InspectedBy
	FROM SchoolInspection
	
	INNER JOIN INSERTED
		ON SchoolInspection.inspID = INSERTED.inspID
	INNER JOIN DELETED	
		ON INSERTED.inspID = DELETED.inspID
	INNER JOIN InspectionSet INSPSET
		ON INSERTED.inspYear = INSPSET.inspsetName


	-- update SchoolAccreditation
	
	UPDATE SchoolAccreditation
	   SET
      saL1 = I.L1
      ,saL2 = I.L2
      ,saL3 = I.L3
      ,saL4 = I.L4
      ,saT1 = I.T1
      ,saT2 = I.T2
      ,saT3 = I.T3
      ,saT4 = I.T4
      ,saD1 = I.D1
      ,saD2 = I.D2
      ,saD3 = I.D3
      ,saD4 = I.D4
      ,saN1 = I.N1
      ,saN2 = I.N2
      ,saN3 = I.N3
      ,saN4 = I.N4
      ,saF1 = I.F1
      ,saF2 = I.F2
      ,saF3 = I.F3
      ,saF4 = I.F4
      ,saS1 = I.S1
      ,saS2 = I.S2
      ,saS3 = I.S3
      ,saS4 = I.S4
      ,saCO1 = I.CO1
      ,saCO2 = I.CO2
      ,saLT1 = I.LT1
      ,saLT2 = I.LT2
      ,saLT3 = I.LT3
      ,saLT4 = I.LT4
      ,saT = I.T
      ,saSchLevel = I.SchLevel
      
      ,pEditUser = I.pEditUser
      ,pEditDateTime = I.pEditDateTime

	FROM SchoolAccreditation
	INNER JOIN INSERTED I
		ON SchoolAccreditation.saID = I.saID
	INNER JOIN DELETED D
		ON I.saID = D.saID

	-- end update

	-- delete

	DELETE FROM SchoolAccreditation
	FROM SchoolAccreditation
	INNER JOIN DELETED D
		ON SchoolAccreditation.saID = D.saID
	LEFT JOIN INSERTED I
		ON D.saID = I.saID
	WHERE I.saID is null

	DELETE FROM SchoolInspection
	FROM SchoolInspection
	INNER JOIN DELETED D
		ON SchoolInspection.inspID = D.saID
	LEFT JOIN INSERTED I
		ON D.saID = I.saID
	WHERE I.saID is null

	-- end delete

END

GO

