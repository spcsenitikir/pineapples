SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pInspectionRead].[SchoolInspections]
WITH VIEW_METADATA
AS
Select inspID
, schNo
, inspPlanned PlannedStartDate
, inspStart StartDate
, inspEnd EndDate
, inspNote Note
, inspBy InspectedBy
, inspsetName	InspProgram
, inspsetType	InspTypeCode
, intyDesc		InspType
FROM SchoolInspection SI
	LEFT JOIN InspectionSet ISET
		ON SI.inspsetID = ISET.inspsetID
	LEFT JOIN lkpInspectionTypes IT
		ON ISET.inspsetType = IT.intyCode
-- RMI Quarterly Report is implemented as an Inspection type, but is not to be included in 'inspections'
WHERE ISET.inspsetType <> 'QUARTER'
GO

