SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[RecursiveRoleMembershipUserOld]
AS
/* this view shows only the roles to which the current_user beliongs */
WITH RoleMembers(RoleName, MemberName, Depth) AS
(
	Select convert(sysname,N'ROLE') as RoleName, m.name as MemberName, 0 as Depth
	from
		sys.sysusers M

		WHERE issqlRole = 1
			and is_member(m.name) = 1
	UNION ALL

select case WHEN cte.Depth = 0 then CTE.MemberName else CTE.RoleName end, m.name as MemberName, CTE.Depth+1 as Depth
	from sys.sysusers R
	INNER JOIN sys.database_role_members RM
	ON RM.role_principal_ID = R.uid
	INNER JOIN sys.sysusers M
	ON RM.member_principal_ID = M.uid
	INNER JOIN RoleMembers CTE
		On CTE.MemberName = R.name


)
SELECT DISTINCT roleName
,cast(Puser.value as nvarchar(40)) PineapplesRole
FROM RoleMembers
Inner JOIN
(Select * from sys.database_principals WHERE [type] = 'R') AllRoles
ON RoleMembers.roleName = AllRoles.[name]
LEFT JOIN
(Select major_id, [value] from sys.extended_properties WHERE Class=4 and [name]='PineapplesUser') Puser
ON Puser.major_ID = AllRoles.principal_id
WHERE Depth <> 0
GO
GRANT SELECT ON [dbo].[RecursiveRoleMembershipUserOld] TO [public] AS [dbo]
GO

