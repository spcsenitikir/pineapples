SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [warehouse].[nationCohort]
WITH VIEW_METADATA
AS
Select SurveyYear
, YearOfEd
, GenderCode
, sum(Enrol) Enrol
, sum(Rep) Rep
, sum(RepNY) RepNY
, sum(TroutNY) TroutNY
, sum(EnrolNYNextLevel) EnrolNYNextLevel
, sum(RepNYNextLevel) RepNYNextLevel
, sum(TrinNYNextLevel) TrinNYNextLevel
From warehouse.Cohort
GROUP BY SurveyYear
, YearOfEd
, GenderCode
UNION ALL
Select SurveyYear
, YearOfEd
, null GenderCode
, sum(Enrol) Enrol
, sum(Rep) Rep
, sum(RepNY) RepNY
, sum(TroutNY) TroutNY
, sum(EnrolNYNextLevel) EnrolNYNextLevel
, sum(RepNYNextLevel) RepNYNextLevel
, sum(TrinNYNextLevel) TrinNYNextLevel
From warehouse.Cohort
GROUP BY SurveyYear
, YearOfEd
GO

