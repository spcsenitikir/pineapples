SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ddRegion]
AS
SELECT     TOP 100 PERCENT Region, SortOrder
FROM         (SELECT     '(all)' AS Region, '' AS SortOrder
                       FROM          dbo.metaNumbers
                       WHERE      (num = 0)
                       UNION
                       SELECT     iOuter, iOuter AS SortOrder
                       FROM         dbo.Islands) AS L
ORDER BY SortOrder
GO
GRANT DELETE ON [dbo].[ddRegion] TO [pAdminAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[ddRegion] TO [pAdminAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ddRegion] TO [pAdminAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[ddRegion] TO [public] AS [dbo]
GO

