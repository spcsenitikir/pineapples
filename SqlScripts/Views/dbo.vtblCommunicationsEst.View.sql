SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblCommunicationsEst]
AS
SELECT
ESTIMATE.LifeYear AS [Survey Year],
ESTIMATE.schNo,
ESTIMATE.bestYear AS [Year OF Data],
ESTIMATE.Estimate,
ESTIMATE.Offset AS [Age Of Data],
ESTIMATE.bestssID, vtblCommunications.*
FROM
ESTIMATE_BestSurveyResourceCategory AS ESTIMATE
INNER JOIN vtblCommunications ON ESTIMATE.bestssID = vtblCommunications.ssID
WHERE (((ESTIMATE.resName)='Communications'))
GO
GRANT SELECT ON [dbo].[vtblCommunicationsEst] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[vtblCommunicationsEst] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[vtblCommunicationsEst] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[vtblCommunicationsEst] TO [pSchoolWrite] AS [dbo]
GO

