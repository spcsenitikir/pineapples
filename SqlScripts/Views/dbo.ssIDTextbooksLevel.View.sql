SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 29 11 2007
-- Description:	summary of textbooks by level, by survey
-- =============================================
CREATE VIEW [dbo].[ssIDTextbooksLevel]
AS
SELECT vtblTextBooks.ssID,
vtblTextBooks.resLevel,
Sum(vtblTextBooks.resNumber) AS NumTextBooks,
Sum(case when [rescondition]=1 then [resNumber] end) AS NumTextBooksGood,
Sum(case when [rescondition]=2 then [resNumber] end) AS NumTextBooksFair,
Sum(case when [rescondition]=3 then [resNumber] end) AS NumTextBooksPoor
FROM vtblTextBooks
GROUP BY vtblTextBooks.ssID, vtblTextBooks.resLevel
GO

