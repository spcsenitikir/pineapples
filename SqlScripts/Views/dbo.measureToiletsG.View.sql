SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW measureToiletsG
AS
SELECT
schNo
, SurveyYear
, Estimate
, SurveyDimensionID
, YearOfData
, G.genderCode
, case G.genderCode
	when 'M' then NumToiletsM
	when 'F' then NumToiletsF
end NumToilets
from measureToilets
	CROSS JOIN DimensionGender G
GO

