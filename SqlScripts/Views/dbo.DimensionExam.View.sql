SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[DimensionExam]
AS
SELECT     dbo.Exams.exCode AS [Exam Code], dbo.lkpExamTypes.exName AS [Exam Name], dbo.Exams.exYear AS [Exam Year], dbo.Exams.exID AS [Exam ID]
FROM         dbo.lkpExamTypes INNER JOIN
                      dbo.Exams ON dbo.lkpExamTypes.exCode = dbo.Exams.exCode
GO

