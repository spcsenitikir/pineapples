SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblSubjectEnrol]
AS
SELECT
PupilTables.ptID,
PupilTables.ssID,
PupilTables.ptCode,
PupilTables.ptLevel,
PupilTables.ptRow AS subjCode,
PupilTables.ptM,
PupilTables.ptF
FROM dbo.PupilTables
-- 7 7 2016 different coding has been used for subject enrlment tables, account for both options
WHERE PupilTables.ptCode in ('SUBJECTS', 'SUBJ')
GO

