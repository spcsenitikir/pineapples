SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[measurePopG]
AS
Select popYear
, dID
, popAge
, G.genderCode
, case G.genderCode
	when 'M' then popM
	when 'F' then popF
 end pop
FROM measurePop
	CROSS JOIN DimensionGender G
GO

