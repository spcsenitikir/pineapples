SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRMetaResourceDefs]
AS
SELECT     mresID,
RD.mresName mresCode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN mresName
	WHEN 1 THEN mresNameL1
	WHEN 2 THEN mresNameL2

END,mresName) AS mresName,
mresCat,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN mresCatName
	WHEN 1 THEN mresCatNameL1
	WHEN 2 THEN mresCatNameL2

END,mresCatName) AS mresCatName,
mresSeq
,mresPromptAdq
,mresPromptAvail
,mresPromptNum
,mresPromptCondition
,mresPromptQty
, mresPromptNote
FROM         dbo.MetaResourceDefs RD
INNER JOIN dbo.MetaResourceCategories RC ON RD.mresCat = RC.mresCatCode
GO
GRANT SELECT ON [dbo].[TRMetaResourceDefs] TO [public] AS [dbo]
GO

