SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[measureEnrolSchool]
AS
SELECT schNo
, LifeYear SurveyYear
, Estimate
, SurveyDimensionssID SurveyDimensionID
, sum(enM) EnrolM
, sum(enF) EnrolF
from dbo.tfnESTIMATE_BestSurveyEnrolments() B
	INNER JOIN Enrollments E
		ON B.SurveyDimensionssID = E.ssID
GROUP BY SchNo, LifeYear, Estimate, Offset, SurveyDimensionssID
GO

