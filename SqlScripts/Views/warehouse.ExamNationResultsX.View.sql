SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [warehouse].[ExamNationResultsX]
AS
Select examID
, examCode
, examYear
, examName
, Gender
, standardID
, standardCode
, standardDesc
, BenchmarkID
, benchmarkCode
, sum(Candidates) Candidates
, sum(case when achievementLevel = 0 then Candidates end) [0]
, sum(case when achievementLevel = 1 then Candidates end) [1]
, sum(case when achievementLevel = 2 then Candidates end) [2]
, sum(case when achievementLevel = 3 then Candidates end) [3]
, sum(case when achievementLevel = 4 then Candidates end) [4]
, sum(case when achievementLevel = 5 then Candidates end) [5]
, sum(case when achievementLevel = 6 then Candidates end) [6]
, sum(case when achievementLevel = 7 then Candidates end) [7]
, sum(case when achievementLevel = 8 then Candidates end) [8]
, sum(case when achievementLevel = 9 then Candidates end) [9]

From warehouse.examNationResults
GROUP BY examID
, examCode
, examYear
, examName
, Gender
, standardID
, standardCode
, standardDesc
, BenchmarkID
, benchmarkCode
GO

