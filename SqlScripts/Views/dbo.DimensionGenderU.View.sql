SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[DimensionGenderU]
AS
SELECT     codeCode AS GenderCode, codeDescription AS Gender
FROM         dbo.TRGender
UNION Select '<>', '<not specified>'
GO

