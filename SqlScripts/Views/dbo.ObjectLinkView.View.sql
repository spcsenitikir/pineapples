SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ObjectLinkView]
WITH SCHEMABINDING , VIEW_METADATA
AS
SELECT
            dbo.ObjectLinks.xlinkID
            , dbo.ObjectLinks.objectType
            , dbo.ObjectLinks.objectID
            , dbo.ObjectLinks.xlinkNote
			, dbo.SchoolLinks.lnkID
			, dbo.SchoolLinks.schNo
			, dbo.SchoolLinks.bldID
			, dbo.SchoolLinks.lnkDate
			, dbo.SchoolLinks.lnkTitle
			, dbo.SchoolLinks.lnkSummary
			, dbo.SchoolLinks.lnkSrc
			, dbo.SchoolLinks.lnkPath
            , dbo.SchoolLinks.lnkKeywords
FROM         dbo.ObjectLinks INNER JOIN
             dbo.SchoolLinks ON dbo.ObjectLinks.lnkID = dbo.SchoolLinks.lnkID
GO
GRANT DELETE ON [dbo].[ObjectLinkView] TO [pExamWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[ObjectLinkView] TO [pExamWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ObjectLinkView] TO [pExamWrite] AS [dbo]
GO
GRANT DELETE ON [dbo].[ObjectLinkView] TO [pInfrastructureWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[ObjectLinkView] TO [pInfrastructureWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ObjectLinkView] TO [pInfrastructureWrite] AS [dbo]
GO
GRANT DELETE ON [dbo].[ObjectLinkView] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[ObjectLinkView] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ObjectLinkView] TO [pSchoolWrite] AS [dbo]
GO
GRANT DELETE ON [dbo].[ObjectLinkView] TO [public] AS [dbo]
GO
GRANT INSERT ON [dbo].[ObjectLinkView] TO [public] AS [dbo]
GO
GRANT SELECT ON [dbo].[ObjectLinkView] TO [public] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ObjectLinkView] TO [public] AS [dbo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 7 1 2010
-- Description:	Remote the logic to add a school link and a reference to that link
-- =============================================
CREATE TRIGGER [dbo].[ObjectLinkViewDelete]
   ON [dbo].[ObjectLinkView]
   INSTEAD OF DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- we'll always delete the ObjectLink record
	-- we'll delete the schoolLink if:
	--  * the schno is null AND
	--  * there are no other objectLinks to this SchoolLink
	
begin try

	begin transaction
	
		DELETE FROM ObjectLinks
		WHERE ObjectLinks.xlinkID in (Select xlinkId from DELETED)
		
		DELETE From SchoolLinks
		WHERE lnkID in (Select lnkID from DELETED WHERE schNo is null)
		AND lnkId not in (Select lnkID from ObjectLinks)
		


	
	commit transaction
end try

/****************************************************************
Generic catch block
****************************************************************/
begin catch
	
    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000), 
        @ErrorSeverity INT, 
        @ErrorState INT; 
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin 
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end 

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState); 
	
end catch



END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 7 1 2010
-- Description:	Remote the logic to add a school link and a reference to that link
-- =============================================
CREATE TRIGGER [dbo].[ObjectLinkViewInsert]
   ON [dbo].[ObjectLinkView]
   INSTEAD OF INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- are we inserting simply a new ObjectLink to an existing school link
	-- or a new schoo link as well
	
begin try

	begin transaction
	
		INSERT INTO objectLinks
				(  ObjectType, ObjectID, lnkID)
		SELECT
			ObjectType, ObjectID, lnkID	--, xlinkNote
		FROM INSERTED
		WHERE INSERTED.lnkID is not null


		INSERT INTO SchoolLinks
				(schNo,  bldID, lnkDate, lnkTitle,  lnkSrc, lnkPath
				, lnkImg, lnkKeywords)
		SELECT 
			schNo, bldID, lnkDate, lnkTitle, lnkSrc, lnkPath
				, 0, lnkKeywords
		FROM INSERTED
		WHERE not exists (Select lnkId from SchoolLinks WHERE SchoolLinks.lnkPath = INSERTED.lnkPath)
			AND INSERTED.lnkID is null

	-- now we have all the links in SchoolLinks

		INSERT INTO objectLinks
				( ObjectType, ObjectID, lnkID)
		Select 	
			ObjectType, ObjectID, SchoolLinks.lnkID
		FROM INSERTED 
			INNER JOIN SchoolLinks
			ON INSERTED.lnkPath = SchoolLinks.lnkPath
		WHERE INSERTED.lnkID is null
	
	commit transaction
end try

/****************************************************************
Generic catch block
****************************************************************/
begin catch
	
    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000), 
        @ErrorSeverity INT, 
        @ErrorState INT; 
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin 
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end 

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState); 
	
end catch



END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 7 1 2010
-- Description:	Remote the logic to add a school link and a reference to that link
-- =============================================
CREATE TRIGGER [dbo].[ObjectLinkViewUpdate]
   ON [dbo].[ObjectLinkView]
   INSTEAD OF UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- are we inserting simply a new ObjectLink to an existing school link
	-- or a new schoo link as well
	
begin try

	begin transaction

-- if the user has selected a different file
-- we'll insert a new record into school links

		declare @count int
		
		Select @Count = count(*)
		from INSERTED
		--INNER JOIN DELETED
			--ON INSERTED.xlinkID = DELETED.xlinkID
			
		print @Count
		
		
		INSERT INTO SchoolLinks
				(schNo, bldID, lnkDate, lnkTitle,  lnkSrc, lnkPath
				, lnkImg, lnkKeywords)
		SELECT 
			INSERTED.schNo
			, INSERTED.bldID
			, INSERTED.lnkDate
			, INSERTED.lnkTitle
			, INSERTED.lnkSrc
			, INSERTED.lnkPath
			, 0
			, INSERTED.lnkKeywords
		FROM INSERTED
			INNER JOIN DELETED
			ON INSERTED.xlinkId = DELETED.xlinkID
		WHERE INSERTED.lnkPath <> DELETED.lnkPath
			AND not exists(select lnkID from SchoolLinks SL WHERE SL.lnkPath = INSERTED.lnkPath)

-- update these objectLink records with the new (or discovered) lnkID		
		UPDATE objectLinks
			SET ObjectType = INSERTED.objectType
				, objectID = INSERTED.objectID
				, lnkID = SchoolLinks.lnkID		-- this is the new record
		FROM INSERTED 
			INNER JOIN SchoolLinks
			ON INSERTED.lnkPath = SchoolLinks.lnkPath
			INNER JOIN DELETED
			ON INSERTED.xlinkId = DELETED.xlinkID
		WHERE INSERTED.lnkPath <> DELETED.lnkPath
			AND INSERTED.xlinkID = objectLinks.xlinkID
	


-- only update schoolLinks if we have the same school link record as before the edit
		UPDATE SchoolLinks
			SET schNo = INSERTED.schNo
				, bldID = INSERTED.bldID
				, lnkDate = INSERTED.lnkDate
				, lnkTitle = INSERTED.lnkTitle
				, lnkSrc = INSERTED.lnkSrc
				, lnkSummary = INSERTED.lnkSummary
				, lnkKeywords = INSERTED.lnkKeywords
		FROM INSERTED
			INNER JOIN DELETED
			On INSERTED.xlinkId = DELETED.xlinkID
		WHERE SchoolLinks.lnkID = INSERTED.lnkID
			AND DELETED.lnkID = INSERTED.lnkID
			AND DELETED.lnkPath = INSERTED.lnkPath
			
--update objectlinks in all circumstances

		UPDATE objectLinks
			SET ObjectType = INSERTED.objectType
				, objectID = INSERTED.objectID
		FROM INSERTED 
			INNER JOIN DELETED
			ON INSERTED.xlinkId = DELETED.xlinkID
		WHERE INSERTED.lnkPath = DELETED.lnkPath
			AND INSERTED.xlinkID = objectLinks.xlinkID

	
	commit transaction
end try

/****************************************************************
Generic catch block
****************************************************************/
begin catch
	
    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000), 
        @ErrorSeverity INT, 
        @ErrorState INT; 
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin 
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end 

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState); 
	
end catch



END
GO

