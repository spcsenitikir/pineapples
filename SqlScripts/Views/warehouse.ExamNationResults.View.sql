SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [warehouse].[ExamNationResults]
AS
Select examID
, examCode
, examYear
, examName
, Gender
, standardID
, standardCode
, standardDesc
, benchmarkID
, benchmarkCode
, achievementLevel
, achievementDesc
, sum(Candidates) Candidates
from warehouse.ExamStateResults
GROUP BY examID
, examCode
, examYear
, examName
, Gender
, standardID
, standardCode
, standardDesc
, benchmarkID
, benchmarkCode
, achievementLevel
, achievementDesc
GO

