SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [warehouse].[PupilClassroom]
AS

Select E.schNo, E.surveyYear, E.Enrol,  NumRooms Classrooms
, DSS.District
, DSS.[District Code]
, DSS.AuthorityCode
, DSS.Authority
, DSS.[SchoolType]

FROM
(
Select schNo, surveyYear, sum(enrol) Enrol
from warehouse.enrol
GROUP BY schNo, surveyYear
) E
	LEFT JOIN
(
	Select 	schNo, surveyYear, sum(NumRooms) NumRooms
	FROM
		warehouse.roomCounts
		WHERE rmType = 'CLASS'
		GROUP BY schNo, surveyYear
) R
		ON E.schNo = R.schNo
		AND E.surveyYEar = R.surveyYear
LEFT JOIN warehouse.bestSurvey BS
	ON BS.schNo = E.schNo
	AND BS.SurveyYear = E.SurveyYear
LEFT JOIN warehouse.dimensionSchoolSurvey DSS
	ON BS.surveyDimensionID = DSS.[survey id]
GO

