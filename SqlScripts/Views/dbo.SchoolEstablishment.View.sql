SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[SchoolEstablishment]
AS
SELECT [syID] estID
      ,[syYear]		estYear
      ,[schNo]
      ,[syDormant]
      ,[syStatus]	estStatus
      ,[systCode]	eststCode
      ,[syAuth]		estAuth
      ,[syParent]	estParent
      ,[syEstablishmentPoint] estEstablishmentPoint
      ,[syFlag]		estFlag
      ,[syNote]		estNote
      ,[syLocked]	estLocked
  FROM dbo.[SchoolYearHistory]
GO
GRANT SELECT ON [dbo].[SchoolEstablishment] TO [pEstablishmentRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[SchoolEstablishment] TO [pEstablishmentWriteX] AS [dbo]
GO
GRANT INSERT ON [dbo].[SchoolEstablishment] TO [pEstablishmentWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SchoolEstablishment] TO [pEstablishmentWriteX] AS [dbo]
GO

