SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pTeacherRead].[TeacherListByAppointment]
WITH VIEW_METADATA
AS
select TI.tID
, TI.tSex Gender
, TI.tDoB
, S.svyYear
, TA.schNo
, SCH.schName
, TA.taDate
, Ta.taRole
, TA.taID
, TA.estpNo
, case when TS.tchsID is not null then 1 else null end SurveyConfirmed
, case sum(convert(int,isnull(ytqQualified, codeQualified)))
	when 1 then 1
	else 0 end Qualified

, case sum(convert(int,isnull(ytqCertified, codeCertified)))
	when 1 then 1
	else 0 end Certified

from TeacherIdentity TI
CROSS JOIN Survey S
INNER JOIN TeacherAppointment TA
	ON TI.tID = TA.tID
	AND TA.taDate <= S.svyCensusDate
	AND (TA.taEndDate > S.svyCensusDate or TA.taEndDate is null)


LEFT JOIN Establishment EST
	ON TA.estpNo = EST.estpNo
LEFT JOIN Schools SCH
	ON TA.schNo = SCH.schNo
-- confirmation of appointment in school survey
LEFT JOIN SchoolSurvey SS
	ON SS.schNo = TA.schNo
	AND SS.svyYEar = S.svyYear
LEFT JOIN TeacherSurvey TS
	ON TS.tID = TI.tID
	AND TS.ssID = SS.ssID
-- related to training
LEFT JOIN TeacherTraining TT
	ON TI.tID = TT.tID
	AND isnull(TT.trYear,0) <= S.svyYear
LEFT JOIN SurveyYearTeacherQual SQ
	ON TT.trQual = SQ.ytqQual
	AND SQ.svyYear = S.svyYEar
LEFT JOIN lkpTeacherQual TQ
	ON TT.trQual = TQ.codeCode

group by TI.tID,  S.svyYEar, ta.taDate, ta.taRole, TA.taID, TA.schNo, SCH.schName, TA.estpNo, TS.tchsID, tSex, tDoB
GO

